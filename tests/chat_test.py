import pytest
import allure

from .pages.admin_page import Setting
from .pages.database.db_logic import ADMINS, CHATS, USERS, ROLES, PERMISSION
from .pages.chat_page import Ticket, Chat
from .pages.precondition import link, password, admin_login, random_en, Roles
from .pages.authentication_page import RegLogin


@pytest.mark.chat
@allure.link(link + '/tickets', name='ТАНДЕМ ЛК - Обращения')
@allure.epic('Функциональное тестирование модуля "CHAT"')
@allure.feature('Функциональное тестирование раздела "Обращения"')
class TestChat_TICKET:
    @pytest.mark.smoke
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_creating_a_ticket_with_correct_data(self, browser):
        type_chat = random_en(8)
        title = random_en(8)
        description = random_en(8)
        status_chat = 'Новое'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новый вид обращения'):
                CHATS().add_ticket_types(type_chat, random_en(8), 1)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Ticket(browser, link)
        with allure.step('Открыть вкладку "Обращения - Список"'):
            page.open_window_chat('Список')
        with allure.step('Нажать кнопку "Добавить"'):
            page.create_new_ticket()
        with allure.step('Выбрать в поле "Автор" - Збирко'):
            page.select_author('Збирко')
        with allure.step('Выбрать в поле "Вид тикета" - ранее созданный вид(Precondition)'):
            page.select_type_id(type_chat)
        with allure.step('Ввести в поле "Заголовок" 8 символов'):
            page.input_title(title)
        with allure.step('Ввести в поле "Текст" 8 символов'):
            page.input_text(description)
        with allure.step('Нажать кнопку "Добавить"'):
            page.save_results_of_changes(2, 'Correct', '')
        with allure.step('Проверить запись в tickets/abstract_chats/chat_users'):
            page.check_records_ticket(title, description, None, int(USERS().get_user_id_by_email(admin_login)),
                                      status_chat, type_chat)
        with allure.step('Удалить ранее созданный тикет'):
            page.send_to_archive_ticket('Correct', title)

    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_creating_a_ticket_with_limit_values(self, browser):
        type_chat = random_en(8)
        title = random_en(191)
        description = random_en(4096)
        status_chat = 'Новое'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новый вид обращения'):
                CHATS().add_ticket_types(type_chat, random_en(8), 1)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Ticket(browser, link)
        with allure.step('Открыть вкладку "Обращения - Список"'):
            page.open_window_chat('Список')
        with allure.step('Нажать кнопку "Добавить"'):
            page.create_new_ticket()
        with allure.step('Выбрать в поле "Автор" - Збирко'):
            page.select_author('Збирко')
        with allure.step('Выбрать в поле "Вид тикета" - ранее созданный вид(Precondition)'):
            page.select_type_id(type_chat)
        with allure.step('Ввести в поле "Заголовок" 191 символов'):
            page.input_title(title)
        with allure.step('Ввести в поле "Текст" 4096 символов'):
            page.input_text(description)
        with allure.step('Нажать кнопку "Добавить"'):
            page.save_results_of_changes(2, 'Correct', '')
        with allure.step('Проверить запись в tickets/abstract_chats/chat_users'):
            page.check_records_ticket(title, description, None, int(USERS().get_user_id_by_email(admin_login)),
                                      status_chat, type_chat)
        with allure.step('Удалить ранее созданный тикет'):
            page.send_to_archive_ticket('Correct', title)

    @allure.story('Негативная проверка функциональности')
    def testing_the_functionality_of_creating_a_ticket_with_out_of_range_values(self, browser):
        type_chat = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новый вид обращения'):
                CHATS().add_ticket_types(type_chat, random_en(8), 1)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Ticket(browser, link)
        with allure.step('Открыть вкладку "Обращения - Список"'):
            page.open_window_chat('Список')
        with allure.step('Нажать кнопку "Добавить"'):
            page.create_new_ticket()
        with allure.step('Подготовка к выполнению тест-кейса'):
            with allure.step('Выбрать в поле "Автор" - Збирко'):
                page.select_author('Збирко')
            with allure.step('Выбрать в поле "Вид тикета" ранее созданный вид'):
                page.select_type_id(type_chat)
            with allure.step('Ввести в поле "Заголовок" 8 символов'):
                page.input_title(random_en(8))
            with allure.step('Ввести в поле "Текст" 8 символов'):
                page.input_text(random_en(8))
        with allure.step('Проверка валидации поля "Заголовок" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Заголовок" 192 символов'):
                page.input_title(random_en(192))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(2, 'Incorrect', 'Тема обращения не может превышать')
            with allure.step('Вернуть в исходное состояние поле "Название"'):
                page.input_title(random_en(8))
        with allure.step('Проверка валидации поля "Текст" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Текст" 4097 символов'):
                page.input_text(random_en(4097))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(2, 'Incorrect', 'Текст не может превышать 4096')
            with allure.step('Вернуть в исходное состояние поле "Текст"'):
                page.input_text(random_en(8))

    @allure.story('Негативная проверка функциональности')
    def testing_the_functionality_of_creating_a_ticket_without_filling_in_the_required_field(self, browser):
        type_chat = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новый вид обращения'):
                CHATS().add_ticket_types(type_chat, random_en(8), 1)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Ticket(browser, link)
        with allure.step('Открыть вкладку "Обращения - Список"'):
            page.open_window_chat('Список')
        with allure.step('Нажать кнопку "Добавить"'):
            page.create_new_ticket()
        with allure.step('Подготовка к выполнению тест-кейса'):
            with allure.step('Выбрать в поле "Автор" - Збирко'):
                page.select_author('Збирко')
            with allure.step('Выбрать в поле "Вид тикета" ранее созданный вид'):
                page.select_type_id(type_chat)
            with allure.step('Ввести в поле "Заголовок" 8 символов'):
                page.input_title(random_en(8))
            with allure.step('Ввести в поле "Текст" 8 символов'):
                page.input_text(random_en(8))
        with allure.step('Проверка валидации обязательного поля "Вид тикета"'):
            with allure.step('Оставить поле "Вид тикета" пустым'):
                page.select_type_id('Не выбрано')
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(2, 'Incorrect', 'Вид обращения обязательно')
            with allure.step('Вернуть в исходное состояние поле "Вид тикета"'):
                page.select_type_id(type_chat)
        with allure.step('Проверка валидации обязательного поля "Тема обращения"'):
            with allure.step('Оставить поле "Заголовок" пустым'):
                page.input_title(' ')
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(2, 'Incorrect', 'Тема обращения обязательно')
            with allure.step('Вернуть в исходное состояние поле "Название"'):
                page.input_title(random_en(8))
        with allure.step('Проверка валидации обязательного поля "Текст"'):
            with allure.step('Оставить поле "Текст" пустым'):
                page.input_text(' ')
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(2, 'Incorrect', 'Текст обязательно')
            with allure.step('Вернуть в исходное состояние поле "Название"'):
                page.input_text(random_en(8))

    @pytest.mark.smoke
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_sending_a_valid_message_to_the_chat(self, browser):
        type_id = random_en(8)
        title_chat = random_en(8)
        status_chat = 'Новое'
        message = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать обращение с ранее созданным типом обращения'):
                with allure.step('Создать новый используемый вид обращения'):
                    CHATS().add_ticket_types(type_id, random_en(8), 1)
                with allure.step('Создать запись в abstract_chats, и закрепить за Администратором'):
                    CHATS().add_new_abstract_chats(title_chat, USERS().get_user_id_by_email(admin_login), 'no')
                with allure.step('Создать запись в tickets без исполнителя'):
                    CHATS().add_new_ticket(title_chat, random_en(8), None, status_chat, type_id, 'no')
                with allure.step('Создать запись в chat_users'):
                    CHATS().add_new_chat_users(admin_login, title_chat)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Ticket(browser, link)
        with allure.step('Открыть вкладку "Обращения - Список"'):
            page.open_window_chat('Список')
        with allure.step('Открыть ранее созданное обращение'):
            page.open_ticket(title_chat, 'yes')
        with allure.step('Ввести в поле "Сообщение" 8 символов'):
            page.input_message(message)
        with allure.step('Нажать кнопку "Отправить"'):
            page.send_message(message, 'Correct', '')
        with allure.step('Проверка записи в abitur.chat_messages'):
            page.check_records_message_ticket(message, admin_login, title_chat)
        with allure.step('Нажать кнопку "К обращениям"'):
            page.back_to_window(status_chat, title_chat)
        with allure.step('Удалить ранее созданный тикет'):
            page.send_to_archive_ticket('Correct', title_chat)

    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_sending_a_message_with_a_limit_value(self, browser):
        type_id = random_en(8)
        title_chat = random_en(8)
        status_chat = 'Новое'
        message = random_en(1024)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать обращение с ранее созданным типом обращения'):
                with allure.step('Создать новый используемый вид обращения'):
                    CHATS().add_ticket_types(type_id, random_en(8), 1)
                with allure.step('Создать запись в abstract_chats, и закрепить за Администратором'):
                    CHATS().add_new_abstract_chats(title_chat, USERS().get_user_id_by_email(admin_login), 'no')
                with allure.step('Создать запись в tickets без исполнителя'):
                    CHATS().add_new_ticket(title_chat, random_en(8), None, status_chat, type_id, 'no')
                with allure.step('Создать запись в chat_users'):
                    CHATS().add_new_chat_users(admin_login, title_chat)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Ticket(browser, link)
        with allure.step('Открыть вкладку "Обращения - Список"'):
            page.open_window_chat('Список')
        with allure.step('Открыть ранее созданное обращение'):
            page.open_ticket(title_chat, 'yes')
        with allure.step('Ввести в поле "Сообщение" 1024 символова'):
            page.input_message(message)
        with allure.step('Нажать кнопку "Отправить"'):
            page.send_message(message, 'Correct', '')
        with allure.step('Нажать кнопку "К обращениям"'):
            page.back_to_window(status_chat, title_chat)
        with allure.step('Удалить ранее созданный тикет'):
            page.send_to_archive_ticket('Correct', title_chat)

    @allure.story('Негативная проверка функциональности')
    def testing_the_functionality_of_sending_a_message_with_an_outrageous_value(self, browser):
        type_id = random_en(8)
        title_chat = random_en(8)
        status_chat = 'Новое'
        message = random_en(1025)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать обращение с ранее созданным типом обращения'):
                with allure.step('Создать новый используемый вид обращения'):
                    CHATS().add_ticket_types(type_id, random_en(8), 1)
                with allure.step('Создать запись в abstract_chats, и закрепить за Администратором'):
                    CHATS().add_new_abstract_chats(title_chat, USERS().get_user_id_by_email(admin_login), 'no')
                with allure.step('Создать запись в tickets без исполнителя'):
                    CHATS().add_new_ticket(title_chat, random_en(8), None, status_chat, type_id, 'no')
                with allure.step('Создать запись в chat_users'):
                    CHATS().add_new_chat_users(admin_login, title_chat)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Ticket(browser, link)
        with allure.step('Открыть вкладку "Обращения - Список"'):
            page.open_window_chat('Список')
        with allure.step('Открыть ранее созданное обращение'):
            page.open_ticket(title_chat, 'yes')
        with allure.step('Ввести в поле "Сообщение" 1025 символа'):
            page.input_message(message)
        with allure.step('Нажать кнопку "Отправить"'):
            page.send_message(message, 'Incorrect', 'не может превышать 1024 символа')
        with allure.step('Нажать кнопку "К обращениям"'):
            page.back_to_window(status_chat, title_chat)
        with allure.step('Удалить ранее созданный тикет'):
            page.send_to_archive_ticket('Correct', title_chat)

    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_editing_the_appeal(self, browser):
        type_id = random_en(8)
        title_chat = random_en(8)
        description = random_en(8)
        new_title = random_en(8)
        new_status = 'В работе'
        new_type_id = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать учётную запись с правом редактирования обращения'):
                with allure.step('Создать верифицированную учётную запись'):
                    user = random_en(8)
                    email_user = random_en(8) + '@chat.test'
                    USERS().create_a_new_verified_user(user, user, user, email_user)
                with allure.step('Назначить право view-tickets'):
                    PERMISSION().assign_permission_to_user(email_user, 'view-tickets')
                with allure.step('Назначить право update-tickets'):
                    PERMISSION().assign_permission_to_user(email_user, 'update-tickets')
                with allure.step('Назначить право assign-responsible-user-tickets'):
                    PERMISSION().assign_permission_to_user(email_user, 'assign-responsible-user-tickets')
            with allure.step('Создать обращение с ранее созданным типом обращения'):
                with allure.step('Создать новый используемый вид обращения'):
                    CHATS().add_ticket_types(type_id, random_en(8), 1)
                with allure.step('Создать запись в abstract_chats, и закрепить за Администратором'):
                    CHATS().add_new_abstract_chats(title_chat, USERS().get_user_id_by_email(admin_login), 'no')
                with allure.step('Создать запись в tickets без исполнителя'):
                    CHATS().add_new_ticket(title_chat, description, None, 'Новое', type_id, 'no')
                with allure.step('Создать запись в chat_users'):
                    CHATS().add_new_chat_users(admin_login, title_chat)
            with allure.step('Создать новый используемый вид обращения'):
                CHATS().add_ticket_types(new_type_id, random_en(8), 1)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Ticket(browser, link)
        with allure.step('Открыть вкладку "Обращения - Список"'):
            page.open_window_chat('Список')
        with allure.step('Открыть ранее созданное обращение'):
            page.open_ticket(title_chat, 'yes')
        with allure.step('Нажать кнопку "Управление --> Изменить"'):
            page.open_setting_ticket('Изменить')
        with allure.step('Выбрать в селекте "Исполнитель" ранее созданного пользователя'):
            page.select_author(user + ' ' + user + ' ' + user)
        with allure.step('Ввести в поле "Заголовок" 8 символов'):
            page.input_title(new_title)
        with allure.step('Выбрать в поле "Вид тикета" - ранее созданный вид(Precondition)'):
            page.select_type_id(new_type_id)
        with allure.step('Выбрать в поле "Статус" - В работе'):
            page.select_status_id(new_status)
        with allure.step('Нажать кнопку "Сохранить"'):
            page.save_results_of_changes(0, 'CorrectUpdate', '')
        with allure.step('Проверить запись в tickets/abstract_chats/chat_users'):
            page.check_records_ticket(new_title, description, int(USERS().get_user_id_by_email(email_user)),
                                      int(USERS().get_user_id_by_email(admin_login)), new_status, new_type_id)
        with allure.step('Нажать кнопку "К обращениям"'):
            page.back_to_window(new_status, new_title)
        with allure.step('Удалить ранее созданный тикет'):
            page.send_to_archive_ticket('Correct', new_title)

    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_editing_the_status_of_a_request(self, browser):
        type_id = random_en(8)
        title_chat = random_en(8)
        description = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать обращение с ранее созданным типом обращения'):
                with allure.step('Создать новый используемый вид обращения'):
                    CHATS().add_ticket_types(type_id, random_en(8), 1)
                with allure.step('Создать запись в abstract_chats, и закрепить за Администратором'):
                    CHATS().add_new_abstract_chats(title_chat, USERS().get_user_id_by_email(admin_login), 'no')
                with allure.step('Создать запись в tickets без исполнителя'):
                    CHATS().add_new_ticket(title_chat, description, None, 'Новое', type_id, 'no')
                with allure.step('Создать запись в chat_users'):
                    CHATS().add_new_chat_users(admin_login, title_chat)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Ticket(browser, link)
        with allure.step('Открыть вкладку "Обращения - Список"'):
            page.open_window_chat('Список')
        with allure.step('Проверка отображение статуса "Не указан"'):
            status = 'Не указан'
            with allure.step('Открыть ранее созданное обращение'):
                page.open_ticket(title_chat, 'yes')
            with allure.step('Нажать кнопку "Управление --> Изменить"'):
                page.open_setting_ticket('Изменить')
            with allure.step('Выбрать в поле "Статус" - Не указан'):
                page.select_status_id(status)
            with allure.step('Нажать кнопку "Сохранить"'):
                page.save_results_of_changes(0, 'CorrectUpdate', '')
            with allure.step('Нажать кнопку "К обращениям"'):
                page.back_to_window('не указан', title_chat)
            with allure.step('Проверка записи статуса в abitur.tickets'):
                page.check_status_ticket(status, title_chat)
        with allure.step('Проверка отображение статуса "В работе"'):
            status = 'В работе'
            with allure.step('Открыть ранее созданное обращение'):
                page.open_ticket(title_chat, 'yes')
            with allure.step('Нажать кнопку "Управление --> Изменить"'):
                page.open_setting_ticket('Изменить')
            with allure.step('Выбрать в поле "Статус" - В работе'):
                page.select_status_id('В работе')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.save_results_of_changes(0, 'CorrectUpdate', '')
            with allure.step('Нажать кнопку "К обращениям"'):
                page.back_to_window(status, title_chat)
            with allure.step('Проверка записи статуса в abitur.tickets'):
                page.check_status_ticket(status, title_chat)
        with allure.step('Проверка отображение статуса "Новое"'):
            status = 'Новое'
            with allure.step('Открыть ранее созданное обращение'):
                page.open_ticket(title_chat, 'yes')
            with allure.step('Нажать кнопку "Управление --> Изменить"'):
                page.open_setting_ticket('Изменить')
            with allure.step('Выбрать в поле "Статус" - Новая'):
                page.select_status_id(status)
            with allure.step('Нажать кнопку "Сохранить"'):
                page.save_results_of_changes(0, 'CorrectUpdate', '')
            with allure.step('Нажать кнопку "К обращениям"'):
                page.back_to_window(status, title_chat)
            with allure.step('Проверка записи статуса в abitur.tickets'):
                page.check_status_ticket(status, title_chat)
        with allure.step('Проверка отображение статуса "Отменена"'):
            status = 'Отклонено'
            with allure.step('Открыть ранее созданное обращение'):
                page.open_ticket(title_chat, 'yes')
            with allure.step('Нажать кнопку "Управление --> Изменить"'):
                page.open_setting_ticket('Изменить')
            with allure.step('Выбрать в поле "Статус" - Отменена'):
                page.select_status_id(status)
            with allure.step('Нажать кнопку "Сохранить"'):
                page.save_results_of_changes(0, 'CorrectUpdate', '')
            with allure.step('Нажать кнопку "К обращениям"'):
                page.back_to_window(status, title_chat)
            with allure.step('Проверка записи статуса в abitur.tickets'):
                page.check_status_ticket(status, title_chat)
        with allure.step('Проверка отображение статуса "Решенная"'):
            status = 'Решено'
            with allure.step('Открыть ранее созданное обращение'):
                page.open_ticket(title_chat, 'yes')
            with allure.step('Нажать кнопку "Управление --> Изменить"'):
                page.open_setting_ticket('Изменить')
            with allure.step('Выбрать в поле "Статус" - Решенная'):
                page.select_status_id(status)
            with allure.step('Нажать кнопку "Сохранить"'):
                page.save_results_of_changes(0, 'CorrectUpdate', '')
            with allure.step('Нажать кнопку "К обращениям"'):
                page.back_to_window(status, title_chat)
            with allure.step('Проверка записи статуса в abitur.tickets'):
                page.check_status_ticket(status, title_chat)
        with allure.step('Удалить ранее созданный тикет'):
            page.send_to_archive_ticket('Correct', title_chat)

    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_sending_a_request_to_the_archive_via_the_control_panel(self, browser):
        type_id = random_en(8)
        title_chat = random_en(8)
        status_chat = 'Новое'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать обращение с ранее созданным типом обращения'):
                with allure.step('Создать новый используемый вид обращения'):
                    CHATS().add_ticket_types(type_id, random_en(8), 1)
                with allure.step('Создать запись в abstract_chats, и закрепить за Администратором'):
                    CHATS().add_new_abstract_chats(title_chat, USERS().get_user_id_by_email(admin_login), 'no')
                with allure.step('Создать запись в tickets без исполнителя'):
                    CHATS().add_new_ticket(title_chat, random_en(8), None, status_chat, type_id, 'no')
                with allure.step('Создать запись в chat_users'):
                    CHATS().add_new_chat_users(admin_login, title_chat)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Ticket(browser, link)
        with allure.step('Открыть вкладку "Обращения - Список"'):
            page.open_window_chat('Список')
        with allure.step('Открыть ранее созданное обращение'):
            page.open_ticket(title_chat, 'yes')
        with allure.step('Нажать кнопку "Управление --> Изменить"'):
            page.open_setting_ticket('Удалить')
        with allure.step('Проверка записи об удалении в abitur.tickets'):
            page.check_deleting_ticket(title_chat)

    @pytest.mark.smoke
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_restoring_accesses_from_the_archive(self, browser):
        type_id = random_en(8)
        title_chat = random_en(8)
        status_chat = 'Новое'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать обращение с ранее созданным типом обращения'):
                with allure.step('Создать новый используемый вид обращения'):
                    CHATS().add_ticket_types(type_id, random_en(8), 1)
                with allure.step('Создать запись в abstract_chats, и закрепить за Администратором'):
                    CHATS().add_new_abstract_chats(title_chat, USERS().get_user_id_by_email(admin_login), 'no')
                with allure.step('Создать запись в tickets без исполнителя'):
                    CHATS().add_new_ticket(title_chat, random_en(8), None, status_chat, type_id, 'yes')
                with allure.step('Создать запись в chat_users'):
                    CHATS().add_new_chat_users(admin_login, title_chat)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Ticket(browser, link)
        with allure.step('Открыть вкладку "Обращения - Архив"'):
            page.open_window_chat('Архив')
        with allure.step('Восстановить ранее созданной обращение'):
            page.restore_a_request_in_the_archive('Correct', title_chat)
        with allure.step('Проверка записи об восстановлении в abitur.tickets'):
            page.check_restore_ticket(title_chat)

    @allure.severity(allure.severity_level.CRITICAL)
    @allure.story('Позитивная проверка функциональности')
    def testing_the_call_recovery_functionality_via_the_control_panel(self, browser):
        type_id = random_en(8)
        title_chat = random_en(8)
        status_chat = 'Новое'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать обращение с ранее созданным типом обращения'):
                with allure.step('Создать новый используемый вид обращения'):
                    CHATS().add_ticket_types(type_id, random_en(8), 1)
                with allure.step('Создать запись в abstract_chats, и закрепить за Администратором'):
                    CHATS().add_new_abstract_chats(title_chat, USERS().get_user_id_by_email(admin_login), 'yes')
                with allure.step('Создать запись в tickets без исполнителя'):
                    CHATS().add_new_ticket(title_chat, random_en(8), None, status_chat, type_id, 'yes')
                with allure.step('Создать запись в chat_users'):
                    CHATS().add_new_chat_users(admin_login, title_chat)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Ticket(browser, link)
        with allure.step('Открыть вкладку "Обращения - Архив"'):
            page.open_window_chat('Архив')
        with allure.step('Открыть ранее созданное обращение'):
            page.open_ticket(title_chat, 'yes')
        with allure.step('Нажать кнопку "Восстановить"'):
            page.restore_ticket_in_the_control_panel()

    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_verifying_the_integrity_of_a_message_in_an_archived_message(self, browser):
        type_id = random_en(8)
        title_chat = random_en(8)
        message = random_en(8)
        status_chat = 'Новое'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать обращение с ранее созданным типом обращения'):
                with allure.step('Создать новый используемый вид обращения'):
                    CHATS().add_ticket_types(type_id, random_en(8), 1)
                with allure.step('Создать запись в abstract_chats, и закрепить за Администратором'):
                    CHATS().add_new_abstract_chats(title_chat, USERS().get_user_id_by_email(admin_login), 'yes')
                with allure.step('Создать запись в tickets без исполнителя'):
                    CHATS().add_new_ticket(title_chat, random_en(8), None, status_chat, type_id, 'yes')
                with allure.step('Создать запись в chat_users'):
                    CHATS().add_new_chat_users(admin_login, title_chat)
                with allure.step('Создать запись в chat_message'):
                    CHATS().add_data_chat_message(title_chat, admin_login, message)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Ticket(browser, link)
        with allure.step('Открыть вкладку "Обращения - Архив"'):
            page.open_window_chat('Архив')
        with allure.step('Открыть ранее созданное обращение'):
            page.open_ticket(title_chat, 'no')
        with allure.step('Проверить отображении ранее отправленного сообщения'):
            page.check_last_message_to_archive(message)

    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_adding_valid_files_to_a_request(self, browser):
        type_id = random_en(8)
        title_chat = random_en(8)
        status_chat = 'Новое'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать обращение с ранее созданным типом обращения'):
                with allure.step('Создать новый используемый вид обращения'):
                    CHATS().add_ticket_types(type_id, random_en(8), 1)
                with allure.step('Создать запись в abstract_chats, и закрепить за Администратором'):
                    CHATS().add_new_abstract_chats(title_chat, USERS().get_user_id_by_email(admin_login), 'no')
                with allure.step('Создать запись в tickets без исполнителя'):
                    CHATS().add_new_ticket(title_chat, random_en(8), None, status_chat, type_id, 'no')
                with allure.step('Создать запись в chat_users'):
                    CHATS().add_new_chat_users(admin_login, title_chat)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Ticket(browser, link)
        with allure.step('Открыть вкладку "Обращения - Список"'):
            page.open_window_chat('Список')
        with allure.step('Открыть ранее созданное обращение'):
            page.open_ticket(title_chat, 'yes')
        with allure.step('Загрузить валидное изображение "photo.jpg"'):
            page.send_file('files/images/photo.jpg', 'Correct')
        with allure.step('Загрузить валидное изображение "photo.jpeg"'):
            page.send_file('files/images/photo.jpeg', 'Correct')
        with allure.step('Загрузить валидное изображение "photo.png"'):
            page.send_file('files/images/photo.png', 'Correct')
        with allure.step('Загрузить валидное изображение "photo.tiff"'):
            page.send_file('files/images/photo.tiff', 'Correct')
        with allure.step('Загрузить валидный документ "doc.pdf"'):
            page.send_file('files/doc/doc.pdf', 'Correct')
        with allure.step('Загрузить валидное изображение "Моё новое фото.png"'):
            page.send_file('files/images/Моё новое фото.png', 'Correct')

    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_adding_invalid_files_to_a_request(self, browser):
        type_id = random_en(8)
        title_chat = random_en(8)
        status_chat = 'Новое'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать обращение с ранее созданным типом обращения'):
                with allure.step('Создать новый используемый вид обращения'):
                    CHATS().add_ticket_types(type_id, random_en(8), 1)
                with allure.step('Создать запись в abstract_chats, и закрепить за Администратором'):
                    CHATS().add_new_abstract_chats(title_chat, USERS().get_user_id_by_email(admin_login), 'no')
                with allure.step('Создать запись в tickets без исполнителя'):
                    CHATS().add_new_ticket(title_chat, random_en(8), None, status_chat, type_id, 'no')
                with allure.step('Создать запись в chat_users'):
                    CHATS().add_new_chat_users(admin_login, title_chat)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Ticket(browser, link)
        with allure.step('Открыть вкладку "Обращения - Список"'):
            page.open_window_chat('Список')
        with allure.step('Открыть ранее созданное обращение'):
            page.open_ticket(title_chat, 'yes')
        with allure.step('Загрузить невалидное изображение "photo.webp"'):
            page.send_file('files/images/photo.webp', 'Incorrect')
        with allure.step('Загрузить невалидное изображение "photo.ico"'):
            page.send_file('files/images/photo.ico', 'Incorrect')
        with allure.step('Загрузить невалидный документ "doc.xlsx"'):
            page.send_file('files/doc/doc.xlsx', 'Incorrect')
        with allure.step('Загрузить невалидный документ "doc.txt"'):
            page.send_file('files/doc/doc.txt', 'Incorrect')
        with allure.step('Загрузить невалидный документ "doc.docx"'):
            page.send_file('files/doc/doc.docx', 'Incorrect')


@pytest.mark.chat
@allure.link(link + '/chats', name='ТАНДЕМ ЛК - Чат')
@allure.epic('Функциональное тестирование модуля "CHAT"')
@allure.feature('Функциональное тестирование раздела "Чат"')
class TestChat_CHAT:
    @pytest.mark.smoke
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_creating_a_chat_with_valid_data(self, browser):
        title_chat = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    email = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email, Roles.entrant)
                with allure.step('Назначить право create-chats'):
                    PERMISSION().assign_permission_to_user(email, 'create-chats')
                with allure.step('Назначить право view-chats'):
                    PERMISSION().assign_permission_to_user(email, 'view-chats')
                with allure.step('Назначить право update-chats'):
                    PERMISSION().assign_permission_to_user(email, 'update-chats')
            with allure.step('Создать второго пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    last_name1 = random_en(8)
                    first_name1 = random_en(8)
                    middle_name1 = random_en(8)
                    email_entrant1 = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(last_name1, first_name1, middle_name1, email_entrant1)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_entrant1, Roles.entrant)
            with allure.step('Создать третьего пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    last_name2 = random_en(8)
                    first_name2 = random_en(8)
                    middle_name2 = random_en(8)
                    email_entrant2 = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(last_name2, first_name2, middle_name2, email_entrant2)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_entrant2, Roles.entrant)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Поступающий)'):
            page.input_email(email)
        with allure.step('Ввести в поле "Пароль" (precondition-Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Chat(browser, link)
        with allure.step('Открыть вкладку "Чат"'):
            page.open_window_chat()
        with allure.step('Нажать кнопку "Создать чат"'):
            page.create_chat()
        with allure.step('Выбрать в поле "Пользователи чата" ранее созданную учётную записи(Precondition)'):
            page.select_user_chat(last_name1)
        with allure.step('Выбрать в поле "Пользователи чата" ранее созданную учётную записи(Precondition)'):
            page.select_user_chat(last_name2)
        with allure.step('Ввести в поле "Заголовок" 8 символов'):
            page.input_title(title_chat, 'clear')
        with allure.step('Нажать кнопку "Сохранить"'):
            page.check_validation('Correct', '')
        with allure.step('Проверка записи в abitur.chats/abitur.chats'):
            page.check_records_chat(title_chat, int(USERS().get_user_id_by_email(email)))
        with allure.step('Проверка записи в abitur.chat_users'):
            page.check_records_chat_users(title_chat, email)
            page.check_records_chat_users(title_chat, email_entrant1)
            page.check_records_chat_users(title_chat, email_entrant2)

    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_adding_valid_files_to_the_chat(self, browser):
        title_chat = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    email = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email, Roles.entrant)
                with allure.step('Назначить право create-chats'):
                    PERMISSION().assign_permission_to_user(email, 'create-chats')
                with allure.step('Назначить право view-chats'):
                    PERMISSION().assign_permission_to_user(email, 'view-chats')
                with allure.step('Назначить право update-chats'):
                    PERMISSION().assign_permission_to_user(email, 'update-chats')
            with allure.step('Создать второго пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    email_entrant = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email_entrant)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_entrant, Roles.entrant)
            with allure.step('Создать чат закрепленный за Поступающим'):
                with allure.step('Создать запись в abstract_chats, и закрепить за Поступающим'):
                    CHATS().add_new_abstract_chats(title_chat, USERS().get_user_id_by_email(email), 'no')
                with allure.step('Создать запись в tickets без исполнителя'):
                    CHATS().add_new_chats(title_chat)
                with allure.step('Создать запись в chat_users'):
                    CHATS().add_new_chat_users(email, title_chat)
                    CHATS().add_new_chat_users(email_entrant, title_chat)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Поступающий)'):
            page.input_email(email)
        with allure.step('Ввести в поле "Пароль" (Precondition-Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Chat(browser, link)
        with allure.step('Открыть вкладку "Чат"'):
            page.open_window_chat()
        with allure.step('Открыть ранее созданный чат(Precondition)'):
            page.open_chat(title_chat)
        page = Ticket(browser, link)
        with allure.step('Загрузить валидное изображение "photo.jpg"'):
            page.send_file('files/images/photo.jpg', 'Correct')
        with allure.step('Загрузить валидное изображение "photo.jpeg"'):
            page.send_file('files/images/photo.jpeg', 'Correct')
        with allure.step('Загрузить валидное изображение "photo.png"'):
            page.send_file('files/images/photo.png', 'Correct')
        with allure.step('Загрузить валидное изображение "photo.tiff"'):
            page.send_file('files/images/photo.tiff', 'Correct')
        with allure.step('Загрузить валидный документ "doc.pdf"'):
            page.send_file('files/doc/doc.pdf', 'Correct')
        with allure.step('Загрузить валидное изображение "Моё новое фото.png"'):
            page.send_file('files/images/Моё новое фото.png', 'Correct')

    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_adding_invalid_files_to_the_chat(self, browser):
        title_chat = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    email = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email, Roles.entrant)
                with allure.step('Назначить право create-chats'):
                    PERMISSION().assign_permission_to_user(email, 'create-chats')
                with allure.step('Назначить право view-chats'):
                    PERMISSION().assign_permission_to_user(email, 'view-chats')
                with allure.step('Назначить право update-chats'):
                    PERMISSION().assign_permission_to_user(email, 'update-chats')
            with allure.step('Создать второго пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    email_entrant = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email_entrant)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_entrant, Roles.entrant)
            with allure.step('Создать чат закрепленный за Поступающим'):
                with allure.step('Создать запись в abstract_chats, и закрепить за Поступающим'):
                    CHATS().add_new_abstract_chats(title_chat, USERS().get_user_id_by_email(email), 'no')
                with allure.step('Создать запись в tickets без исполнителя'):
                    CHATS().add_new_chats(title_chat)
                with allure.step('Создать запись в chat_users'):
                    CHATS().add_new_chat_users(email, title_chat)
                    CHATS().add_new_chat_users(email_entrant, title_chat)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Поступающий)'):
            page.input_email(email)
        with allure.step('Ввести в поле "Пароль" (Precondition-Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Chat(browser, link)
        with allure.step('Открыть вкладку "Чат"'):
            page.open_window_chat()
        with allure.step('Открыть ранее созданный чат(Precondition)'):
            page.open_chat(title_chat)
        page = Ticket(browser, link)
        with allure.step('Загрузить невалидное изображение "photo.webp"'):
            page.send_file('files/images/photo.webp', 'Incorrect')
        with allure.step('Загрузить невалидное изображение "photo.ico"'):
            page.send_file('files/images/photo.ico', 'Incorrect')
        with allure.step('Загрузить невалидный документ "doc.xlsx"'):
            page.send_file('files/doc/doc.xlsx', 'Incorrect')
        with allure.step('Загрузить невалидный документ "doc.txt"'):
            page.send_file('files/doc/doc.txt', 'Incorrect')
        with allure.step('Загрузить невалидный документ "doc.docx"'):
            page.send_file('files/doc/doc.docx', 'Incorrect')

    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_deleting_a_user_in_creating_a_chat(self, browser):
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    email = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email, Roles.entrant)
                with allure.step('Назначить право create-chats'):
                    PERMISSION().assign_permission_to_user(email, 'create-chats')
                with allure.step('Назначить право view-chats'):
                    PERMISSION().assign_permission_to_user(email, 'view-chats')
                with allure.step('Назначить право update-chats'):
                    PERMISSION().assign_permission_to_user(email, 'update-chats')
            with allure.step('Создать второго пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    last_name1 = random_en(8)
                    first_name1 = random_en(8)
                    middle_name1 = random_en(8)
                    email_entrant1 = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(last_name1, first_name1, middle_name1, email_entrant1)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_entrant1, Roles.entrant)
            with allure.step('Создать третьего пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    last_name2 = random_en(8)
                    first_name2 = random_en(8)
                    middle_name2 = random_en(8)
                    email_entrant2 = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(last_name2, first_name2, middle_name2, email_entrant2)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_entrant2, Roles.entrant)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Поступающий)'):
            page.input_email(email)
        with allure.step('Ввести в поле "Пароль" (precondition-Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Chat(browser, link)
        with allure.step('Открыть вкладку "Чат"'):
            page.open_window_chat()
        with allure.step('Нажать кнопку "Создать чат"'):
            page.create_chat()
        with allure.step('Выбрать в поле "Пользователи чата" ранее созданную учётную записи(Precondition)'):
            page.select_user_chat(last_name1)
        with allure.step('Выбрать в поле "Пользователи чата" ранее созданную учётную записи(Precondition)'):
            page.select_user_chat(last_name2)
        with allure.step('Удалить в поле "Пользователи чата" ранее выбранные учётные записи(Precondition)'):
            page.delete_select_user(last_name1)
        with allure.step('Удалить в поле "Пользователи чата" ранее выбранные учётные записи(Precondition)'):
            page.delete_select_user(last_name2)
        with allure.step('Нажать кнопку "Сохранить"'):
            page.check_validation('Incorrect', 'хотя бы одного пользователя')

    @allure.story('Позитивная проверка функциональности')
    def testing_the_header_generation_functionality_when_creating_a_chat(self, browser):
        first_name = random_en(8)
        first_name1 = random_en(8)
        first_name2 = random_en(8)
        title_chat = first_name + ', ' + first_name1 + ', ' + first_name2
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    email = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(random_en(8), first_name, random_en(8), email)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email, Roles.entrant)
                with allure.step('Назначить право create-chats'):
                    PERMISSION().assign_permission_to_user(email, 'create-chats')
                with allure.step('Назначить право view-chats'):
                    PERMISSION().assign_permission_to_user(email, 'view-chats')
                with allure.step('Назначить право update-chats'):
                    PERMISSION().assign_permission_to_user(email, 'update-chats')
            with allure.step('Создать второго пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    last_name1 = random_en(8)
                    middle_name1 = random_en(8)
                    email_entrant1 = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(last_name1, first_name1, middle_name1, email_entrant1)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_entrant1, Roles.entrant)
            with allure.step('Создать третьего пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    last_name2 = random_en(8)
                    middle_name2 = random_en(8)
                    email_entrant2 = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(last_name2, first_name2, middle_name2, email_entrant2)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_entrant2, Roles.entrant)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Поступающий)'):
            page.input_email(email)
        with allure.step('Ввести в поле "Пароль" (precondition-Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Chat(browser, link)
        with allure.step('Открыть вкладку "Чат"'):
            page.open_window_chat()
        with allure.step('Нажать кнопку "Создать чат"'):
            page.create_chat()
        with allure.step('Выбрать в поле "Пользователи чата" ранее созданную учётную записи(Precondition)'):
            page.select_user_chat(last_name1)
        with allure.step('Выбрать в поле "Пользователи чата" ранее созданную учётную записи(Precondition)'):
            page.select_user_chat(last_name2)
        with allure.step('Ввести в поле "Заголовок" 8 символов'):
            page.input_title(title_chat, 'clear')
        with allure.step('Нажать кнопку "Сохранить"'):
            page.check_validation('Correct', '')
        with allure.step('Проверка записи в abitur.chats/abitur.chats'):
            page.check_records_chat(title_chat, int(USERS().get_user_id_by_email(email)))
        with allure.step('Проверка записи в abitur.chat_users'):
            page.check_records_chat_users(title_chat, email)
            page.check_records_chat_users(title_chat, email_entrant1)
            page.check_records_chat_users(title_chat, email_entrant2)

    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_creating_a_chat_with_limit_values(self, browser):
        title_chat = random_en(191)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    email = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email, Roles.entrant)
                with allure.step('Назначить право create-chats'):
                    PERMISSION().assign_permission_to_user(email, 'create-chats')
                with allure.step('Назначить право view-chats'):
                    PERMISSION().assign_permission_to_user(email, 'view-chats')
                with allure.step('Назначить право update-chats'):
                    PERMISSION().assign_permission_to_user(email, 'update-chats')
            with allure.step('Создать второго пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    last_name1 = random_en(8)
                    first_name1 = random_en(8)
                    middle_name1 = random_en(8)
                    email_entrant1 = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(last_name1, first_name1, middle_name1, email_entrant1)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_entrant1, Roles.entrant)
            with allure.step('Создать третьего пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    last_name2 = random_en(8)
                    first_name2 = random_en(8)
                    middle_name2 = random_en(8)
                    email_entrant2 = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(last_name2, first_name2, middle_name2, email_entrant2)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_entrant2, Roles.entrant)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Поступающий)'):
            page.input_email(email)
        with allure.step('Ввести в поле "Пароль" (precondition-Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Chat(browser, link)
        with allure.step('Открыть вкладку "Чат"'):
            page.open_window_chat()
        with allure.step('Нажать кнопку "Создать чат"'):
            page.create_chat()
        with allure.step('Выбрать в поле "Пользователи чата" ранее созданную учётную записи(Precondition)'):
            page.select_user_chat(last_name1)
        with allure.step('Выбрать в поле "Пользователи чата" ранее созданную учётную записи(Precondition)'):
            page.select_user_chat(last_name2)
        with allure.step('Ввести в поле "Заголовок" 191 символов'):
            page.input_title(title_chat, 'clear')
        with allure.step('Нажать кнопку "Сохранить"'):
            page.check_validation('Correct', '')
        with allure.step('Проверка записи в abitur.chats/abitur.chats'):
            page.check_records_chat(title_chat, int(USERS().get_user_id_by_email(email)))
        with allure.step('Проверка записи в abitur.chat_users'):
            page.check_records_chat_users(title_chat, email)
            page.check_records_chat_users(title_chat, email_entrant1)
            page.check_records_chat_users(title_chat, email_entrant2)

    @allure.story('Негативная проверка функциональности')
    def testing_the_functionality_of_creating_a_chat_with_out_of_range_values(self, browser):
        title_chat = random_en(192)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    email = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email, Roles.entrant)
                with allure.step('Назначить право create-chats'):
                    PERMISSION().assign_permission_to_user(email, 'create-chats')
                with allure.step('Назначить право view-chats'):
                    PERMISSION().assign_permission_to_user(email, 'view-chats')
                with allure.step('Назначить право update-chats'):
                    PERMISSION().assign_permission_to_user(email, 'update-chats')
            with allure.step('Создать второго пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    last_name1 = random_en(8)
                    first_name1 = random_en(8)
                    middle_name1 = random_en(8)
                    email_entrant1 = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(last_name1, first_name1, middle_name1, email_entrant1)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_entrant1, Roles.entrant)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Поступающий)'):
            page.input_email(email)
        with allure.step('Ввести в поле "Пароль" (precondition-Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Chat(browser, link)
        with allure.step('Открыть вкладку "Чат"'):
            page.open_window_chat()
        with allure.step('Нажать кнопку "Создать чат"'):
            page.create_chat()
        with allure.step('Проверка валидации поля "Заголовок" на ввод запредельных значений'):
            with allure.step('Выбрать в поле "Пользователи чата" ранее созданную учётную записи(Precondition)'):
                page.select_user_chat(last_name1)
            with allure.step('Ввести в поле "Заголовок" 192 символов'):
                page.input_title(title_chat, 'clear')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.check_validation('Incorrect', '«название чата» не может превышать')

    @allure.story('Негативная проверка функциональности')
    def testing_the_functionality_of_creating_a_chat_without_filling_in_the_required_field(self, browser):
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    email = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email, Roles.entrant)
                with allure.step('Назначить право create-chats'):
                    PERMISSION().assign_permission_to_user(email, 'create-chats')
                with allure.step('Назначить право view-chats'):
                    PERMISSION().assign_permission_to_user(email, 'view-chats')
                with allure.step('Назначить право update-chats'):
                    PERMISSION().assign_permission_to_user(email, 'update-chats')
            with allure.step('Создать второго пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    last_name1 = random_en(8)
                    first_name1 = random_en(8)
                    middle_name1 = random_en(8)
                    email_entrant1 = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(last_name1, first_name1, middle_name1, email_entrant1)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_entrant1, Roles.entrant)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Поступающий)'):
            page.input_email(email)
        with allure.step('Ввести в поле "Пароль" (precondition-Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Chat(browser, link)
        with allure.step('Открыть вкладку "Чат"'):
            page.open_window_chat()
        with allure.step('Нажать кнопку "Создать чат"'):
            page.create_chat()
        with allure.step('Проверка валидации обязательного поля "Пользователи чата"'):
            with allure.step('Ввести в поле "Заголовок" 8 символов'):
                page.input_title(random_en(8), 'clear')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.check_validation('Incorrect', 'хотя бы одного пользователя')
        with allure.step('Проверка валидации обязательного поля "Заголовок"'):
            with allure.step('Выбрать в поле "Пользователи чата" ранее созданную учётную записи(Precondition)'):
                page.select_user_chat(last_name1)
            with allure.step('Оставить поле "Заголовок" пустым'):
                page.input_title(' ', 'clear')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.check_validation('Incorrect', '«название чата» обязательно')

    @pytest.mark.smoke
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_sending_a_message_to_a_chat_with_valid_data(self, browser):
        title_chat = random_en(8)
        message = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    email = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email, Roles.entrant)
                with allure.step('Назначить право create-chats'):
                    PERMISSION().assign_permission_to_user(email, 'create-chats')
                with allure.step('Назначить право view-chats'):
                    PERMISSION().assign_permission_to_user(email, 'view-chats')
                with allure.step('Назначить право update-chats'):
                    PERMISSION().assign_permission_to_user(email, 'update-chats')
            with allure.step('Создать второго пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    email_entrant = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email_entrant)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_entrant, Roles.entrant)
            with allure.step('Создать чат закрепленный за Поступающим'):
                with allure.step('Создать запись в abstract_chats, и закрепить за Поступающим'):
                    CHATS().add_new_abstract_chats(title_chat, USERS().get_user_id_by_email(email), 'no')
                with allure.step('Создать запись в tickets без исполнителя'):
                    CHATS().add_new_chats(title_chat)
                with allure.step('Создать запись в chat_users'):
                    CHATS().add_new_chat_users(email, title_chat)
                    CHATS().add_new_chat_users(email_entrant, title_chat)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Поступающий)'):
            page.input_email(email)
        with allure.step('Ввести в поле "Пароль" (Precondition-Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Chat(browser, link)
        with allure.step('Открыть вкладку "Чат"'):
            page.open_window_chat()
        with allure.step('Открыть ранее созданный чат(Precondition)'):
            page.open_chat(title_chat)
        page = Ticket(browser, link)
        with allure.step('Ввести в поле "Сообщение" 8 символов'):
            page.input_message(message)
        with allure.step('Нажать кнопку "Отправить"'):
            page.send_message(message, 'Correct', '')
        with allure.step('Проверка записи в abitur.chat_messages'):
            page.check_records_message_ticket(message, email, title_chat)
        page = Chat(browser, link)
        with allure.step('Нажать кнопку "Назад"'):
            page.back_to_window()
        page = Ticket(browser, link)
        with allure.step('Удалить ранее созданный чат'):
            page.leave_the_chat('Correct', title_chat)

    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_sending_a_message_to_a_chat_with_limit_values(self, browser):
        title_chat = random_en(8)
        message = random_en(1024)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    email = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email, Roles.entrant)
                with allure.step('Назначить право create-chats'):
                    PERMISSION().assign_permission_to_user(email, 'create-chats')
                with allure.step('Назначить право view-chats'):
                    PERMISSION().assign_permission_to_user(email, 'view-chats')
                with allure.step('Назначить право update-chats'):
                    PERMISSION().assign_permission_to_user(email, 'update-chats')
            with allure.step('Создать второго пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    email_entrant = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email_entrant)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_entrant, Roles.entrant)
            with allure.step('Создать чат закрепленный за Поступающим'):
                with allure.step('Создать запись в abstract_chats, и закрепить за Поступающим'):
                    CHATS().add_new_abstract_chats(title_chat, USERS().get_user_id_by_email(email), 'no')
                with allure.step('Создать запись в tickets без исполнителя'):
                    CHATS().add_new_chats(title_chat)
                with allure.step('Создать запись в chat_users'):
                    CHATS().add_new_chat_users(email, title_chat)
                    CHATS().add_new_chat_users(email_entrant, title_chat)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Поступающий)'):
            page.input_email(email)
        with allure.step('Ввести в поле "Пароль" (Precondition-Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Chat(browser, link)
        with allure.step('Открыть вкладку "Чат"'):
            page.open_window_chat()
        with allure.step('Открыть ранее созданный чат(Precondition)'):
            page.open_chat(title_chat)
        page = Ticket(browser, link)
        with allure.step('Ввести в поле "Сообщение" 1024 символова'):
            page.input_message(message)
        with allure.step('Нажать кнопку "Отправить"'):
            page.send_message(message, 'Correct', '')
        page = Chat(browser, link)
        with allure.step('Нажать кнопку "Назад"'):
            page.back_to_window()
        page = Ticket(browser, link)
        with allure.step('Удалить ранее созданный чат'):
            page.leave_the_chat('Correct', title_chat)

    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_sending_a_message_to_a_chat_with_out_of_range_values(self, browser):
        title_chat = random_en(8)
        message = random_en(1025)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    email = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email, Roles.entrant)
                with allure.step('Назначить право create-chats'):
                    PERMISSION().assign_permission_to_user(email, 'create-chats')
                with allure.step('Назначить право view-chats'):
                    PERMISSION().assign_permission_to_user(email, 'view-chats')
                with allure.step('Назначить право update-chats'):
                    PERMISSION().assign_permission_to_user(email, 'update-chats')
            with allure.step('Создать второго пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    email_entrant = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email_entrant)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_entrant, Roles.entrant)
            with allure.step('Создать чат закрепленный за Поступающим'):
                with allure.step('Создать запись в abstract_chats, и закрепить за Поступающим'):
                    CHATS().add_new_abstract_chats(title_chat, USERS().get_user_id_by_email(email), 'no')
                with allure.step('Создать запись в tickets без исполнителя'):
                    CHATS().add_new_chats(title_chat)
                with allure.step('Создать запись в chat_users'):
                    CHATS().add_new_chat_users(email, title_chat)
                    CHATS().add_new_chat_users(email_entrant, title_chat)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Поступающий)'):
            page.input_email(email)
        with allure.step('Ввести в поле "Пароль" (Precondition-Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Chat(browser, link)
        with allure.step('Открыть вкладку "Чат"'):
            page.open_window_chat()
        with allure.step('Открыть ранее созданный чат(Precondition)'):
            page.open_chat(title_chat)
        page = Ticket(browser, link)
        with allure.step('Ввести в поле "Сообщение" 1025 символа'):
            page.input_message(message)
        with allure.step('Нажать кнопку "Отправить"'):
            page.send_message(message, 'Incorrect', 'не может превышать 1024 символа')
        page = Chat(browser, link)
        with allure.step('Нажать кнопку "Назад"'):
            page.back_to_window()
        page = Ticket(browser, link)
        with allure.step('Удалить ранее созданный чат'):
            page.leave_the_chat('Correct', title_chat)

    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_editing_the_created_chat_in_the_editor(self, browser):
        title_chat = random_en(8)
        new_title_chat = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    email = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email, Roles.entrant)
                with allure.step('Назначить право create-chats'):
                    PERMISSION().assign_permission_to_user(email, 'create-chats')
                with allure.step('Назначить право view-chats'):
                    PERMISSION().assign_permission_to_user(email, 'view-chats')
                with allure.step('Назначить право update-chats'):
                    PERMISSION().assign_permission_to_user(email, 'update-chats')
            with allure.step('Создать второго пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    email_entrant = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email_entrant)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_entrant, Roles.entrant)
            with allure.step('Создать третьего пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    last_name = random_en(8)
                    email_entrant1 = random_en(8) + '@tes.com'
                    USERS().create_a_new_verified_user(last_name, random_en(8), random_en(8), email_entrant1)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_entrant1, Roles.entrant)
            with allure.step('Создать чат закрепленный за Поступающим'):
                with allure.step('Создать запись в abstract_chats, и закрепить за Поступающим'):
                    CHATS().add_new_abstract_chats(title_chat, USERS().get_user_id_by_email(email), 'no')
                with allure.step('Создать запись в tickets без исполнителя'):
                    CHATS().add_new_chats(title_chat)
                with allure.step('Создать запись в chat_users'):
                    CHATS().add_new_chat_users(email, title_chat)
                    CHATS().add_new_chat_users(email_entrant, title_chat)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Поступающий)'):
            page.input_email(email)
        with allure.step('Ввести в поле "Пароль" (Precondition-Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Chat(browser, link)
        with allure.step('Открыть вкладку "Чат"'):
            page.open_window_chat()
        with allure.step('Открыть ранее созданный чат(Precondition)'):
            page.open_chat(title_chat)
        with allure.step('Нажать кнопку "Управление --> Изменить"'):
            page.open_setting_chat('Изменить')
        with allure.step('Выбрать в поле "Пользователи чата" ранее созданную учётную записи(Precondition)'):
            page.select_user_chat(last_name)
        with allure.step('Ввести в поле "Заголовок" 8 символов'):
            page.input_title(new_title_chat, 'clear')
        with allure.step('Нажать кнопку "Сохранить"'):
            page.check_validation('CorrectUpdate', '')
        with allure.step('Проверка записи в abitur.chats/abitur.chats'):
            page.check_records_chat(new_title_chat, int(USERS().get_user_id_by_email(email)))
        with allure.step('Проверка записи в abitur.chat_users'):
            page.check_records_chat_users(new_title_chat, email_entrant1)
        with allure.step('Нажать кнопку "Назад"'):
            page.back_to_window()
        page = Ticket(browser, link)
        with allure.step('Удалить ранее созданный чат'):
            page.leave_the_chat('Correct', new_title_chat)

    @allure.story('Позитивная проверка функциональности')
    def testing_the_leave_chat_functionality_in_a_previously_created_chat_in_the_editor(self, browser):
        title_chat = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    email = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email, Roles.entrant)
                with allure.step('Назначить право create-chats'):
                    PERMISSION().assign_permission_to_user(email, 'create-chats')
                with allure.step('Назначить право view-chats'):
                    PERMISSION().assign_permission_to_user(email, 'view-chats')
                with allure.step('Назначить право update-chats'):
                    PERMISSION().assign_permission_to_user(email, 'update-chats')
            with allure.step('Создать второго пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    email_entrant = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email_entrant)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_entrant, Roles.entrant)
            with allure.step('Создать чат закрепленный за Поступающим'):
                with allure.step('Создать запись в abstract_chats, и закрепить за Поступающим'):
                    CHATS().add_new_abstract_chats(title_chat, USERS().get_user_id_by_email(email), 'no')
                with allure.step('Создать запись в tickets без исполнителя'):
                    CHATS().add_new_chats(title_chat)
                with allure.step('Создать запись в chat_users'):
                    CHATS().add_new_chat_users(email, title_chat)
                    CHATS().add_new_chat_users(email_entrant, title_chat)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Поступающий)'):
            page.input_email(email)
        with allure.step('Ввести в поле "Пароль" (Precondition-Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Chat(browser, link)
        with allure.step('Открыть вкладку "Чат"'):
            page.open_window_chat()
        with allure.step('Открыть ранее созданный чат(Precondition)'):
            page.open_chat(title_chat)
        with allure.step('Нажать кнопку "Управление --> Покинуть чат"'):
            page.open_setting_chat('Покинуть чат')
        with allure.step('Проверка присвоения {deleted_at} в таблице chat_users'):
            page.check_records_deleted_chat_users(email, title_chat)

    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_deleting_a_previously_created_chat_in_the_editor(self, browser):
        title_chat = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    email = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email, Roles.entrant)
                with allure.step('Назначить право create-chats'):
                    PERMISSION().assign_permission_to_user(email, 'create-chats')
                with allure.step('Назначить право view-chats'):
                    PERMISSION().assign_permission_to_user(email, 'view-chats')
                with allure.step('Назначить право update-chats'):
                    PERMISSION().assign_permission_to_user(email, 'update-chats')
            with allure.step('Создать второго пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    email_entrant = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email_entrant)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_entrant, Roles.entrant)
            with allure.step('Создать чат закрепленный за Поступающим'):
                with allure.step('Создать запись в abstract_chats, и закрепить за Поступающим'):
                    CHATS().add_new_abstract_chats(title_chat, USERS().get_user_id_by_email(email), 'no')
                with allure.step('Создать запись в tickets без исполнителя'):
                    CHATS().add_new_chats(title_chat)
                with allure.step('Создать запись в chat_users'):
                    CHATS().add_new_chat_users(email, title_chat)
                    CHATS().add_new_chat_users(email_entrant, title_chat)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Поступающий)'):
            page.input_email(email)
        with allure.step('Ввести в поле "Пароль" (Precondition-Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Chat(browser, link)
        with allure.step('Открыть вкладку "Чат"'):
            page.open_window_chat()
        with allure.step('Открыть ранее созданный чат(Precondition)'):
            page.open_chat(title_chat)
        with allure.step('Нажать кнопку "Управление --> Удалить"'):
            page.open_setting_chat('Удалить')
        with allure.step('Проверка записи об удалении в abitur.chats'):
            page.check_deleting_chats(title_chat)
        with allure.step('Проверка записи об удалении в abitur.abstract_chats'):
            page.check_deleting_abstract_chats(title_chat)

    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_deleting_a_previously_created_chat(self, browser):
        title_chat = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    email = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email, Roles.entrant)
                with allure.step('Назначить право create-chats'):
                    PERMISSION().assign_permission_to_user(email, 'create-chats')
                with allure.step('Назначить право view-chats'):
                    PERMISSION().assign_permission_to_user(email, 'view-chats')
                with allure.step('Назначить право update-chats'):
                    PERMISSION().assign_permission_to_user(email, 'update-chats')
            with allure.step('Создать второго пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    email_entrant = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email_entrant)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_entrant, Roles.entrant)
            with allure.step('Создать чат закрепленный за Поступающим'):
                with allure.step('Создать запись в abstract_chats, и закрепить за Поступающим'):
                    CHATS().add_new_abstract_chats(title_chat, USERS().get_user_id_by_email(email), 'no')
                with allure.step('Создать запись в tickets без исполнителя'):
                    CHATS().add_new_chats(title_chat)
                with allure.step('Создать запись в chat_users'):
                    CHATS().add_new_chat_users(email, title_chat)
                    CHATS().add_new_chat_users(email_entrant, title_chat)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Поступающий)'):
            page.input_email(email)
        with allure.step('Ввести в поле "Пароль" (Precondition-Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Chat(browser, link)
        with allure.step('Открыть вкладку "Чат"'):
            page.open_window_chat()
        page = Ticket(browser, link)
        with allure.step('Удалить ранее созданный тикет'):
            page.leave_the_chat('Correct', title_chat)
        page = Chat(browser, link)
        with allure.step('Проверка записи об удалении в abitur.chats'):
            page.check_deleting_chats(title_chat)
        with allure.step('Проверка записи об удалении в abitur.abstract_chats'):
            page.check_deleting_abstract_chats(title_chat)

    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_leave_chat(self, browser):
        title_chat = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    email = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email, Roles.entrant)
                with allure.step('Назначить право create-chats'):
                    PERMISSION().assign_permission_to_user(email, 'create-chats')
                with allure.step('Назначить право view-chats'):
                    PERMISSION().assign_permission_to_user(email, 'view-chats')
                with allure.step('Назначить право update-chats'):
                    PERMISSION().assign_permission_to_user(email, 'update-chats')
            with allure.step('Создать второго пользователя с ролью "Поступающий"'):
                with allure.step('Создать верифицированную учётную запись'):
                    email_entrant = random_en(8) + '@test.com'
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email_entrant)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_entrant, Roles.entrant)
            with allure.step('Создать чат закрепленный за Поступающим'):
                with allure.step('Создать запись в abstract_chats, и закрепить за Поступающим'):
                    CHATS().add_new_abstract_chats(title_chat, USERS().get_user_id_by_email(email), 'no')
                with allure.step('Создать запись в tickets без исполнителя'):
                    CHATS().add_new_chats(title_chat)
                with allure.step('Создать запись в chat_users'):
                    CHATS().add_new_chat_users(email, title_chat)
                    CHATS().add_new_chat_users(email_entrant, title_chat)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Поступающий)'):
            page.input_email(email)
        with allure.step('Ввести в поле "Пароль" (Precondition-Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Chat(browser, link)
        with allure.step('Открыть вкладку "Чат"'):
            page.open_window_chat()
        with allure.step('Покинуть ранее созданный тикет'):
            page.leave_chat()
        with allure.step('Проверка присвоения {deleted_at} в таблице chat_users'):
            page.check_records_deleted_chat_users(email, title_chat)


@pytest.mark.chat
@allure.link(link + '/admin/tickets/types', name='ТАНДЕМ ЛК - Виды обращений')
@allure.epic('Функциональное тестирование модуля "CHAT"')
@allure.feature('Функциональное тестирование раздела "Виды обращений"')
class TestChat_TICKET_TYPES:
    @pytest.mark.smoke
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_creating_a_type_of_chat_with_correct_data_and_deleting_it(self, browser):
        title = random_en(8)
        description = random_en(8)
        is_active = 1
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Виды обращений"'):
            page.open_window_setting('Виды обращений')
        with allure.step('Нажать кнопку "+ Вид обращения"'):
            page.add_a_new_record()
        with allure.step('Ввести в поле "Заголовок" 8 символов'):
            page.input_title(title)
        with allure.step('Ввести в поле "Описание" 8 символов'):
            page.input_description(description)
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_validators('Correct', '')
        with allure.step('Проверка записи в abitur.ticket_types'):
            page.check_record_type_chat(title, description, is_active)
        with allure.step('Найти ранее созданную запись'):
            page.action_type_chats(title, 'remove', 'Correct')

    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_creating_a_type_of_chat_with_a_limit_value(self, browser):
        title = random_en(191)
        description = random_en(2048)
        is_active = 0
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Виды обращений"'):
            page.open_window_setting('Виды обращений')
        with allure.step('Нажать кнопку "+ Вид обращения"'):
            page.add_a_new_record()
        with allure.step('Ввести в поле "Заголовок" 191 символов'):
            page.input_title(title)
        with allure.step('Ввести в поле "Описание" 2048 символов'):
            page.input_description(description)
        with allure.step('Выключть чекбокс "Используется"'):
            page.checkbox_is_active()
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_validators('Correct', '')
        with allure.step('Проверка записи в abitur.ticket_types'):
            page.check_record_type_chat(title, description, is_active)
        with allure.step('Найти ранее созданную запись'):
            page.action_type_chats(title, 'remove', 'Correct')

    @allure.story('Негативная проверка функциональности')
    def testing_the_functionality_of_creating_a_type_of_chat_with_exorbitant_values(self, browser):
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Виды обращений"'):
            page.open_window_setting('Виды обращений')
        with allure.step('Нажать кнопку "+ Вид обращения"'):
            page.add_a_new_record()
        with allure.step('Проверка валидации поля при вводе предельного значения в поле "Заголовок"'):
            with allure.step('Ввести в поле "Заголовок" 191 символов'):
                page.input_title(random_en(192))
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«заголовок» не может превышать')
            with allure.step('Вернуть в исходное состояние поле "Заголовок"'):
                page.input_title(random_en(8))
        with allure.step('Проверка валидации поля при вводе предельного значения в поле "Описание"'):
            with allure.step('Ввести в поле "Описание" 2049 символов'):
                page.input_description(random_en(2049))
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«описание» не может превышать')
            with allure.step('Вернуть в исходное состояние поле "Заголовок"'):
                page.input_description(random_en(8))

    @allure.story('Негативная проверка функциональности')
    def testing_the_functionality_of_the_unique_title_of_the_type_of_request(self, browser):
        title = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новый вид обращения'):
                CHATS().add_ticket_types(title, random_en(8), 1)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Виды обращений"'):
            page.open_window_setting('Виды обращений')
        with allure.step('Нажать кнопку "+ Вид обращения"'):
            page.add_a_new_record()
        with allure.step('Проверка валидации уникального значения в поле "Заголовок"'):
            with allure.step('Ввести в поле "Заголовок" значение ранее созданного вида (Precondition)'):
                page.input_title(title)
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«заголовок» уже существует')
            with allure.step('Вернуть в исходное состояние поле "Заголовок"'):
                page.input_title(random_en(8))

    @allure.story('Позитивная проверка функциональности')
    def testing_the_editing_functionality_of_a_previously_created_request_type(self, browser):
        title = random_en(8)
        new_title = random_en(8)
        description = random_en(8)
        is_active = 0
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новый вид обращения'):
                CHATS().add_ticket_types(title, random_en(8), 1)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Виды обращений"'):
            page.open_window_setting('Виды обращений')
        with allure.step('Редактировать ранее созданную запись'):
            page.action_type_chats(title, 'edit', 'Correct')
        with allure.step('Ввести в поле "Заголовок" 8 символов'):
            page.input_title(new_title)
        with allure.step('Ввести в поле "Описание" 8 символов'):
            page.input_description(description)
        with allure.step('Выключть чекбокс "Используется"'):
            page.checkbox_is_active()
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_validators('Correct', '')
        with allure.step('Проверка записи в abitur.ticket_types'):
            page.check_record_type_chat(new_title, description, is_active)
        with allure.step('Нажать кнопку "Назад"'):
            page.click_back_button()
        with allure.step('Удалить ранее созданную запись'):
            page.action_type_chats(new_title, 'remove', 'Correct')

    @allure.story('Негативная проверка функциональности')
    def testing_the_functionality_of_creating_a_type_of_chat_without_filling_in_the_required_fields(self, browser):
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Виды обращений"'):
            page.open_window_setting('Виды обращений')
        with allure.step('Нажать кнопку "+ Вид обращения"'):
            page.add_a_new_record()
        with allure.step('Проверка валидации обязательного поля "Заголовок"'):
            with allure.step('Оставить пустым поле "Заголовок"'):
                page.input_title(' ')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«заголовок» обязательно')
            with allure.step('Вернуть в исходное состояние поле "Заголовок"'):
                page.input_title(random_en(8))

    @allure.story('Негативная проверка функциональности')
    def testing_the_functionality_of_deleting_the_type_of_address_used_in_the_chat(self, browser):
        type_id = random_en(8)
        title_chat = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать обращение с ранее созданным типом обращения'):
                with allure.step('Создать новый используемый вид обращения'):
                    CHATS().add_ticket_types(type_id, random_en(8), 1)
                with allure.step('Создать запись в abstract_chats, и закрепить за Администратором'):
                    CHATS().add_new_abstract_chats(title_chat, USERS().get_user_id_by_email(admin_login), 'no')
                with allure.step('Создать запись в tickets без исполнителя'):
                    CHATS().add_new_ticket(title_chat, random_en(8), None, 'Новое', type_id, 'no')
                with allure.step('Создать запись в chat_users'):
                    CHATS().add_new_chat_users(admin_login, title_chat)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Виды обращений"'):
            page.open_window_setting('Виды обращений')
        with allure.step('Удалить ранее созданную запись'):
            page.action_type_chats(type_id, 'remove', 'Incorrect')
