import pytest
import allure

from .pages.cabinet_page import Profile
from .pages.precondition import link, random_en, password, admin_login
from .pages.authentication_page import RegLogin


@pytest.mark.cabinet
@pytest.mark.skip(reason='Тесты не доработаны до актуальной сборки.')
@allure.link(link + 'user/profile', name='ТАНДЕМ Личный кабинет')
@allure.epic('Функциональное тестирование интерфейса ЛК')
@allure.feature('Проверка функциональности вкладки "Профиль"')
class TestProfile:
    @pytest.mark.smoke
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.story('Позитивная проверка функциональности модуля')
    def testing_the_functionality_of_the_block_profile_information_with_correct_data(self, browser):
        with allure.step('Пдготовка аккаунта к выполнению тест-кейса'):
            box = random_en(8) + '@test.com'
            page = RegLogin(browser, link)
            with allure.step('Открыть стенд ЛКА'):
                page.open()
            with allure.step('Нажать "Регистрация"'):
                page.open_registration_page()
            with allure.step('Ввести в поле "Email" временную почту'):
                page.input_email(box.email)
            with allure.step('Ввести в поле "Фамилия" 8 символов'):
                page.input_last_name(random_en(8))
            with allure.step('Ввести в поле "Имя" 8 символов'):
                page.input_name(random_en(8))
            with allure.step('Ввести в поле "Пароль" 8 символов'):
                page.input_password(password)
            with allure.step('Повторить ранее введённые данные в поле "Пароль еще раз"'):
                page.input_password_confirmation(password)
            with allure.step('Поставить галочку "Даю согласие на обработку"'):
                page.put_a_tick_in_the_checkbox()
            with allure.step('Нажать "Зарегистрироваться"'):
                page.data_validation_check('Correct', 'Login', '')
        page = Profile(browser, link)
        with allure.step('Нажать линк "Профиль" в левом боковом меню'):
            page.open_profile()
        with allure.step('Ввести в поле "Email" валидную почту'):
            page.input_email(random_en(8) + '@test.com')
        with allure.step('Ввести в поле "Фамилия" 8 символов'):
            page.input_last_name(random_en(8))
        with allure.step('Ввести в поле Имя" 8 символов'):
            page.input_name(random_en(8))
        with allure.step('Ввести в поле Отчество" 8 символов'):
            page.input_middle_name(random_en(8))
        with allure.step('Нажать "Сохранить"'):
            page.save_results_of_changes_profile('CorrectEmail')

    @allure.story('Позитивная проверка функциональности модуля')
    def testing_the_functionality_of_the_block_profile_information_with_limit_values(self, browser):
        with allure.step('Пдготовка аккаунта к выполнению тест-кейса'):
            box = random_en(8) + '@test.com'
            page = RegLogin(browser, link)
            with allure.step('Открыть стенд ЛКА'):
                page.open()
            with allure.step('Нажать "Регистрация"'):
                page.open_registration_page()
            with allure.step('Ввести в поле "Email" временную почту'):
                page.input_email(box.email)
            with allure.step('Ввести в поле "Фамилия" 8 символов'):
                page.input_last_name(random_en(8))
            with allure.step('Ввести в поле "Имя" 8 символов'):
                page.input_name(random_en(8))
            with allure.step('Ввести в поле "Пароль" 8 символов'):
                page.input_password(password)
            with allure.step('Повторить ранее введённые данные в поле "Пароль еще раз"'):
                page.input_password_confirmation(password)
            with allure.step('Поставить галочку "Даю согласие на обработку"'):
                page.put_a_tick_in_the_checkbox()
            with allure.step('Нажать "Зарегистрироваться"'):
                page.data_validation_check('Correct', 'Login', '')
        page = Profile(browser, link)
        with allure.step('Нажать линк "Профиль" в левом боковом меню'):
            page.open_profile()
        with allure.step('Ввести в поле "Email" 255 символов'):
            page.input_email(random_en(246) + '@test.com')
        with allure.step('Ввести в поле "Фамилия" 255 символов'):
            page.input_last_name(random_en(255))
        with allure.step('Ввести в поле Имя" 255 символов'):
            page.input_name(random_en(255))
        with allure.step('Ввести в поле Отчество" 255 символов'):
            page.input_middle_name(random_en(255))
        with allure.step('Нажать "Сохранить"'):
            page.save_results_of_changes_profile('CorrectEmail')

    @allure.story('Позитивная проверка функциональности модуля')
    def testing_the_functionality_of_the_block_profile_information_with_valid_photos(self, browser):
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Profile(browser, link)
        with allure.step('Нажать линк "Профиль" в левом боковом меню'):
            page.open_profile()
        with allure.step('Загрузка аватара в формате "jpg"'):
            page.profile_edit_avatar('files/images/photo.jpg')
        with allure.step('Нажать "Сохранить"'):
            page.save_results_of_changes_avatar('Correct')
        with allure.step('Загрузка аватара в формате "png"'):
            page.profile_edit_avatar('files/images/photo.png')
        with allure.step('Нажать "Сохранить"'):
            page.save_results_of_changes_avatar('Correct')
        with allure.step('Загрузка аватара в формате "gif"'):
            page.profile_edit_avatar('files/images/photo.gif')
        with allure.step('Нажать "Сохранить"'):
            page.save_results_of_changes_avatar('Correct')
        with allure.step('Загрузка аватара в формате "webm"'):
            page.profile_edit_avatar('files/images/photo.webp')
        with allure.step('Нажать "Сохранить"'):
            page.save_results_of_changes_avatar('Correct')

    @allure.story('Позитивная проверка функциональности модуля')
    def testing_the_validation_of_the_block_profile_information_with_the_used_email_address(self, browser):
        with allure.step('Пдготовка аккаунта к выполнению тест-кейса'):
            box = random_en(8) + '@test.com'
            page = RegLogin(browser, link)
            with allure.step('Открыть стенд ЛКА'):
                page.open()
            with allure.step('Нажать "Регистрация"'):
                page.open_registration_page()
            with allure.step('Ввести в поле "Email" временную почту'):
                page.input_email(box.email)
            with allure.step('Ввести в поле "Фамилия" 8 символов'):
                page.input_last_name(random_en(8))
            with allure.step('Ввести в поле "Имя" 8 символов'):
                page.input_name(random_en(8))
            with allure.step('Ввести в поле "Пароль" 8 символов'):
                page.input_password(password)
            with allure.step('Повторить ранее введённые данные в поле "Пароль еще раз"'):
                page.input_password_confirmation(password)
            with allure.step('Поставить галочку "Даю согласие на обработку"'):
                page.put_a_tick_in_the_checkbox()
            with allure.step('Нажать "Зарегистрироваться"'):
                page.data_validation_check('Correct', 'Login', '')
        page = Profile(browser, link)
        with allure.step('Нажать линк "Профиль" в левом боковом меню'):
            page.open_profile()
        with allure.step('Ввести в поле "Email" существующую почту'):
            page.input_email(admin_login)
        with allure.step('Нажать "Сохранить"'):
            page.save_results_of_changes_profile('Incorrect')

    @allure.story('Негативная проверка функциональности модуля')
    def testing_the_validation_of_the_block_profile_information_with_out_of_range_values(self, browser):
        with allure.step('Пдготовка аккаунта к выполнению тест-кейса'):
            box = random_en(8) + '@test.com'
            page = RegLogin(browser, link)
            with allure.step('Открыть стенд ЛКА'):
                page.open()
            with allure.step('Нажать "Регистрация"'):
                page.open_registration_page()
            with allure.step('Ввести в поле "Email" временную почту'):
                page.input_email(box)
            with allure.step('Ввести в поле "Фамилия" 8 символов'):
                page.input_last_name(random_en(8))
            with allure.step('Ввести в поле "Имя" 8 символов'):
                page.input_name(random_en(8))
            with allure.step('Ввести в поле "Пароль" 8 символов'):
                page.input_password(password)
            with allure.step('Повторить ранее введённые данные в поле "Пароль еще раз"'):
                page.input_password_confirmation(password)
            with allure.step('Поставить галочку "Даю согласие на обработку"'):
                page.put_a_tick_in_the_checkbox()
            with allure.step('Нажать "Зарегистрироваться"'):
                page.data_validation_check('Correct', 'Login', '')
        page = Profile(browser, link)
        with allure.step('Нажать линк "Профиль" в левом боковом меню'):
            page.open_profile()
        with allure.step('Ввести в поле "Email" 256 символов'):
            page.input_email(random_en(247) + '@test.com')
        with allure.step('Нажать "Сохранить"'):
            page.save_results_of_changes_profile('Incorrect')
        with allure.step('Подготовка к выполнению тест-кейса'):
            with allure.step('Ввести в поле "Email" валидную почту'):
                page.input_email(random_en(8) + '@test.com')
        with allure.step('Ввести в поле "Фамилия" 256 символов'):
            page.input_last_name(random_en(256))
        with allure.step('Нажать "Сохранить"'):
            page.save_results_of_changes_profile('Incorrect')
        with allure.step('Подготовка к выполнению тест-кейса'):
            with allure.step('Ввести в поле "Фамилия" 8 символов'):
                page.input_last_name(random_en(8))
        with allure.step('Ввести в поле Имя" 256 символов'):
            page.input_name(random_en(256))
        with allure.step('Нажать "Сохранить"'):
            page.save_results_of_changes_profile('Incorrect')
        with allure.step('Подготовка к выполнению тест-кейса'):
            with allure.step('Ввести в поле Имя" 8 символов'):
                page.input_name(random_en(8))
        with allure.step('Ввести в поле Отчество" 256 символов'):
            page.input_middle_name(random_en(256))
        with allure.step('Нажать "Сохранить"'):
            page.save_results_of_changes_profile('Incorrect')

    @pytest.mark.xfail(
        reason='Ошибка функциональности валидатора при загрузке объемного аватара в профиль пользователя')
    @allure.story('Негативная проверка функциональности модуля')
    def testing_the_functionality_of_the_block_profile_information_with_invalid_photos(self, browser):
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Profile(browser, link)
        with allure.step('Открыть "Профиль" в левом боковом меню'):
            page.open_profile()
        with allure.step('Загрузка аватара в формате "pdf"'):
            page.profile_edit_avatar('files/doc/doc.pdf')
        with allure.step('Нажать "Сохранить"'):
            page.save_results_of_changes_avatar('Incorrect')
        with allure.step('Загрузка аватара "png" размером более 500кб"'):
            page.profile_edit_avatar('files/images/photo500.png')
        with allure.step('Нажать "Сохранить"'):
            page.save_results_of_changes_avatar('Incorrect')
        with allure.step('Загрузка аватара в формате "txt"'):
            page.profile_edit_avatar('files/doc/doc.txt')
        with allure.step('Нажать "Сохранить"'):
            page.save_results_of_changes_avatar('Incorrect')

    @allure.story('Позитивная проверка функциональности модуля')
    def testing_the_functionality_authorization_with_changed_email_addresses(self, browser):
        random_email = random_en(8) + '@test.com'
        with allure.step('Пдготовка аккаунта к выполнению тест-кейса'):
            box = random_en(8) + '@test.com'
            page = RegLogin(browser, link)
            with allure.step('Открыть стенд ЛКА'):
                page.open()
            with allure.step('Нажать "Регистрация"'):
                page.open_registration_page()
            with allure.step('Ввести в поле "Email" временную почту'):
                page.input_email(box)
            with allure.step('Ввести в поле "Фамилия" 8 символов'):
                page.input_last_name(random_en(8))
            with allure.step('Ввести в поле "Имя" 8 символов'):
                page.input_name(random_en(8))
            with allure.step('Ввести в поле "Пароль" 8 символов'):
                page.input_password(password)
            with allure.step('Повторить ранее введённые данные в поле "Пароль еще раз"'):
                page.input_password_confirmation(password)
            with allure.step('Поставить галочку "Даю согласие на обработку"'):
                page.put_a_tick_in_the_checkbox()
            with allure.step('Нажать "Зарегистрироваться"'):
                page.data_validation_check('Correct', 'Login')
        page = Profile(browser, link)
        with allure.step('Нажать линк "Профиль" в левом боковом меню'):
            page.open_profile()
        with allure.step('Ввести в поле "Email" новый валидный адрес'):
            page.input_email(random_email)
        with allure.step('Нажать "Сохранить"'):
            page.save_results_of_changes_profile('CorrectEmail')
        page = RegLogin(browser, link)
        with allure.step('Ввести в поле "E-mail" (Precondition)'):
            page.input_email(random_email)
        with allure.step('Ввести в поле "Пароль" (Precondition)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')

    @pytest.mark.smoke
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.story('Позитивная проверка функциональности модуля')
    def testing_the_validation_of_the_block_password_update_with_correct_data(self, browser):
        with allure.step('Пдготовка аккаунта к выполнению тест-кейса'):
            box = random_en(8) + '@test.com'
            random_password = random_en(8)
            page = RegLogin(browser, link)
            with allure.step('Открыть стенд ЛКА'):
                page.open()
            with allure.step('Нажать "Регистрация"'):
                page.open_registration_page()
            with allure.step('Ввести в поле "Email" временную почту'):
                page.input_email(box)
            with allure.step('Ввести в поле "Фамилия" 8 символов'):
                page.input_last_name(random_en(8))
            with allure.step('Ввести в поле "Имя" 8 символов'):
                page.input_name(random_en(8))
            with allure.step('Ввести в поле "Пароль" 8 символов'):
                page.input_password(password)
            with allure.step('Повторить ранее введённые данные в поле "Пароль еще раз"'):
                page.input_password_confirmation(password)
            with allure.step('Поставить галочку "Даю согласие на обработку"'):
                page.put_a_tick_in_the_checkbox()
            with allure.step('Нажать "Зарегистрироваться"'):
                page.data_validation_check('Correct', 'Login')
        page = Profile(browser, link)
        with allure.step('Нажать линк "Профиль" в левом боковом меню'):
            page.open_profile()
        with allure.step('Ввести в поле "Текущий пароль" данные из (Precondition)'):
            page.input_current_password(password)
        with allure.step('Ввести в поле "Новый пароль" 8 символов'):
            page.input_password(random_password)
        with allure.step('Повторить ранее введённый пароль в поле "Подтверждение пароля"'):
            page.input_password_confirmation(random_password)
        with allure.step('Нажать "Сохранить"'):
            page.save_results_of_changes_password('Correct', 1)
        with allure.step('Нажать кнопку "Покинуть учётную запись"'):
            page.exit_profile()
        page = RegLogin(browser, link)
        with allure.step('Ввести в поле "E-mail" (Precondition)'):
            page.input_email(box)
        with allure.step('Ввести в поле "Пароль" новый пароль'):
            page.input_password(random_password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')

    @allure.story('Негативная проверка функциональности модуля')
    def testing_the_validation_of_the_block_password_update_with_incorrect_data(self, browser):
        with allure.step('Пдготовка аккаунта к выполнению тест-кейса'):
            box = random_en(8) + '@test.com'
            page = RegLogin(browser, link)
            with allure.step('Открыть стенд ЛКА'):
                page.open()
            with allure.step('Нажать "Регистрация"'):
                page.open_registration_page()
            with allure.step('Ввести в поле "Email" временную почту'):
                page.input_email(box)
            with allure.step('Ввести в поле "Фамилия" 8 символов'):
                page.input_last_name(random_en(8))
            with allure.step('Ввести в поле "Имя" 8 символов'):
                page.input_name(random_en(8))
            with allure.step('Ввести в поле "Пароль" 8 символов'):
                page.input_password(password)
            with allure.step('Повторить ранее введённые данные в поле "Пароль еще раз"'):
                page.input_password_confirmation(password)
            with allure.step('Поставить галочку "Даю согласие на обработку"'):
                page.put_a_tick_in_the_checkbox()
            with allure.step('Нажать "Зарегистрироваться"'):
                page.data_validation_check('Correct', 'Login')
        page = Profile(browser, link)
        with allure.step('Нажать линк "Профиль" в левом боковом меню'):
            page.open_profile()
        with allure.step('Проверка валидации формы с некорректным полем "Текущий пароль"'):
            with allure.step('Ввести в поле "Текущий пароль" 8 символов'):
                page.input_current_password(random_en(8))
            with allure.step('Ввести в поле "Новый пароль" 8 символов'):
                page.input_password(password)
            with allure.step('Повторить ранее введённый пароль в поле "Подтверждение пароля"'):
                page.input_password_confirmation(password)
            with allure.step('Нажать "Сохранить"'):
                page.save_results_of_changes_password('Incorrect', 1)
        with allure.step('Проверка валидации формы с некорректным полем "Новый пароль"'):
            with allure.step('Ввести в поле "Текущий пароль" действующий пароль'):
                page.input_current_password(password)
            with allure.step('Ввести в поле "Новый пароль" 8 символов'):
                page.input_password(random_en(8))
            with allure.step('Ввести в поле "Подтверждение пароля" 8 символов'):
                page.input_password_confirmation(password)
            with allure.step('Нажать "Сохранить"'):
                page.save_results_of_changes_password('Incorrect', 1)
        with allure.step('Проверка валидации формы с некорректным полем "Подтверждение пароля"'):
            with allure.step('Ввести в поле "Текущий пароль" действующий пароль'):
                page.input_current_password(password)
            with allure.step('Ввести в поле "Новый пароль" 8 символов'):
                page.input_password(password)
            with allure.step('Ввести в поле "Подтверждение пароля" 8 символов'):
                page.input_password_confirmation(random_en(8))
            with allure.step('Нажать "Сохранить"'):
                page.save_results_of_changes_password('Incorrect', 1)
        with allure.step('Проверка валидации формы с некорректным полем "Новый пароль"'):
            with allure.step('Ввести в поле "Текущий пароль" действующий пароль'):
                page.input_current_password(password)
            with allure.step('Ввести в поле "Новый пароль" 2 символа'):
                page.input_password(random_en(2))
            with allure.step('Ввести в поле "Подтверждение пароля" 8 символов'):
                page.input_password_confirmation(password)
            with allure.step('Нажать "Сохранить"'):
                page.save_results_of_changes_password('Incorrect', 1)
        with allure.step('Проверка валидации формы с некорректным полем "Подтверждение пароля"'):
            with allure.step('Ввести в поле "Текущий пароль" действующий пароль'):
                page.input_current_password(password)
            with allure.step('Ввести в поле "Новый пароль" 8 символов'):
                page.input_password(password)
            with allure.step('Ввести в поле "Подтверждение пароля" 2 символа'):
                page.input_password_confirmation(random_en(2))
            with allure.step('Нажать "Сохранить"'):
                page.save_results_of_changes_password('Incorrect', 1)

    @allure.story('Позитивная проверка функциональности модуля')
    def testing_the_functionality_of_the_2FA_with_correct_recovery_code(self, browser):
        with allure.step('Пдготовка аккаунта к выполнению тест-кейса'):
            box = random_en(8) + '@test.com'
            page = RegLogin(browser, link)
            with allure.step('Открыть стенд ЛКА'):
                page.open()
            with allure.step('Нажать "Регистрация"'):
                page.open_registration_page()
            with allure.step('Ввести в поле "Email" временную почту'):
                page.input_email(box)
            with allure.step('Ввести в поле "Фамилия" 8 символов'):
                page.input_last_name(random_en(8))
            with allure.step('Ввести в поле "Имя" 8 символов'):
                page.input_name(random_en(8))
            with allure.step('Ввести в поле "Пароль" 8 символов'):
                page.input_password(password)
            with allure.step('Повторить ранее введённые данные в поле "Пароль еще раз"'):
                page.input_password_confirmation(password)
            with allure.step('Поставить галочку "Даю согласие на обработку"'):
                page.put_a_tick_in_the_checkbox()
            with allure.step('Нажать "Зарегистрироваться"'):
                page.data_validation_check('Correct', 'Login')
        page = Profile(browser, link)
        with allure.step('Нажать линк "Профиль" в левом боковом меню'):
            page.open_profile()
        with allure.step('Нажать кнопку "Включить" в блоке Двухфакторная аутентификация'):
            page.click_select_button(3)
        with allure.step('Ввести корректный пароль от учётной записи'):
            page.input_password_modal_windows(password, 0)
        with allure.step('Нажать кнопку "Подтвердить"'):
            page.click_enter_button(2)
        with allure.step('Скопировать один код восстановления учетной записи'):
            page.copy_token_two_factor_authentication()
        with allure.step('Нажать кнопку "Покинуть учётную запись"'):
            page.exit_profile()
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(box)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Two-Factor-Challenge')
        with allure.step('Нажать "Использовать код восстановления"'):
            page.click_use_recovery_code()
        page = Profile(browser, link)
        with allure.step('Ввести ранее скопированый код востановления'):
            page.input_global_token()
        page = RegLogin(browser, link)
        with allure.step('Нажать кнопку "Вход"'):
            page.data_validation_check('Correct', 'Login')

    @allure.story('Негативная проверка функциональности модуля')
    def testing_the_validation_of_a_2FA_with_incorrect_recovery_code(self, browser):
        with allure.step('Пдготовка аккаунта к выполнению тест-кейса'):
            box = random_en(8) + '@test.com'
            page = RegLogin(browser, link)
            with allure.step('Открыть стенд ЛКА'):
                page.open()
            with allure.step('Нажать "Регистрация"'):
                page.open_registration_page()
            with allure.step('Ввести в поле "Email" временную почту'):
                page.input_email(box)
            with allure.step('Ввести в поле "Фамилия" 8 символов'):
                page.input_last_name(random_en(8))
            with allure.step('Ввести в поле "Имя" 8 символов'):
                page.input_name(random_en(8))
            with allure.step('Ввести в поле "Пароль" 8 символов'):
                page.input_password(password)
            with allure.step('Повторить ранее введённые данные в поле "Пароль еще раз"'):
                page.input_password_confirmation(password)
            with allure.step('Поставить галочку "Даю согласие на обработку"'):
                page.put_a_tick_in_the_checkbox()
            with allure.step('Нажать "Зарегистрироваться"'):
                page.data_validation_check('Correct', 'Login')
            page = Profile(browser, link)
            with allure.step('Нажать линк "Профиль" в левом боковом меню'):
                page.open_profile()
            with allure.step('Нажать кнопку "Включить" в блоке Двухфакторная аутентификация'):
                page.click_select_button(3)
            with allure.step('Ввести корректный пароль от учётной записи'):
                page.input_password_modal_windows(password, 0)
            with allure.step('Нажать кнопку "Подтвердить"'):
                page.click_enter_button(2)
            with allure.step('Нажать кнопку "Покинуть учётную запись"'):
                page.exit_profile()
            page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(box)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Two-Factor-Challenge')
        with allure.step('Нажать "Использовать код восстановления"'):
            page.click_use_recovery_code()
        page = Profile(browser, link)
        with allure.step('Ввести некорректный код востановления'):
            page.input_token(random_en(10) + '-' + random_en(10))
        page = RegLogin(browser, link)
        with allure.step('Нажать кнопку "Вход"'):
            page.data_validation_check('Incorrect', 'Login')
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(box)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Two-Factor-Challenge')
        with allure.step('Нажать "Использовать код восстановления"'):
            page.click_use_recovery_code()
        page = Profile(browser, link)
        with allure.step('Ввести в поле "Код востановления" 255 символов'):
            page.input_token(random_en(127) + '-' + random_en(127))
        page = RegLogin(browser, link)
        with allure.step('Нажать кнопку "Вход"'):
            page.data_validation_check('Incorrect', 'Login')
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(box)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Two-Factor-Challenge')
        with allure.step('Нажать "Использовать код восстановления"'):
            page.click_use_recovery_code()
        page = Profile(browser, link)
        with allure.step('Ввести в поле "Код востановления" 256 символов'):
            page.input_token(random_en(128) + '-' + random_en(127))
        page = RegLogin(browser, link)
        with allure.step('Нажать кнопку "Вход"'):
            page.data_validation_check('Incorrect', 'Login')

    @allure.story('Позитивная проверка функциональности модуля')
    def testing_the_functionality_of_2FA_on_code_regeneration(self, browser):
        with allure.step('Пдготовка аккаунта к выполнению тест-кейса'):
            box = random_en(8) + '@test.com'
            page = RegLogin(browser, link)
            with allure.step('Открыть стенд ЛКА'):
                page.open()
            with allure.step('Нажать "Регистрация"'):
                page.open_registration_page()
            with allure.step('Ввести в поле "Email" временную почту'):
                page.input_email(box)
            with allure.step('Ввести в поле "Фамилия" 8 символов'):
                page.input_last_name(random_en(8))
            with allure.step('Ввести в поле "Имя" 8 символов'):
                page.input_name(random_en(8))
            with allure.step('Ввести в поле "Пароль" 8 символов'):
                page.input_password(password)
            with allure.step('Повторить ранее введённые данные в поле "Пароль еще раз"'):
                page.input_password_confirmation(password)
            with allure.step('Поставить галочку "Даю согласие на обработку"'):
                page.put_a_tick_in_the_checkbox()
            with allure.step('Нажать "Зарегистрироваться"'):
                page.data_validation_check('Correct', 'Login')
        page = Profile(browser, link)
        with allure.step('Нажать линк "Профиль" в левом боковом меню'):
            page.open_profile()
        with allure.step('Нажать кнопку "Включить" в блоке Двухфакторная аутентификация'):
            page.click_select_button(3)
        with allure.step('Ввести корректный пароль от учётной записи'):
            page.input_password_modal_windows(password, 0)
        with allure.step('Нажать кнопку "Подтвердить"'):
            page.click_enter_button(2)
        with allure.step('Нажать кнопку "Перегенерировать коды восстановления"'):
            page.regenerate_recovery_codes()
        with allure.step('Скопировать один код восстановления учетной записи'):
            page.copy_token_two_factor_authentication()
        with allure.step('Нажать кнопку "Покинуть учётную запись"'):
            page.exit_profile()
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(box)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Two-Factor-Challenge')
        with allure.step('Нажать "Использовать код восстановления"'):
            page.click_use_recovery_code()
        page = Profile(browser, link)
        with allure.step('Ввести ранее скопированый код востановления'):
            page.input_global_token()
        page = RegLogin(browser, link)
        with allure.step('Нажать кнопку "Вход"'):
            page.data_validation_check('Correct', 'Login')

    @allure.story('Негативная проверка функциональности модуля')
    def testing_the_verification_of_2FA_form_when_re_entering_the_recovery_code(self, browser):
        with allure.step('Пдготовка аккаунта к выполнению тест-кейса'):
            box = random_en(8) + '@test.com'
            page = RegLogin(browser, link)
            with allure.step('Открыть стенд ЛКА'):
                page.open()
            with allure.step('Нажать "Регистрация"'):
                page.open_registration_page()
            with allure.step('Ввести в поле "Email" временную почту'):
                page.input_email(box)
            with allure.step('Ввести в поле "Фамилия" 8 символов'):
                page.input_last_name(random_en(8))
            with allure.step('Ввести в поле "Имя" 8 символов'):
                page.input_name(random_en(8))
            with allure.step('Ввести в поле "Пароль" 8 символов'):
                page.input_password(password)
            with allure.step('Повторить ранее введённые данные в поле "Пароль еще раз"'):
                page.input_password_confirmation(password)
            with allure.step('Поставить галочку "Даю согласие на обработку"'):
                page.put_a_tick_in_the_checkbox()
            with allure.step('Нажать "Зарегистрироваться"'):
                page.data_validation_check('Correct', 'Login')
            page = Profile(browser, link)
            with allure.step('Нажать линк "Профиль" в левом боковом меню'):
                page.open_profile()
            with allure.step('Нажать кнопку "Включить" в блоке Двухфакторная аутентификация'):
                page.click_select_button(3)
            with allure.step('Ввести корректный пароль от учётной записи'):
                page.input_password_modal_windows(password, 0)
            with allure.step('Нажать кнопку "Подтвердить"'):
                page.click_enter_button(2)
            with allure.step('Скопировать один код восстановления учетной записи'):
                page.copy_token_two_factor_authentication()
            with allure.step('Нажать кнопку "Покинуть учётную запись"'):
                page.exit_profile()
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(box)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Two-Factor-Challenge')
        with allure.step('Нажать "Использовать код восстановления"'):
            page.click_use_recovery_code()
        page = Profile(browser, link)
        with allure.step('Ввести ранее скопированый код востановления'):
            page.input_global_token()
        page = RegLogin(browser, link)
        with allure.step('Нажать кнопку "Вход"'):
            page.data_validation_check('Correct', 'Login')
        with allure.step('Нажать кнопку "Покинуть учётную запись"'):
            page = Profile(browser, link)
            page.exit_profile()
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(box)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Two-Factor-Challenge')
        with allure.step('Нажать "Использовать код восстановления"'):
            page.click_use_recovery_code()
        with allure.step('Ввести ранее использованный код востановления'):
            page = Profile(browser, link)
            page.input_global_token()
        with allure.step('Нажать кнопку "Вход"'):
            page = RegLogin(browser, link)
            page.data_validation_check('Incorrect', 'Login')

    @allure.story('Негативная проверка функциональности модуля')
    def testing_the_functionality_of_2FA_with_incorrect_password(self, browser):
        with allure.step('Пдготовка аккаунта к выполнению тест-кейса'):
            box = random_en(8) + '@test.com'
            page = RegLogin(browser, link)
            with allure.step('Открыть стенд ЛКА'):
                page.open()
            with allure.step('Нажать "Регистрация"'):
                page.open_registration_page()
            with allure.step('Ввести в поле "Email" временную почту'):
                page.input_email(box)
            with allure.step('Ввести в поле "Фамилия" 8 символов'):
                page.input_last_name(random_en(8))
            with allure.step('Ввести в поле "Имя" 8 символов'):
                page.input_name(random_en(8))
            with allure.step('Ввести в поле "Пароль" 8 символов'):
                page.input_password(password)
            with allure.step('Повторить ранее введённые данные в поле "Пароль еще раз"'):
                page.input_password_confirmation(password)
            with allure.step('Поставить галочку "Даю согласие на обработку"'):
                page.put_a_tick_in_the_checkbox()
            with allure.step('Нажать "Зарегистрироваться"'):
                page.data_validation_check('Correct', 'Login')
        page = Profile(browser, link)
        with allure.step('Нажать линк "Профиль" в левом боковом меню'):
            page.open_profile()
        with allure.step('Нажать кнопку "Включить" в блоке Двухфакторная аутентификация'):
            page.click_select_button(3)
        with allure.step('Ввести некорректный пароль от учётной записи'):
            page.input_password_modal_windows(random_en(8), 0)
        with allure.step('Нажать кнопку "Подтвердить"'):
            page.save_results_of_changes_password('Incorrect', 2)

    @allure.story('Позитивная проверка функциональности модуля')
    def testing_the_functionality_of_the_browsers_session_exit_with_the_correct_password(self, browser):
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Profile(browser, link)
        with allure.step('Нажать линк "Профиль" в левом боковом меню'):
            page.open_profile()
        with allure.step('Нажать кнопку "Выйти из других сессий браузеров"'):
            page.click_enter_button(3)
        with allure.step('Ввести корректный пароль от учётной записи'):
            page.input_password_modal_windows(password, 1)
        with allure.step('Нажать кнопку "Подтвердить"'):
            page.click_enter_button(4)
            page.checking_the_list_of_browsers()

    @allure.story('Негативная проверка функциональности модуля')
    def testing_the_functionality_of_the_browsers_session_exit_with_the_incorrect_password(self, browser):
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Profile(browser, link)
        with allure.step('Нажать линк "Профиль" в левом боковом меню'):
            page.open_profile()
        with allure.step('Нажать кнопку "Выйти из других сессий браузеров"'):
            page.click_enter_button(3)
        with allure.step('Ввести некорректный пароль от учётной записи'):
            page.input_password_modal_windows(random_en(8), 1)
        with allure.step('Нажать кнопку "Подтвердить"'):
            page.save_results_of_changes_password('Incorrect', 4)

    @allure.story('Позитивная проверка функциональности модуля')
    def testing_the_functionality_of_deleting_an_account_with_correct_password(self, browser):
        with allure.step('Пдготовка аккаунта к выполнению тест-кейса'):
            box = random_en(8) + '@test.com'
            page = RegLogin(browser, link)
            with allure.step('Открыть стенд ЛКА'):
                page.open()
            with allure.step('Нажать "Регистрация"'):
                page.open_registration_page()
            with allure.step('Ввести в поле "Email" временную почту'):
                page.input_email(box)
            with allure.step('Ввести в поле "Фамилия" 8 символов'):
                page.input_last_name(random_en(8))
            with allure.step('Ввести в поле "Имя" 8 символов'):
                page.input_name(random_en(8))
            with allure.step('Ввести в поле "Пароль" 8 символов'):
                page.input_password(password)
            with allure.step('Повторить ранее введённые данные в поле "Пароль еще раз"'):
                page.input_password_confirmation(password)
            with allure.step('Поставить галочку "Даю согласие на обработку"'):
                page.put_a_tick_in_the_checkbox()
            with allure.step('Нажать "Зарегистрироваться"'):
                page.data_validation_check('Correct', 'Login')
        page = Profile(browser, link)
        with allure.step('Нажать линк "Профиль" в левом боковом меню'):
            page.open_profile()
        with allure.step('Нажать на кнопку "Удалить учетную запись"'):
            page.delete_profile_account('Skip', 0)
        with allure.step('Ввести корректный пароль от учётной записи'):
            page.input_password_modal_windows(password, 2)
        with allure.step('Нажать кнопку "Удалить учетную запись"'):
            page.delete_profile_account('Correct', 1)
        page = RegLogin(browser, link)
        with allure.step('Ввести в поле "E-mail" (Precondition)'):
            page.input_email(box)
        with allure.step('Ввести в поле "Пароль" новый пароль'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Incorrect', 'Login')

    @allure.story('Негативная проверка функциональности модуля')
    def testing_the_functionality_of_deleting_an_account_with_incorrect_password(self, browser):
        with allure.step('Пдготовка аккаунта к выполнению тест-кейса'):
            box = random_en(8) + '@test.com'
            page = RegLogin(browser, link)
            with allure.step('Открыть стенд ЛКА'):
                page.open()
            with allure.step('Нажать "Регистрация"'):
                page.open_registration_page()
            with allure.step('Ввести в поле "Email" временную почту'):
                page.input_email(box)
            with allure.step('Ввести в поле "Фамилия" 8 символов'):
                page.input_last_name(random_en(8))
            with allure.step('Ввести в поле "Имя" 8 символов'):
                page.input_name(random_en(8))
            with allure.step('Ввести в поле "Пароль" 8 символов'):
                page.input_password(password)
            with allure.step('Повторить ранее введённые данные в поле "Пароль еще раз"'):
                page.input_password_confirmation(password)
            with allure.step('Поставить галочку "Даю согласие на обработку"'):
                page.put_a_tick_in_the_checkbox()
            with allure.step('Нажать "Зарегистрироваться"'):
                page.data_validation_check('Correct', 'Login')
        page = Profile(browser, link)
        with allure.step('Нажать линк "Профиль" в левом боковом меню'):
            page.open_profile()
        with allure.step('Нажать на кнопку "Удалить учетную запись"'):
            page.delete_profile_account('Skip', 0)
        with allure.step('Ввести некорректный пароль от учётной записи'):
            page.input_password_modal_windows(random_en(8), 2)
        with allure.step('Нажать кнопку "Удалить учетную запись"'):
            page.delete_profile_account('Incorrect', 1)
