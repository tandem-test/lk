import pytest
import allure

from .pages.services_page import Services
from .pages.precondition import link, password, admin_login, random_en, random_number
from .pages.authentication_page import RegLogin


@pytest.mark.eservices
@pytest.mark.skip(reason='Тесты не доработаны до актуальной сборки.')
@allure.link(link + '/services/requests', name='ТАНДЕМ Личный кабинет')
@allure.epic('Функциональное тестирование панели управления')
@allure.feature('Проверка функциональности модуля "Заявки"')
class TestService_Applications:
    def test_test(self, browser):
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)


@pytest.mark.eservices
@pytest.mark.skip(reason='Тесты не доработаны до актуальной сборки.')
@allure.link(link + '/services/categories', name='ТАНДЕМ Личный кабинет')
@allure.epic('Функциональное тестирование панели управления')
@allure.feature('Проверка функциональности модуля "Категории"')
class TestService_Categories:
    @allure.story('Позитивная проверка функциональности модуля')
    @pytest.mark.smoke
    @allure.severity(allure.severity_level.CRITICAL)
    def testing_the_functionality_of_creating_a_service_category_with_valid_data(self, browser):
        title = random_en(8)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Категории"'):
            page.open_window_service('Категории')
        with allure.step('Нажать кнопку "Новая категория"'):
            page.button_create_a_new_category()
        with allure.step('Ввести в поле "Название" 8 символов'):
            page.input_form_modal(0, title)
        with allure.step('Ввести в поле "Сокращенное название" 4 символов'):
            page.input_form_modal(1, random_en(4))
        with allure.step('Ввести в поле "Код услуги" 4 символов'):
            page.input_form_modal(2, random_en(4))
        with allure.step('Ввести в поле "Приоритет" 4 цифры'):
            page.input_form_modal(3, '1' + random_number(3))
        with allure.step('Нажать кнопку "Добавить"'):
            page.save_results_of_changes(0, 'Correct', title, '')
        with allure.step('Удалить ранее созданную категорию'):
            page.remove_form(title, 'Category', 'Correct')

    @allure.story('Позитивная проверка функциональности модуля')
    def testing_the_functionality_of_creating_a_service_category_by_filling_in_only_required_fields(self, browser):
        title = random_en(8)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Категории"'):
            page.open_window_service('Категории')
        with allure.step('Нажать кнопку "Новая категория"'):
            page.button_create_a_new_category()
        with allure.step('Ввести в поле "Название" 8 символов'):
            page.input_form_modal(0, title)
        with allure.step('Ввести в поле "Код услуги" 4 символов'):
            page.input_form_modal(2, random_en(4))
        with allure.step('Ввести в поле "Приоритет" 4 цифры'):
            page.input_form_modal(3, '1' + random_number(3))
        with allure.step('Нажать кнопку "Добавить"'):
            page.save_results_of_changes(0, 'Correct', title, '')
        with allure.step('Удалить ранее созданную категорию'):
            page.remove_form(title, 'Category', 'Correct')

    @pytest.mark.xfail(reason='Ошибка функциональности поля "Приоритет", вкладка "Категории", при вводе предельного '
                              'значения')
    @allure.story('Позитивная проверка функциональности модуля')
    def testing_the_functionality_of_creating_a_service_category_with_limit_values(self, browser):
        title = random_en(255)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Категории"'):
            page.open_window_service('Категории')
        with allure.step('Нажать кнопку "Новая категория"'):
            page.button_create_a_new_category()
        with allure.step('Ввести в поле "Название" 255 символов'):
            page.input_form_modal(0, title)
        with allure.step('Ввести в поле "Сокращенное название" 255 символов'):
            page.input_form_modal(1, random_en(255))
        with allure.step('Ввести в поле "Код услуги" 255 символов'):
            page.input_form_modal(2, random_en(255))
        with allure.step('Ввести в поле "Приоритет" 19 цифр'):
            page.input_form_modal(3, '1' + random_number(18))
        with allure.step('Нажать кнопку "Добавить"'):
            page.save_results_of_changes(0, 'Correct', title, '')
        with allure.step('Удалить ранее созданную категорию'):
            page.remove_form(title, 'Category', 'Correct')

    @allure.story('Позитивная проверка функциональности модуля')
    def testing_the_functionality_of_sending_and_restoring_a_service_category(self, browser):
        title = random_en(8)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Категории"'):
            page.open_window_service('Категории')
        with allure.step('Подготовка к выполнению тест-кейса'):
            with allure.step('Нажать кнопку "Новая категория"'):
                page.button_create_a_new_category()
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(0, title)
            with allure.step('Ввести в поле "Код услуги" 4 символов'):
                page.input_form_modal(2, random_en(4))
            with allure.step('Ввести в поле "Приоритет" 4 цифры'):
                page.input_form_modal(3, '1' + random_number(3))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'Correct', title, '')
        with allure.step('Отправить в архив ранее созданную категорию'):
            page.archive_form('Category', 'Send', 2)
        with allure.step('Восстановить ранее созданную категорию'):
            page.archive_form('Category', 'Restore', 3)
        with allure.step('Удалить ранее созданную категорию'):
            page.remove_form(title, 'Category', 'Correct')

    @allure.story('Негативная проверка функциональности модуля')
    def testing_the_functionality_of_creating_a_service_category_with_out_of_range_values(self, browser):
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Категории"'):
            page.open_window_service('Категории')
        with allure.step('Нажать кнопку "Новая категория"'):
            page.button_create_a_new_category()
        with allure.step('Подготовка к выполнению тест-кейса'):
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(0, random_en(8))
            with allure.step('Ввести в поле "Код услуги" 4 символов'):
                page.input_form_modal(2, random_en(4))
            with allure.step('Ввести в поле "Приоритет" 4 цифры'):
                page.input_form_modal(3, '1' + random_number(3))
        with allure.step('Проверка валидации поля "Название" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Название" 256 символов'):
                page.input_form_modal(0, random_en(256))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'Incorrect', '', '«название» не может превышать 255')
            with allure.step('Вернуть в исходное состояние поле "Название"'):
                page.input_form_modal(0, random_en(8))
        with allure.step('Проверка валидации поля "Сокращенное название" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Сокращенное название" 256 символов'):
                page.input_form_modal(1, random_en(256))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'Incorrect', '', '«сокращенное название» не может превышать 255')
            with allure.step('Вернуть в исходное состояние поле "Сокращенное название"'):
                page.input_form_modal(1, random_en(4))
        with allure.step('Проверка валидации поля "Код услуги" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Код услуги" 256 символов'):
                page.input_form_modal(2, random_en(256))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'Incorrect', '', '«код» не может превышать 255')
            with allure.step('Вернуть в исходное состояние поле "Код услуги"'):
                page.input_form_modal(2, random_en(4))
        with allure.step('Проверка валидации поля "Приоритет" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Приоритет" 256 цифр'):
                page.input_form_modal(3, random_number(256))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'Incorrect', '', '«приоритет» должно быть целым числом')
            with allure.step('Вернуть в исходное состояние поле "Приоритет"'):
                page.input_form_modal(3, random_en(4))

    @allure.story('Негативная проверка функциональности модуля')
    def testing_the_functionality_of_creating_a_service_category_without_filling_in_the_required_field(self, browser):
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Категории"'):
            page.open_window_service('Категории')
        with allure.step('Нажать кнопку "Новая категория"'):
            page.button_create_a_new_category()
        with allure.step('Подготовка к выполнению тест-кейса'):
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(0, random_en(8))
            with allure.step('Ввести в поле "Код услуги" 4 символов'):
                page.input_form_modal(2, random_en(4))
            with allure.step('Ввести в поле "Приоритет" 4 цифры'):
                page.input_form_modal(3, '1' + random_number(3))
        with allure.step('Проверка валидации обязательного поля "Название"'):
            with allure.step('Оставить поле "Название" пустым'):
                page.input_form_modal(0, ' ')
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'Incorrect', '', ' «название» обязательно')
            with allure.step('Вернуть в исходное состояние поле "Название"'):
                page.input_form_modal(0, random_en(8))
        with allure.step('Проверка валидации обязательного поля "Код услуги"'):
            with allure.step('Оставить поле "Код услуги" пустым'):
                page.input_form_modal(2, ' ')
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'Incorrect', '', ' «код» обязательно')
            with allure.step('Вернуть в исходное состояние поле "Код услуги"'):
                page.input_form_modal(2, random_en(4))
        with allure.step('Проверка валидации обязательного поля "Приоритет"'):
            with allure.step('Оставить поле "Приоритет" пустым'):
                page.input_form_modal(3, ' ')
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'Incorrect', '', ' «приоритет» обязательно')
            with allure.step('Вернуть в исходное состояние поле "Приоритет"'):
                page.input_form_modal(3, '1' + random_en(4))

    @allure.story('Негативная проверка функциональности модуля')
    def testing_the_functionality_of_creating_a_service_category_with_the_existing_code(self, browser):
        title = random_en(8)
        code = random_en(4)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        with allure.step('Подготовка к выполнению тест-кейса'):
            page = Services(browser, link)
            with allure.step('Открыть вкладку "Категории"'):
                page.open_window_service('Категории')
            with allure.step('Нажать кнопку "Новая категория"'):
                page.button_create_a_new_category()
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(0, title)
            with allure.step('Ввести в поле "Код услуги" 4 символов'):
                page.input_form_modal(2, code)
            with allure.step('Ввести в поле "Приоритет" 4 цифры'):
                page.input_form_modal(3, '1' + random_number(3))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'Correct', title, '')
        with allure.step('Открыть вкладку "Категории"'):
            page.open_window_service('Категории')
        with allure.step('Нажать кнопку "Новая категория"'):
            page.button_create_a_new_category()
        with allure.step('Ввести в поле "Название" 8 символов'):
            page.input_form_modal(0, title)
        with allure.step('Ввести в поле "Код услуги" 4 символов'):
            page.input_form_modal(2, code)
        with allure.step('Ввести в поле "Приоритет" 4 цифры'):
            page.input_form_modal(3, '1' + random_number(3))
        with allure.step('Нажать кнопку "Добавить"'):
            page.save_results_of_changes(0, 'Incorrect', '', '«код» уже существует')
        with allure.step('Нажать кнопку "Отмена"'):
            page.cancel_modal(0)
        with allure.step('Удалить ранее созданную категорию'):
            page.remove_form(title, 'Category', 'Correct')

    @pytest.mark.skip(reason='Ошибка функциональности валидатора "Пиктограмма", вкладка "Категории", при загрузке '
                             'файла с невалидным названием')
    @allure.story('Негативная проверка функциональности модуля')
    def testing_the_functionality_of_the_icon_loading_validator_with_invalid_formats(self, browser):
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Категории"'):
            page.open_window_service('Категории')
        with allure.step('Нажать кнопку "Новая категория"'):
            page.button_create_a_new_category()
        with allure.step('Подготовка к выполнению тест-кейса'):
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(0, random_en(8))
            with allure.step('Ввести в поле "Код услуги" 4 символов'):
                page.input_form_modal(2, random_en(4))
            with allure.step('Ввести в поле "Приоритет" 4 цифры'):
                page.input_form_modal(3, '1' + random_number(3))
        with allure.step('Проверка валидации поля "Пиктограмма" на загрузку файла *.pdf'):
            with allure.step('Загрузить пиктограмму в формате "pdf"'):
                page.load_file('files/doc/doc.pdf')
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_pictogram(0, 'Incorrect', '.pdf', '', 'no')
        with allure.step('Проверка валидации поля "Пиктограмма" на загрузку файла *.txt'):
            with allure.step('Загрузить пиктограмму в формате "txt"'):
                page.load_file('files/doc/doc.txt')
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_pictogram(0, 'Incorrect', '.txt', '', 'no')
        with allure.step('Проверка валидации поля "Пиктограмма" на загрузку файла *.jpeg с длинным названием'):
            with allure.step('Загрузить пиктограмму в формате "jpeg" с длинным названием'):
                page.load_file('files/images/photo — A file for checking the download with a long name, testing is '
                               'good, and even better, when there are not so many bugs ааыфвфывфывфывфы.jpeg')
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_pictogram(0, 'Incorrect', '.jpeg', '', 'no')
        with allure.step('Проверка валидации поля "Пиктограмма" на загрузку файла *.docx'):
            with allure.step('Загрузить пиктограмму в формате "docx"'):
                page.load_file('files/doc/doc.docx')
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_pictogram(0, 'Incorrect', '.docx', '', 'no')

    @allure.story('Позитивная проверка функциональности модуля')
    def testing_the_functionality_of_the_icon_loading_validator_with_invalid_formats(self, browser):
        title = random_en(8)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Категории"'):
            page.open_window_service('Категории')
        with allure.step('Нажать кнопку "Новая категория"'):
            page.button_create_a_new_category()
        with allure.step('Подготовка к выполнению тест-кейса'):
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(0, title)
            with allure.step('Ввести в поле "Код услуги" 4 символов'):
                page.input_form_modal(2, random_en(4))
            with allure.step('Ввести в поле "Приоритет" 4 цифры'):
                page.input_form_modal(3, '1' + random_number(3))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'Correct', title, '')
        with allure.step('Проверка валидации поля "Пиктограмма" на загрузку файла *.bmp'):
            with allure.step('Нажать кнопку "Редактировать" в ранее созданной форме'):
                page.edit_form('')
            with allure.step('Загрузить пиктограмму в формате *.bmp'):
                page.load_file('files/images/photo.bmp')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.save_results_of_pictogram(1, 'Correct', '.bmp', title, 'yes')
        with allure.step('Проверка валидации поля "Пиктограмма" на загрузку файла *.gif'):
            with allure.step('Нажать кнопку "Редактировать" в ранее созданной форме'):
                page.edit_form('')
            with allure.step('Загрузить пиктограмму в формате *.gif'):
                page.load_file('files/images/photo.gif')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.save_results_of_pictogram(1, 'Correct', '.gif', title, 'yes')
        with allure.step('Проверка валидации поля "Пиктограмма" на загрузку файла *.jpeg'):
            with allure.step('Нажать кнопку "Редактировать" в ранее созданной форме'):
                page.edit_form('')
            with allure.step('Загрузить пиктограмму в формате *.jpeg'):
                page.load_file('files/images/photo.jpeg')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.save_results_of_pictogram(1, 'Correct', '.jpeg', title, 'yes')
        with allure.step('Проверка валидации поля "Пиктограмма" на загрузку файла *.jpg'):
            with allure.step('Нажать кнопку "Редактировать" в ранее созданной форме'):
                page.edit_form('')
            with allure.step('Загрузить пиктограмму в формате *.jpg'):
                page.load_file('files/images/photo.jpg')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.save_results_of_pictogram(1, 'Correct', '.jpg', title, 'yes')
        with allure.step('Проверка валидации поля "Пиктограмма" на загрузку файла *.png'):
            with allure.step('Нажать кнопку "Редактировать" в ранее созданной форме'):
                page.edit_form('')
            with allure.step('Загрузить пиктограмму в формате *.png'):
                page.load_file('files/images/photo.png')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.save_results_of_pictogram(1, 'Correct', '.png', title, 'yes')
        with allure.step('Проверка валидации поля "Пиктограмма" на загрузку файла *.webp'):
            with allure.step('Нажать кнопку "Редактировать" в ранее созданной форме'):
                page.edit_form('')
            with allure.step('Загрузить пиктограмму в формате *.webp'):
                page.load_file('files/images/photo.webp')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.save_results_of_pictogram(1, 'Correct', '.webp', title, 'yes')
        with allure.step('Удалить ранее созданную категорию'):
            page.remove_form(title, 'Category', 'Correct')

    @pytest.mark.skip(reason='Отсутствует функциональность удаления пиктограммы при редактировании')
    @allure.story('Позитивная проверка функциональности модуля')
    def testing_the_editing_functionality_of_a_previously_created_service_category(self, browser):
        title = random_en(8)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Категории"'):
            page.open_window_service('Категории')
        with allure.step('Подготовка к выполнению тест-кейса'):
            with allure.step('Нажать кнопку "Новая категория"'):
                page.button_create_a_new_category()
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(0, title)
            with allure.step('Ввести в поле "Код услуги" 4 символов'):
                page.input_form_modal(2, random_en(4))
            with allure.step('Ввести в поле "Приоритет" 4 цифры'):
                page.input_form_modal(3, '1' + random_number(3))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'Correct', title, '')
        with allure.step('Нажать кнопку "Редактировать" в ранее созданной форме'):
            page.edit_form('')
        with allure.step('Ввести в поле "Название" 8 символов'):
            page.input_form_modal(5, title)
        with allure.step('Ввести в поле "Код услуги" 4 символов'):
            page.input_form_modal(7, random_en(4))
        with allure.step('Ввести в поле "Приоритет" 4 цифры'):
            page.input_form_modal(8, '1' + random_number(3))
        with allure.step('Нажать кнопку "Сохранить"'):
            page.save_results_of_pictogram(1, 'Correct', '.jpg', title, 'yes')
        with allure.step('Удалить ранее созданную категорию'):
            page.remove_form(title, 'Category', 'Correct')

    @allure.story('Интеграционное тестирование модуля')
    def testing_deleting_a_category_with_a_bound_service(self, browser):
        title = random_en(8)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        with allure.step('Подготовка к выполнению тест-кейса'):
            page = Services(browser, link)
            with allure.step('Открыть вкладку "Категории"'):
                page.open_window_service('Категории')
            with allure.step('Нажать кнопку "Новая категория"'):
                page.button_create_a_new_category()
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(0, title)
            with allure.step('Ввести в поле "Код услуги" 4 символов'):
                page.input_form_modal(2, random_en(4))
            with allure.step('Ввести в поле "Приоритет" 4 цифры'):
                page.input_form_modal(3, '1' + random_number(3))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'Correct', title, '')
        with allure.step('Открыть вкладку "Услуги"'):
            page.open_window_service('Услуги')
        with allure.step('Нажать кнопку "Новая услуга"'):
            page.button_create_a_new_category()
        with allure.step('Ввести в поле "Название" 8 символов'):
            page.input_form_modal(0, title)
        with allure.step('Ввести в поле "Описание" 8 символов'):
            page.input_description(random_en(8))
        with allure.step('Выбрать доступную категорию в поле "Категория"'):
            page.select_category(title)
        with allure.step('Нажать кнопку "Сохранить"'):
            page.save_results_of_changes(0, 'CorrectSer', title, '')
        with allure.step('Открыть вкладку "Категории"'):
            page.open_window_service('Категории')
        with allure.step('Удалить ранее созданную категорию'):
            page.remove_form(title, 'Category', 'Incorrect')
        with allure.step('Возвращение в исходное состояние'):
            with allure.step('Открыть вкладку "Услуги"'):
                page.open_window_service('Услуги')
            with allure.step('Удалить ранее созданную услугу'):
                page.remove_form(title, 'Service', 'Correct')
            with allure.step('Открыть вкладку "Категории"'):
                page.open_window_service('Категории')
            with allure.step('Удалить ранее созданную категорию'):
                page.remove_form(title, 'Category', 'Correct')


@pytest.mark.skip(reason='Тесты не доработаны до актуальной сборки.')
@pytest.mark.eservices
@allure.link(link + '/services/services', name='ТАНДЕМ Личный кабинет')
@allure.epic('Функциональное тестирование панели управления')
@allure.feature('Проверка функциональности модуля "Услуги"')
class TestService_Services:
    @pytest.mark.smoke
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.story('Позитивная проверка функциональности модуля')
    def testing_the_functionality_of_creating_a_service_with_valid_data(self, browser):
        title = random_en(8)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Услуги"'):
            page.open_window_service('Услуги')
        with allure.step('Нажать кнопку "Новая услуга"'):
            page.button_create_a_new_category()
        with allure.step('Ввести в поле "Название" 8 символов'):
            page.input_form_modal(0, title)
        with allure.step('Ввести в поле "Описание" 8 символов'):
            page.input_description(random_en(8))
        with allure.step('Выбрать доступную категорию в поле "Категория"'):
            page.select_category('Социальное обеспечение')
        with allure.step('Нажать кнопку "Сохранить"'):
            page.save_results_of_changes(0, 'CorrectSer', title, '')
        with allure.step('Удалить ранее созданную услугу'):
            page.remove_form(title, 'Service', 'Correct')

    @allure.story('Позитивная проверка функциональности модуля')
    def testing_the_functionality_of_creating_a_service_with_limit_values(self, browser):
        title = random_en(255)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Услуги"'):
            page.open_window_service('Услуги')
        with allure.step('Нажать кнопку "Новая услуга"'):
            page.button_create_a_new_category()
        with allure.step('Ввести в поле "Название" 255 символов'):
            page.input_form_modal(0, title)
        with allure.step('Ввести в поле "Описание" 255 символов'):
            page.input_description(random_en(255))
        with allure.step('Выбрать доступную категорию в поле "Категория"'):
            page.select_category('Социальное обеспечение')
        with allure.step('Нажать кнопку "Сохранить"'):
            page.save_results_of_changes(0, 'CorrectSer', title, '')
        with allure.step('Удалить ранее созданную услугу'):
            page.remove_form(title, 'Service', 'Correct')

    @allure.story('Позитивная проверка функциональности модуля')
    def testing_the_functionality_of_sending_and_restoring_a_service(self, browser):
        title = random_en(8)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Услуги"'):
            page.open_window_service('Услуги')
        with allure.step('Подготовка к выполнению тест-кейса'):
            with allure.step('Нажать кнопку "Новая услуга"'):
                page.button_create_a_new_category()
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(0, title)
            with allure.step('Ввести в поле "Описание" 8 символов'):
                page.input_description(random_en(8))
            with allure.step('Выбрать доступную категорию в поле "Категория"'):
                page.select_category('Социальное обеспечение')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.save_results_of_changes(0, 'CorrectSer', title, '')
        with allure.step('Отправить в архив ранее созданную категорию'):
            page.archive_form('Service', 'Send', 1)
        with allure.step('Восстановить ранее созданную категорию'):
            page.archive_form('Service', 'Restore', 2)
        with allure.step('Удалить ранее созданную услугу'):
            page.remove_form(title, 'Service', 'Correct')

    @pytest.mark.skip(reason='Отсутствует валидатор предельного значения в поле "Описание", вкладка "Услуги"')
    @allure.story('Негативная проверка функциональности модуля')
    def testing_the_functionality_of_creating_a_service_with_out_of_range_values(self, browser):
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Услуги"'):
            page.open_window_service('Услуги')
        with allure.step('Нажать кнопку "Новая услуга"'):
            page.button_create_a_new_category()
        with allure.step('Подготовка к выполнению тест-кейса'):
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(0, random_en(8))
            with allure.step('Ввести в поле "Описание" 8 символов'):
                page.input_description(random_en(8))
        with allure.step('Проверка валидации поля "Название" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Название" 256 символов'):
                page.input_form_modal(0, random_en(256))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'Incorrect', '', '«название» не может превышать 255')
            with allure.step('Вернуть в исходное состояние поле "Сокращенное название"'):
                page.input_form_modal(0, random_en(8))
        # with allure.step('Проверка валидации поля "Описание" на ввод запредельных значений'):
        #     with allure.step('Ввести в поле "Описание" 45 000 символов'):
        #         page.input_description(random_char(45000))
        #     with allure.step('Нажать кнопку "Добавить"'):
        #         page.save_results_of_changes(0, 'Incorrect', '', '«описание» не может превышать 255')
        #     with allure.step('Вернуть в исходное состояние поле "Сокращенное название"'):
        #         page.input_description(random_char(8))

    @allure.story('Негативная проверка функциональности модуля')
    def testing_the_functionality_of_creating_a_service_without_filling_in_the_required_field(self, browser):
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Услуги"'):
            page.open_window_service('Услуги')
        with allure.step('Нажать кнопку "Новая услуга"'):
            page.button_create_a_new_category()
        with allure.step('Подготовка к выполнению тест-кейса'):
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(0, random_en(8))
            with allure.step('Ввести в поле "Описание" 8 символов'):
                page.input_description(random_en(8))
        with allure.step('Проверка валидации обязательного поля "Название"'):
            with allure.step('Оставить поле "Название" пустым'):
                page.input_form_modal(0, ' ')
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'Incorrect', '', ' «название» обязательно')
            with allure.step('Вернуть в исходное состояние поле "Название"'):
                page.input_form_modal(0, random_en(8))
        with allure.step('Проверка валидации обязательного поля "Описание"'):
            with allure.step('Оставить поле "Описание" пустым'):
                page.input_description('')
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'Incorrect', '', ' «описание» обязательно')
            with allure.step('Вернуть в исходное состояние поле "Описание"'):
                page.input_description(random_en(8))

    @allure.story('Позитивная проверка функциональности модуля')
    def testing_the_editing_functionality_of_a_previously_created_service(self, browser):
        title = random_en(8)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Услуги"'):
            page.open_window_service('Услуги')
        with allure.step('Нажать кнопку "Новая услуга"'):
            page.button_create_a_new_category()
        with allure.step('Подготовка к выполнению тест-кейса'):
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(0, title)
            with allure.step('Ввести в поле "Описание" 8 символов'):
                page.input_description(random_en(8))
            with allure.step('Нажать кнопку "Сохранить"'):
                page.save_results_of_changes(0, 'CorrectSer', title, '')
        with allure.step('Нажать кнопку "Редактировать" в ранее созданной форме'):
            page.edit_form('')
        with allure.step('Ввести в поле "Название" 8 символов'):
            page.input_form_modal(0, title)
        with allure.step('Ввести в поле "Описание" 8 символов'):
            page.input_description(random_en(8))
        with allure.step('Нажать кнопку "Сохранить"'):
            page.save_results_of_changes(0, 'CorrectSer', title, '')
        with allure.step('Удалить ранее созданную услугу'):
            page.remove_form(title, 'Service', 'Correct')

    @allure.story('Интеграционное тестирование модуля')
    def testing_the_deletion_of_a_service_used_in_implementation(self, browser):
        title = random_en(8)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        with allure.step('Подготовка к выполнению тест-кейса'):
            page = Services(browser, link)
            with allure.step('Открыть вкладку "Услуги"'):
                page.open_window_service('Услуги')
            with allure.step('Нажать кнопку "Новая услуга"'):
                page.button_create_a_new_category()
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(0, title)
            with allure.step('Ввести в поле "Описание" 8 символов'):
                page.input_description(random_en(8))
            with allure.step('Выбрать доступную категорию в поле "Категория"'):
                page.select_category('Социальное обеспечение')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.save_results_of_changes(0, 'CorrectSer', title, '')
            with allure.step('Открыть вкладку "Реализации"'):
                page.open_window_service('Реализации')
            with allure.step('Нажать кнопку "Новая реализация"'):
                page.button_create_a_new_realization()
            with allure.step('Ввести в поле "Описание" 8 символов'):
                page.input_description_1(random_en(8))
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_title(title)
            with allure.step('Выбрать в поле "Услуга" доступное значение'):
                page.select_service(title)
            with allure.step('Нажать кнопку "Создать"'):
                page.check_adding_implementation('Correct', title, '')
        with allure.step('Открыть вкладку "Услуги"'):
            page.open_window_service('Услуги')
        with allure.step('Удалить ранее созданную услугу'):
            page.remove_form(title, 'Service', 'Incorrect')
        with allure.step('Возвращение в исходное состояние'):
            with allure.step('Открыть вкладку "Реализации"'):
                page.open_window_service('Реализации')
            with allure.step('Удалить ранее созданную реализацию'):
                page.remove_form(title, 'Realization', 'Correct')
            with allure.step('Открыть вкладку "Услуги"'):
                page.open_window_service('Услуги')
            with allure.step('Удалить ранее созданную услугу'):
                page.remove_form(title, 'Service', 'Correct')


@pytest.mark.skip(reason='Тесты не доработаны до актуальной сборки.')
@pytest.mark.eservices
@allure.link(link + '/services/implementations', name='ТАНДЕМ Личный кабинет')
@allure.epic('Функциональное тестирование панели управления')
@allure.feature('Проверка функциональности модуля "Реализации"')
class TestService_Implementation:
    @pytest.mark.smoke
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.story('Позитивная проверка функциональности модуля')
    def testing_the_functionality_of_creating_a_implementation_with_valid_data(self, browser):
        title = random_en(8)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Реализации"'):
            page.open_window_service('Реализации')
        with allure.step('Нажать кнопку "Новая реализация"'):
            page.button_create_a_new_realization()
        with allure.step('Ввести в поле "Описание" 8 символов'):
            page.input_description_1(random_en(8))
        with allure.step('Ввести в поле "Название" 8 символов'):
            page.input_title(title)
        with allure.step('Выбрать в поле "Услуга" доступное значение'):
            page.select_service('Материальная поддержка')
        with allure.step('Выбрать в поле "Форма" доступное значение'):
            page.select_form('Стандартная форма')
        with allure.step('Выбрать в поле "Тип обработки" доступное значение'):
            page.select_process_type('В личном кабинете')
        with allure.step('Выбрать в поле "Ответственное подразделение" доступное значение'):
            page.select_responsible_department('')
        with allure.step('Выбрать в поле "Место получения" доступное значение'):
            page.select_place_of_receipt('')
        with allure.step('Включить тумблер "Приложен документ"'):
            page.input_checkbox('Document')
        with allure.step('Нажать кнопку "Создать"'):
            page.check_adding_implementation('Correct', title, '')
        with allure.step('Удалить ранее созданную реализацию'):
            page.remove_form(title, 'Realization', 'Correct')

    @allure.story('Позитивная проверка функциональности модуля')
    def testing_the_functionality_of_creating_a_implementation_by_filling_in_only_required_fields(self, browser):
        select_service = 'Материальная поддержка'
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Реализации"'):
            page.open_window_service('Реализации')
        with allure.step('Нажать кнопку "Новая реализация"'):
            page.button_create_a_new_realization()
        with allure.step('Ввести в поле "Описание" 8 символов'):
            page.input_description_1(random_en(8))
        with allure.step('Выбрать в поле "Услуга" доступное значение'):
            page.select_service(select_service)
        with allure.step('Нажать кнопку "Создать"'):
            page.check_adding_implementation('Correct', select_service, '')
        with allure.step('Удалить ранее созданную реализацию'):
            page.remove_form(select_service, 'Realization', 'Correct')

    @allure.story('Позитивная проверка функциональности модуля')
    def testing_the_functionality_creating_a_implementation_with_limit_values(self, browser):
        title = random_en(255)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Реализации"'):
            page.open_window_service('Реализации')
        with allure.step('Нажать кнопку "Новая реализация"'):
            page.button_create_a_new_realization()
        with allure.step('Ввести в поле "Описание" 255 символов'):
            page.input_description_1(random_en(255))
        with allure.step('Ввести в поле "Название" 255 символов'):
            page.input_title(title)
        with allure.step('Выбрать в поле "Услуга" доступное значение'):
            page.select_service('Материальная поддержка')
        with allure.step('Выбрать в поле "Форма" доступное значение'):
            page.select_form('Стандартная форма')
        with allure.step('Выбрать в поле "Тип обработки" доступное значение'):
            page.select_process_type('В личном кабинете')
        with allure.step('Выбрать в поле "Ответственное подразделение" доступное значение'):
            page.select_responsible_department('')
        with allure.step('Выбрать в поле "Место получения" доступное значение'):
            page.select_place_of_receipt('')
        with allure.step('Включить тумблер "Приложен документ"'):
            page.input_checkbox('Document')
        with allure.step('Нажать кнопку "Создать"'):
            page.check_adding_implementation('Correct', title, '')
        with allure.step('Удалить ранее созданную реализацию'):
            page.remove_form(title, 'Realization', 'Correct')

    @allure.story('Позитивная проверка функциональности модуля')
    def testing_the_functionality_of_sending_and_restoring_a_implementation(self, browser):
        title = random_en(8)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Реализации"'):
            page.open_window_service('Реализации')
        with allure.step('Подготовка к выполнению тест-кейса'):
            with allure.step('Нажать кнопку "Новая реализация"'):
                page.button_create_a_new_realization()
            with allure.step('Ввести в поле "Описание" 8 символов'):
                page.input_description_1(random_en(8))
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_title(title)
            with allure.step('Нажать кнопку "Создать"'):
                page.check_adding_implementation('Correct', title, '')
        with allure.step('Отправить в архив ранее созданную категорию'):
            page.archive_form('Realization', 'Send', 0)
        with allure.step('Восстановить ранее созданную категорию'):
            page.archive_form('Realization', 'Restore', 1)
        with allure.step('Удалить ранее созданную реализацию'):
            page.remove_form(title, 'Realization', 'Correct')

    @pytest.mark.skip(reason='Отсутствует валидатор предельного значения в поле "Описание", вкладка "Услуги"')
    @allure.story('Негативная проверка функциональности модуля')
    def testing_the_functionality_creating_a_implementation_with_out_of_range_values(self, browser):
        title = random_en(255)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Реализации"'):
            page.open_window_service('Реализации')
        with allure.step('Нажать кнопку "Новая реализация"'):
            page.button_create_a_new_realization()
        with allure.step('Подготовка к выполнению тест-кейса'):
            with allure.step('Ввести в поле "Описание" 8 символов'):
                page.input_description_1(random_en(8))
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_title(title)
        # with allure.step('Проверка валидации поля "Описание" на ввод запредельных значений'):
        #     with allure.step('Ввести в поле "Описание" 45 000 символов'):
        #         page.input_description_1(random_char(45000))
        #     with allure.step('Нажать кнопку "Добавить"'):
        #         page.check_adding_implementation('Incorrect', '', '«описание» не может превышать 255')
        #     with allure.step('Вернуть в исходное состояние поле "Сокращенное название"'):
        #         page.input_description_1(random_char(8))
        with allure.step('Проверка валидации поля "Название" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Название" 256 символов'):
                page.input_title(random_en(256))
            with allure.step('Нажать кнопку "Добавить"'):
                page.check_adding_implementation('Incorrect', '', '«название» не может превышать 255')
            with allure.step('Вернуть в исходное состояние поле "Сокращенное название"'):
                page.input_title(random_en(8))

    @allure.story('Негативная проверка функциональности модуля')
    def testing_the_functionality_creating_a_implementation_without_filling_in_the_required_field(self, browser):
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Реализации"'):
            page.open_window_service('Реализации')
        with allure.step('Нажать кнопку "Новая реализация"'):
            page.button_create_a_new_realization()
        with allure.step('Подготовка к выполнению тест-кейса'):
            with allure.step('Ввести в поле "Описание" 8 символов'):
                page.input_description_1(random_en(8))
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_title(random_en(8))
        with allure.step('Проверка валидации обязательного поля "Описание"'):
            with allure.step('Оставить поле "Описание" пустым'):
                page.input_description_1('')
            with allure.step('Нажать кнопку "Добавить"'):
                page.check_adding_implementation('Incorrect', '', '«описание» обязательно')
            with allure.step('Вернуть в исходное состояние поле "Описание"'):
                page.input_description_1(random_en(8))

    @allure.story('Позитивная проверка функциональности модуля')
    def testing_the_editing_functionality_of_a_previously_created_implementation(self, browser):
        title = random_en(8)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Реализации"'):
            page.open_window_service('Реализации')
        with allure.step('Нажать кнопку "Новая реализация"'):
            page.button_create_a_new_realization()
        with allure.step('Подготовка к выполнению тест-кейса'):
            with allure.step('Ввести в поле "Описание" 8 символов'):
                page.input_description_1(random_en(8))
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_title(title)
            with allure.step('Нажать кнопку "Создать"'):
                page.check_adding_implementation('Correct', title, '')
        with allure.step('Нажать кнопку "Редактировать" в ранее созданной форме'):
            page.edit_form('Realization')
        with allure.step('Ввести в поле "Описание" 8 символов'):
            page.input_description_1(random_en(8))
        with allure.step('Ввести в поле "Название" 8 символов'):
            page.input_title(title)
        with allure.step('Нажать кнопку "Сохранить"'):
            page.check_adding_implementation('Correct', title, '')
        with allure.step('Удалить ранее созданную реализацию'):
            page.remove_form(title, 'Realization', 'Correct')


@pytest.mark.eservices
@pytest.mark.skip(reason='Тесты не доработаны до актуальной сборки.')
@allure.link(link + '/services/forms', name='ТАНДЕМ Личный кабинет')
@allure.epic('Функциональное тестирование панели управления')
@allure.feature('Проверка функциональности модуля "Редактор форм"')
class TestService_FormEditor:
    @pytest.mark.smoke
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.story('Позитивная проверка функциональности модуля')
    def testing_the_functionality_of_creating_a_form_editor_with_valid_data(self, browser):
        title = random_en(8)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Редактор форм"'):
            page.open_window_service('Редактор форм')
        with allure.step('Нажать кнопку "Новая форма"'):
            page.button_create_a_new_category()
        with allure.step('Ввести в поле "Название" 8 символов'):
            page.input_form_modal(0, title)
        with allure.step('Ввести в поле "Сообщение пользователю" 8 символов'):
            page.input_form_modal(1, random_en(8))
        with allure.step('Ввести в поле "Приоритет" 4 цифры'):
            page.input_form_modal(2, '1' + random_number(3))
        with allure.step('Нажать кнопку "Добавить"'):
            page.save_results_of_changes(0, 'CorrectSer', title, '')
        with allure.step('Удалить ранее созданную категорию'):
            page.remove_form(title, 'Service', 'Correct')

    @allure.story('Позитивная проверка функциональности модуля')
    def testing_the_functionality_of_creating_a_form_editor_by_filling_in_only_required_fields(self, browser):
        title = random_en(8)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Редактор форм"'):
            page.open_window_service('Редактор форм')
        with allure.step('Нажать кнопку "Новая форма"'):
            page.button_create_a_new_category()
        with allure.step('Ввести в поле "Название" 8 символов'):
            page.input_form_modal(0, title)
        with allure.step('Ввести в поле "Приоритет" 4 цифры'):
            page.input_form_modal(2, '1' + random_number(3))
        with allure.step('Нажать кнопку "Добавить"'):
            page.save_results_of_changes(0, 'CorrectSer', title, '')
        with allure.step('Удалить ранее созданную категорию'):
            page.remove_form(title, 'Service', 'Correct')

    @pytest.mark.xfail(reason='Ошибка функциональности поля "Приоритет", вкладка "Категории", при вводе предельного '
                              'значения')
    @allure.story('Позитивная проверка функциональности модуля')
    def testing_the_functionality_of_creating_a_form_editor_with_limit_values(self, browser):
        title = random_en(255)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Редактор форм"'):
            page.open_window_service('Редактор форм')
        with allure.step('Нажать кнопку "Новая форма"'):
            page.button_create_a_new_category()
        with allure.step('Ввести в поле "Название" 255 символов'):
            page.input_form_modal(0, title)
        with allure.step('Ввести в поле "Сообщение пользователю" 8 символов'):
            page.input_form_modal(1, random_en(255))
        with allure.step('Ввести в поле "Приоритет" 19 цифр'):
            page.input_form_modal(2, '1' + random_number(18))
        with allure.step('Нажать кнопку "Добавить"'):
            page.save_results_of_changes(0, 'CorrectSer', title, '')
        with allure.step('Удалить ранее созданную категорию'):
            page.remove_form(title, 'Service', 'Correct')

    @allure.story('Негативная проверка функциональности модуля')
    def testing_the_functionality_of_creating_a_form_editor_with_out_of_range_values(self, browser):
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Редактор форм"'):
            page.open_window_service('Редактор форм')
        with allure.step('Нажать кнопку "Новая форма"'):
            page.button_create_a_new_category()
        with allure.step('Подготовка к выполнению тест-кейса'):
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(0, random_en(8))
            with allure.step('Ввести в поле "Приоритет" 4 цифры'):
                page.input_form_modal(2, '1' + random_number(3))
        with allure.step('Проверка валидации поля "Название" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Название" 256 символов'):
                page.input_form_modal(0, random_en(256))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'Incorrect', '', '«название» не может превышать 255')
            with allure.step('Вернуть в исходное состояние поле "Название"'):
                page.input_form_modal(0, random_en(8))
        with allure.step('Проверка валидации поля "Сообщение пользователю" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Сообщение пользователю" 256 символов'):
                page.input_form_modal(1, random_en(256))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'Incorrect', '', '«сообщение пользователю» не может превышать 255')
            with allure.step('Вернуть в исходное состояние поле "Сообщение пользователю"'):
                page.input_form_modal(1, random_en(4))
        with allure.step('Проверка валидации поля "Приоритет" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Приоритет" 256 цифр'):
                page.input_form_modal(2, random_number(256))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'Incorrect', '', '«приоритет» должно быть целым числом')
            with allure.step('Вернуть в исходное состояние поле "Приоритет"'):
                page.input_form_modal(2, '1' + random_number(3))

    @pytest.mark.xfail(reason='Ошибка локализации алерта в поле "Сообщение пользователю", вкладка "Редактор форм", '
                              'при вводе запредельного значения')
    @allure.story('Негативная проверка функциональности модуля')
    def testing_the_functionality_of_creating_a_form_editor_with_out_of_range_values(self, browser):
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Редактор форм"'):
            page.open_window_service('Редактор форм')
        with allure.step('Нажать кнопку "Новая форма"'):
            page.button_create_a_new_category()
        with allure.step('Подготовка к выполнению тест-кейса'):
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(0, random_en(8))
            with allure.step('Ввести в поле "Приоритет" 4 цифры'):
                page.input_form_modal(2, '1' + random_number(3))
        with allure.step('Проверка валидации поля "Название" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Название" 256 символов'):
                page.input_form_modal(0, random_en(256))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'Incorrect', '', '«название» не может превышать 255')
            with allure.step('Вернуть в исходное состояние поле "Название"'):
                page.input_form_modal(0, random_en(8))
        with allure.step('Проверка валидации поля "Сообщение пользователю" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Сообщение пользователю" 256 символов'):
                page.input_form_modal(1, random_en(256))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'Incorrect', '', '«сообщение пользователю» не может превышать 255')
            with allure.step('Вернуть в исходное состояние поле "Сообщение пользователю"'):
                page.input_form_modal(1, random_en(4))
        with allure.step('Проверка валидации поля "Приоритет" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Приоритет" 256 цифр'):
                page.input_form_modal(2, random_number(256))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'Incorrect', '', '«приоритет» должно быть целым числом')
            with allure.step('Вернуть в исходное состояние поле "Приоритет"'):
                page.input_form_modal(2, random_en(4))

    @allure.story('Негативная проверка функциональности модуля')
    def testing_the_functionality_of_creating_a_form_editor_without_filling_in_the_required_field(self, browser):
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Редактор форм"'):
            page.open_window_service('Редактор форм')
        with allure.step('Нажать кнопку "Новая форма"'):
            page.button_create_a_new_category()
        with allure.step('Подготовка к выполнению тест-кейса'):
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(0, random_en(8))
            with allure.step('Ввести в поле "Приоритет" 4 цифры'):
                page.input_form_modal(2, '1' + random_number(3))
        with allure.step('Проверка валидации обязательного поля "Название"'):
            with allure.step('Оставить поле "Название" пустым'):
                page.input_form_modal(0, ' ')
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'Incorrect', '', ' «название» обязательно')
            with allure.step('Вернуть в исходное состояние поле "Название"'):
                page.input_form_modal(0, random_en(8))
        with allure.step('Проверка валидации обязательного поля "Приоритет"'):
            with allure.step('Оставить поле "Приоритет" пустым'):
                page.input_form_modal(2, ' ')
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'Incorrect', '', ' «приоритет» обязательно')
            with allure.step('Вернуть в исходное состояние поле "Приоритет"'):
                page.input_form_modal(2, '1' + random_en(4))

    @allure.story('Позитивная проверка функциональности модуля')
    def testing_the_editing_functionality_of_a_previously_created_form_editor(self, browser):
        title = random_en(8)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Редактор форм"'):
            page.open_window_service('Редактор форм')
        with allure.step('Подготовка к выполнению тест-кейса'):
            with allure.step('Нажать кнопку "Новая форма"'):
                page.button_create_a_new_category()
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(0, title)
            with allure.step('Ввести в поле "Сообщение пользователю" 8 символов'):
                page.input_form_modal(1, random_en(8))
            with allure.step('Ввести в поле "Приоритет" 4 цифры'):
                page.input_form_modal(2, '1' + random_number(3))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'CorrectSer', title, '')
        with allure.step('Нажать кнопку "Редактировать" в ранее созданной форме'):
            page.edit_form('')
        with allure.step('Ввести в поле "Название" 8 символов'):
            page.input_form_modal(3, title)
        with allure.step('Ввести в поле "Сообщение пользователю" 8 символов'):
            page.input_form_modal(4, random_en(8))
        with allure.step('Ввести в поле "Приоритет" 4 цифры'):
            page.input_form_modal(5, '1' + random_number(3))
        with allure.step('Нажать кнопку "Сохранить"'):
            page.save_results_of_changes(1, 'CorrectSer', title, '')
        with allure.step('Удалить ранее созданную услугу'):
            page.remove_form(title, 'Service', 'Correct')

    @pytest.mark.smoke
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.story('Позитивная проверка функциональности модуля')
    def testing_the_functionality_of_the_form_constructor_with_valid_data(self, browser):
        title = random_en(8)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Редактор форм"'):
            page.open_window_service('Редактор форм')
        with allure.step('Подготовка к выполнению тест-кейса'):
            with allure.step('Нажать кнопку "Новая форма"'):
                page.button_create_a_new_category()
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(0, title)
            with allure.step('Ввести в поле "Приоритет" 4 цифры'):
                page.input_form_modal(2, '1' + random_number(3))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'CorrectSer', title, '')
        title_form = random_en(8)
        with allure.step('Открыть "Конструктор" в ранее созданной форме'):
            page.construct_form()
        with allure.step('Нажать кнопку "Новая группа"'):
            page.button_create_a_new_category()
        with allure.step('Ввести в поле "Заголовок для группы" 8 символов'):
            page.input_form_modal(0, title_form)
        with allure.step('Ввести в поле "Приоритет" 4 цифры'):
            page.input_form_modal(1, '1' + random_number(3))
        with allure.step('Нажать кнопку "Добавить"'):
            page.save_results_of_changes(0, 'CorrectCon', title_form, '')
        with allure.step('Создание поля с типом "Текстовое поле"'):
            title_input_0 = random_en(8)
            with allure.step('Нажать кнопку "Добавить поле"'):
                page.group_control(0)
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(4, title_input_0)
            with allure.step('Ввести в поле "Отображаемый текст" 8 символов'):
                page.input_form_modal(5, title_input_0)
            with allure.step('Ввести в поле "Приоритет" цифру 0'):
                page.input_form_modal(6, '8')
            with allure.step('Выбрать в поле "Тип поля" - Текстовое поле'):
                page.select_type_input('text', 0)
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(2, 'CorrectInput', title_input_0, '')
        with allure.step('Создание поля с типом "Электронная почта"'):
            title_input_1 = random_en(8)
            with allure.step('Нажать кнопку "Добавить поле"'):
                page.group_control(0)
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(4, title_input_1)
            with allure.step('Ввести в поле "Отображаемый текст" 8 символов'):
                page.input_form_modal(5, title_input_1)
            with allure.step('Ввести в поле "Приоритет" цифру 0'):
                page.input_form_modal(6, '7')
            with allure.step('Выбрать в поле "Тип поля" - Электронная почта'):
                page.select_type_input('email', 0)
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(2, 'CorrectInput', title_input_1, '')
        with allure.step('Создание поля с типом "Число"'):
            title_input_2 = random_en(8)
            with allure.step('Нажать кнопку "Добавить поле"'):
                page.group_control(0)
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(4, title_input_2)
            with allure.step('Ввести в поле "Отображаемый текст" 8 символов'):
                page.input_form_modal(5, title_input_2)
            with allure.step('Ввести в поле "Приоритет" цифру 0'):
                page.input_form_modal(6, '6')
            with allure.step('Выбрать в поле "Тип поля" - Число'):
                page.select_type_input('number', 0)
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(2, 'CorrectInput', title_input_2, '')
        with allure.step('Создание поля с типом "Переключатель"'):
            title_input_3 = random_en(8)
            with allure.step('Нажать кнопку "Добавить поле"'):
                page.group_control(0)
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(4, title_input_3)
            with allure.step('Ввести в поле "Отображаемый текст" 8 символов'):
                page.input_form_modal(5, title_input_3)
            with allure.step('Ввести в поле "Приоритет" цифру 0'):
                page.input_form_modal(6, '5')
            with allure.step('Выбрать в поле "Тип поля" - Переключатель'):
                page.select_type_input('checkbox', 0)
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(2, 'CorrectInput', title_input_3, '')
        with allure.step('Создание поля с типом "Выбор"'):
            title_input_4 = random_en(8)
            with allure.step('Нажать кнопку "Добавить поле"'):
                page.group_control(0)
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(4, title_input_4)
            with allure.step('Ввести в поле "Отображаемый текст" 8 символов'):
                page.input_form_modal(5, title_input_4)
            with allure.step('Ввести в поле "Приоритет" цифру 0'):
                page.input_form_modal(6, '4')
            with allure.step('Выбрать в поле "Тип поля" - Выбор'):
                page.select_type_input('radio', 0)
            with allure.step('Ввести в поле "Допустимые значения" - значение: название'):
                page.input_valid_values('значение: название', 0)
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(2, 'CorrectInput', title_input_4, '')
        with allure.step('Создание поля с типом "Список"'):
            title_input_5 = random_en(8)
            with allure.step('Нажать кнопку "Добавить поле"'):
                page.group_control(0)
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(4, title_input_5)
            with allure.step('Ввести в поле "Отображаемый текст" 8 символов'):
                page.input_form_modal(5, title_input_5)
            with allure.step('Ввести в поле "Приоритет" цифру 0'):
                page.input_form_modal(6, '3')
            with allure.step('Выбрать в поле "Тип поля" - Список'):
                page.select_type_input('select', 0)
            with allure.step('Ввести в поле "Допустимые значения" - значение: название'):
                page.input_valid_values('значение: название', 0)
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(2, 'CorrectInput', title_input_5, '')
        with allure.step('Создание поля с типом "Список с множественным выбором"'):
            title_input_6 = random_en(8)
            with allure.step('Нажать кнопку "Добавить поле"'):
                page.group_control(0)
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(4, title_input_6)
            with allure.step('Ввести в поле "Отображаемый текст" 8 символов'):
                page.input_form_modal(5, title_input_6)
            with allure.step('Ввести в поле "Приоритет" цифру 0'):
                page.input_form_modal(6, '2')
            with allure.step('Выбрать в поле "Тип поля" - Список с множественным выбором'):
                page.select_type_input('multiselect', 0)
            with allure.step('Ввести в поле "Допустимые значения" - значение: название'):
                page.input_valid_values('значение: название', 0)
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(2, 'CorrectInput', title_input_6, '')
        with allure.step('Создание поля с типом "Файл"'):
            title_input_7 = random_en(8)
            with allure.step('Нажать кнопку "Добавить поле"'):
                page.group_control(0)
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(4, title_input_7)
            with allure.step('Ввести в поле "Отображаемый текст" 8 символов'):
                page.input_form_modal(5, title_input_7)
            with allure.step('Ввести в поле "Приоритет" цифру 0'):
                page.input_form_modal(6, '1')
            with allure.step('Выбрать в поле "Тип поля" - Файл'):
                page.select_type_input('checkbox', 0)
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(2, 'CorrectInput', title_input_7, '')
        with allure.step('Нажать кнопку "Назад"'):
            page.click_back_button(0)
        with allure.step('Удалить ранее созданную категорию'):
            page.remove_form(title, 'Service', 'Correct')

    @allure.story('Позитивная проверка функциональности модуля')
    def testing_the_functionality_of_editing_the_form_designer_group(self, browser):
        title = random_en(8)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Редактор форм"'):
            page.open_window_service('Редактор форм')
        with allure.step('Подготовка к выполнению тест-кейса'):
            with allure.step('Нажать кнопку "Новая форма"'):
                page.button_create_a_new_category()
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(0, title)
            with allure.step('Ввести в поле "Приоритет" 4 цифры'):
                page.input_form_modal(2, '1' + random_number(3))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'CorrectSer', title, '')
            title_form = random_en(8)
            with allure.step('Открыть "Конструктор" в ранее созданной форме'):
                page.construct_form()
            with allure.step('Нажать кнопку "Новая группа"'):
                page.button_create_a_new_category()
            with allure.step('Ввести в поле "Заголовок для группы" 8 символов'):
                page.input_form_modal(0, title_form)
            with allure.step('Ввести в поле "Приоритет" 4 цифры'):
                page.input_form_modal(1, '1' + random_number(3))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'CorrectCon', title_form, '')
        title_form_1 = random_en(8)
        with allure.step('Нажать кнопку "Редактировать группу"'):
            page.group_control(1)
        with allure.step('Ввести в поле "Заголовок для группы" 8 символов'):
            page.input_form_modal(2, title_form_1)
        with allure.step('Ввести в поле "Приоритет" 4 цифры'):
            page.input_form_modal(3, '1' + random_number(3))
        with allure.step('Нажать кнопку "Добавить"'):
            page.save_results_of_changes(1, 'CorrectCon', title_form_1, '')
        with allure.step('Нажать кнопку "Назад"'):
            page.click_back_button(0)
        with allure.step('Удалить ранее созданную категорию'):
            page.remove_form(title, 'Service', 'Correct')

    @pytest.mark.skip(reason='Ошибка функциональности поля "Приоритет" при вводе предельного значения')
    @allure.story('Позитивная проверка функциональности модуля')
    def testing_the_functionality_of_the_form_constructor_with_limit_values(self, browser):
        title = random_en(8)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Редактор форм"'):
            page.open_window_service('Редактор форм')
        with allure.step('Подготовка к выполнению тест-кейса'):
            with allure.step('Нажать кнопку "Новая форма"'):
                page.button_create_a_new_category()
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(0, title)
            with allure.step('Ввести в поле "Приоритет" 4 цифры'):
                page.input_form_modal(2, '1' + random_number(3))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'CorrectSer', title, '')
        title_form = random_en(255)
        with allure.step('Открыть "Конструктор" в ранее созданной форме'):
            page.construct_form()
        with allure.step('Нажать кнопку "Новая группа"'):
            page.button_create_a_new_category()
        with allure.step('Ввести в поле "Заголовок для группы" 255 символов'):
            page.input_form_modal(0, title_form)
        with allure.step('Ввести в поле "Приоритет" 19 цифры'):
            page.input_form_modal(1, '1' + random_number(18))
        with allure.step('Нажать кнопку "Добавить"'):
            page.save_results_of_changes(0, 'CorrectCon', title_form, '')
        with allure.step('Нажать кнопку "Удалить форму"'):
            page.delete_form_construct('Correct')
        with allure.step('Нажать кнопку "Назад"'):
            page.click_back_button(0)
        with allure.step('Удалить ранее созданную категорию'):
            page.remove_form(title, 'Service', 'Correct')

    @pytest.mark.skip(reason='Ошибка локализации алерта в поле "Заголовок для группы", вкладка "Конструктор формы", '
                             'при вводе запредельного значения')
    @allure.link('http://tracker.tandemservice.ru/browse/LK-121?focusedCommentId=268511&page=com.atlassian.jira'
                 '.plugin.system.issuetabpanels:comment-tabpanel#comment-268511', name='JIRA Репорт')
    @allure.story('Негативная проверка функциональности модуля')
    def testing_the_functionality_of_the_form_constructor_with_out_of_range_values(self, browser):
        title = random_en(8)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Редактор форм"'):
            page.open_window_service('Редактор форм')
        with allure.step('Подготовка к выполнению тест-кейса'):
            with allure.step('Нажать кнопку "Новая форма"'):
                page.button_create_a_new_category()
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(0, title)
            with allure.step('Ввести в поле "Приоритет" 4 цифры'):
                page.input_form_modal(2, '1' + random_number(3))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'CorrectSer', title, '')
            with allure.step('Открыть "Конструктор" в ранее созданной форме'):
                page.construct_form()
            with allure.step('Нажать кнопку "Новая группа"'):
                page.button_create_a_new_category()
            with allure.step('Ввести в поле "Заголовок для группы" 8 символов'):
                page.input_form_modal(0, random_en(8))
            with allure.step('Ввести в поле "Приоритет" 4 цифры'):
                page.input_form_modal(1, '1' + random_number(3))
        with allure.step('Проверка валидации поля "Заголовок для группы" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Заголовок для группы" 256 цифр'):
                page.input_form_modal(0, random_number(256))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'Incorrect', '', '«заголовок для группы» не может превышать 255.')
            with allure.step('Вернуть в исходное состояние поле "Заголовок для группы"'):
                page.input_form_modal(0, random_en(8))
        with allure.step('Проверка валидации поля "Приоритет" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Приоритет" 256 цифр'):
                page.input_form_modal(1, random_number(256))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'Incorrect', '', '«приоритет» должно быть целым числом')
            with allure.step('Вернуть в исходное состояние поле "Приоритет"'):
                page.input_form_modal(1, '1' + random_en(3))
        with allure.step('Нажать кнопку "Отмена"'):
            page.cancel_modal(3)
        with allure.step('Нажать кнопку "Назад"'):
            page.click_back_button(0)
        with allure.step('Удалить ранее созданную категорию'):
            page.remove_form(title, 'Service', 'Correct')

    @allure.story('Негативная проверка функциональности модуля')
    def testing_the_functionality_of_the_form_constructor_without_filling_in_the_required_field(self, browser):
        title = random_en(8)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Редактор форм"'):
            page.open_window_service('Редактор форм')
        with allure.step('Подготовка к выполнению тест-кейса'):
            with allure.step('Нажать кнопку "Новая форма"'):
                page.button_create_a_new_category()
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(0, title)
            with allure.step('Ввести в поле "Приоритет" 4 цифры'):
                page.input_form_modal(2, '1' + random_number(3))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'CorrectSer', title, '')
            with allure.step('Открыть "Конструктор" в ранее созданной форме'):
                page.construct_form()
            with allure.step('Нажать кнопку "Новая группа"'):
                page.button_create_a_new_category()
            with allure.step('Ввести в поле "Заголовок для группы" 8 символов'):
                page.input_form_modal(0, random_en(8))
            with allure.step('Ввести в поле "Приоритет" 4 цифры'):
                page.input_form_modal(1, '1' + random_number(3))
        with allure.step('Проверка валидации обязательного поля "Приоритет"'):
            with allure.step('Оставить поле "Приоритет" пустым'):
                page.input_form_modal(1, ' ')
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'Incorrect', '', '«приоритет» обязательно')
            with allure.step('Вернуть в исходное состояние поле "Приоритет"'):
                page.input_form_modal(1, '1' + random_en(3))
        with allure.step('Нажать кнопку "Отмена"'):
            page.cancel_modal(3)
        with allure.step('Нажать кнопку "Назад"'):
            page.click_back_button(0)
        with allure.step('Удалить ранее созданную категорию'):
            page.remove_form(title, 'Service', 'Correct')

    @pytest.mark.skip(reason='Ошибка функциональности поля "Приоритет" при вводе предельного значения')
    @allure.story('Позитивная проверка функциональности модуля')
    def testing_the_functionality_of_adding_a_field_in_the_constructor_with_limit_values(self, browser):
        title = random_en(8)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Редактор форм"'):
            page.open_window_service('Редактор форм')
        with allure.step('Подготовка к выполнению тест-кейса'):
            with allure.step('Нажать кнопку "Новая форма"'):
                page.button_create_a_new_category()
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(0, title)
            with allure.step('Ввести в поле "Приоритет" 4 цифры'):
                page.input_form_modal(2, '1' + random_number(3))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'CorrectSer', title, '')
            title_form = random_en(8)
            with allure.step('Открыть "Конструктор" в ранее созданной форме'):
                page.construct_form()
            with allure.step('Нажать кнопку "Новая группа"'):
                page.button_create_a_new_category()
            with allure.step('Ввести в поле "Заголовок для группы" 8 символов'):
                page.input_form_modal(0, title_form)
            with allure.step('Ввести в поле "Приоритет" 4 цифры'):
                page.input_form_modal(1, '1' + random_number(3))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'CorrectCon', title_form, '')
        title_input = random_en(255)
        with allure.step('Нажать кнопку "Добавить поле"'):
            page.group_control(0)
        with allure.step('Ввести в поле "Название" 255 символов'):
            page.input_form_modal(4, title_input)
        with allure.step('Ввести в поле "Отображаемый текст" 255 символов'):
            page.input_form_modal(5, title_input)
        with allure.step('Ввести в поле "Приоритет" 19 цифр'):
            page.input_form_modal(6, random_number(19))
        with allure.step('Выбрать в поле "Тип поля" - Выбор'):
            page.select_type_input('radio', 0)
        with allure.step('Ввести в поле "Допустимые значения" 255 символов'):
            page.input_valid_values(random_en(127) + ':' + random_en(127), 0)
        with allure.step('Нажать кнопку "Добавить"'):
            page.save_results_of_changes(2, 'CorrectInput', title_input, '')
        with allure.step('Нажать кнопку "Удалить форму"'):
            page.delete_form_construct('Correct')
        with allure.step('Нажать кнопку "Назад"'):
            page.click_back_button(0)
        with allure.step('Удалить ранее созданную категорию'):
            page.remove_form(title, 'Service', 'Correct')

    @allure.story('Негативная проверка функциональности модуля')
    def testing_the_functionality_of_adding_a_field_in_the_constructor_with_out_of_range_values(self, browser):
        title = random_en(8)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Редактор форм"'):
            page.open_window_service('Редактор форм')
        with allure.step('Подготовка к выполнению тест-кейса'):
            with allure.step('Нажать кнопку "Новая форма"'):
                page.button_create_a_new_category()
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(0, title)
            with allure.step('Ввести в поле "Приоритет" 4 цифры'):
                page.input_form_modal(2, '1' + random_number(3))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'CorrectSer', title, '')
            title_form = random_en(8)
            with allure.step('Открыть "Конструктор" в ранее созданной форме'):
                page.construct_form()
            with allure.step('Нажать кнопку "Новая группа"'):
                page.button_create_a_new_category()
            with allure.step('Ввести в поле "Заголовок для группы" 8 символов'):
                page.input_form_modal(0, title_form)
            with allure.step('Ввести в поле "Приоритет" 4 цифры'):
                page.input_form_modal(1, '1' + random_number(3))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'CorrectCon', title_form, '')
            with allure.step('Нажать кнопку "Добавить поле"'):
                page.group_control(0)
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(4, random_en(8))
            with allure.step('Ввести в поле "Отображаемый текст" 8 символов'):
                page.input_form_modal(5, random_en(8))
            with allure.step('Ввести в поле "Приоритет" 4 цифр'):
                page.input_form_modal(6, '1' + random_number(3))
            with allure.step('Выбрать в поле "Тип поля" - Выбор'):
                page.select_type_input('radio', 0)
            with allure.step('Ввести в поле "Допустимые значения" 8 символов'):
                page.input_valid_values(random_en(4) + ':' + random_en(3), 0)
        with allure.step('Проверка валидации поля "Название" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Название" 256 цифр'):
                page.input_form_modal(4, random_number(256))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(2, 'Incorrect', '', '«название» не может превышать 255.')
            with allure.step('Вернуть в исходное состояние поле "Название"'):
                page.input_form_modal(4, random_en(4))
        with allure.step('Проверка валидации поля "Отображаемый текст" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Отображаемый текст" 256 цифр'):
                page.input_form_modal(5, random_number(256))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(2, 'Incorrect', '', '«отображаемый текст» не может превышать 255.')
            with allure.step('Вернуть в исходное состояние поле "Отображаемый текст"'):
                page.input_form_modal(5, random_en(4))
        with allure.step('Проверка валидации поля "Приоритет" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Приоритет" 256 цифр'):
                page.input_form_modal(6, random_number(256))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(2, 'Incorrect', '', '«приоритет» должно быть целым числом')
            with allure.step('Вернуть в исходное состояние поле "Приоритет"'):
                page.input_form_modal(6, '1' + random_number(3))
        with allure.step('Проверка валидации поля "Допустимые значения" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Допустимые значения" 256 цифр'):
                page.input_valid_values(random_en(256), 0)
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(2, 'Incorrect', '', '«допустимые значения» необходимо указать')
            with allure.step('Вернуть в исходное состояние поле "Приоритет"'):
                page.input_valid_values('1' + random_en(3), 0)
        with allure.step('Нажать кнопку "Отмена"'):
            page.cancel_modal(8)
        with allure.step('Нажать кнопку "Удалить форму"'):
            page.delete_form_construct('Correct')
        with allure.step('Нажать кнопку "Назад"'):
            page.click_back_button(0)
        with allure.step('Удалить ранее созданную категорию'):
            page.remove_form(title, 'Service', 'Correct')

    @allure.story('Негативная проверка функциональности модуля')
    def testing_the_functionality_of_adding_a_field_in_the_const_without_filling_in_the_required_field(self, browser):
        title = random_en(8)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Редактор форм"'):
            page.open_window_service('Редактор форм')
        with allure.step('Подготовка к выполнению тест-кейса'):
            with allure.step('Нажать кнопку "Новая форма"'):
                page.button_create_a_new_category()
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(0, title)
            with allure.step('Ввести в поле "Приоритет" 4 цифры'):
                page.input_form_modal(2, '1' + random_number(3))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'CorrectSer', title, '')
            title_form = random_en(8)
            with allure.step('Открыть "Конструктор" в ранее созданной форме'):
                page.construct_form()
            with allure.step('Нажать кнопку "Новая группа"'):
                page.button_create_a_new_category()
            with allure.step('Ввести в поле "Заголовок для группы" 8 символов'):
                page.input_form_modal(0, title_form)
            with allure.step('Ввести в поле "Приоритет" 4 цифры'):
                page.input_form_modal(1, '1' + random_number(3))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'CorrectCon', title_form, '')
            with allure.step('Нажать кнопку "Добавить поле"'):
                page.group_control(0)
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(4, random_en(8))
            with allure.step('Ввести в поле "Отображаемый текст" 8 символов'):
                page.input_form_modal(5, random_en(8))
            with allure.step('Ввести в поле "Приоритет" 4 цифр'):
                page.input_form_modal(6, '1' + random_number(3))
            with allure.step('Выбрать в поле "Тип поля" - Выбор'):
                page.select_type_input('radio', 0)
            with allure.step('Ввести в поле "Допустимые значения" 8 символов'):
                page.input_valid_values(random_en(4) + ':' + random_en(3), 0)
        with allure.step('Проверка валидации обязательного поля "Название"'):
            with allure.step('Оставить поле "Название" пустым'):
                page.input_form_modal(4, ' ')
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(2, 'Incorrect', '', '«название» обязательно')
            with allure.step('Вернуть в исходное состояние поле "Название"'):
                page.input_form_modal(4, random_en(4))
        with allure.step('Проверка валидации обязательного поля "Отображаемый текст"'):
            with allure.step('Оставить поле "Отображаемый текст" пустым'):
                page.input_form_modal(5, ' ')
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(2, 'Incorrect', '', '«отображаемый текст» обязательно')
            with allure.step('Вернуть в исходное состояние поле "Отображаемый текст"'):
                page.input_form_modal(5, random_en(4))
        with allure.step('Проверка валидации обязательного поля "Приоритет"'):
            with allure.step('Оставить поле "Приоритет" пустым'):
                page.input_form_modal(6, ' ')
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(2, 'Incorrect', '', '«приоритет» обязательно')
            with allure.step('Вернуть в исходное состояние поле "Приоритет"'):
                page.input_form_modal(6, '1' + random_number(3))
        with allure.step('Проверка валидации обязательного поля "Допустимые значения"'):
            with allure.step('Оставить поле "Допустимые значения" пустым'):
                page.input_valid_values(' ', 0)
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(2, 'Incorrect', '', '«допустимые значения» обязательно')
            with allure.step('Вернуть в исходное состояние поле "Приоритет"'):
                page.input_valid_values('1' + random_en(3), 0)
        with allure.step('Нажать кнопку "Отмена"'):
            page.cancel_modal(8)
        with allure.step('Нажать кнопку "Удалить форму"'):
            page.delete_form_construct('Correct')
        with allure.step('Нажать кнопку "Назад"'):
            page.click_back_button(0)
        with allure.step('Удалить ранее созданную категорию'):
            page.remove_form(title, 'Service', 'Correct')

    @allure.story('Позитивная проверка функциональности модуля')
    def testing_the_functionality_of_editing_a_field_in_the_constructor(self, browser):
        title = random_en(8)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Редактор форм"'):
            page.open_window_service('Редактор форм')
        with allure.step('Подготовка к выполнению тест-кейса'):
            with allure.step('Нажать кнопку "Новая форма"'):
                page.button_create_a_new_category()
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(0, title)
            with allure.step('Ввести в поле "Приоритет" 4 цифры'):
                page.input_form_modal(2, '1' + random_number(3))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'CorrectSer', title, '')
            title_form = random_en(8)
            with allure.step('Открыть "Конструктор" в ранее созданной форме'):
                page.construct_form()
            with allure.step('Нажать кнопку "Новая группа"'):
                page.button_create_a_new_category()
            with allure.step('Ввести в поле "Заголовок для группы" 8 символов'):
                page.input_form_modal(0, title_form)
            with allure.step('Ввести в поле "Приоритет" 4 цифры'):
                page.input_form_modal(1, '1' + random_number(3))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'CorrectCon', title_form, '')
            title_input_0 = random_en(8)
            with allure.step('Нажать кнопку "Добавить поле"'):
                page.group_control(0)
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(4, title_input_0)
            with allure.step('Ввести в поле "Отображаемый текст" 8 символов'):
                page.input_form_modal(5, title_input_0)
            with allure.step('Ввести в поле "Приоритет" цифру 0'):
                page.input_form_modal(6, '8')
            with allure.step('Выбрать в поле "Тип поля" - Текстовое поле'):
                page.select_type_input('text', 0)
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(2, 'CorrectInput', title_input_0, '')
        with allure.step('Нажать на ранее созданное поле'):
            page.editing_input(0)
        title_input_1 = random_en(8)
        with allure.step('Ввести в поле "Название" 8 символов'):
            page.input_form_modal(7, title_input_1)
        with allure.step('Ввести в поле "Отображаемый текст" 8 символов'):
            page.input_form_modal(8, title_input_1)
        with allure.step('Ввести в поле "Приоритет" цифру 0'):
            page.input_form_modal(9, '3')
        with allure.step('Выбрать в поле "Тип поля" - Список'):
            page.select_type_input('select', 2)
        with allure.step('Ввести в поле "Допустимые значения" 8 символов'):
            page.input_valid_values(random_en(4) + ':' + random_number(4), 1)
        with allure.step('Нажать кнопку "Добавить"'):
            page.save_results_of_changes(3, 'CorrectInput', title_input_1, '')
        with allure.step('Нажать кнопку "Назад"'):
            page.click_back_button(0)
        with allure.step('Удалить ранее созданную форму'):
            page.remove_form(title, 'Service', 'Correct')

    @pytest.mark.skip(reason='Некорректное поведение модуля "Редактор форм", при удалении записи, привязанной к '
                             '"Реализации"')
    @allure.story('Интеграционное тестирование модуля')
    def testing_for_deleting_a_form_used_in_an_implementation(self, browser):
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login')
        page = Services(browser, link)
        with allure.step('Открыть вкладку "Редактор форм"'):
            page.open_window_service('Редактор форм')
        with allure.step('Подготовка к выполнению тест-кейса'):
            title_form = random_en(8)
            with allure.step('Нажать кнопку "Новая форма"'):
                page.button_create_a_new_category()
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_form_modal(0, title_form)
            with allure.step('Ввести в поле "Приоритет" 4 цифры'):
                page.input_form_modal(2, '1' + random_number(3))
            with allure.step('Нажать кнопку "Добавить"'):
                page.save_results_of_changes(0, 'CorrectSer', title_form, '')
            title = random_en(8)
            with allure.step('Открыть вкладку "Реализации"'):
                page.open_window_service('Реализации')
            with allure.step('Нажать кнопку "Новая реализация"'):
                page.button_create_a_new_realization()
            with allure.step('Ввести в поле "Описание" 8 символов'):
                page.input_description_1(random_en(8))
            with allure.step('Ввести в поле "Название" 8 символов'):
                page.input_title(title)
            with allure.step('Выбрать в поле "Форма" доступное значение'):
                page.select_form(title_form)
            with allure.step('Нажать кнопку "Создать"'):
                page.check_adding_implementation('Correct', title, '')
        with allure.step('Открыть вкладку "Редактор форм"'):
            page.open_window_service('Редактор форм')
        with allure.step('Удалить ранее созданную форму'):
            page.remove_form(title_form, 'Service', 'Incorrect')
        with allure.step('Возвращение в исходное состояние'):
            with allure.step('Открыть вкладку "Реализации"'):
                page.open_window_service('Реализации')
            with allure.step('Удалить ранее созданную реализацию'):
                page.remove_form(title, 'Realization', 'Correct')
            with allure.step('Открыть вкладку "Редактор форм"'):
                page.open_window_service('Редактор форм')
            with allure.step('Удалить ранее созданную форму'):
                page.remove_form(title_form, 'Service', 'Correct')
