import os
import time

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.wait import WebDriverWait
from .locators.LOCATORS import CONTROL, ALERT, PROFILE
from .base_page import BasePage


class Services(BasePage):
    def open_window_service(self, tab):
        WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable((CONTROL.service))).click()
        if tab == 'Заявки':
            WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable((CONTROL.service_categories))).click()
        elif tab == 'Категории':
            WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable((CONTROL.service_applications))).click()
        elif tab == 'Услуги':
            WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable((CONTROL.service_services))).click()
        elif tab == 'Реализации':
            WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable((CONTROL.service_realization))).click()
        elif tab == 'Редактор форм':
            WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable((CONTROL.service_form_editor))).click()

    # Категории
    def button_create_a_new_category(self):
        self.browser.find_elements(*CONTROL.create_button)[0].click()
        WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_modal)))

    def save_results_of_changes(self, button, result, name, field):
        self.browser.find_elements(*CONTROL.enter_button)[button].click()
        if result == 'Correct':
            WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_close_modal)))
            title = self.browser.find_elements(*CONTROL.search_applications)[0]
            assert name in title.text, 'ERROR: Запись не отображена в таблице'
        elif result == 'CorrectSer':
            WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_close_modal)))
            title = self.browser.find_elements(*CONTROL.search_service)[0]
            assert name in title.text, 'ERROR: Запись не отображена в таблице'
        elif result == 'CorrectCon':
            WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_close_modal)))
            title = self.browser.find_elements(*CONTROL.search_constructor)[0]
            assert name in title.text, 'ERROR: Форма не отображена в таблице'
        elif result == 'CorrectInput':
            WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_close_modal)))
            title = self.browser.find_elements(*CONTROL.search_input)[0]
            assert name in title.text, 'ERROR: Поле не отображена в форме'
        elif result == 'Incorrect':
            alert = WebDriverWait(self.browser, 3).until(EC.text_to_be_present_in_element((ALERT.alert_input2), field))
            assert alert, 'ERROR: Предупреждение об ограничении не вылезло'

    def save_results_of_pictogram(self, button, result, formats, name, update):
        if update == 'yes':
            WebDriverWait(self.browser, 3).until(EC.text_to_be_present_in_element((CONTROL.update_pictogram), formats))
        elif update == 'no':
            WebDriverWait(self.browser, 3).until(EC.text_to_be_present_in_element((CONTROL.load_pictogram), formats))
        self.browser.find_elements(*CONTROL.enter_button)[button].click()
        if result == 'Correct':
            WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_close_modal)))
            title = self.browser.find_elements(*CONTROL.search_applications)[0]
            assert name in title.text, 'ERROR: Запись не отображена в таблице'
        if result == 'Incorrect':
            WebDriverWait(self.browser, 3).until(EC.text_to_be_present_in_element((CONTROL.load_pictogram), formats))
            self.browser.find_elements(*CONTROL.enter_button)[button].click()
            alert = WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ALERT.alert_input2)))
            assert alert, 'ERROR: Предупреждение об ограничении не вылезло'

    def cancel_modal(self, button):
        self.browser.find_elements(*CONTROL.cancel_modal)[button].click()
        WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_close_modal)))

    def input_form_modal(self, form, text):
        self.browser.find_elements(*CONTROL.input_forms)[form].clear()
        self.browser.find_elements(*CONTROL.input_forms)[form].send_keys(text)

    def remove_form(self, name, tab, result):
        if tab == 'Realization':
            self.browser.find_elements(*CONTROL.remove_realization)[0].click()
        else:
            self.browser.find_elements(*CONTROL.remove_button)[0].click()
        if result == 'Correct':
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((PROFILE.delete_button))).click()
            WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_close_modal)))
            if tab == 'Category':
                title = self.browser.find_elements(*CONTROL.search_applications)[0]
                assert not name in title.text, 'ERROR: Запись не удалена'
            elif tab == 'Service' or 'Realization':
                title = self.browser.find_elements(*CONTROL.search_service)[0]
                assert not name in title.text, 'ERROR: Запись не удалена'
        elif result == 'Incorrect':
            alert = WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ALERT.alert_input)))
            assert alert, 'ERROR: Предупреждение о привязанной форме не вылезло'

    def delete_form_construct(self, result):
        self.browser.find_element(*CONTROL.delete_group).click()
        if result == 'Correct':
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((CONTROL.delete_form))).click()
            WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_close_modal)))
        elif result == 'Incorrect':
            alert = WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ALERT.alert_input)))
            assert alert, 'ERROR: Предупреждение о привязанной форме не вылезло'

    def delete_input_construct(self, result):
        self.browser.find_element(*CONTROL.remove_input).click()
        if result == 'Correct':
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((CONTROL.delete_input))).click()
            WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_close_modal)))
            WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.null_form)))
        elif result == 'Incorrect':
            alert = WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ALERT.alert_input)))
            assert alert, 'ERROR: Предупреждение о привязанной форме не вылезло'

    def construct_form(self):
        self.browser.find_elements(*CONTROL.archive_button)[0].click()
        WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.null_form)))

    def archive_form(self, tab, result, button):
        if tab == 'Realization':
            self.browser.find_elements(*CONTROL.archive_realization)[0].click()
        else:
            self.browser.find_elements(*CONTROL.archive_button)[0].click()
        WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_modal)))
        self.browser.find_elements(*CONTROL.send_and_restore)[button].click()
        WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_close_modal)))
        if result == 'Send':
            if tab == 'Category':
                status = self.browser.find_elements(*CONTROL.status_archive)[0]
                assert 'Да' in status.text, 'ERROR: Запись должна быть помещена в архив'
            elif tab == 'Service' or 'Realization':
                status = self.browser.find_elements(*CONTROL.status_archive_realization)[0]
                assert 'Да' in status.text, 'ERROR: Запись должна быть помещена в архив'
        elif result == 'Restore':
            if tab == 'Category':
                status = self.browser.find_elements(*CONTROL.status_archive)[0]
                assert 'Нет' in status.text, 'ERROR: Запись должна быть убрана из архива'
            elif tab == 'Service' or 'Realization':
                status = self.browser.find_elements(*CONTROL.status_archive_realization)[0]
                assert 'Нет' in status.text, 'ERROR: Запись должна быть помещена в архив'

    def edit_form(self, tab):
        self.browser.find_elements(*CONTROL.edit_button)[0].click()
        if tab == 'Realization':
            time.sleep(1)
        else:
            WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_modal)))

    def load_file(self, file):
        element = self.browser.find_element(*PROFILE.input_file)
        current_dir = os.path.abspath(os.path.dirname(__file__))
        file_path = os.path.join(current_dir, file)
        element.send_keys(file_path)

    # Услуги
    def input_description(self, description):
        self.browser.find_element(*CONTROL.description).clear()
        self.browser.find_element(*CONTROL.description).send_keys(description)

    def select_category(self, category):
        select = Select(self.browser.find_element(*CONTROL.select_category))
        select.select_by_visible_text(category)

    # Реализации
    def button_create_a_new_realization(self):
        self.browser.find_elements(*CONTROL.create_button)[0].click()
        WebDriverWait(self.browser, 2).until(EC.presence_of_element_located((CONTROL.description_attach)))
        time.sleep(1)

    def select_service(self, service):
        self.browser.find_element(*CONTROL.select_service)
        select = Select(self.browser.find_element(*CONTROL.select_service))
        select.select_by_visible_text(service)

    def select_form(self, form):
        self.browser.find_element(*CONTROL.select_form)
        select = Select(self.browser.find_element(*CONTROL.select_form))
        select.select_by_visible_text(form)

    def select_process_type(self, process_type):
        self.browser.find_element(*CONTROL.select_process_type)
        select = Select(self.browser.find_element(*CONTROL.select_process_type))
        select.select_by_visible_text(process_type)

    def select_responsible_department(self, search):
        WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable
                                             ((CONTROL.select_responsible_department))).click()
        field = WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable((CONTROL.input_search_select)))
        field.send_keys(search)
        field.send_keys(Keys.ENTER)

    def select_place_of_receipt(self, search):
        WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable((CONTROL.select_place_of_receipt))).click()
        field = WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable((CONTROL.input_search_select)))
        field.send_keys(search)
        field.send_keys(Keys.ENTER)

    def input_checkbox(self, checkbox):
        if checkbox == 'Electronic':
            WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable((CONTROL.checkbox_electronic))).click()
        elif checkbox == 'Paper':
            WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable((CONTROL.checkbox_paper))).click()
        elif checkbox == 'Document':
            WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable((CONTROL.checkbox_document))).click()

    def input_description_1(self, description):
        self.browser.find_element(*CONTROL.description_name).clear()
        self.browser.find_element(*CONTROL.description_name).send_keys(description)

    def input_title(self, title):
        self.browser.find_element(*CONTROL.title_name).clear()
        self.browser.find_element(*CONTROL.title_name).send_keys(title)

    def check_adding_implementation(self, check, name, field):
        self.browser.find_element(*CONTROL.save_button).click()
        if check == 'Correct':
            alert = WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ALERT.alert_correct)))
            assert alert, 'ERROR: Уведомление о создании новой формы не вылезло'
            title = self.browser.find_elements(*CONTROL.search_service)[0]
            assert name in title.text, 'ERROR: Запись не отображена в таблице'
        elif check == 'Incorrect':
            alert = WebDriverWait(self.browser, 3).until(EC.text_to_be_present_in_element((ALERT.alert_input2), field))
            assert alert, 'ERROR: Предупреждение об ограничении не вылезло'

    # Редактор форм - Конструктор формы
    def group_control(self, control):
        self.browser.find_elements(*CONTROL.group_control)[control].click()
        WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_modal)))

    def select_type_input(self, type_input, selects):
        select = Select(self.browser.find_elements(*CONTROL.select_type_input)[selects])
        select.select_by_value(type_input)
        # VALUES:
        # text - Текстовое поле
        # email - Электронная почта
        # number - Число
        # checkbox - Переключатель
        # radio - Выбор
        # select - Список
        # multiselect - Список с множественным выбором
        # file - Файл

    def input_valid_values(self, valid_values, field):
        self.browser.find_elements(*CONTROL.input_valid_values)[field].clear()
        self.browser.find_elements(*CONTROL.input_valid_values)[field].send_keys(valid_values)

    def click_back_button(self, button):
        self.browser.find_elements(*CONTROL.select_button)[button].click()

    def editing_input(self, form):
        self.browser.find_elements(*CONTROL.search_input)[form].click()
        WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_modal)))
