import time

from .base_page import BasePage
from .database.db_logic import USERS
from .locators.LOCATORS import REGLOG, PROFILE, ALERT
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait


class RegLogin(BasePage):
    def open_registration_page(self):
        WebDriverWait(self.browser, 5).until(EC.element_to_be_clickable((REGLOG.registration_button))).click()
        time.sleep(0.5)  # Задержка для локального стенда

    def open_support_page(self):
        WebDriverWait(self.browser, 5).until(EC.element_to_be_clickable((REGLOG.support_button))).click()
        time.sleep(0.5)  # Задержка для локального стенда

    def input_email(self, email):
        self.browser.find_element(*REGLOG.email).clear()
        self.browser.find_element(*REGLOG.email).send_keys(email)

    def input_last_name(self, last_name):
        self.browser.find_element(*REGLOG.last_name).clear()
        self.browser.find_element(*REGLOG.last_name).send_keys(last_name)

    def input_name(self, name):
        self.browser.find_element(*REGLOG.name).clear()
        self.browser.find_element(*REGLOG.name).send_keys(name)

    def input_middle_name(self, middle_name):
        self.browser.find_element(*REGLOG.middle_name).clear()
        self.browser.find_element(*REGLOG.middle_name).send_keys(middle_name)

    def put_a_tick_in_the_checkbox(self):
        self.browser.find_element(*REGLOG.checkbox_rule).click()

    def input_password(self, password):
        self.browser.find_element(*REGLOG.password).clear()
        self.browser.find_element(*REGLOG.password).send_keys(password)

    def input_password_confirmation(self, password_confirmation):
        self.browser.find_element(*REGLOG.password_confirmation).clear()
        self.browser.find_element(*REGLOG.password_confirmation).send_keys(password_confirmation)

    def send_new_link_the_verification_window(self):
        WebDriverWait(self.browser, 5).until(EC.element_to_be_clickable((REGLOG.send_new_link))).click()

    def exit_the_verification_window(self):
        WebDriverWait(self.browser, 5).until(EC.element_to_be_clickable((REGLOG.verification_exit))).click()
        time.sleep(0.5)  # Задержка для локального стенда

    def data_validation_check(self, result, reglog, field):
        self.browser.find_element(*REGLOG.enter_button).click()
        time.sleep(0.5)  # Задержка для локального стенда
        if result == 'Correct':
            if reglog == 'Registration':
                WebDriverWait(self.browser, 5).until(EC.element_to_be_clickable((REGLOG.enter_button)))
                assert '/email/verify' in self.browser.current_url, 'В URL должен быть /email/verify'
            elif reglog == 'Login':
                assert '/dashboard' in self.browser.current_url, 'В URL должен быть "/dashboard"'
            elif reglog == 'Two-Factor-Challenge':
                assert '/two-factor-challenge' in self.browser.current_url, 'В URL должен быть /two-factor-challenge '
        if result == 'Incorrect':
            if reglog == 'Registration':
                assert WebDriverWait(self.browser, 3).until(EC.text_to_be_present_in_element((ALERT.invalid), field)), \
                    'ERROR: Предупреждение об ограничении не появилось'
            elif reglog == 'Login':
                assert WebDriverWait(self.browser, 3).until(EC.text_to_be_present_in_element((ALERT.alert_input1),
                                                                                             field)), \
                    'ERROR: Предупреждение об ограничении не появилось'
                # assert not '/dashboard' in self.browser.current_url

    @staticmethod
    def check_records_database(email, last_name, first_name, middle_name):
        assert (email, last_name, first_name, middle_name) == USERS().get_data_about_a_new_user_by_email(email), \
            'Некорректная запись в таблице abitur.users'

    def click_use_recovery_code(self):
        self.browser.find_element(*PROFILE.select_button).click()

    def input_title(self, text, clear):
        if clear != (None):
            self.browser.find_element(*REGLOG.title).clear()
        self.browser.find_element(*REGLOG.title).send_keys(text)

    def input_reply_email(self, text, clear):
        if clear != (None):
            self.browser.find_element(*REGLOG.reply_email).clear()
        self.browser.find_element(*REGLOG.reply_email).send_keys(text)

    def input_content(self, text, clear):
        if clear != (None):
            self.browser.find_element(*REGLOG.content).clear()
        self.browser.find_element(*REGLOG.content).send_keys(text)

    def click_send_button(self, result, field):
        self.browser.find_element(*REGLOG.send_message).click()
        if result == 'Correct':
            assert WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ALERT.alert_correct1))), \
                'Предупреждение об успешной отправке данных не появилось'
        elif result == 'Incorrect':
            assert WebDriverWait(self.browser, 3).until(EC.text_to_be_present_in_element((ALERT.alert_input3), field)),\
                'ERROR: Предупреждение об ограничении не появилось'


