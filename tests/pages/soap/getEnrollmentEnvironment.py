import datetime
import requests
import xml.etree.ElementTree as ET

from ..precondition import SYSTEM

endpoint = SYSTEM + "/services/EnrCampaignInfoService"


# Форматировать текущую дату и время для верификации SOAP запроса
def date_Created():
    time = datetime.datetime.now()
    date_pull = time.year, time.month, time.day, time.hour - 3, time.minute, time.second - 4
    date_type = '%d-%02d-%02dT%02d:%02d:%02d.637Z' % date_pull
    print(date_type)
    return date_type


def date_Expires():
    time = datetime.datetime.now()
    date_pull = time.year, time.month, time.day, time.hour - 3, time.minute + 2, time.second - 4
    date_type = '%d-%02d-%02dT%02d:%02d:%02d.637Z' % date_pull
    print(date_type)
    return date_type


class EnrollmentEnvironment:
    @staticmethod
    def getEnrollmentEnvironment(CampaignId):
        body = f"""<?xml version="1.0" encoding="UTF-8"?>
        <SOAP-ENV:Envelope
            xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"
            xmlns:ns1="http://campaignInfo.ws.unienr14.tandemservice.ru/">
            <soapenv:Header
                xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
                <wsse:Security
                    xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" soapenv:mustUnderstand="1">
                    <wsu:Timestamp
                        xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" wsu:Id="Timestamp-4">
                        <wsu:Created>{date_Created()}</wsu:Created>
                        <wsu:Expires>{date_Expires()}</wsu:Expires>
                    </wsu:Timestamp>
                    <wsse:UsernameToken
                        xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"
                        xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" wsu:Id="UsernameToken-3">
                        <wsse:Username>tandem</wsse:Username>
                        <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">123456</wsse:Password>
                    </wsse:UsernameToken>
                </wsse:Security>
            </soapenv:Header>
            <SOAP-ENV:Body>
                <ns1:getEnrollmentEnvironment>
                    <enrollmentCampaignId>{CampaignId}</enrollmentCampaignId>
                </ns1:getEnrollmentEnvironment>
            </SOAP-ENV:Body>
        </SOAP-ENV:Envelope>
        """
        body = body.encode('utf-8')
        session = requests.session()
        session.headers = {"Content-Type": "text/xml; charset=utf-8"}
        response = session.post(url=endpoint, data=body, verify=False)
        return response.text

    @staticmethod
    def getEnrollmentEnvironmentCompetition(RequestType, CampaignId):
        # competitionType="04" - ОБЩИЙ КОНКУРС
        xml = ET.fromstring(EnrollmentEnvironment.getEnrollmentEnvironment(CampaignId))
        requestType = 1
        while True:
            try:
                xml_EduLevel = xml.find('.//programSet/row[@requestType="' + RequestType + '"][' + str(
                    requestType) + ']').attrib['id']
                print(xml_EduLevel)
                if xml_EduLevel:
                    xml_competition = xml.find('.//competition/row[@programSet="' + xml_EduLevel +
                                               '"][@competitionType="04"]').attrib['id']
                    break
            except:
                requestType += 1
        print(xml_competition)
        return xml_competition

    @staticmethod
    def getEnrollmentEnvironmentEDUProgram(RequestType, CampaignId):
        xml = ET.fromstring(EnrollmentEnvironment.getEnrollmentEnvironment(CampaignId))
        requestType = 1
        while True:
            try:
                xml_EduLevel = xml.find('.//programSet/row[@requestType="' + RequestType + '"][' + str(
                    requestType) + ']').attrib['id']
                print('xml_EduLevel: ', xml_EduLevel)
                xml_competition = xml.find('.//competition/row[@programSet="' + xml_EduLevel +
                                           '"][@competitionType="04"]').attrib['id']
                print('xml_competition: ', xml_competition)
                if xml_competition:
                    xml_eduProgram = xml.find('.//programSet/row[@id="' + xml_EduLevel + '"]/eduProgram').attrib['id']
                    break
            except:
                requestType += 1
        print('xml_eduProgram: ', xml_eduProgram)
        return xml_eduProgram
