import datetime
import requests

from pages.precondition import SYSTEM

endpoint = SYSTEM + "/services/EnrOnlineEntrantService"


# Форматировать текущую дату и время для верификации SOAP запроса
def date_Created():
    time = datetime.datetime.now()
    date_pull = time.year, time.month, time.day, time.hour - 3, time.minute, time.second - 4
    date_type = '%d-%02d-%02dT%02d:%02d:%02d.637Z' % date_pull
    print(date_type)
    return date_type


def date_Expires():
    time = datetime.datetime.now()
    date_pull = time.year, time.month, time.day, time.hour - 3, time.minute + 2, time.second - 4
    date_type = '%d-%02d-%02dT%02d:%02d:%02d.637Z' % date_pull
    print(date_type)
    return date_type


def getOnlineEntrantRequest():
    body = f"""
    <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
        <SOAP-ENV:Header xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
            <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"
                           xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"
                           soap:mustUnderstand="1">
                <wsu:Timestamp wsu:Id="TS-4144489A0889546697143013800563728">
                    <wsu:Created>{date_Created()}</wsu:Created>
                    <wsu:Expires>{date_Expires()}</wsu:Expires>
                </wsu:Timestamp>
                <wsse:UsernameToken wsu:Id="UsernameToken-4144489A0889546697143013800563727">
                    <wsse:Username>tandem</wsse:Username>
                    <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">123456</wsse:Password>
                </wsse:UsernameToken>
            </wsse:Security>
        </SOAP-ENV:Header>
        <soap:Body>
            <ns2:getOnlineEntrantRequest xmlns:ns2="http://onlineentrant.ws.unienr14.tandemservice.ru/">
             <onlineEntrantRequestId>1</onlineEntrantRequestId>
             <fullData>true</fullData>
            </ns2:getOnlineEntrantRequest>
        </soap:Body>
    </soap:Envelope>
    """

    body = body.encode('utf-8')
    session = requests.session()
    session.headers = {"Content-Type": "text/xml; charset=utf-8"}
    response = session.post(url=endpoint, data=body, verify=False)
    print(response.content)
    return response.content

getOnlineEntrantRequest()