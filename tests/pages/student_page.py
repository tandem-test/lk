from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from .locators.LOCATORS import STUDENT
from .base_page import BasePage


class Student(BasePage):
    def open_window_student(self, tab):
        wait = WebDriverWait(self.browser, 4)
        if tab == 'Услуги':
            wait.until(EC.element_to_be_clickable((STUDENT.service))).click()
        elif tab == 'Оценки':
            wait.until(EC.element_to_be_clickable((STUDENT.estimates))).click()
        elif tab == 'Приказы':
            wait.until(EC.element_to_be_clickable((STUDENT.orders))).click()
        elif tab == 'Портфолио':
            wait.until(EC.element_to_be_clickable((STUDENT.portfolio))).click()
        elif tab == 'Достижения':
            wait.until(EC.element_to_be_clickable((STUDENT.progress))).click()
        elif tab == 'Траектория':
            wait.until(EC.element_to_be_clickable((STUDENT.trajectory))).click()
        elif tab == 'Зачетная книжка':
            wait.until(EC.element_to_be_clickable((STUDENT.record_book))).click()
        elif tab == 'Запись на курсы':
            wait.until(EC.element_to_be_clickable((STUDENT.registration_for_courses))).click()

    def open_test_service(self):
        self.browser.find_browsers(*STUDENT.search_service)[0].click()



