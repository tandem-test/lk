import os
import time
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.wait import WebDriverWait
from .database.db_logic import CHATS, USERS
from .locators.LOCATORS import CHAT, CONTROL, PROFILE, ALERT, ADMIN
from .base_page import BasePage


class Ticket(BasePage):
    def open_window_chat(self, page):
        WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((CHAT.chat))).click()
        if page == 'Список':
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((CHAT.list_ticket))).click()
        elif page == 'Архив':
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((CHAT.archive_ticket))).click()
        time.sleep(0.5)

    def create_new_ticket(self):
        self.browser.find_elements(*CONTROL.create_button)[0].click()
        # WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_modal)))
        time.sleep(0.5)

    def select_author(self, name):
        WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable((CHAT.input_author))).send_keys(name)
        WebDriverWait(self.browser, 3).until(EC.text_to_be_present_in_element((CHAT.choose_author), name))
        WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CHAT.choose_author))).click()

    def input_title(self, title):
        time.sleep(0.5)
        self.browser.find_element(*CONTROL.title_name).clear()
        self.browser.find_element(*CONTROL.title_name).send_keys(title)

    def input_text(self, text):
        self.browser.find_element(*CHAT.text).clear()
        self.browser.find_element(*CHAT.text).send_keys(text)

    def select_tracker_id(self, tracker_id):
        select = Select(self.browser.find_element(*CHAT.tracker_id))
        select.select_by_visible_text(tracker_id)

    def select_type_id(self, type_id):
        select = Select(self.browser.find_element(*CHAT.type_id))
        select.select_by_visible_text(type_id)

    def select_status_id(self, status_id):
        select = Select(self.browser.find_element(*CHAT.status_id))
        select.select_by_visible_text(status_id)

    def select_priority_id(self, priority_id):
        select = Select(self.browser.find_element(*CHAT.priority_id))
        select.select_by_visible_text(priority_id)

    def save_results_of_changes(self, button, result, field):
        time.sleep(0.5)
        self.browser.find_elements(*CONTROL.create_button)[button].click()
        if result == 'Correct':
            # WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_close_modal)))
            assert WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ALERT.alert_correct))), \
                'ERROR: Отсутствует оповещение "Обращение успешно создано"'
        if result == 'CorrectUpdate':
            WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_close_modal)))
            alert = WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ALERT.alert_correct)))
            assert alert, 'ERROR: Отсутствует оповещение "Обращение успешно отредактировано"'
        elif result == 'Incorrect':
            alert = WebDriverWait(self.browser, 3).until(EC.text_to_be_present_in_element((ALERT.alert_input2), field))
            assert alert, 'ERROR: Предупреждение об ограничении не вылезло'

    def send_to_archive_ticket(self, result, title_ticket):
        search_title = 0
        while True:
            display_ticket = self.browser.find_elements(*CHAT.search_ticket)[search_title]
            if title_ticket == display_ticket.text:
                time.sleep(0.5)  # Задержка для локального стенда
                self.browser.find_elements(*CHAT.remove_button)[search_title].click()
                break
            elif search_title != 14:
                search_title += 1
                continue
            elif search_title == 14:
                last_page = self.browser.find_elements(*ADMIN.page_next)
                last_page[-1].click()
                time.sleep(0.5)  # Задержка для локального стенда
                search_title = 0
                continue
            else:
                break
        WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((CHAT.send_to_archive))).click()
        WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_close_modal)))
        if result == 'Correct':
            assert WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ALERT.alert_correct))), \
                'ERROR: Отсутствует предупреждение "Вы успешно удалили тикет"'
        elif result == 'Incorrect':
            alert = WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ALERT.alert_input)))
            assert alert, 'ERROR: Предупреждение о привязанной форме не вылезло'

    def leave_the_chat(self, result, title_ticket):
        search_title = 0
        while True:
            display_ticket = self.browser.find_elements(*CHAT.search_ticket)[search_title]
            if title_ticket == display_ticket.text:
                time.sleep(0.5)  # Задержка для локального стенда
                self.browser.find_elements(*CHAT.remove_button)[search_title].click()
                break
            elif search_title != 14:
                search_title += 1
                continue
            elif search_title == 14:
                last_page = self.browser.find_elements(*ADMIN.page_next)
                last_page[-1].click()
                time.sleep(0.5)  # Задержка для локального стенда
                search_title = 0
                continue
            else:
                break
        WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((CHAT.delete_button))).click()
        WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_close_modal)))
        if result == 'Correct':
            assert WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ALERT.alert_correct))), \
                'ERROR: Отсутствует предупреждение "Вы успешно удалили чат"'
        elif result == 'Incorrect':
            alert = WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ALERT.alert_input)))
            assert alert, 'ERROR: Предупреждение о привязанной форме не вылезло'

    def restore_a_request_in_the_archive(self, result, title_ticket):
        search_title = 0
        while True:
            display_ticket = self.browser.find_elements(*CHAT.search_ticket)[search_title]
            if title_ticket == display_ticket.text:
                time.sleep(0.5)  # Задержка для локального стенда
                self.browser.find_elements(*CONTROL.edit_button)[search_title].click()
                break
            elif search_title != 14:
                search_title += 1
                continue
            elif search_title == 14:
                last_page = self.browser.find_elements(*ADMIN.page_next)
                last_page[-1].click()
                time.sleep(0.5)  # Задержка для локального стенда
                search_title = 0
                continue
            else:
                break
        WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((CHAT.restore_ticket))).click()
        WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_close_modal)))
        if result == 'Correct':
            assert WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ALERT.alert_correct))), \
                'ERROR: Отсутствует предупреждение "Вы успешно удалили тикет"'
        elif result == 'Incorrect':
            alert = WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ALERT.alert_input)))
            assert alert, 'ERROR: Предупреждение о привязанной форме не вылезло'

    def accept_ticket(self):
        self.browser.find_elements(*CONTROL.edit_button)[-1].click()
        WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_modal)))
        WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((CONTROL.send_and_restore)))
        alert = WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ALERT.alert_correct)))
        assert alert, 'ERROR: Уведомление о создании новой формы не вылезло'

    def open_ticket(self, title_ticket, empty):
        search_title = 0
        while True:
            display_ticket = self.browser.find_elements(*CHAT.search_ticket)[search_title]
            if title_ticket == display_ticket.text:
                time.sleep(0.5)  # Задержка для локального стенда
                display_ticket.click()
                break
            elif search_title != 14:
                search_title += 1
                continue
            elif search_title == 14:
                last_page = self.browser.find_elements(*ADMIN.page_next)
                last_page[-1].click()
                time.sleep(0.5)  # Задержка для локального стенда
                search_title = 0
                continue
            else:
                break
        time.sleep(0.5)  # Задержка для локального стенда
        if empty == 'yes':
            empty = WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CHAT.empty_chat)))
            assert empty, 'ERROR: Отсутствует приветственное сообщение в пустом чате'

    def input_message(self, message):
        WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((CHAT.field_message)))
        self.browser.find_element(*CHAT.field_message).send_keys(message)

    def send_message(self, text, result, field):
        WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((CHAT.send_message))).click()
        if result == 'Correct':
            message = WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CHAT.messages_container)))
            assert text in message.text, 'ERROR: Сообщение не отображено в чате'
        elif result == 'Incorrect':
            alert = WebDriverWait(self.browser, 3).until(EC.text_to_be_present_in_element((ALERT.alert_input2), field))
            assert alert, 'ERROR: Предупреждение об ограничении не вылезло'

    def send_file(self, file, result):
        element = self.browser.find_element(*CHAT.load_file)
        current_dir = os.path.abspath(os.path.dirname(__file__))
        file_path = os.path.join(current_dir, file)
        element.send_keys(file_path)
        WebDriverWait(self.browser, 5).until(EC.presence_of_element_located((CHAT.check_load_file)))
        if result == 'Correct':
            WebDriverWait(self.browser, 10).until(EC.invisibility_of_element((CHAT.check_load_file)))
            file_display = self.browser.find_elements(*CHAT.display_file)[-1].text
            assert file_display in file, 'Ранее загруженный валидный файл не отображаеться в чате'
        elif result == 'Incorrect':
            WebDriverWait(self.browser, 5).until(EC.presence_of_element_located((ALERT.alert_input)))

    def check_last_message_to_archive(self, text):
        message = WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CHAT.messages_archive)))
        assert text in message.text, 'ERROR: Сообщение не отображено в чате'

    def open_setting_ticket(self, setting):
        WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((CHAT.setting_ticket))).click()
        WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CHAT.setting_wait)))
        if setting == 'Изменить':
            self.browser.find_elements(*CHAT.setting_menu)[0].click()
            WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_modal)))
        elif setting == 'Удалить':
            self.browser.find_elements(*CHAT.setting_menu)[1].click()
            WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_modal)))
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((CHAT.send_to_archive))).click()
            assert WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ALERT.alert_correct))), \
                'ERROR: Отсутствует предупреждение "Вы успешно удалили обращение"'

    def restore_ticket_in_the_control_panel(self):
        WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((CHAT.setting_restore))).click()
        WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_modal)))
        WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CHAT.restore_ticket))).click()
        assert WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ALERT.alert_correct))), \
            'ERROR: Отсутствует предупреждение "Вы успешно восстановили обращение"'
        assert self.browser.find_element(*CHAT.setting_ticket), 'Отсутвует панель управления обращением'

    def back_to_window(self, status, title_ticket):
        self.browser.find_element(*CHAT.back_window).click()
        time.sleep(0.5)  # Задержка для локального стенда
        search_title = 0
        while True:
            display_ticket = self.browser.find_elements(*CHAT.search_ticket)[search_title]
            if title_ticket == display_ticket.text:
                time.sleep(0.5)  # Задержка для локального стенда
                status_in = self.browser.find_elements(*CHAT.status_ticket)[search_title]
                assert status in status_in.text, 'ERROR: Некорректный статус'
                break
            elif search_title != 14:
                search_title += 1
                continue
            elif search_title == 14:
                last_page = self.browser.find_elements(*ADMIN.page_next)
                last_page[-1].click()
                time.sleep(0.5)  # Задержка для локального стенда
                search_title = 0
                continue
            else:
                break

    @staticmethod
    def check_records_ticket(title, description, responsible_id, creator_id, status_id, type_id):
        time.sleep(0.5)
        assert (responsible_id, int(CHATS().get_ticket_statuses_id_by_title(status_id)),
                int(CHATS().get_ticket_type_id_by_title(type_id)), description) \
               == CHATS().get_data_ticket_by_abstract_chat_id(title), 'Некорректная запись в abitur.tickets'
        assert (creator_id, title) == CHATS().get_data_abstract_chat_by_title(title), 'Некорректная запись в ' \
                                                                                      'abitur.abstract_chats'
        assert (int(CHATS().get_id_abstract_chats_by_title(title)), creator_id) == CHATS(
        ).get_data_chat_users_by_title(title), 'Некорректная запись в abitur.chat_users'

    @staticmethod
    def check_records_message_ticket(message, email, title_chat):
        time.sleep(1)
        assert "'" + message + "'" in CHATS().get_data_chat_message_by_sender_id(email, title_chat), \
            'Сообщение не записано в chat_messages'

    @staticmethod
    def check_status_ticket(status, title):
        assert CHATS().get_ticket_statuses_id_by_title(status) == CHATS().get_status_ticket_by_abstract_chat_id(title),\
            'Некорректная запись в tickets.status_id'

    @staticmethod
    def check_deleting_ticket(title):
        assert 'None' != CHATS().get_deleted_at_ticket_by_abstract_chat_id(title), 'Отсутвует запись об удаление'

    @staticmethod
    def check_restore_ticket(title):
        assert 'None' == CHATS().get_deleted_at_ticket_by_abstract_chat_id(title), \
            'Обращение должно иметь значение {null} в tickets.deleted_at'


class Chat(BasePage):
    def open_window_chat(self):
        WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((CHAT.ticket))).click()
        time.sleep(0.5)

    def create_chat(self):
        self.browser.find_elements(*CONTROL.create_button)[0].click()
        WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.title_name)))
        time.sleep(0.5)

    def input_title(self, title, clear):
        time.sleep(0.5)
        if clear == 'clear':
            self.browser.find_element(*CONTROL.title_name).clear()
            time.sleep(0.5)
        self.browser.find_element(*CONTROL.title_name).send_keys(title)

    def select_user_chat(self, user):
        self.browser.find_element(*CHAT.input_users).send_keys(user)
        WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((CHAT.select_user))).click()
        WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CHAT.list_users)))
        users = 0
        while users == 10:
            try:
                list_user = self.browser.find_elements(*CHAT.list_users)[users]
                assert user in list_user.text, 'Ранее указанный пользователь отсутвует в списке "Пользователи чата"'
                break
            except:
                users += 1

    def delete_select_user(self, user):
        number = 0
        list_user = self.browser.find_elements(*CHAT.list_users)[number].text
        while user == list_user:
            number += 1
        self.browser.find_elements(*CHAT.delete_users)[-1].click()
        time.sleep(0.5)

    def check_validation(self, result, field):
        self.browser.find_element(*ADMIN.save_button).click()
        if result == 'Correct':
            assert WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CHAT.empty_chat))), \
                'Отсутствует приветственное сообщение в пустом чате'
            assert WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ALERT.alert_correct))), \
                'Отсутствует оповещение "Чат успешно создан"'
        elif result == 'CorrectUpdate':
            WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_close_modal)))
            assert WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ALERT.alert_correct))), \
                'ERROR: Отсутствует оповещение "Обращение успешно отредактировано"'
        elif result == 'Incorrect':
            alert = WebDriverWait(self.browser, 3).until(EC.text_to_be_present_in_element((ALERT.alert_input2), field))
            assert alert, 'ERROR: Предупреждение об ограничении не вылезло'

    @staticmethod
    def check_records_chat(title, creator_id):
        assert CHATS().get_id_abstract_chats_by_title(title) == CHATS(
        ).get_chats_abstract_chat_id_by_abstract_chat_id(title), 'Некорректная запись в abitur.chats'
        assert (creator_id, title) == CHATS().get_data_abstract_chat_by_title(title), 'Некорректная запись в ' \
                                                                                      'abitur.abstract_chats'

    @staticmethod
    def check_records_chat_users(title, email):
        assert (int(CHATS().get_id_abstract_chats_by_title(title)), int(USERS().get_user_id_by_email(email))) == \
               CHATS().get_data_chats_users_by_title(email), 'Некорректная запись в abitur.chat_users'

    def open_chat(self, title):
        # last_page = self.browser.find_elements(*ADMIN.page_next)
        # if last_page:
        #     last_page[-2].click()
        #     time.sleep(0.5)  # Задержка для локального стенда
        title_display = self.browser.find_elements(*CONTROL.search_categories)[-1]
        assert title in title_display.text, 'Чат не найден'
        title_display.click()
        time.sleep(0.5)  # Задержка для локального стенда

    def back_to_window(self):
        self.browser.find_element(*CHAT.back_window).click()
        time.sleep(0.5)  # Задержка для локального стенда

    def open_setting_chat(self, setting):
        WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((CHAT.setting_ticket))).click()
        WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CHAT.setting_wait)))
        if setting == 'Изменить':
            self.browser.find_elements(*CHAT.setting_menu)[0].click()
            WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_modal)))
        elif setting == 'Покинуть чат':
            self.browser.find_elements(*CHAT.setting_menu)[1].click()
            WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_modal)))
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((CHAT.leave_chat))).click()
            assert WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ALERT.alert_correct))), \
                'Отсутствует предупреждение "Вы покинули чат"'
        elif setting == 'Удалить':
            self.browser.find_elements(*CHAT.setting_menu)[2].click()
            WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_modal)))
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((PROFILE.delete_button))).click()
            assert WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ALERT.alert_correct))), \
                'Отсутствует предупреждение "Вы успешно удалили чат"'

    @staticmethod
    def check_records_deleted_chat_users(email, title_chat):
        assert CHATS().get_chat_user_deleted_at_by_user_id(email, title_chat) != 'None', \
            'Отсутвует запись {deleted_at} в таблице chat_user'

    @staticmethod
    def check_deleting_abstract_chats(title_chat):
        assert CHATS().get_deleted_abstract_chat_by_title(title_chat) != 'None', \
            'Отсутвует запись {deleted_at} в таблице abstract_chat'

    @staticmethod
    def check_deleting_chats(title_chat):
        assert CHATS().get_deleted_abstract_chat_by_title(title_chat) != 'None', \
            'Отсутвует запись {deleted_at} в таблице chats'

    def leave_chat(self):
        self.browser.find_elements(*CONTROL.edit_button)[-1].click()
        WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_modal)))
        WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((CHAT.leave_chat))).click()
        assert WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ALERT.alert_correct))), \
            'Уведомление "Вы покинули чат" отсутвует'
