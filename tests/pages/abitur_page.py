import os
import time

from selenium.webdriver.support.select import Select
from .database.db_logic import ABITUR_DB
from .locators.LOCATORS import ABITUR, ALERT
from .base_page import BasePage
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from .precondition import EduLevel, RequestType, ReturnWay, PreferredForm, Exam, EntranceExams

stand_delay = 0.5


class Applications(BasePage):
    """
                                                 Выбор ОО/ПК/Заявки
    """

    def open_page_applications(self):
        WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable((ABITUR.applications))).click()
        time.sleep(stand_delay)

    def submit_a_new_application_in_multi_company_mode(self, new):
        if new == 'new':
            text = 'У вас нет поданных онлайн-заявлений.'
            WebDriverWait(self.browser, 4).until(EC.text_to_be_present_in_element((ABITUR.app_stub), text))
        WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable((ABITUR.submit_new_online_app))).click()
        assert WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ABITUR.search_oo))), \
            'Фильтр для поиска образовательной организации отсутствует'
        assert '/org-units' in self.browser.current_url, 'В URL должен быть /org-units'
        time.sleep(stand_delay)

    def submit_a_new_application_in_a_single_company_mode(self, new, first_time, result):
        if new == 'new':
            text = 'У вас нет поданных онлайн-заявлений.'
            WebDriverWait(self.browser, 4).until(EC.text_to_be_present_in_element((ABITUR.app_stub), text))
        WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable((ABITUR.submit_new_online_app))).click()
        if result == 'Correct':
            if first_time == 'yes':
                assert WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ABITUR.select_pk))), \
                    'Фильтр для поиска ПК отсутствует'
                assert '/org-units/' in self.browser.current_url, 'В URL должен быть /org-units'
            elif first_time == 'no':
                assert WebDriverWait(self.browser, 10).until(EC.presence_of_element_located((ABITUR.next_button))), \
                    'Элемент управления заявлением не появился'
                assert 'steps/1' in self.browser.current_url, 'В URL должен быть steps/1'
        elif result == 'Incorrect':
            field = 'Нельзя подать новое заявление, все предыдущие заявления должны быть оформлены до конца.'
            assert WebDriverWait(self.browser, 8).until(EC.text_to_be_present_in_element((ALERT.alert_input1), field)), \
                'ERROR: Предупреждение об ограничении не появилось'
        time.sleep(stand_delay)

    def search_educational_organization(self, title_org):
        time.sleep(stand_delay)
        self.browser.find_element(*ABITUR.search_oo).send_keys(title_org)
        while True:
            display_org = self.browser.find_element(*ABITUR.select_oo).text
            try:
                if title_org in display_org:
                    assert display_org == title_org, 'Название найденной ОО не совпадает, либо её кол-во не равно 1'
                    break
                else:
                    time.sleep(stand_delay)
            except:
                assert False

    def open_educational_organization(self):
        self.browser.find_element(*ABITUR.select_oo).click()
        time.sleep(stand_delay)

    def select_a_reception_company(self, title_pk):
        pk = 0
        while True:
            display_pk = self.browser.find_elements(*ABITUR.select_pk)[pk]
            try:
                if title_pk in display_pk.text:
                    assert title_pk in display_pk.text, 'Приёмная компания отсутствует в "Выберите приемную кампанию"'
                    display_pk.click()
                    break
                else:
                    pk += 1
            except:
                assert False
        assert WebDriverWait(self.browser, 10).until(EC.presence_of_element_located((ABITUR.next_button))), \
            'Элемент управления заявлением не появился'
        assert 'steps/1' in self.browser.current_url, 'В URL должен быть steps/1'
        time.sleep(stand_delay)

    """
                                                    Шаг 1
                                                Основные данные
    """

    def select_haveEduLevel(self, education):
        if education == EduLevel.BASIC_GENERAL:
            self.browser.find_element(*ABITUR.education_1).click()
        elif education == EduLevel.SECONDARY_GENERAL:
            self.browser.find_element(*ABITUR.education_2).click()
        elif education == EduLevel.SECONDARY_VOCATIONAL:
            self.browser.find_elements(*ABITUR.education_3)[0].click()
        elif education == EduLevel.UNDERGRADUATE:
            self.browser.find_element(*ABITUR.education_4).click()
        elif education == EduLevel.MASTER:
            self.browser.find_element(*ABITUR.education_5).click()
        elif education == EduLevel.HIGHEST_QUALIFICATION:
            self.browser.find_element(*ABITUR.education_6).click()
        time.sleep(stand_delay)

    def select_wantRequestType(self, education):
        if education == RequestType.SPO:
            self.browser.find_elements(*ABITUR.program_1)[1].click()
        elif education == RequestType.BACH_SPEC:
            self.browser.find_element(*ABITUR.program_2).click()
        elif education == RequestType.MASTER:
            self.browser.find_element(*ABITUR.program_3).click()
        elif education == RequestType.POSTGRADUATE:
            self.browser.find_element(*ABITUR.program_4).click()
        elif education == RequestType.RESIDENCY:
            self.browser.find_element(*ABITUR.program_5).click()
        time.sleep(stand_delay)

    def select_originalReturnWay(self, return_method):
        if return_method == ReturnWay.PERSONALLY:
            self.browser.find_element(*ABITUR.return_method_1).click()
        elif return_method == ReturnWay.BY_MAIL:
            self.browser.find_element(*ABITUR.return_method_2).click()
        elif return_method == ReturnWay.BY_PROXY:
            self.browser.find_element(*ABITUR.return_method_3).click()
        elif return_method == ReturnWay.IN_ELECTRONIC_FORM:
            self.browser.find_element(*ABITUR.return_method_4).click()
        elif return_method == ReturnWay.VIA_THE_SERVICE_PORTAL:
            self.browser.find_element(*ABITUR.return_method_5).click()
        time.sleep(stand_delay)

    def select_haveDisability(self):
        self.browser.find_element(*ABITUR.checkbox_haveDisability).click()
        time.sleep(stand_delay)

    def load_document(self, select_document, file):
        element = self.browser.find_elements(*ABITUR.load_file)[select_document]
        current_dir = os.path.abspath(os.path.dirname(__file__))
        file_path = os.path.join(current_dir, file)
        element.send_keys(file_path)
        time.sleep(stand_delay)

    def select_preferredForm(self, form):
        if form == PreferredForm.FULL_TIME:
            self.browser.find_element(*ABITUR.delivery_form_1).click()
        elif form == PreferredForm.REMOTE:
            self.browser.find_element(*ABITUR.delivery_form_2).click()
        time.sleep(stand_delay)

    def select_Exam(self, exam, score, year, opens):
        if exam == Exam.ENGLISH_LANGUAGE:
            if opens == 'on':
                self.browser.find_element(*ABITUR.EGE_english_language).click()
            time.sleep(stand_delay)
            exam_select = 0
        elif exam == Exam.BIOLOGY:
            if opens == 'on':
                self.browser.find_element(*ABITUR.EGE_biology).click()
            exam_select = 1
        elif exam == Exam.GEOGRAPHY:
            if opens == 'on':
                self.browser.find_element(*ABITUR.EGE_geography).click()
            exam_select = 2
        elif exam == Exam.COMPUTER_SCIENCE:
            if opens == 'on':
                self.browser.find_element(*ABITUR.EGE_computer_science).click()
            exam_select = 3
        elif exam == Exam.SPANISH_LANGUAGE:
            if opens == 'on':
                self.browser.find_element(*ABITUR.EGE_spanish_language).click()
            exam_select = 4
        elif exam == Exam.HISTORY:
            if opens == 'on':
                self.browser.find_element(*ABITUR.EGE_history).click()
            exam_select = 5
        elif exam == Exam.LITERATURE:
            if opens == 'on':
                self.browser.find_element(*ABITUR.EGE_literature).click()
            exam_select = 6
        elif exam == Exam.MATHEMATICS:
            if opens == 'on':
                self.browser.find_element(*ABITUR.EGE_mathematics).click()
            exam_select = 7
        elif exam == Exam.GERMAN_LANGUAGE:
            if opens == 'on':
                self.browser.find_element(*ABITUR.EGE_german_language).click()
            exam_select = 8
        elif exam == Exam.SOCIAL_STUDIES:
            if opens == 'on':
                self.browser.find_element(*ABITUR.EGE_social_studies).click()
            exam_select = 9
        elif exam == Exam.RUSSIAN:
            if opens == 'on':
                self.browser.find_element(*ABITUR.EGE_russian).click()
            exam_select = 10
        elif exam == Exam.PHYSICS:
            if opens == 'on':
                self.browser.find_element(*ABITUR.EGE_physics).click()
            exam_select = 11
        elif exam == Exam.FRENCH_LANGUAGE:
            if opens == 'on':
                self.browser.find_element(*ABITUR.EGE_french_language).click()
            exam_select = 12
        elif exam == Exam.CHEMISTRY:
            if opens == 'on':
                self.browser.find_element(*ABITUR.EGE_chemistry).click()
            exam_select = 13
        else:
            exam_select = False
        time.sleep(stand_delay)
        while True:
            try:
                self.browser.find_elements(*ABITUR.EGE_score)[exam_select].clear()
                self.browser.find_elements(*ABITUR.EGE_score)[exam_select].send_keys(score)
                time.sleep(stand_delay)
                self.browser.find_elements(*ABITUR.EGE_year)[exam_select].send_keys(year)
                break
            except:
                continue

    @staticmethod
    def check_records_Exam(email_abitur, ExamCodes, ExamMarks, ExamYears):
        assert (ExamMarks, ExamYears) == ABITUR_DB().get_stateExam_online_entrant_requests_step_1(
            email_abitur, ExamCodes), 'Некорректная запись данных ЕГЭ в online_entrant_requests.data'

    def select_internalDisciplineCodes(self, action, Entrance, discipline):
        if action == 'on':
            self.browser.find_element(*ABITUR.agreeEntranceExams).click()
        WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ABITUR.select_Entrance)))
        select = Select(self.browser.find_element(*ABITUR.select_Entrance))
        select.select_by_value(Entrance)
        if discipline == EntranceExams.GERMAN_LANGUAGE:
            self.browser.find_element(*ABITUR.ET_german_language).click()
        elif discipline == EntranceExams.LITERATURE:
            self.browser.find_element(*ABITUR.ET_literature).click()
        elif discipline == EntranceExams.SPANISH_LANGUAGE:
            self.browser.find_element(*ABITUR.ET_spanish).click()
        elif discipline == EntranceExams.COMPUTER_SCIENCE:
            self.browser.find_element(*ABITUR.ET_computer_science).click()
        elif discipline == EntranceExams.MATHEMATICS:
            self.browser.find_element(*ABITUR.ET_mathematics).click()
        elif discipline == EntranceExams.RUSSIAN:
            self.browser.find_element(*ABITUR.ET_russian).click()
        elif discipline == EntranceExams.CHEMISTRY:
            self.browser.find_element(*ABITUR.ET_chemistry).click()
        elif discipline == EntranceExams.FRENCH_LANGUAGE:
            self.browser.find_element(*ABITUR.ET_french).click()
        elif discipline == EntranceExams.PHYSICS:
            self.browser.find_element(*ABITUR.ET_physics).click()
        elif discipline == EntranceExams.SOCIAL_STUDIES:
            self.browser.find_element(*ABITUR.ET_social_studies).click()
        elif discipline == EntranceExams.HISTORY:
            self.browser.find_element(*ABITUR.ET_history).click()
        elif discipline == EntranceExams.BIOLOGY:
            self.browser.find_element(*ABITUR.ET_biology).click()
        elif discipline == EntranceExams.ENGLISH_LANGUAGE:
            self.browser.find_element(*ABITUR.ET_english).click()
        elif discipline == EntranceExams.GEOGRAPHY:
            self.browser.find_element(*ABITUR.ET_geography).click()
        elif discipline == EntranceExams.PHYSICAL_CULTURE:
            self.browser.find_element(*ABITUR.ET_physical_education).click()
        time.sleep(stand_delay)

    @staticmethod
    def check_records_internalDisciplineCodes(email_abitur, selectEntranceExams, ExamsReason, DisciplineCodes):
        assert (selectEntranceExams, ExamsReason) == ABITUR_DB().get_DisciplineCodes_online_entrant_requests_step_1(
            email_abitur, DisciplineCodes), 'Некорректная запись данных ВИ в online_entrant_requests.data'

    def select_aveAgreementTargetEdu(self, action):
        self.browser.find_element(*ABITUR.checkbox_aveAgreementTargetEdu).click()
        if action == 'on':
            assert WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ABITUR.agreementDate))), \
                'Шторка для заполнения не открылась'
        elif action == 'off':
            assert WebDriverWait(self.browser, 4).until(EC.invisibility_of_element((ABITUR.agreementDate))), \
                'Шторка для заполнения не закрылась'
        time.sleep(stand_delay)

    def input_agreementNumber(self, number):
        self.browser.find_element(*ABITUR.agreementNumber).clear()
        self.browser.find_element(*ABITUR.agreementNumber).send_keys(number)
        time.sleep(stand_delay)

    def input_agreementDate(self, date):
        self.browser.find_element(*ABITUR.agreementDate).clear()
        self.browser.find_element(*ABITUR.agreementDate).send_keys(date)
        self.browser.find_element(*ABITUR.agreementDate).send_keys(Keys.ENTER)
        time.sleep(stand_delay)

    def input_agreementOrg(self, data):
        self.browser.find_element(*ABITUR.agreementOrg).clear()
        self.browser.find_element(*ABITUR.agreementOrg).send_keys(data)
        time.sleep(stand_delay)

    def input_agreementEduProgram(self, data):
        self.browser.find_element(*ABITUR.agreementEduProgram).clear()
        self.browser.find_element(*ABITUR.agreementEduProgram).send_keys(data)
        time.sleep(stand_delay)

    @staticmethod
    def check_record_data_step_1(email_abitur, step, haveEduLevel, wantRequestType, originalReturnWay, haveDisability,
                                 preferredForm, aveAgreementTargetEdu, agreementNumber, agreementDate, agreementOrg,
                                 agreementEduProgram):
        time.sleep(1)
        assert (step, haveEduLevel, wantRequestType, originalReturnWay, haveDisability, preferredForm,
                aveAgreementTargetEdu, agreementNumber, agreementDate, agreementOrg, agreementEduProgram) \
               == ABITUR_DB().get_data_online_entrant_requests_step_1(email_abitur, haveDisability, preferredForm), \
            'Некорректная запись данных первого шага в online_entrant_requests.data'

    def select_checkbox_haveOlympiad(self, olymp_title, opens):
        self.browser.find_element(*ABITUR.haveOlympiad).click()
        if opens == 'on':
            time.sleep(stand_delay)
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ABITUR.inputOlympiad)))
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ABITUR.inputOlympiad))).send_keys(
                olymp_title)

    @staticmethod
    def check_records_Olympiad(email_abitur, haveOlympiad, olympiadInfo):
        time.sleep(stand_delay)
        assert (haveOlympiad, olympiadInfo) == ABITUR_DB().get_haveOlympiad_online_entrant_requests_step_1(
            email_abitur), 'Некорректная запись {haveOlympiad/olympiadInfo} в online_entrant_requests.data'

    def select_checkbox_exclusiveRights(self, WithoutExams, Budget, EmptiveEnroll):
        self.browser.find_element(*ABITUR.exclusiveRights).click()
        wait = WebDriverWait(self.browser, 4)
        if WithoutExams:
            wait.until(EC.element_to_be_clickable((ABITUR.exclusiveRightsWithoutExams))).click()
            time.sleep(stand_delay)
        if Budget:
            wait.until(EC.element_to_be_clickable((ABITUR.exclusiveRightsBudget))).click()
            time.sleep(stand_delay)
        if EmptiveEnroll:
            wait.until(EC.element_to_be_clickable((ABITUR.exclusiveRightsEmptiveEnroll))).click()
            time.sleep(stand_delay)

    @staticmethod
    def check_records_exclusiveRights(email_abitur, WithoutExams, Budget, EmptiveEnroll):
        assert (WithoutExams, Budget, EmptiveEnroll) == ABITUR_DB().get_exclusiveRights_online_entrant_requests_step_1(
            email_abitur), 'Некорректная запись {exclusiveRights} в online_entrant_requests.data'

    """
                                                    Шаг 2
                                                Выбор конкурсов
    """

    def choose_a_contest(self, competitions):
        elem = self.browser.find_element_by_css_selector('input[value="' + competitions + '"]')
        elem.click()
        WebDriverWait(self.browser, 20).until(EC.element_to_be_clickable((ABITUR.select_priority)))
        time.sleep(stand_delay)

    def give_priority_to_the_competition(self, priority, text):
        self.browser.find_elements(*ABITUR.select_priority)[priority].send_keys(text)
        time.sleep(stand_delay)

    @staticmethod
    def check_record_data_step_2(email_abitur, step, competitions, competitionPriorities, edu_program,
                                 requestedProgramPriorities):
        time.sleep(1)
        assert (step, competitions, competitionPriorities, "'" + edu_program + "': " + "'" +
                requestedProgramPriorities + "'") \
               == ABITUR_DB().get_data_online_entrant_requests_step_2(email_abitur),\
               'Некорректная запись данных первого шага в online_entrant_requests.data'

    """
                                                    Шаг 3
                                                Выбор конкурсов
    """

    # Индивидуальные достижения
    def choose_a_individualAchievements(self, individualAchievements):
        elem = self.browser.find_element_by_css_selector('input[value="' + individualAchievements + '"]')
        elem.click()
        time.sleep(stand_delay)

    """
                                                Инструменты навигации
    """

    def go_to_the_NextStep(self, result, next_step, field):
        time.sleep(1.5)
        self.browser.find_element(*ABITUR.next_button).click()
        if result == 'Correct':
            if next_step == 2:
                WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ABITUR.all_form)))
            elif next_step == 3:
                WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ABITUR.snils)))
        elif result == 'Incorrect':
            assert WebDriverWait(self.browser, 8).until(EC.text_to_be_present_in_element((ALERT.alert_input2), field)), \
                'ERROR: Предупреждение об ограничении не появилось'

    def cancel_the_application(self):
        WebDriverWait(self.browser, 5).until(EC.element_to_be_clickable((ABITUR.cancel_button))).click()
        WebDriverWait(self.browser, 5).until(EC.element_to_be_clickable((ABITUR.change_RC)))
        time.sleep(stand_delay)

    def change_the_education_organization(self, result):
        WebDriverWait(self.browser, 5).until(EC.element_to_be_clickable((ABITUR.change_RC))).click()
        if result == 'Correct':
            assert WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ABITUR.search_oo))), \
                'Фильтр для поиска образовательной организации отсутствует'
        elif result == 'Incorrect':
            field = 'Нельзя сменить образовательную организацию. У Вас есть неотправленное заявление.'
            assert WebDriverWait(self.browser, 8).until(EC.text_to_be_present_in_element((ALERT.alert_input1), field)), \
                'Предупреждение об ограничении не появилось'

    def change_the_receiving_campaign(self, title_pk, result):
        if result == 'Correct' or result == 'Incorrect':
            WebDriverWait(self.browser, 5).until(EC.element_to_be_clickable((ABITUR.change_RC))).click()
        if result == 'Correct' or result == 'SkipChange':
            time.sleep(stand_delay)
            pk = 0
            while True:
                display_pk = self.browser.find_elements(*ABITUR.select_pk)[pk]
                try:
                    if title_pk in display_pk.text:
                        assert title_pk in display_pk.text, 'ПК отсутствует в блоке "Выберите приемную кампанию"'
                        display_pk.click()
                        break
                    else:
                        pk += 1
                except:
                    assert False
            WebDriverWait(self.browser, 5).until(EC.element_to_be_clickable((ABITUR.change_RC)))
            assert 'org-units' not in self.browser.current_url, 'В URL не должно быть org-units'
        elif result == 'Incorrect':
            field = 'Нельзя сменить приемную кампанию. У Вас есть неотправленное заявление.'
            assert WebDriverWait(self.browser, 8).until(EC.text_to_be_present_in_element((ALERT.alert_input1), field)), \
                'Предупреждение об ограничении не появилось'
        time.sleep(stand_delay)

    def actions_on_the_application(self, action):
        if action == 'Удалить заявление':
            WebDriverWait(self.browser, 5).until(EC.element_to_be_clickable((ABITUR.delete_application))).click()
            WebDriverWait(self.browser, 5).until(EC.element_to_be_clickable((ABITUR.remove_accept))).click()
            text = 'У вас нет поданных онлайн-заявлений.'
            assert WebDriverWait(self.browser, 4).until(EC.text_to_be_present_in_element((ABITUR.app_stub), text)), \
                'Отсутствует заглушка "У вас нет поданных онлайн-заявлений"'
        elif action == 'Редактировать':
            WebDriverWait(self.browser, 5).until(EC.element_to_be_clickable((ABITUR.edit_application))).click()
        time.sleep(stand_delay)

    def go_to_filling_in_the_master_data(self, step):
        if step == 1:
            WebDriverWait(self.browser, 5).until(EC.element_to_be_clickable((ABITUR.go_filling_step_1))).click()
        elif step == 2:
            WebDriverWait(self.browser, 5).until(EC.element_to_be_clickable((ABITUR.go_filling_step_2))).click()
        assert WebDriverWait(self.browser, 5).until(EC.element_to_be_clickable((ABITUR.next_button))), \
            'Отсутвует элемент управления "Далее" в заявление'
