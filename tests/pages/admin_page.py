import os
import time

from selenium.common.exceptions import TimeoutException, NoSuchElementException
from .database.db_logic import USERS, ROLES, SETTING, CHATS, PERMISSION, ADMINS
from .locators.LOCATORS import ADMIN, PROFILE, ALERT, REGLOG, CONTROL, CHAT
from .base_page import BasePage
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support.ui import Select
from .precondition import password_encrypt, ScanCopies, OrderOfSpecialties, FirstStep, SecondStep, CompetitionLists, \
    ContractCommon, ContractAction, ContractEntrantPersonType, ContractOtherPersonType, ContractLegalPersonType, \
    AbiturMain, ThirdStep, EntrantsSearch, EntrantLists


class Authorization(BasePage):
    def open_authorization_window(self, page):
        time.sleep(0.5)
        self.browser.find_elements(*ADMIN.authorization)[-1].click()
        if page == 'Разрешения':
            WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable((ADMIN.authorization_permissions))).click()
        elif page == 'Роли':
            WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable((ADMIN.authorization_roles))).click()
        time.sleep(0.5)

    def search_permission(self, permission):
        last_page = self.browser.find_elements(*ADMIN.page_next)
        if last_page:
            last_page[-2].click()
            time.sleep(0.5)  # Задержка для локального стенда
        search = self.browser.find_elements(*ADMIN.search_permission)[-1]
        assert permission in search.text, 'Ранее созданная запись не отображается'

    def edit_role_permissions(self, permission, role):
        try:
            if role == 'student':
                self.browser.find_elements(*ADMIN.setting_role)[0].click()
            elif role == 'employee':
                self.browser.find_elements(*ADMIN.setting_role)[1].click()
            elif role == 'parent':
                self.browser.find_elements(*ADMIN.setting_role)[2].click()
            elif role == 'entrant':
                self.browser.find_elements(*ADMIN.setting_role)[3].click()
            WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ADMIN.list_roles)))
            time.sleep(0.5)  # Задержка для локального стенда
        except:
            PERMISSION().deleted_new_permission(permission)

    def select_checkbox_permissions(self, permission):
        result_role = PERMISSION().get_id_permission_by_name(permission)
        self.browser.find_element_by_id(result_role).click()

    def back_to_table_roles(self):
        self.browser.find_element(*ADMIN.list_roles).click()
        WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ADMIN.edit_role)))

    @staticmethod
    def check_record_permission_in_role(permission, role):
        data = (PERMISSION().get_id_permission_by_name(permission), ROLES().get_id_by_name(role))
        assert str(PERMISSION().get_data_model_has_roles(permission)) == str(data).replace("'", ""), \
            'Отсутвует запись в таблице abitur.role_has_permissions'


class Users(BasePage):
    def open_window_users(self):
        WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable((ADMIN.users))).click()
        time.sleep(0.5)  # Задержка для локального стенда

    def search_user(self, search_data, unbar, email):
        last_page = self.browser.find_elements(*ADMIN.last_page_button)
        if last_page:
            last_page[-2].click()
            time.sleep(0.5)  # Задержка для локального стенда
        search_user = self.browser.find_elements(*ADMIN.select_name)[-1]
        assert search_data in search_user.get_attribute('text'), 'Ранее созданный пользователь не отображается'
        if unbar == 'OPEN':
            search_user.click()
            time.sleep(0.5)  # Задержка для локального стенда
            assert search_data in self.browser.find_element(*ADMIN.user_name).text
            assert email in self.browser.find_element(*ADMIN.user_data).text

    def assign_administration_rule(self):
        self.browser.find_element(*ADMIN.select_checkbox).click()

    @staticmethod
    def check_record_admins_rule(email):
        time.sleep(1)
        assert USERS().get_user_id_by_email(email) == ADMINS().get_id_admins_by_id(email), \
            'Отсутвует запись о назначении прав администратора'

    def open_tubs(self, tab):
        if tab == 'Роли и разрешения':
            self.browser.find_elements(*ADMIN.sub_windows_select)[0].click()
        elif tab == 'Безопасность':
            self.browser.find_elements(*ADMIN.sub_windows_select)[1].click()

    def open_tab_window(self, window):
        if window == 'Разрешения':
            WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable((ADMIN.window_permissions))).click()

    def login_as_user(self):
        time.sleep(0.5)  # Задержка для локального стенда
        WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable((ADMIN.login_as_user))).click()
        try:
            WebDriverWait(self.browser, 2).until(EC.alert_is_present()).dismiss()
        except TimeoutException:
            "no alert"
        assert '/dashboard' in self.browser.current_url, "ERROR: В URL должен быть '/dashboard'"

    def click_add_button(self, action, page):
        time.sleep(0.5)  # Задержка для локального стенда
        if action == 'OPEN':
            if page == 'Роли':
                self.browser.find_elements(*ADMIN.to_appoint)[0].click()
            elif page == 'Разрешения':
                self.browser.find_elements(*ADMIN.to_appoint)[1].click()
            WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((CONTROL.wait_modal)))
        elif action == 'CLOSE':
            if page == 'Роли':
                self.browser.find_elements(*ADMIN.add_button)[0].click()
            elif page == 'Разрешения':
                self.browser.find_elements(*ADMIN.add_button)[1].click()
            WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((CONTROL.wait_close_modal)))

    def select_permission(self, permission):
        select = Select(self.browser.find_element(*ADMIN.select_permission))
        select.select_by_value(permission)

    @staticmethod
    def role_entry_validation(role, email):
        assert (ROLES().get_id_by_name(role), USERS().get_user_id_by_email(email)) \
               == ROLES().get_data_on_role_assignment_by_model_id(email), 'Запись о назначении роли в таблице ' \
                                                                          'abitur.model_has_roles отсутствует'

    def select_role(self, role):
        select = Select(self.browser.find_element(*ADMIN.select_role))
        select.select_by_value(role)

    def click_delete_button(self, amount, types):
        time.sleep(0.5)  # Задержка для локального стенда
        if types == 'Роли':
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ADMIN.attached_role))).click()
            WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable((PROFILE.delete_button))).click()
            WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((CONTROL.wait_close_modal)))
            if amount == 0:
                assert WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((
                    ADMIN.null_attached_role))), 'Отсутствует уведомление - "Пока не назначено ни одной роли."'
            else:
                time.sleep(0.5)  # Задержка для локального стенда
                attached_role = len(self.browser.find_elements(*ADMIN.attached_role))
                assert attached_role == amount, ('ERROR: Количество привязанных ролей должно быть ', amount)
        elif types == 'Разрешения':
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ADMIN.attached_permission))).click()
            WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable((PROFILE.withdraw_button))).click()
            WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((CONTROL.wait_close_modal)))
            if amount == 0:
                assert WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((
                    ADMIN.null_attached_permission))), 'Отсутствует уведомление - "Пока не выдано ни одного разрешения"'
            else:
                time.sleep(0.5)  # Задержка для локального стенда
                attached_permission = len(self.browser.find_elements(*ADMIN.attached_permission))
                assert attached_permission == amount, ('ERROR: Количество привязанных прав должно быть ', amount)

    def checking_password_validators(self, result, email, field):
        self.browser.find_element(*ADMIN.save_button).click()
        if result == 'Correct':
            assert WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ALERT.alert_correct))), \
                'Предупреждение об успешном сохранение данных не появилось'
            assert password_encrypt != USERS().get_password_by_email(email), 'Запись не обновилась в users.password'
        elif result == 'Incorrect':
            time.sleep(0.5)
            assert WebDriverWait(self.browser, 3).until(EC.text_to_be_present_in_element((ALERT.alert_input3), field)), \
                'ERROR: Предупреждение об ограничении не появилось'


class Setting(BasePage):
    def open_window_setting(self, tab):
        WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable((ADMIN.setting))).click()
        if tab == 'getting_roles':
            WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable((ADMIN.setting_getting_roles))).click()
        elif tab == 'general':
            WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable((ADMIN.setting_general))).click()
        elif tab == 'Виды обращений':
            WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable((ADMIN.setting_type_chat))).click()
        elif tab == 'Список ОО':
            WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable((ADMIN.setting_edu_org))).click()
        elif tab == 'Приемная кампания':
            WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable((ADMIN.setting_adm_camp))).click()
        elif tab == 'Нормативные документы':
            WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable((ADMIN.setting_reg_cat))).click()
        time.sleep(0.5)

    def open_page_setting_role(self, role):
        if role == 'Студент':
            self.browser.find_elements(*ADMIN.edit_role)[0].click()
        elif role == 'Сотрудник':
            self.browser.find_elements(*ADMIN.edit_role)[1].click()
        elif role == 'Родитель':
            self.browser.find_elements(*ADMIN.edit_role)[2].click()
        WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((REGLOG.inn)))
        time.sleep(0.5)  # Задержка для локального стенда
        title = self.browser.find_element(*ADMIN.title_edit_role)
        assert role in title.text, 'Открыта некорректная вкладка редактирования настроек роли'

    def input_checkbox_widget(self, widget):
        if widget == 'first_name':
            self.browser.find_element(*REGLOG.name).click()
        elif widget == 'last_name':
            self.browser.find_element(*REGLOG.last_name).click()
        elif widget == 'middle_name':
            self.browser.find_element(*REGLOG.middle_name).click()
        elif widget == 'birth_date':
            self.browser.find_element(*REGLOG.birth_date).click()
        elif widget == 'last_4_numbers_passport':
            self.browser.find_element(*REGLOG.last_4_numbers_passport).click()
        elif widget == 'snils':
            self.browser.find_element(*REGLOG.snils).click()
        elif widget == 'inn':
            self.browser.find_element(*REGLOG.inn).click()
        elif widget == 'book_number':
            self.browser.find_element(*REGLOG.book_number).click()
        elif widget == 'code':
            self.browser.find_element(*REGLOG.code).click()

    def checking_checkbox_validators(self, result, field):
        self.browser.find_element(*ADMIN.save_button).click()
        if result == 'Correct':
            assert WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ALERT.alert_correct))), \
                'ERROR: Предупреждение об успешном сохранение данных не появилось'
        elif result == 'Incorrect':
            time.sleep(0.5)
            assert WebDriverWait(self.browser, 3).until(EC.text_to_be_present_in_element((ALERT.alert_input1), field)), \
                'ERROR: Предупреждение об ограничении не появилось'

    @staticmethod
    def check_records_form(role, form):
        assert (int(ROLES().get_id_by_title(role)), SETTING().get_id_find_human_by_human_id_column(form)) \
               == SETTING().get_a_record_of_assigning_a_field_to_a_role(
            form), 'Запись отсутвует в assign_role_by_columns'

    @staticmethod
    def check_records_assignment_description(description, role):
        assert description == ROLES().get_assignment_description_by_title(role), "Описание отсутствует в abitur.roles"

    @staticmethod
    def check_records_mail_setting(setting, text):
        time.sleep(0.5)
        assert str(text) == SETTING().get_settings_app_by_name(setting), ('Отсутвует запись в поле ', setting)

    def clear_table_and_refresh(self):
        SETTING().delete_entries_in_assign_role_by_columns()
        self.browser.refresh()

    def click_to_the_list_of_roles(self):
        time.sleep(0.5)
        WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable((ADMIN.list_roles))).click()
        assert self.browser.find_elements(*ADMIN.select_name), 'ERROR: Кнопка работает некорректно'

    def input_assignment_description(self, description):
        self.browser.find_element(*ADMIN.assignment_description).clear()
        self.browser.find_element(*ADMIN.assignment_description).send_keys(description)

    def input_show_assign_role_widget_from_home(self):
        WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable((ADMIN.show_assign_role_widget))).click()

    def select_checkbox_is_multiple_org_units(self):
        WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable((ADMIN.is_multiple_org_units))).click()

    def checking_general_validators(self, button, result, field):
        if result == 'Correct':
            self.browser.find_elements(*REGLOG.enter_button)[button].click()
            time.sleep(0.5)
            alert = WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ALERT.alert_correct)))
            assert alert, 'ERROR: Предупреждение об успешном сохранение данных не появилось'
        elif result == 'CorrectFile':
            WebDriverWait(self.browser, 3).until(EC.invisibility_of_element_located((PROFILE.enter_disabled)))
            self.browser.find_elements(*REGLOG.enter_button)[button].click()
        elif result == 'Incorrect':
            self.browser.find_elements(*REGLOG.enter_button)[button].click()
            time.sleep(0.5)
            assert WebDriverWait(self.browser, 3).until(EC.text_to_be_present_in_element((ALERT.alert_input3), field)), \
                'ERROR: Предупреждение об ограничении не появилось'
        elif result == 'IncorrectFile':
            WebDriverWait(self.browser, 3).until(EC.invisibility_of_element_located((PROFILE.enter_disabled)))
            time.sleep(0.5)
            self.browser.find_elements(*REGLOG.enter_button)[button].click()
            assert WebDriverWait(self.browser, 3).until(EC.text_to_be_present_in_element((ALERT.alert_input3), field)), \
                'ERROR: Предупреждение об ограничении не появилось'

    def input_app_name(self, app_name):
        self.browser.find_element(*ADMIN.app_name).clear()
        self.browser.find_element(*ADMIN.app_name).send_keys(app_name)

    def input_app_url(self, app_url):
        self.browser.find_element(*ADMIN.app_url).clear()
        self.browser.find_element(*ADMIN.app_url).send_keys(app_url)

    def check_title_page(self, app_name):
        self.browser.refresh()
        title = self.browser.title
        assert app_name == title, 'TITLE не изменился'

    def input_mail_server(self, mail_server):
        self.browser.find_element(*ADMIN.mail_server).clear()
        self.browser.find_element(*ADMIN.mail_server).send_keys(mail_server)

    def input_mail_server_port(self, mail_server_port):
        self.browser.find_element(*ADMIN.mail_server_port).clear()
        self.browser.find_element(*ADMIN.mail_server_port).send_keys(mail_server_port)

    def input_mail_server_login(self, mail_server_login):
        self.browser.find_element(*ADMIN.mail_server_login).clear()
        self.browser.find_element(*ADMIN.mail_server_login).send_keys(mail_server_login)

    def input_mail_server_password(self, mail_server_password):
        self.browser.find_element(*ADMIN.mail_server_password).clear()
        self.browser.find_element(*ADMIN.mail_server_password).send_keys(mail_server_password)

    def input_mail_server_encryption(self, mail_server_encryption):
        self.browser.find_element(*ADMIN.mail_server_encryption).clear()
        self.browser.find_element(*ADMIN.mail_server_encryption).send_keys(mail_server_encryption)

    def input_mail_server_from_address(self, mail_server_from_address):
        self.browser.find_element(*ADMIN.mail_server_from_address).clear()
        self.browser.find_element(*ADMIN.mail_server_from_address).send_keys(mail_server_from_address)

    def input_mail_server_from_name(self, mail_server_from_name):
        self.browser.find_element(*ADMIN.mail_server_from_name).clear()
        self.browser.find_element(*ADMIN.mail_server_from_name).send_keys(mail_server_from_name)

    def change_logo_app(self, logo):
        element = self.browser.find_element(*PROFILE.input_file)
        current_dir = os.path.abspath(os.path.dirname(__file__))
        file_path = os.path.join(current_dir, logo)
        element.send_keys(file_path)
        time.sleep(2)

    def change_icon_app(self, icon):
        element = self.browser.find_element(*ADMIN.app_icon)
        current_dir = os.path.abspath(os.path.dirname(__file__))
        file_path = os.path.join(current_dir, icon)
        element.send_keys(file_path)

    def deleting_logo_app(self):
        self.browser.find_elements(*PROFILE.delete_button)[1].click()
        WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_modal)))
        self.browser.find_elements(*PROFILE.delete_button)[0].click()
        assert WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ALERT.alert_correct))), \
            'ERROR: Предупреждение об успешном сохранение данных не появилось'

    def prone_logo_app(self):
        WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable((ADMIN.app_logo_prone))).click()
        self.browser.find_element(*ADMIN.save_button).click()
        assert WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ALERT.alert_correct))), \
            'ERROR: Предупреждение об успешном сохранение данных не появилось'

    def select_settings_allow_delete_profile(self):
        self.browser.find_element(*ADMIN.allow_delete_profile).click()

    def select_setting_enable_2fa(self):
        self.browser.find_element(*ADMIN.enable_2fa).click()

    def select_settings_allow_change_fullname(self):
        self.browser.find_element(*ADMIN.allow_change_fullname).click()

    def input_support_email(self, text):
        self.browser.find_element(*ADMIN.support_email).clear()
        self.browser.find_element(*ADMIN.support_email).send_keys(text)

    def input_support_email(self, text, clear):
        if clear != (None):
            self.browser.find_element(*ADMIN.support_email).clear()
        self.browser.find_element(*ADMIN.support_email).send_keys(text)

    def input_support_link_name(self, text, clear):
        if clear != (None):
            self.browser.find_element(*ADMIN.support_link_name).clear()
        self.browser.find_element(*ADMIN.support_link_name).send_keys(text)

    def input_support_prompt(self, text, clear):
        if clear != (None):
            self.browser.find_element(*ADMIN.support_prompt).clear()
        self.browser.find_element(*ADMIN.support_prompt).send_keys(text)

    def select_checkbox_support_active(self):
        self.browser.find_element(*ADMIN.support_active).click()

    ####################################################################################################################
    def add_a_new_record(self):
        WebDriverWait(self.browser, 2).until(EC.element_to_be_clickable((ADMIN.add_new))).click()
        assert WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((CONTROL.title_name)))
        assert 'types/create' in self.browser.current_url, 'В URL должен быть types/create'
        time.sleep(0.5)

    def input_title(self, title):
        self.browser.find_element(*CONTROL.title_name).clear()
        self.browser.find_element(*CONTROL.title_name).send_keys(title)

    def input_description(self, description):
        self.browser.find_element(*CONTROL.description).clear()
        self.browser.find_element(*CONTROL.description).send_keys(description)

    @staticmethod
    def check_record_type_chat(title, description, is_active):
        assert (title, description, is_active) == CHATS().get_data_type_chat_by_title(title)

    def checkbox_is_active(self):
        self.browser.find_element(*ADMIN.is_active).click()

    def action_type_chats(self, title, action, result):
        time.sleep(0.5)  # Задержка для локального стенда
        last_page = self.browser.find_elements(*ADMIN.page_next)
        if last_page:
            last_page[-2].click()
            time.sleep(0.5)  # Задержка для локального стенда
        search_user = self.browser.find_elements(*ADMIN.search_type_chat)[-1]
        assert title in search_user.text, 'Ранее созданная запись не отображается'
        if action == 'edit':
            self.browser.find_elements(*CONTROL.edit_button)[-1].click()
            WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((CONTROL.title_name)))
            time.sleep(0.5)  # Задержка для локального стенда
        elif action == 'remove':
            self.browser.find_elements(*CONTROL.archive_button)[-1].click()
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((PROFILE.delete_button))).click()
            WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_close_modal)))
            if result == 'Correct':
                assert WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ALERT.alert_correct))), \
                    'ERROR: Предупреждение об успешном сохранение данных не появилось'
            elif result == 'Incorrect':
                assert WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ALERT.alert_input1))), \
                    'ERROR: Предупреждение об успешном сохранение данных не появилось'

    def click_back_button(self):
        time.sleep(0.5)  # Задержка для локального стенда
        self.browser.find_element(*ADMIN.back_button).click()
        time.sleep(0.5)  # Задержка для локального стенда

    def checking_validators(self, result, field):
        self.browser.find_element(*ADMIN.save_button).click()
        time.sleep(0.5)  # Задержка для локального стенда
        if result == 'Correct':
            assert WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ALERT.alert_correct))), \
                'ERROR: Предупреждение об успешном сохранение данных не появилось'
        elif result == 'Incorrect':
            time.sleep(0.5)
            assert WebDriverWait(self.browser, 3).until(EC.text_to_be_present_in_element((ALERT.alert_input), field)), \
                'ERROR: Предупреждение об ограничении не появилось'

    # Список ОО ########################################################################################################
    def create_new_company(self, result):
        self.browser.find_element(*ADMIN.add_new).click()
        if result == 'Correct':
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ADMIN.save_button)))
        elif result == 'Incorrect':
            assert WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ALERT.alert_input1))), \
                'Отсутствует уведомление "Для подключения необходимо сменить режим работы ЛК"'
        time.sleep(0.5)

    def input_field(self, field, text, clear):
        if clear == 'clear':
            self.browser.find_elements(*ADMIN.field_input)[field].clear()
            time.sleep(0.3)
        self.browser.find_elements(*ADMIN.field_input)[field].send_keys(text)
        time.sleep(0.3)

    def input_textarea(self, field, text, clear):
        if clear == 'clear':
            self.browser.find_elements(*ADMIN.field_textarea)[field].clear()
            time.sleep(0.3)
        self.browser.find_elements(*ADMIN.field_textarea)[field].send_keys(text)
        time.sleep(0.3)

    def select_field(self, field, value):
        if field == 'Приемная кампания':
            select = Select(self.browser.find_elements(*ADMIN.select_field)[0])
            select.select_by_visible_text(value)
        elif field == 'Тип' or field == 'Тип(Редактирование)':
            if field == 'Тип':
                select = 1
            else:
                select = 0
            result = SETTING().get_id_campaign_types_by_title(value)
            select = Select(self.browser.find_elements(*ADMIN.select_field)[select])
            select.select_by_value(result)
        elif field == 'Система' and field == 'Система(Редактирование)':
            if field == 'Система':
                select = 2
            else:
                select = 1
            result = SETTING().get_id_uni_installations_by_url(value)
            select = Select(self.browser.find_elements(*ADMIN.select_field)[select])
            select.select_by_value(result)
        elif field == 'Приемная кампания':
            result = SETTING().get_enrollment_campaigns_id(value)
            select = Select(self.browser.find_elements(*ADMIN.select_field)[0])
            select.select_by_value(result)
        elif field == 'Настройки ОО':
            result = SETTING().get_id_org_by_title(value)
            select = Select(self.browser.find_elements(*ADMIN.select_field)[0])
            select.select_by_value(result)
        elif field == 'Настройки ПК':
            result = SETTING().get_enrollment_campaigns_id(value)
            select = Select(self.browser.find_elements(*ADMIN.select_field)[1])
            select.select_by_value(result)
        time.sleep(0.5)

    def select_checkbox(self, checkbox):
        self.browser.find_elements(*ADMIN.select_checkbox)[checkbox].click()

    def input_description_org(self, text, clear):
        if clear == 'clear':
            self.browser.find_element(*CONTROL.input_valid_values).clear()
            time.sleep(0.5)
        self.browser.find_element(*CONTROL.input_valid_values).send_keys(text)
        time.sleep(0.3)

    def perform_an_action_on_an_organization(self, title, action, result):
        search_title = 0
        while True:
            display_org = self.browser.find_elements(*CONTROL.search_applications)[search_title]
            if title == display_org.text:
                time.sleep(0.5)  # Задержка для локального стенда
                if action == 'edit':
                    self.browser.find_elements(*CONTROL.edit_button)[search_title].click()
                    WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ADMIN.field_input)))
                    time.sleep(0.5)  # Задержка для локального стенда
                elif action == 'remove':
                    self.browser.find_elements(*CONTROL.archive_button)[search_title].click()
                    WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((PROFILE.delete_button))).click()
                    if result == 'Correct':
                        WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_close_modal)))
                        assert WebDriverWait(self.browser, 3).until(
                            EC.presence_of_element_located((ALERT.alert_correct))), \
                            'ERROR: Предупреждение об успешном сохранение данных не появилось'
                    elif result == 'Incorrect':
                        assert WebDriverWait(self.browser, 3).until(
                            EC.presence_of_element_located((ALERT.alert_in_modal))), \
                            'ERROR: Предупреждение об ошибке сохранения данных не появилось'
                break
            elif search_title != 14:
                search_title += 1
                continue
            elif search_title == 14:
                last_page = self.browser.find_elements(*ADMIN.page_next)
                last_page[-1].click()
                time.sleep(0.5)  # Задержка для локального стенда
                search_title = 0
                continue
            else:
                break

    def input_parent_organization(self, text):
        self.browser.find_elements(*ADMIN.field_input)[2].send_keys(text)
        WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ADMIN.select_user))).click()
        WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ADMIN.list_user)))

    def delete_parent_organization(self):
        self.browser.find_element(*CHAT.delete_users).click()
        self.browser.find_elements(*ADMIN.close_alert)[-1].click()
        WebDriverWait(self.browser, 4).until(EC.invisibility_of_element_located((ADMIN.list_user)))

    @staticmethod
    def check_record_parent_org(title, parent_title):
        time.sleep(1)
        assert SETTING().get_parent_id_by_title(title) == SETTING().get_id_org_by_title(parent_title), \
            'Некорректная запись родительской организации'

    @staticmethod
    def check_record_org(title, short_title, parent_id, ur_address, fac_address, description):
        assert (title, short_title, parent_id, ur_address, fac_address, description) == \
               SETTING().get_data_external_org_units(title), 'Отсутвует запись в abitur.external_org_units'

    @staticmethod
    def check_record_logo_org(title, logo):
        if logo == 'yes':
            assert 'None' != SETTING().get_logo_org_by_title(title), 'Логотип отсутвует в abitur.external_org_units'
        elif logo == 'no':
            assert 'None' == SETTING().get_logo_org_by_title(title), 'Логотип присутствует в abitur.external_org_units'

    @staticmethod
    def check_record_uni(title, url, login, password, active):
        time.sleep(0.5)
        assert (url, login, password, int(active)) == SETTING().get_data_uni_by_org_id(title, login), \
            'Отсутствует запись в abitur.uni_installations'

    def open_org_setting(self, setting):
        if setting == 'Информация об организации':
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ADMIN.info_page))).click()
        elif setting == 'Системы':
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ADMIN.system_page))).click()
            WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ADMIN.system_create)))
        elif setting == 'Приемные кампании':
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ADMIN.campaigns_page))).click()
            WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ADMIN.campaigns_create)))
        elif setting == 'Контакты':
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ADMIN.contact_page))).click()
            WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ADMIN.contact_create)))

    def create_new_org_setting(self, setting):
        time.sleep(0.5)
        if setting == 'Системы':
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ADMIN.system_create))).click()
            WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ADMIN.check_connection)))
        elif setting == 'Приемные кампании':
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ADMIN.campaigns_create))).click()
        elif setting == 'Контакты':
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ADMIN.contact_create))).click()
        assert '/create' in self.browser.current_url, 'В URL должен быть /create'
        time.sleep(0.5)

    def checking_the_stub_at_creation(self, setting, action):
        if setting == 'Приемные кампании':
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ADMIN.campaigns_create))).click()
            if action == 'Система отсутствует':
                assert WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ADMIN.stub_in_creation))), \
                    'Отсутствует заглушка при создании "ПК" без "Системы'
            elif action == 'Система недоступна':
                assert WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ALERT.alert_input1))), \
                    'Отсутствует заглушка при создании "ПК" без активной системы'
        elif setting == 'Контакты':
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ADMIN.contact_create))).click()
            assert WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ADMIN.stub_in_creation))), \
                'Отсутствует заглушка при создании "Контактов" без "ПК"'
        assert '/create' in self.browser.current_url, 'В URL должен быть /create'

    def click_check_connection(self, result):
        WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ADMIN.check_connection))).click()
        if result == 'Correct':
            assert WebDriverWait(self.browser, 8).until(EC.presence_of_element_located((ALERT.alert_correct))), \
                'ERROR: Предупреждение об успешном сохранение данных не появилось'
        elif result == 'Incorrect':
            time.sleep(0.5)
            assert WebDriverWait(self.browser, 8).until(EC.presence_of_element_located(ALERT.alert_incorrect)), \
                'ERROR: Предупреждение об ограничении не появилось'

    def perform_an_action_on_an_system(self, title, action, result):
        search_user = self.browser.find_elements(*CONTROL.search_service)[0]
        assert title in search_user.text, 'Ранее созданная запись не отображается'
        if action == 'edit':
            self.browser.find_elements(*CONTROL.edit_button)[0].click()
            WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ADMIN.field_input)))
            time.sleep(0.5)  # Задержка для локального стенда
        elif action == 'remove':
            self.browser.find_elements(*CONTROL.archive_button)[0].click()
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((PROFILE.delete_button))).click()
            if result == 'Correct':
                assert WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ALERT.alert_correct))), \
                    'ERROR: Предупреждение об успешном сохранение данных не появилось'
            elif result == 'Incorrect':
                assert WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ALERT.alert_in_modal))), \
                    'ERROR: Предупреждение об ошибке сохранения данных не появилось'

    @staticmethod
    def check_record_enrollment_campaigns(org, type_pk, system, title_pk, original_pk):
        assert (int(SETTING().get_id_campaign_types_by_title(type_pk)), int(SETTING().get_id_uni_installations_by_url(
            system)), title_pk, original_pk) == SETTING().get_data_enrollment_campaigns(org, title_pk), \
            'Некорректная запись в enrollment_campaigns'

    @staticmethod
    def check_record_enrollment_campaigns_disabled_at(title_org, title_pk, status):
        time.sleep(0.5)
        if status == 'inactive':
            assert 'None' != SETTING().get_disabled_at_enrollment_campaigns_by_title(title_org, title_pk), \
                'В колонке disabled_at должена быть дата'
        elif status == 'active':
            assert 'None' == SETTING().get_disabled_at_enrollment_campaigns_by_title(title_org, title_pk), \
                'В колонке disabled_at должен быть null'

    def perform_an_action_on_an_pk(self, title, action, result):
        search_user = self.browser.find_elements(*CONTROL.search_service)[-1]
        assert title in search_user.text, 'Ранее созданная запись не отображается'
        if action == 'edit':
            self.browser.find_elements(*CONTROL.edit_button)[-1].click()
            WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ADMIN.field_input)))
            time.sleep(0.5)  # Задержка для локального стенда
        elif action == 'remove':
            self.browser.find_elements(*CONTROL.archive_button)[-1].click()
            self.browser.find_elements(*PROFILE.delete_button)[1].click()
            if result == 'Correct':
                # WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_close_modal)))
                assert WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((ALERT.alert_correct))), \
                    'ERROR: Предупреждение об успешном сохранение данных не появилось'
            elif result == 'Incorrect':
                assert WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ALERT.alert_in_modal))), \
                    'ERROR: Предупреждение об ошибке сохранения данных не появилось'

    @staticmethod
    def check_record_general_external_org_contacts(title_org, title_pk, address, description, phone, faxes, email):
        assert ((address, description, [phone], [faxes],
                 [email]) == SETTING().get_data_general_settings_external_org_contacts(
            title_org, title_pk)), 'Некорректная запись "Общие настройки" в external_org_contacts'

    @staticmethod
    def check_record_executive_external_org_contacts(title_org, title_pk, name_sec, phone_sec, email_sec, office_sec,
                                                     position):
        assert (name_sec, phone_sec, email_sec, office_sec, position) == (SETTING().
            get_data_executive_external_org_contacts(
            title_org, title_pk)), 'Некорректная запись "Ответственный секретарь" в external_org_contacts'

    @staticmethod
    def check_record_working_hours_external_org_contacts(title_org, title_pk, with_time, by_time, day):
        assert (with_time, by_time) == (SETTING().get_data_working_hours_external_org_contacts(
            title_org, title_pk, day)), 'Некорректная запись "График работы приемной комиссии" в external_org_contacts'

    def perform_an_action_on_an_contact(self, title, action, result):
        search_user = self.browser.find_elements(*CONTROL.search_service)[-1]
        assert title in search_user.text, 'Ранее созданная запись не отображается'
        if action == 'edit':
            self.browser.find_elements(*CONTROL.edit_button)[-1].click()
            WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ADMIN.field_input)))
            time.sleep(0.5)  # Задержка для локального стенда
        elif action == 'remove':
            self.browser.find_elements(*CONTROL.archive_button)[-1].click()
            self.browser.find_elements(*PROFILE.delete_button)[-1].click()
            if result == 'Correct':
                WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_close_modal)))
                assert WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ALERT.alert_correct))), \
                    'ERROR: Предупреждение об успешном сохранение данных не появилось'
            elif result == 'Incorrect':
                assert WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ALERT.alert_in_modal))), \
                    'ERROR: Предупреждение об ошибке сохранения данных не появилось'

    # Нормативные документы ############################################################################################
    @staticmethod
    def check_record_regulation_categories(title, short_title, active):
        assert (title, short_title, active) == SETTING().get_data_regulation_categories(title), \
            'Некорректная запись в regulation_categories'

    @staticmethod
    def check_record_regulation_types(regulation_category_id, title, short_title, active):
        assert (int(regulation_category_id), title, short_title, active) == SETTING().get_data_regulation_types(
            title), 'Некорректная запись в regulation_types '

    def perform_an_action_on_an_document(self, title, action, result):
        search_title = 0
        while True:
            display_org = self.browser.find_elements(*ADMIN.search_type_chat)[search_title]
            if title == display_org.text:
                time.sleep(0.5)  # Задержка для локального стенда
                if action == 'edit':
                    self.browser.find_elements(*CONTROL.edit_button)[search_title].click()
                    WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ADMIN.field_input)))
                    time.sleep(0.5)  # Задержка для локального стенда
                elif action == 'remove':
                    self.browser.find_elements(*CONTROL.archive_button)[search_title].click()
                    WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((PROFILE.delete_button))).click()
                    if result == 'Correct':
                        WebDriverWait(self.browser, 4).until(EC.presence_of_element_located((CONTROL.wait_close_modal)))
                        assert WebDriverWait(self.browser, 3).until(
                            EC.presence_of_element_located((ALERT.alert_correct))), \
                            'ERROR: Предупреждение об успешном сохранение данных не появилось'
                    elif result == 'Incorrect':
                        assert WebDriverWait(self.browser, 3).until(
                            EC.presence_of_element_located((ALERT.alert_in_modal))), \
                            'ERROR: Предупреждение об ошибке сохранения данных не появилось'
                break
            elif search_title != 14:
                search_title += 1
                continue
            elif search_title == 14:
                last_page = self.browser.find_elements(*ADMIN.page_next)
                last_page[-1].click()
                time.sleep(0.5)  # Задержка для локального стенда
                search_title = 0
                continue
            else:
                break

    def switch_the_section_of_the_regulatory_document(self, selection):
        if selection == 'Категории':
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ADMIN.categories_docs))).click()
        elif selection == 'Типы':
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ADMIN.types_docs))).click()
        time.sleep(0.5)

    def select_categories_regulatory_document(self, title_category):
        select = Select(self.browser.find_elements(*ADMIN.select_field)[0])
        select.select_by_value(title_category)

    def select_disable_categories_regulatory_document(self, title_category):
        try:
            self.browser.find_element_by_link_text(title_category)
            assert False
        except NoSuchElementException:
            assert True

    def add_uploadFiles(self, icon):
        element = self.browser.find_element(*ADMIN.uploadFiles)
        current_dir = os.path.abspath(os.path.dirname(__file__))
        file_path = os.path.join(current_dir, icon)
        element.send_keys(file_path)

    def checking_validators_regulations(self, result, field):
        self.browser.find_element(*ADMIN.save_button).click()
        time.sleep(0.5)  # Задержка для локального стенда
        if result == 'Correct':
            assert WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ADMIN.add_new))), \
                'Предупреждение об успешном сохранение данных не появилось'
        elif result == 'Incorrect':
            time.sleep(0.5)
            assert WebDriverWait(self.browser, 3).until(EC.text_to_be_present_in_element((ALERT.alert_input), field)), \
                'Предупреждение об ограничении не появилось'

    @staticmethod
    def check_record_regulation(title, short_title, regulation_type_id, description, link, active, title_org, title_pk):
        assert (title, short_title, int(regulation_type_id), description, link, active,
                int(SETTING().get_id_org_by_title(title_org)), int(SETTING().get_enrollment_campaigns_id(title_pk))) \
               == SETTING().get_data_regulations(title), 'Некорректная запись в regulations'

    ####################################################################################################################
    def open_setting_pk(self, setting_pk):
        if setting_pk == 'EduLevel':
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ADMIN.setting_EduLevel))).click()
        elif setting_pk == 'RequestType':
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ADMIN.setting_RequestType))).click()
        elif setting_pk == 'ScanCopies':
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ADMIN.setting_ScanCopies))).click()
        elif setting_pk == 'OrderOfSpecialties':
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ADMIN.setting_OrderOfSpecialties))).click()
        elif setting_pk == 'FirstStep':
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ADMIN.setting_FirstStep))).click()
        elif setting_pk == 'SecondStep':
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ADMIN.setting_SecondStep))).click()
        elif setting_pk == 'ThirdStep':
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ADMIN.setting_ThirdStep))).click()
        elif setting_pk == 'CompetitionLists':
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ADMIN.setting_CompetitionLists))).click()
        elif setting_pk == 'EntrantsSearch':
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ADMIN.setting_EntrantsSearch))).click()
        elif setting_pk == 'Contracts':
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ADMIN.setting_Contracts))).click()
        elif setting_pk == 'AbiturMain':
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ADMIN.setting_AbiturMain))).click()
        elif setting_pk == 'Regulations':
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ADMIN.setting_Regulations))).click()
        elif setting_pk == 'Tips_for_applicant':
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ADMIN.setting_TipsApplicant))).click()
        elif setting_pk == 'EntrantLists':
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ADMIN.setting_EntrantLists))).click()
        elif setting_pk == 'DocumentDeadlines':
            WebDriverWait(self.browser, 4).until(EC.element_to_be_clickable((ADMIN.setting_DocumentDeadlines))).click()

        time.sleep(0.5)  # Задержка для локального стенда

    def select_checkbox_setting_EduLevel(self, doc):
        if doc == 'BASIC_GENERAL':
            self.browser.find_elements(*ADMIN.select_checkbox)[0].click()
        elif doc == 'SECONDARY_GENERAL':
            self.browser.find_elements(*ADMIN.select_checkbox)[1].click()
        elif doc == 'SECONDARY_VOCATIONAL':
            self.browser.find_elements(*ADMIN.select_checkbox)[2].click()
        elif doc == 'UNDERGRADUATE':
            self.browser.find_elements(*ADMIN.select_checkbox)[3].click()
        elif doc == 'MASTER':
            self.browser.find_elements(*ADMIN.select_checkbox)[4].click()
        elif doc == 'HIGHEST_QUALIFICATION':
            self.browser.find_elements(*ADMIN.select_checkbox)[5].click()
        time.sleep(0.7)  # Задержка для локального стенда

    @staticmethod
    def check_records_EduLevel(title_pk, BASIC_GENERAL, SECONDARY_GENERAL, SECONDARY_VOCATIONAL,
                               UNDERGRADUATE, MASTER, HIGHEST_QUALIFICATION):
        assert [BASIC_GENERAL, SECONDARY_GENERAL, SECONDARY_VOCATIONAL, UNDERGRADUATE, MASTER, HIGHEST_QUALIFICATION] \
               == (SETTING().get_data_basic_education_levels(title_pk)), \
            'Некорректная запись в settings.basic_education_levels'

    def select_checkbox_setting_RequestType(self, doc):
        if doc == 'SPO':
            self.browser.find_elements(*ADMIN.select_checkbox)[0].click()
        elif doc == 'BACH_SPEC':
            self.browser.find_elements(*ADMIN.select_checkbox)[1].click()
        elif doc == 'MASTER':
            self.browser.find_elements(*ADMIN.select_checkbox)[2].click()
        elif doc == 'POSTGRADUATE':
            self.browser.find_elements(*ADMIN.select_checkbox)[3].click()
        elif doc == 'RESIDENCY':
            self.browser.find_elements(*ADMIN.select_checkbox)[4].click()
        time.sleep(0.7)  # Задержка для локального стенда

    @staticmethod
    def check_records_RequestType(title_pk, SPO, BACH_SPEC, MASTER, POSTGRADUATE, RESIDENCY):
        assert [SPO, BACH_SPEC, MASTER, POSTGRADUATE, RESIDENCY] == \
               (SETTING().get_data_implemented_education_levels(title_pk)), \
            'Некорректная запись в settings.implemented_education_levels'

    def radiobutton_setting_ScanCopies(self, doc, field_type, doc_size):
        if doc == ScanCopies.IDENTITY_CARD:
            field = 0
            if field_type == ScanCopies.DOCUMENT_OPTIONAL:
                self.browser.find_elements(*ADMIN.radiobutton_OPTIONAL)[field].click()
            elif field_type == ScanCopies.DOCUMENT_REQUIRED:
                self.browser.find_elements(*ADMIN.radiobutton_REQUIRED)[field].click()
            elif field_type == ScanCopies.DOCUMENT_NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.radiobutton_NOT_REQUIRED)[field].click()
        elif doc == ScanCopies.EDUCATION_DOCUMENT:
            field = 1
            if field_type == ScanCopies.DOCUMENT_OPTIONAL:
                self.browser.find_elements(*ADMIN.radiobutton_OPTIONAL)[field].click()
            elif field_type == ScanCopies.DOCUMENT_REQUIRED:
                self.browser.find_elements(*ADMIN.radiobutton_REQUIRED)[field].click()
            elif field_type == ScanCopies.DOCUMENT_NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.radiobutton_NOT_REQUIRED)[field].click()
        elif doc == ScanCopies.SNILS:
            field = 2
            if field_type == ScanCopies.DOCUMENT_OPTIONAL:
                self.browser.find_elements(*ADMIN.radiobutton_OPTIONAL)[field].click()
            elif field_type == ScanCopies.DOCUMENT_REQUIRED:
                self.browser.find_elements(*ADMIN.radiobutton_REQUIRED)[field].click()
            elif field_type == ScanCopies.DOCUMENT_NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.radiobutton_NOT_REQUIRED)[field].click()
        elif doc == ScanCopies.INDIVIDUAL_ACHIEVEMENTS:
            field = 3
            if field_type == ScanCopies.DOCUMENT_OPTIONAL:
                self.browser.find_elements(*ADMIN.radiobutton_OPTIONAL)[field].click()
            elif field_type == ScanCopies.DOCUMENT_REQUIRED:
                self.browser.find_elements(*ADMIN.radiobutton_REQUIRED)[field].click()
            elif field_type == ScanCopies.DOCUMENT_NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.radiobutton_NOT_REQUIRED)[field].click()
        elif doc == ScanCopies.DOCUMENT_PROVING_DISABILITY:
            field = 4
            if field_type == ScanCopies.DOCUMENT_OPTIONAL:
                self.browser.find_elements(*ADMIN.radiobutton_OPTIONAL)[field].click()
            elif field_type == ScanCopies.DOCUMENT_REQUIRED:
                self.browser.find_elements(*ADMIN.radiobutton_REQUIRED)[field].click()
            elif field_type == ScanCopies.DOCUMENT_NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.radiobutton_NOT_REQUIRED)[field].click()
        elif doc == ScanCopies.TARGETED_TRAINING_CONTRACT:
            field = 5
            if field_type == ScanCopies.DOCUMENT_OPTIONAL:
                self.browser.find_elements(*ADMIN.radiobutton_OPTIONAL)[field].click()
            elif field_type == ScanCopies.DOCUMENT_REQUIRED:
                self.browser.find_elements(*ADMIN.radiobutton_REQUIRED)[field].click()
            elif field_type == ScanCopies.DOCUMENT_NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.radiobutton_NOT_REQUIRED)[field].click()
        elif doc == ScanCopies.BENEFIT_FOR_DORM:
            field = 6
            if field_type == ScanCopies.DOCUMENT_OPTIONAL:
                self.browser.find_elements(*ADMIN.radiobutton_OPTIONAL)[field].click()
            elif field_type == ScanCopies.DOCUMENT_REQUIRED:
                self.browser.find_elements(*ADMIN.radiobutton_REQUIRED)[field].click()
            elif field_type == ScanCopies.DOCUMENT_NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.radiobutton_NOT_REQUIRED)[field].click()
        elif doc == ScanCopies.CONSENT_TO_PERSONAL_DATA:
            field = 7
            if field_type == ScanCopies.DOCUMENT_OPTIONAL:
                self.browser.find_elements(*ADMIN.radiobutton_OPTIONAL)[field].click()
            elif field_type == ScanCopies.DOCUMENT_REQUIRED:
                self.browser.find_elements(*ADMIN.radiobutton_REQUIRED)[field].click()
            elif field_type == ScanCopies.DOCUMENT_NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.radiobutton_NOT_REQUIRED)[field].click()
        elif doc == ScanCopies.APPLICATION:
            field = 8
            if field_type == ScanCopies.DOCUMENT_OPTIONAL:
                self.browser.find_elements(*ADMIN.radiobutton_OPTIONAL)[field].click()
            elif field_type == ScanCopies.DOCUMENT_REQUIRED:
                self.browser.find_elements(*ADMIN.radiobutton_REQUIRED)[field].click()
            elif field_type == ScanCopies.DOCUMENT_NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.radiobutton_NOT_REQUIRED)[field].click()
        elif doc == ScanCopies.CONSENT_TO_ENROLLMENT:
            field = 9
            if field_type == ScanCopies.DOCUMENT_OPTIONAL:
                self.browser.find_elements(*ADMIN.radiobutton_OPTIONAL)[field].click()
            elif field_type == ScanCopies.DOCUMENT_REQUIRED:
                self.browser.find_elements(*ADMIN.radiobutton_REQUIRED)[field].click()
            elif field_type == ScanCopies.DOCUMENT_NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.radiobutton_NOT_REQUIRED)[field].click()
        elif doc == ScanCopies.DOCUMENT_SIZE:
            self.browser.find_element(*ADMIN.field_input).clear()
            self.browser.find_element(*ADMIN.field_input).send_keys(doc_size)
            self.browser.find_element(*ADMIN.save_button).click()
            if field_type == 'Correct':
                assert WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ALERT.alert_correct))), \
                    'ERROR: Предупреждение об успешном сохранение данных не появилось'
            elif field_type == 'Incorrect':
                assert WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ALERT.alert_in_modal))), \
                    'ERROR: Предупреждение об ошибке сохранения данных не появилось'
        elif doc == ScanCopies.IDENTITY_CARD_FIELDS_COUNT:
            field = 10
            if field_type == ScanCopies.DOCUMENT_REQUIRED:
                self.browser.find_elements(*ADMIN.radiobutton_REQUIRED)[field].click()
            elif field_type == ScanCopies.DOCUMENT_FIELDS_DEFAULT_COUNT:
                self.browser.find_elements(*ADMIN.radiobutton_DEFAULT)[field].click()
        elif doc == ScanCopies.EDUCATION_DOCUMENT_FIELDS_COUNT:
            field = 11
            if field_type == ScanCopies.DOCUMENT_REQUIRED:
                self.browser.find_elements(*ADMIN.radiobutton_REQUIRED)[field].click()
            elif field_type == ScanCopies.DOCUMENT_FIELDS_DEFAULT_COUNT:
                self.browser.find_elements(*ADMIN.radiobutton_DEFAULT)[field].click()
        time.sleep(0.5)  # Задержка для локального стенда

    @staticmethod
    def check_records_ScanCopies(title_pk, IDENTITY_CARD, EDUCATION_DOCUMENT, SNILS, INDIVIDUAL_ACHIEVEMENTS,
                                 DOCUMENT_PROVING_DISABILITY, TARGETED_TRAINING_CONTRACT, BENEFIT_FOR_DORM,
                                 CONSENT_TO_PERSONAL_DATA, APPLICATION, CONSENT_TO_ENROLLMENT, DOCUMENT_SIZE,
                                 IDENTITY_CARD_FIELDS_COUNT, EDUCATION_DOCUMENT_FIELDS_COUNT):
        assert (IDENTITY_CARD, EDUCATION_DOCUMENT, SNILS, INDIVIDUAL_ACHIEVEMENTS, DOCUMENT_PROVING_DISABILITY,
                TARGETED_TRAINING_CONTRACT, BENEFIT_FOR_DORM, CONSENT_TO_PERSONAL_DATA, APPLICATION,
                CONSENT_TO_ENROLLMENT, int(DOCUMENT_SIZE), IDENTITY_CARD_FIELDS_COUNT, EDUCATION_DOCUMENT_FIELDS_COUNT) \
               == SETTING().get_data_abitur_scan_copies(title_pk), 'Некорректная запись settings.abitur_scan_copies'

    def select_position_OrderOfSpecialties(self, doc, position):
        if doc == OrderOfSpecialties.Construction_id:
            self.browser.find_elements(*ADMIN.field_input)[0].clear()
            self.browser.find_elements(*ADMIN.field_input)[0].send_keys(position)
        elif doc == OrderOfSpecialties.Informatics_id:
            self.browser.find_elements(*ADMIN.field_input)[1].clear()
            self.browser.find_elements(*ADMIN.field_input)[1].send_keys(position)
        elif doc == OrderOfSpecialties.Instrumentation_id:
            self.browser.find_elements(*ADMIN.field_input)[2].clear()
            self.browser.find_elements(*ADMIN.field_input)[2].send_keys(position)
        elif doc == OrderOfSpecialties.Land_management_id:
            self.browser.find_elements(*ADMIN.field_input)[3].clear()
            self.browser.find_elements(*ADMIN.field_input)[3].send_keys(position)
        elif doc == OrderOfSpecialties.Economy_id:
            self.browser.find_elements(*ADMIN.field_input)[4].clear()
            self.browser.find_elements(*ADMIN.field_input)[4].send_keys(position)
        elif doc == OrderOfSpecialties.Commercial_business_id:
            self.browser.find_elements(*ADMIN.field_input)[5].clear()
            self.browser.find_elements(*ADMIN.field_input)[5].send_keys(position)
        time.sleep(0.5)  # Задержка для локального стенда

    def check_validation_OrderOfSpecialties(self, result, field):
        self.browser.find_element(*ADMIN.apply_placement).click()
        if result == 'Correct':
            assert WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ALERT.alert_correct))), \
                'ERROR: Предупреждение об успешном сохранение данных не появилось'
        elif result == 'IncorrectAll':
            assert WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ALERT.alert_input1))), \
                'ERROR: Предупреждение об ошибке сохранения данных не появилось'
        elif result == 'Incorrect':
            assert WebDriverWait(self.browser, 3).until(EC.text_to_be_present_in_element((ALERT.alert_input), field)), \
                'ERROR: Предупреждение об ошибке сохранения данных не появилось'

    def return_default_OrderOfSpecialties(self):
        self.browser.find_element(*ADMIN.default_placement).click()
        assert WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ALERT.alert_correct))), \
            'ERROR: Предупреждение об успешном сохранение данных не появилось'

    @staticmethod
    def check_records_position_OrderOfSpecialties(title_pk, Construction, Informatics, Instrumentation,
                                                  Land_management, Economy, Commercial_business):
        assert (Construction, Informatics, Instrumentation, Land_management, Economy, Commercial_business) \
               == SETTING().get_data_order_of_specialties(title_pk), \
            'Некорректная запись в settings.order_of_specialties'

    @staticmethod
    def check_records_default_position_OrderOfSpecialties(title_pk):
        time.sleep(0.5)
        assert [] == SETTING().get_default_data_order_of_specialties(title_pk), \
            'Некорректная запись в settings.order_of_specialties'

    def select_radiobutton_FirstStep(self, doc, field_type):
        if doc == FirstStep.RETURN_WAY_INFO:
            if field_type == FirstStep.RETURN_WAY_INFO_YES:
                self.browser.find_elements(*ADMIN.select_radiobutton)[0].click()
            elif field_type == FirstStep.RETURN_WAY_INFO_NO:
                self.browser.find_elements(*ADMIN.select_radiobutton)[1].click()
        elif doc == FirstStep.EXAM_SCORES_AND_EXAM_YEARS:
            if field_type == FirstStep.EXAM_SCORES_AND_EXAM_YEARS_NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[2].click()
            elif field_type == FirstStep.EXAM_SCORES_AND_EXAM_YEARS_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[3].click()
            elif field_type == FirstStep.EXAM_SCORES_AND_EXAM_YEARS_NO:
                self.browser.find_elements(*ADMIN.select_radiobutton)[4].click()
        elif doc == FirstStep.CHECK_EXAM_SCORES:
            if field_type == FirstStep.CHECK_EXAM_SCORES_DISALLOW:
                self.browser.find_elements(*ADMIN.select_radiobutton)[5].click()
            elif field_type == FirstStep.CHECK_EXAM_SCORES_WARN:
                self.browser.find_elements(*ADMIN.select_radiobutton)[6].click()
            elif field_type == FirstStep.CHECK_EXAM_SCORES_NO:
                self.browser.find_elements(*ADMIN.select_radiobutton)[7].click()
        elif doc == FirstStep.PARTICIPATION_IN_TESTS:
            if field_type == FirstStep.PARTICIPATION_IN_TESTS_AVAILABLE:
                self.browser.find_elements(*ADMIN.select_radiobutton)[8].click()
            elif field_type == FirstStep.PARTICIPATION_IN_TESTS_NOT_AVAILABLE:
                self.browser.find_elements(*ADMIN.select_radiobutton)[9].click()
        elif doc == FirstStep.BASED_ON_UNIVERSITY_MATERIALS:
            if field_type == FirstStep.BASED_ON_UNIVERSITY_MATERIALS_YES:
                self.browser.find_elements(*ADMIN.select_radiobutton)[10].click()
            elif field_type == FirstStep.BASED_ON_UNIVERSITY_MATERIALS_NO:
                self.browser.find_elements(*ADMIN.select_radiobutton)[11].click()
        elif doc == FirstStep.SELECTING_TESTS:
            if field_type == FirstStep.SELECTING_TESTS_YES:
                self.browser.find_elements(*ADMIN.select_radiobutton)[12].click()
            elif field_type == FirstStep.SELECTING_TESTS_NO:
                self.browser.find_elements(*ADMIN.select_radiobutton)[13].click()
        elif doc == FirstStep.NEED_FOR_SPECIAL_CONDITIONS_INFO:
            if field_type == FirstStep.NEED_FOR_SPECIAL_CONDITIONS_INFO_YES:
                self.browser.find_elements(*ADMIN.select_radiobutton)[14].click()
            elif field_type == FirstStep.NEED_FOR_SPECIAL_CONDITIONS_INFO_NO:
                self.browser.find_elements(*ADMIN.select_radiobutton)[15].click()
        elif doc == FirstStep.PREFERRED_FORM_INFO:
            if field_type == FirstStep.PREFERRED_FORM_INFO_YES:
                self.browser.find_elements(*ADMIN.select_radiobutton)[16].click()
            elif field_type == FirstStep.PREFERRED_FORM_INFO_NO:
                self.browser.find_elements(*ADMIN.select_radiobutton)[17].click()
        elif doc == FirstStep.PARTICIPATION_IN_OLYMPIADS_INFO:
            if field_type == FirstStep.PARTICIPATION_IN_OLYMPIADS_INFO_YES:
                self.browser.find_elements(*ADMIN.select_radiobutton)[18].click()
            elif field_type == FirstStep.PARTICIPATION_IN_OLYMPIADS_INFO_NO:
                self.browser.find_elements(*ADMIN.select_radiobutton)[19].click()
        elif doc == FirstStep.SPECIAL_RIGHTS_INFO:
            if field_type == FirstStep.SPECIAL_RIGHTS_INFO_YES:
                self.browser.find_elements(*ADMIN.select_radiobutton)[20].click()
            elif field_type == FirstStep.SPECIAL_RIGHTS_INFO_NO:
                self.browser.find_elements(*ADMIN.select_radiobutton)[21].click()
        elif doc == FirstStep.CONCLUDED_AGREEMENT_INFO:
            if field_type == FirstStep.CONCLUDED_AGREEMENT_INFO_YES:
                self.browser.find_elements(*ADMIN.select_radiobutton)[22].click()
            elif field_type == FirstStep.CONCLUDED_AGREEMENT_INFO_NO:
                self.browser.find_elements(*ADMIN.select_radiobutton)[23].click()
        time.sleep(0.3)

    @staticmethod
    def check_records_FirstStep(title_pk, RETURN_WAY_INFO, EXAM_SCORES_AND_EXAM_YEARS, CHECK_EXAM_SCORES,
                                PARTICIPATION_IN_TESTS, BASED_ON_UNIVERSITY_MATERIALS, SELECTING_TESTS,
                                NEED_FOR_SPECIAL_CONDITIONS_INFO, PREFERRED_FORM_INFO, PARTICIPATION_IN_OLYMPIADS_INFO,
                                SPECIAL_RIGHTS_INFO, CONCLUDED_AGREEMENT_INFO):
        assert (RETURN_WAY_INFO, EXAM_SCORES_AND_EXAM_YEARS, CHECK_EXAM_SCORES, PARTICIPATION_IN_TESTS,
                BASED_ON_UNIVERSITY_MATERIALS, SELECTING_TESTS, NEED_FOR_SPECIAL_CONDITIONS_INFO, PREFERRED_FORM_INFO,
                PARTICIPATION_IN_OLYMPIADS_INFO, SPECIAL_RIGHTS_INFO, CONCLUDED_AGREEMENT_INFO) == \
               SETTING().get_data_abitur_first_step(title_pk), 'Некорректная запись в settings.abitur_first_step'

    def select_radiobutton_SecondStep(self, doc, field_type):
        if doc == SecondStep.EDUCATIONAL_PROGRAMS_CHOICE:
            if field_type == SecondStep.EDUCATIONAL_PROGRAMS_CHOICE_YES:
                self.browser.find_elements(*ADMIN.select_radiobutton)[0].click()
            elif field_type == SecondStep.EDUCATIONAL_PROGRAMS_CHOICE_NO:
                self.browser.find_elements(*ADMIN.select_radiobutton)[1].click()
        elif doc == SecondStep.PLACES_COLUMN:
            if field_type == SecondStep.PLACES_COLUMN_YES:
                self.browser.find_elements(*ADMIN.select_radiobutton)[2].click()
            elif field_type == SecondStep.PLACES_COLUMN_NO:
                self.browser.find_elements(*ADMIN.select_radiobutton)[3].click()
        elif doc == SecondStep.REQUESTS_COLUMN:
            if field_type == SecondStep.REQUESTS_COLUMN_YES:
                self.browser.find_elements(*ADMIN.select_radiobutton)[4].click()
            elif field_type == SecondStep.REQUESTS_COLUMN_NO:
                self.browser.find_elements(*ADMIN.select_radiobutton)[5].click()
        elif doc == SecondStep.CHOICE_BUDGET_AND_CONTRACT:
            if field_type == SecondStep.CHOICE_BUDGET_AND_CONTRACT_YES:
                self.browser.find_elements(*ADMIN.select_radiobutton)[6].click()
            elif field_type == SecondStep.CHOICE_BUDGET_AND_CONTRACT_NOT_NO:
                self.browser.find_elements(*ADMIN.select_radiobutton)[7].click()
        elif doc == SecondStep.HIDE_CONTESTS:
            if field_type == SecondStep.HIDE_CONTESTS_YES:
                self.browser.find_elements(*ADMIN.select_radiobutton)[8].click()
            elif field_type == SecondStep.HIDE_CONTESTS_NO:
                self.browser.find_elements(*ADMIN.select_radiobutton)[9].click()
        elif doc == SecondStep.PRIORITIES_CHOICE:
            if field_type == SecondStep.PRIORITIES_CHOICE_YES:
                self.browser.find_elements(*ADMIN.select_radiobutton)[10].click()
            elif field_type == SecondStep.PRIORITIES_CHOICE_NO:
                self.browser.find_elements(*ADMIN.select_radiobutton)[11].click()
        elif doc == SecondStep.EDUCATIONAL_PROGRAMS_AND_PRIORITIES_CHOICE:
            if field_type == SecondStep.EDUCATIONAL_PROGRAMS_AND_PRIORITIES_CHOICE_NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[12].click()
            elif field_type == SecondStep.EDUCATIONAL_PROGRAMS_AND_PRIORITIES_CHOICE_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[13].click()
            elif field_type == SecondStep.EDUCATIONAL_PROGRAMS_AND_PRIORITIES_CHOICE_NO:
                self.browser.find_elements(*ADMIN.select_radiobutton)[14].click()
        time.sleep(0.4)

    @staticmethod
    def check_records_SecondStep(title_pk, EDUCATIONAL_PROGRAMS_CHOICE, PLACES_COLUMN, REQUESTS_COLUMN,
                                 CHOICE_BUDGET_AND_CONTRACT, HIDE_CONTESTS, PRIORITIES_CHOICE,
                                 EDUCATIONAL_PROGRAMS_AND_PRIORITIES_CHOICE):
        time.sleep(0.3)
        assert (EDUCATIONAL_PROGRAMS_CHOICE, PLACES_COLUMN, REQUESTS_COLUMN, CHOICE_BUDGET_AND_CONTRACT, HIDE_CONTESTS,
                PRIORITIES_CHOICE, EDUCATIONAL_PROGRAMS_AND_PRIORITIES_CHOICE) == \
               SETTING().get_data_abitur_second_step(title_pk), 'Некорректная запись в settings.abitur_second_step'

    def select_radiobutton_CompetitionLists(self, doc, field_type):
        if doc == CompetitionLists.COMPETITION_LISTS_SECTION:
            if field_type == CompetitionLists.SHOW:
                self.browser.find_elements(*ADMIN.select_radiobutton)[0].click()
            elif field_type == CompetitionLists.NOT_SHOW:
                self.browser.find_elements(*ADMIN.select_radiobutton)[1].click()
        elif doc == CompetitionLists.EDUCATIONAL_PROGRAMS_COMPETITION_LISTS:
            if field_type == CompetitionLists.SHOW:
                self.browser.find_elements(*ADMIN.select_radiobutton)[2].click()
            elif field_type == CompetitionLists.NOT_SHOW:
                self.browser.find_elements(*ADMIN.select_radiobutton)[3].click()
        elif doc == CompetitionLists.EDUCATIONAL_PROGRAMS_COMPETITION_CARD:
            if field_type == CompetitionLists.SHOW:
                self.browser.find_elements(*ADMIN.select_radiobutton)[4].click()
            elif field_type == CompetitionLists.NOT_SHOW:
                self.browser.find_elements(*ADMIN.select_radiobutton)[5].click()
        elif doc == CompetitionLists.COMPETITION_CARD_LINKS:
            if field_type == CompetitionLists.SHOW:
                self.browser.find_elements(*ADMIN.select_radiobutton)[6].click()
            elif field_type == CompetitionLists.NOT_SHOW:
                self.browser.find_elements(*ADMIN.select_radiobutton)[7].click()
        elif doc == CompetitionLists.SNILS_OR_UNIQUE_NUMBER:
            if field_type == CompetitionLists.SHOW:
                self.browser.find_elements(*ADMIN.select_radiobutton)[8].click()
            elif field_type == CompetitionLists.NOT_SHOW:
                self.browser.find_elements(*ADMIN.select_radiobutton)[9].click()
        elif doc == CompetitionLists.TARGETED_ADMISSION:
            if field_type == CompetitionLists.SHOW:
                self.browser.find_elements(*ADMIN.select_radiobutton)[10].click()
            elif field_type == CompetitionLists.SHOW_APPLICATION_NUMBER:
                self.browser.find_elements(*ADMIN.select_radiobutton)[11].click()
            elif field_type == CompetitionLists.NOT_SHOW:
                self.browser.find_elements(*ADMIN.select_radiobutton)[12].click()
        elif doc == CompetitionLists.WITHOUT_ENTRANCE_EXAMINATIONS:
            if field_type == CompetitionLists.SHOW:
                self.browser.find_elements(*ADMIN.select_radiobutton)[13].click()
            elif field_type == CompetitionLists.NOT_SHOW:
                self.browser.find_elements(*ADMIN.select_radiobutton)[14].click()
        elif doc == CompetitionLists.AVERAGE_MARK:
            if field_type == CompetitionLists.SHOW:
                self.browser.find_elements(*ADMIN.select_radiobutton)[15].click()
            elif field_type == CompetitionLists.NOT_SHOW:
                self.browser.find_elements(*ADMIN.select_radiobutton)[16].click()
        elif doc == CompetitionLists.COMPETITION_POINTS_SUM:
            if field_type == CompetitionLists.SHOW:
                self.browser.find_elements(*ADMIN.select_radiobutton)[17].click()
            elif field_type == CompetitionLists.NOT_SHOW:
                self.browser.find_elements(*ADMIN.select_radiobutton)[18].click()
        elif doc == CompetitionLists.ENTRANCE_EXAMINATIONS_POINTS_SUM:
            if field_type == CompetitionLists.SHOW:
                self.browser.find_elements(*ADMIN.select_radiobutton)[19].click()
            elif field_type == CompetitionLists.NOT_SHOW:
                self.browser.find_elements(*ADMIN.select_radiobutton)[20].click()
        elif doc == CompetitionLists.INDIVIDUAL_ACHIEVEMENTS_POINTS_SUM:
            if field_type == CompetitionLists.SHOW:
                self.browser.find_elements(*ADMIN.select_radiobutton)[21].click()
            elif field_type == CompetitionLists.NOT_SHOW:
                self.browser.find_elements(*ADMIN.select_radiobutton)[22].click()
        elif doc == CompetitionLists.ENTRANCE_EXAMINATIONS_POINTS_DETAILING:
            if field_type == CompetitionLists.SHOW:
                self.browser.find_elements(*ADMIN.select_radiobutton)[23].click()
            elif field_type == CompetitionLists.NOT_SHOW:
                self.browser.find_elements(*ADMIN.select_radiobutton)[24].click()
        elif doc == CompetitionLists.INDIVIDUAL_ACHIEVEMENTS_POINTS_DETAILING:
            if field_type == CompetitionLists.SHOW:
                self.browser.find_elements(*ADMIN.select_radiobutton)[25].click()
            elif field_type == CompetitionLists.NOT_SHOW:
                self.browser.find_elements(*ADMIN.select_radiobutton)[26].click()
        elif doc == CompetitionLists.PREEMPTIVE_RIGHTS:
            if field_type == CompetitionLists.SHOW:
                self.browser.find_elements(*ADMIN.select_radiobutton)[27].click()
            elif field_type == CompetitionLists.NOT_SHOW:
                self.browser.find_elements(*ADMIN.select_radiobutton)[28].click()
        elif doc == CompetitionLists.ORIGINAL_EDUCATIONAL_DOCUMENT:
            if field_type == CompetitionLists.SHOW:
                self.browser.find_elements(*ADMIN.select_radiobutton)[29].click()
            elif field_type == CompetitionLists.NOT_SHOW:
                self.browser.find_elements(*ADMIN.select_radiobutton)[30].click()
        elif doc == CompetitionLists.CONSENT_TO_ENROLLMENT:
            if field_type == CompetitionLists.SHOW:
                self.browser.find_elements(*ADMIN.select_radiobutton)[31].click()
            elif field_type == CompetitionLists.NOT_SHOW:
                self.browser.find_elements(*ADMIN.select_radiobutton)[32].click()
        elif doc == CompetitionLists.STATEMENT_STATE:
            if field_type == CompetitionLists.SHOW:
                self.browser.find_elements(*ADMIN.select_radiobutton)[33].click()
            elif field_type == CompetitionLists.NOT_SHOW:
                self.browser.find_elements(*ADMIN.select_radiobutton)[34].click()
        time.sleep(0.5)

    @staticmethod
    def check_records_CompetitionLists(title_pk, EDUCATIONAL_PROGRAMS_COMPETITION_LISTS,
                                       EDUCATIONAL_PROGRAMS_COMPETITION_CARD, SNILS_OR_UNIQUE_NUMBER,
                                       TARGETED_ADMISSION, WITHOUT_ENTRANCE_EXAMINATIONS, AVERAGE_MARK,
                                       COMPETITION_POINTS_SUM, ENTRANCE_EXAMINATIONS_POINTS_SUM,
                                       INDIVIDUAL_ACHIEVEMENTS_POINTS_SUM, ENTRANCE_EXAMINATIONS_POINTS_DETAILING,
                                       INDIVIDUAL_ACHIEVEMENTS_POINTS_DETAILING, PREEMPTIVE_RIGHTS,
                                       ORIGINAL_EDUCATIONAL_DOCUMENT, CONSENT_TO_ENROLLMENT, STATEMENT_STATE):
        time.sleep(0.5)
        assert (EDUCATIONAL_PROGRAMS_COMPETITION_LISTS, EDUCATIONAL_PROGRAMS_COMPETITION_CARD, SNILS_OR_UNIQUE_NUMBER,
                TARGETED_ADMISSION, WITHOUT_ENTRANCE_EXAMINATIONS, AVERAGE_MARK, COMPETITION_POINTS_SUM,
                ENTRANCE_EXAMINATIONS_POINTS_SUM, INDIVIDUAL_ACHIEVEMENTS_POINTS_SUM,
                ENTRANCE_EXAMINATIONS_POINTS_DETAILING, INDIVIDUAL_ACHIEVEMENTS_POINTS_DETAILING, PREEMPTIVE_RIGHTS,
                ORIGINAL_EDUCATIONAL_DOCUMENT, CONSENT_TO_ENROLLMENT, STATEMENT_STATE) == SETTING(). \
                   get_data_abitur_competition_lists(
            title_pk), 'Некорректная запись в settings.abitur_competition_lists'

    def select_radiobutton_ContractCommon(self, doc, field_type, quantity):
        if doc == ContractCommon.CONTRACT_REQUEST_COUNT:
            self.browser.find_element(*ADMIN.field_input).clear()
            self.browser.find_element(*ADMIN.field_input).send_keys(quantity)
        elif doc == ContractCommon.MOTHER_CERT_SCAN:
            if field_type == ContractAction.REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[0].click()
            elif field_type == ContractAction.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[1].click()
            elif field_type == ContractAction.NO:
                self.browser.find_elements(*ADMIN.select_radiobutton)[2].click()
        time.sleep(0.3)  # Задержка для локального стенда

    def select_radiobutton_ContractEntrantPersonType(self, doc, field_type):
        if doc == ContractEntrantPersonType.PERSONAL_DATA_AGREEMENT:
            if field_type == ContractAction.YES:
                self.browser.find_elements(*ADMIN.select_radiobutton)[3].click()
            elif field_type == ContractAction.NO:
                self.browser.find_elements(*ADMIN.select_radiobutton)[4].click()
        time.sleep(0.3)  # Задержка для локального стенда

    def select_radiobutton_ContractOtherPersonType(self, doc, field_type):
        if doc == ContractOtherPersonType.PERSONAL_DATA_AGREEMENT:
            if field_type == ContractAction.YES:
                self.browser.find_elements(*ADMIN.select_radiobutton)[5].click()
            elif field_type == ContractAction.NO:
                self.browser.find_elements(*ADMIN.select_radiobutton)[6].click()
        elif doc == ContractOtherPersonType.PHONE:
            if field_type == ContractAction.REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[7].click()
            elif field_type == ContractAction.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[8].click()
        elif doc == ContractOtherPersonType.SNILS:
            if field_type == ContractAction.REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[9].click()
            elif field_type == ContractAction.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[10].click()
        elif doc == ContractOtherPersonType.INN:
            if field_type == ContractAction.REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[11].click()
            elif field_type == ContractAction.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[12].click()
        elif doc == ContractOtherPersonType.BANK_NAME:
            if field_type == ContractAction.REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[13].click()
            elif field_type == ContractAction.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[14].click()
        elif doc == ContractOtherPersonType.BIK:
            if field_type == ContractAction.REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[15].click()
            elif field_type == ContractAction.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[16].click()
        elif doc == ContractOtherPersonType.BANK_ACCOUNT:
            if field_type == ContractAction.REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[17].click()
            elif field_type == ContractAction.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[18].click()
        elif doc == ContractOtherPersonType.WORK:
            if field_type == ContractAction.REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[19].click()
            elif field_type == ContractAction.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[20].click()
        elif doc == ContractOtherPersonType.POSITION:
            if field_type == ContractAction.REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[21].click()
            elif field_type == ContractAction.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[22].click()
        elif doc == ContractOtherPersonType.PASSPORT_SCAN:
            if field_type == ContractAction.REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[23].click()
            elif field_type == ContractAction.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[24].click()
        time.sleep(0.5)  # Задержка для локального стенда

    def select_radiobutton_ContractLegalPersonType(self, doc, field_type):
        if doc == ContractLegalPersonType.REGISTRATION_COUNTRY:
            if field_type == ContractAction.REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[25].click()
            elif field_type == ContractAction.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[26].click()
        elif doc == ContractLegalPersonType.REGISTRATION_DATE:
            if field_type == ContractAction.REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[27].click()
            elif field_type == ContractAction.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[28].click()
        elif doc == ContractLegalPersonType.AUTHORIZED_PERSON:
            if field_type == ContractAction.REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[29].click()
            elif field_type == ContractAction.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[30].click()
        elif doc == ContractLegalPersonType.INN:
            if field_type == ContractAction.REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[31].click()
            elif field_type == ContractAction.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[32].click()
        elif doc == ContractLegalPersonType.KPP:
            if field_type == ContractAction.REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[33].click()
            elif field_type == ContractAction.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[34].click()
        elif doc == ContractLegalPersonType.OGRN:
            if field_type == ContractAction.REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[35].click()
            elif field_type == ContractAction.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[36].click()
        elif doc == ContractLegalPersonType.OKPO:
            if field_type == ContractAction.REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[37].click()
            elif field_type == ContractAction.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[38].click()
        elif doc == ContractLegalPersonType.CARD_LINK:
            if field_type == ContractAction.REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[39].click()
            elif field_type == ContractAction.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[40].click()
        elif doc == ContractLegalPersonType.LEGAL_ADDRESS:
            if field_type == ContractAction.REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[41].click()
            elif field_type == ContractAction.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[42].click()
        elif doc == ContractLegalPersonType.REAL_ADDRESS:
            if field_type == ContractAction.REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[43].click()
            elif field_type == ContractAction.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[44].click()
        elif doc == ContractLegalPersonType.BANK_NAME:
            if field_type == ContractAction.REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[45].click()
            elif field_type == ContractAction.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[46].click()
        elif doc == ContractLegalPersonType.BIK:
            if field_type == ContractAction.REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[47].click()
            elif field_type == ContractAction.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[48].click()
        elif doc == ContractLegalPersonType.CHECKING_ACCOUNT:
            if field_type == ContractAction.REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[49].click()
            elif field_type == ContractAction.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[50].click()
        elif doc == ContractLegalPersonType.CORRESPONDENT_ACCOUNT:
            if field_type == ContractAction.REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[51].click()
            elif field_type == ContractAction.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[52].click()
        elif doc == ContractLegalPersonType.PHONE:
            if field_type == ContractAction.REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[53].click()
            elif field_type == ContractAction.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[54].click()
        elif doc == ContractLegalPersonType.GUARANTEE_LETTER_SCAN:
            if field_type == ContractAction.REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[55].click()
            elif field_type == ContractAction.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[56].click()
        elif doc == ContractLegalPersonType.CARD_SCAN:
            if field_type == ContractAction.REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[57].click()
            elif field_type == ContractAction.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[58].click()
        elif doc == ContractLegalPersonType.CREDENTIAL_PROOF_SCAN:
            if field_type == ContractAction.REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[59].click()
            elif field_type == ContractAction.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[60].click()
        time.sleep(0.3)  # Задержка для локального стенда

    @staticmethod
    def check_records_ContractCommon(title_pk, CONTRACT_REQUEST_COUNT, MOTHER_CERT_SCAN):
        time.sleep(0.5)
        assert (CONTRACT_REQUEST_COUNT, MOTHER_CERT_SCAN) == SETTING().get_data_contract_common(title_pk), \
            'Некорректная запись в settings.contract_common'

    @staticmethod
    def check_records_ContractEntrantPersonType(title_pk, PERSONAL_DATA_AGREEMENT):
        time.sleep(0.5)
        assert (PERSONAL_DATA_AGREEMENT) == SETTING().get_data_contract_entrant_person_type(title_pk), \
            'Некорректная запись в settings.contract_entrant_person_type'

    @staticmethod
    def check_records_ContractOtherPersonType(title_pk, PERSONAL_DATA_AGREEMENT, PHONE, SNILS, INN, BANK_NAME, BIK,
                                              BANK_ACCOUNT, WORK, POSITION, PASSPORT_SCAN):
        time.sleep(0.5)
        assert (PERSONAL_DATA_AGREEMENT, PHONE, SNILS, INN, BANK_NAME, BIK, BANK_ACCOUNT, WORK, POSITION,
                PASSPORT_SCAN) == SETTING().get_data_contract_other_person_type(title_pk), \
            'Некорректная запись в settings.contract_other_person_type'

    @staticmethod
    def check_records_ContractLegalPersonType(title_pk, REGISTRATION_COUNTRY, REGISTRATION_DATE, AUTHORIZED_PERSON, INN,
                                              KPP, OGRN, OKPO, CARD_LINK, LEGAL_ADDRESS, REAL_ADDRESS, BANK_NAME, BIK,
                                              CHECKING_ACCOUNT, CORRESPONDENT_ACCOUNT, PHONE, GUARANTEE_LETTER_SCAN,
                                              CARD_SCAN, CREDENTIAL_PROOF_SCAN):
        time.sleep(0.5)
        assert (REGISTRATION_COUNTRY, REGISTRATION_DATE, AUTHORIZED_PERSON, INN, KPP, OGRN, OKPO, CARD_LINK,
                LEGAL_ADDRESS, REAL_ADDRESS, BANK_NAME, BIK, CHECKING_ACCOUNT, CORRESPONDENT_ACCOUNT, PHONE,
                GUARANTEE_LETTER_SCAN, CARD_SCAN, CREDENTIAL_PROOF_SCAN) == \
               SETTING().get_data_contract_legal_person_type(title_pk), \
            'Некорректная запись в settings.contract_legal_person_type'

    def select_checkbox_AbiturMain(self, doc):
        if doc == AbiturMain.PDF:
            self.browser.find_elements(*ADMIN.select_checkbox)[0].click()
        elif doc == AbiturMain.JPG:
            self.browser.find_elements(*ADMIN.select_checkbox)[1].click()
        elif doc == AbiturMain.PNG:
            self.browser.find_elements(*ADMIN.select_checkbox)[2].click()
        elif doc == AbiturMain.GIF:
            self.browser.find_elements(*ADMIN.select_checkbox)[3].click()
        elif doc == AbiturMain.DOC:
            self.browser.find_elements(*ADMIN.select_checkbox)[4].click()
        elif doc == AbiturMain.DOCX:
            self.browser.find_elements(*ADMIN.select_checkbox)[5].click()
        elif doc == AbiturMain.RTF:
            self.browser.find_elements(*ADMIN.select_checkbox)[6].click()
        elif doc == AbiturMain.RAR:
            self.browser.find_elements(*ADMIN.select_checkbox)[7].click()
        elif doc == AbiturMain.ZIP:
            self.browser.find_elements(*ADMIN.select_checkbox)[8].click()
        time.sleep(0.3)  # Задержка для локального стенда

    def check_validation_settings(self):
        assert WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ALERT.alert_input1))), \
            'ERROR: Предупреждение об ошибке сохранения данных не появилось'

    @staticmethod
    def check_records_AbiturMain(title_pk, ZIP, PDF, JPG, PNG, GIF, DOC, DOCX, RTF, RAR, online_requests_count):
        time.sleep(0.5)  # Задержка для локального стенда
        assert (ZIP, PDF, JPG, PNG, GIF, DOC, DOCX, RTF, RAR, online_requests_count) == \
               SETTING().get_data_abitur_main(title_pk), 'Некорректная запись в settings.abitur_main'

    def select_checkbox_ThirdStep(self, doc, field_type):
        if doc == ThirdStep.SNILS:
            if field_type == ThirdStep.REQUIRED_FOR_RF:
                self.browser.find_elements(*ADMIN.select_radiobutton)[0].click()
            elif field_type == ThirdStep.OPTIONAL:
                self.browser.find_elements(*ADMIN.select_radiobutton)[1].click()
            elif field_type == ThirdStep.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[2].click()
        elif doc == ThirdStep.EDUCATIONAL_PROGRAM_RESULTS:
            if field_type == ThirdStep.REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[3].click()
            elif field_type == ThirdStep.OPTIONAL:
                self.browser.find_elements(*ADMIN.select_radiobutton)[4].click()
            elif field_type == ThirdStep.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[5].click()
        elif doc == ThirdStep.RELATIVE_DATA:
            if field_type == ThirdStep.REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[6].click()
            elif field_type == ThirdStep.REQUIRED_FOR_UNDERAGE:
                self.browser.find_elements(*ADMIN.select_radiobutton)[7].click()
            elif field_type == ThirdStep.OPTIONAL:
                self.browser.find_elements(*ADMIN.select_radiobutton)[8].click()
        elif doc == ThirdStep.SECOND_RELATIVE_DATA:
            if field_type == ThirdStep.OPTIONAL:
                self.browser.find_elements(*ADMIN.select_radiobutton)[9].click()
            elif field_type == ThirdStep.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[10].click()
        elif doc == ThirdStep.RELATIVE_CONTACT_PHONE:
            if field_type == ThirdStep.REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[11].click()
            elif field_type == ThirdStep.OPTIONAL:
                self.browser.find_elements(*ADMIN.select_radiobutton)[12].click()
        elif doc == ThirdStep.MAIN_FOREIGN_LANGUAGE:
            if field_type == ThirdStep.OPTIONAL:
                self.browser.find_elements(*ADMIN.select_radiobutton)[13].click()
            elif field_type == ThirdStep.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[14].click()
        elif doc == ThirdStep.WORK_POSITION:
            if field_type == ThirdStep.OPTIONAL:
                self.browser.find_elements(*ADMIN.select_radiobutton)[15].click()
            elif field_type == ThirdStep.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[16].click()
        elif doc == ThirdStep.INN:
            if field_type == ThirdStep.REQUIRED_FOR_RF:
                self.browser.find_elements(*ADMIN.select_radiobutton)[17].click()
            elif field_type == ThirdStep.OPTIONAL:
                self.browser.find_elements(*ADMIN.select_radiobutton)[18].click()
            elif field_type == ThirdStep.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[19].click()
        elif doc == ThirdStep.COURSES_INFO:
            if field_type == ThirdStep.WORK_POSITION:
                self.browser.find_elements(*ADMIN.select_radiobutton)[20].click()
            elif field_type == ThirdStep.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[21].click()
        elif doc == ThirdStep.EDUCATIONAL_ORGANIZATION_INFO_SOURCES:
            if field_type == ThirdStep.WORK_POSITION:
                self.browser.find_elements(*ADMIN.select_radiobutton)[22].click()
            elif field_type == ThirdStep.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[23].click()
        elif doc == ThirdStep.SPORTS_ACHIEVEMENTS_INFO:
            if field_type == ThirdStep.WORK_POSITION:
                self.browser.find_elements(*ADMIN.select_radiobutton)[24].click()
            elif field_type == ThirdStep.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[25].click()
        elif doc == ThirdStep.MILITARY_INFO:
            if field_type == ThirdStep.WORK_POSITION:
                self.browser.find_elements(*ADMIN.select_radiobutton)[26].click()
            elif field_type == ThirdStep.NOT_REQUIRED:
                self.browser.find_elements(*ADMIN.select_radiobutton)[27].click()
        time.sleep(0.3)  # Задержка для локального стенда

    @staticmethod
    def check_records_ThirdStep(title_pk, SNILS, EDUCATIONAL_PROGRAM_RESULTS, RELATIVE_DATA, SECOND_RELATIVE_DATA,
                                RELATIVE_CONTACT_PHONE, MAIN_FOREIGN_LANGUAGE, WORK_POSITION, INN, COURSES_INFO,
                                EDUCATIONAL_ORGANIZATION_INFO_SOURCES, SPORTS_ACHIEVEMENTS_INFO, MILITARY_INFO):
        time.sleep(0.5)
        assert (SNILS, EDUCATIONAL_PROGRAM_RESULTS, RELATIVE_DATA, SECOND_RELATIVE_DATA, RELATIVE_CONTACT_PHONE,
                MAIN_FOREIGN_LANGUAGE, WORK_POSITION, INN, COURSES_INFO, EDUCATIONAL_ORGANIZATION_INFO_SOURCES,
                SPORTS_ACHIEVEMENTS_INFO, MILITARY_INFO) == SETTING().get_data_abitur_third_step(title_pk), \
            'Некорректная запись в settings.abitur_third_step'

    def select_checkbox_EntrantsSearch(self, doc, field_type, text):
        if doc == EntrantsSearch.DATA:
            if field_type == EntrantsSearch.RECEIVE:
                self.browser.find_elements(*ADMIN.select_radiobutton)[0].click()
            elif field_type == EntrantsSearch.NOT_RECEIVE:
                self.browser.find_elements(*ADMIN.select_radiobutton)[1].click()
        elif doc == EntrantsSearch.SEARCH_SECTION:
            if field_type == EntrantsSearch.RECEIVE:
                self.browser.find_elements(*ADMIN.select_radiobutton)[2].click()
            elif field_type == EntrantsSearch.NOT_RECEIVE:
                self.browser.find_elements(*ADMIN.select_radiobutton)[3].click()
        elif doc == EntrantsSearch.PROMPT_TEXT:
            self.browser.find_element(*ADMIN.textarea).clear()
            self.browser.find_element(*ADMIN.textarea).send_keys(text)
            self.browser.find_element(*ADMIN.save_button).click()
            if field_type == 'Correct':
                assert WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ALERT.alert_correct))), \
                    'ERROR: Предупреждение об успешном сохранение данных не появилось'
            elif field_type == 'Incorrect':
                assert WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ALERT.alert_in_modal))), \
                    'ERROR: Предупреждение об ошибке сохранения данных не появилось'
        elif doc == EntrantsSearch.SNILS_OR_UNIQUE_NUMBER:
            if field_type == EntrantsSearch.RECEIVE:
                self.browser.find_elements(*ADMIN.select_radiobutton)[4].click()
            elif field_type == EntrantsSearch.NOT_RECEIVE:
                self.browser.find_elements(*ADMIN.select_radiobutton)[5].click()
        elif doc == EntrantsSearch.ENTRANCE_EXAMINATIONS_SCHEDULE:
            if field_type == EntrantsSearch.RECEIVE:
                self.browser.find_elements(*ADMIN.select_radiobutton)[6].click()
            elif field_type == EntrantsSearch.NOT_RECEIVE:
                self.browser.find_elements(*ADMIN.select_radiobutton)[7].click()
        time.sleep(0.3)  # Задержка для локального стенда

    @staticmethod
    def check_records_EntrantsSearch(title_pk, DATA, SEARCH_SECTION, PROMPT_TEXT, SNILS_OR_UNIQUE_NUMBER,
                                     ENTRANCE_EXAMINATIONS_SCHEDULE):
        time.sleep(0.5)
        assert (DATA, SEARCH_SECTION, PROMPT_TEXT, SNILS_OR_UNIQUE_NUMBER, ENTRANCE_EXAMINATIONS_SCHEDULE) == \
               SETTING().get_data_abitur_entrants_search(title_pk), \
            'Некорректная запись в settings.abitur_entrants_search'

    @staticmethod
    def check_records_TipsForApplicant(title_pk, haveEduLevel, wantRequestType, originalReturnWay, stateExamCodes,
                                       agreeEntranceExams, haveDisability, haveOlympiad, exclusiveRights,
                                       haveAgreementTargetEdu, competitionParams, competitions,
                                       competitionPriorities, cardType, citizenship, issuancePlace, birthPlace,
                                       regAddress, factAddress, snilsNumber, eduOrganizationCountry,
                                       eduOrganizationSettlement, eduOrganization, eduDocumentKind,
                                       seriaEduDocument, mark3, mark4, mark5, avgMarkAsLong, avgScoreAsLong,
                                       marks, individualAchievements, needDorm, relatives, foreignLanguage,
                                       identityCardPagesScans, otherIdentityCardForPassExamsPagesScans,
                                       educationDocumentTitlePageScans, snilsScans, individualAchievementsScans,
                                       benefitForDormScans, consentToPersonalDataScans, applicationScans,
                                       consentToEnrollmentScans, submittingConsentForEnrollment,
                                       enrollmentConsentStatementScan, submittingRefusalToEnroll,
                                       enrollmentRefusalApplicationScan, withdrawalOfApplication,
                                       applicationToWithdrawApplicationScan):
        assert (haveEduLevel, wantRequestType, originalReturnWay, stateExamCodes, agreeEntranceExams, haveDisability,
                haveOlympiad, exclusiveRights, haveAgreementTargetEdu, competitionParams, competitions,
                competitionPriorities, cardType, citizenship, issuancePlace, birthPlace, regAddress, factAddress,
                snilsNumber, eduOrganizationCountry, eduOrganizationSettlement, eduOrganization, eduDocumentKind,
                seriaEduDocument, mark3, mark4, mark5, avgMarkAsLong, avgScoreAsLong, marks, individualAchievements,
                needDorm, relatives, foreignLanguage, identityCardPagesScans, otherIdentityCardForPassExamsPagesScans,
                educationDocumentTitlePageScans, snilsScans, individualAchievementsScans, benefitForDormScans,
                consentToPersonalDataScans, applicationScans, consentToEnrollmentScans, submittingConsentForEnrollment,
                enrollmentConsentStatementScan, submittingRefusalToEnroll, enrollmentRefusalApplicationScan,
                withdrawalOfApplication, applicationToWithdrawApplicationScan) == SETTING().get_data_tips_for_applicant(
            title_pk), 'Некорректная запись в settings.tips_for_applicant'

    def select_radiobutton_EntrantLists(self, doc, field_type):
        if doc == EntrantLists.ENTRANT_LISTS_SECTION:
            if field_type == EntrantLists.ENABLE:
                self.browser.find_elements(*ADMIN.select_radiobutton)[0].click()
            elif field_type == EntrantLists.DISABLE:
                self.browser.find_elements(*ADMIN.select_radiobutton)[1].click()
        elif doc == EntrantLists.EDUCATIONAL_PROGRAMS_ENTRANT_LISTS:
            if field_type == EntrantLists.ENABLE:
                self.browser.find_elements(*ADMIN.select_radiobutton)[2].click()
            elif field_type == EntrantLists.DISABLE:
                self.browser.find_elements(*ADMIN.select_radiobutton)[3].click()
        elif doc == EntrantLists.EDUCATIONAL_PROGRAMS_ENTRANT_CARD:
            if field_type == EntrantLists.ENABLE:
                self.browser.find_elements(*ADMIN.select_radiobutton)[4].click()
            elif field_type == EntrantLists.DISABLE:
                self.browser.find_elements(*ADMIN.select_radiobutton)[5].click()
        elif doc == EntrantLists.SNILS_OR_UNIQUE_NUMBER:
            if field_type == EntrantLists.ENABLE:
                self.browser.find_elements(*ADMIN.select_radiobutton)[6].click()
            elif field_type == EntrantLists.DISABLE:
                self.browser.find_elements(*ADMIN.select_radiobutton)[7].click()
        elif doc == EntrantLists.COMPETITION_PRIORITY:
            if field_type == EntrantLists.ENABLE:
                self.browser.find_elements(*ADMIN.select_radiobutton)[8].click()
            elif field_type == EntrantLists.OPTIONAL:
                self.browser.find_elements(*ADMIN.select_radiobutton)[9].click()
            elif field_type == EntrantLists.DISABLE:
                self.browser.find_elements(*ADMIN.select_radiobutton)[10].click()
        elif doc == EntrantLists.TARGETED_ADMISSION:
            if field_type == EntrantLists.ENABLE:
                self.browser.find_elements(*ADMIN.select_radiobutton)[11].click()
            elif field_type == EntrantLists.DISABLE:
                self.browser.find_elements(*ADMIN.select_radiobutton)[12].click()
        elif doc == EntrantLists.WITHOUT_ENTRANCE_EXAMINATIONS:
            if field_type == EntrantLists.ENABLE:
                self.browser.find_elements(*ADMIN.select_radiobutton)[13].click()
            elif field_type == EntrantLists.DISABLE:
                self.browser.find_elements(*ADMIN.select_radiobutton)[14].click()
        elif doc == EntrantLists.ORIGINAL_EDUCATIONAL_DOCUMENT:
            if field_type == EntrantLists.ENABLE:
                self.browser.find_elements(*ADMIN.select_radiobutton)[15].click()
            elif field_type == EntrantLists.DISABLE:
                self.browser.find_elements(*ADMIN.select_radiobutton)[16].click()
        elif doc == EntrantLists.CONSENT_TO_ENROLLMENT:
            if field_type == EntrantLists.ENABLE:
                self.browser.find_elements(*ADMIN.select_radiobutton)[17].click()
            elif field_type == EntrantLists.DISABLE:
                self.browser.find_elements(*ADMIN.select_radiobutton)[18].click()
        elif doc == EntrantLists.REQUEST_NUMBER:
            if field_type == EntrantLists.ENABLE:
                self.browser.find_elements(*ADMIN.select_radiobutton)[19].click()
            elif field_type == EntrantLists.DISABLE:
                self.browser.find_elements(*ADMIN.select_radiobutton)[20].click()
        time.sleep(0.3)  # Задержка для локального стенда

    @staticmethod
    def check_records_EntrantLists(title_pk, ENTRANT_LISTS_SECTION, EDUCATIONAL_PROGRAMS_ENTRANT_LISTS,
                                   EDUCATIONAL_PROGRAMS_ENTRANT_CARD, SNILS_OR_UNIQUE_NUMBER, COMPETITION_PRIORITY,
                                   TARGETED_ADMISSION, WITHOUT_ENTRANCE_EXAMINATIONS, ORIGINAL_EDUCATIONAL_DOCUMENT,
                                   CONSENT_TO_ENROLLMENT, REQUEST_NUMBER):
        assert (ENTRANT_LISTS_SECTION, EDUCATIONAL_PROGRAMS_ENTRANT_LISTS, EDUCATIONAL_PROGRAMS_ENTRANT_CARD,
                SNILS_OR_UNIQUE_NUMBER, COMPETITION_PRIORITY, TARGETED_ADMISSION, WITHOUT_ENTRANCE_EXAMINATIONS,
                ORIGINAL_EDUCATIONAL_DOCUMENT, CONSENT_TO_ENROLLMENT, REQUEST_NUMBER) == \
               SETTING().get_data_abitur_entrant_list(title_pk), 'Некорректная запись в settings.abitur_entrant_list'

    def input_date_DocumentDeadlines(self, forms, types, times):
        search = 0
        while True:
            display_form = self.browser.find_elements(*ADMIN.search_form_doc)[search]
            display_type = self.browser.find_elements(*ADMIN.search_type_doc)[search]
            if forms == display_form.text and types == display_type.text:
                if times == 'start':
                    self.browser.find_elements(*ADMIN.input_StartDate)[search].send_keys(Keys.SPACE)
                    self.browser.find_elements(*ADMIN.input_StartDate)[search].send_keys(Keys.ENTER)
                elif times == 'end':
                    self.browser.find_elements(*ADMIN.input_EndDate)[search].send_keys(Keys.SPACE)
                    self.browser.find_elements(*ADMIN.input_EndDate)[search].send_keys(Keys.ENTER)
                time.sleep(0.3)  # Задержка для локального стенда
                break
            elif forms != display_form.text or types != display_type.text:
                search += 1
                continue
            else:
                break

    @staticmethod
    def check_records_DocumentDeadlines(title_pk, request_type_id, edu_program_form_id, compensation_type_id,
                                        starts_at, ends_in):
        assert (request_type_id, edu_program_form_id, compensation_type_id, starts_at, ends_in) == SETTING().\
            get_data_accepting_document_deadlines(title_pk), 'Некорректная запись в accepting_document_deadlines'
