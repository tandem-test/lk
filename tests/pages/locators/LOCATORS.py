from selenium.webdriver.common.by import By


class ALERT:
    invalid = (By.CLASS_NAME, 'text-xs.mt-2')
    alert_incorrect = (By.CLASS_NAME, 'bg-red-100')
    alert_in_modal = (By.CLASS_NAME, 'text-red-700')
    alert_input = (By.CSS_SELECTOR, 'p.text-xs')
    alert_input1 = (By.CLASS_NAME, 'text-red-800')
    alert_input2 = (By.CLASS_NAME, 'text-xs.text-red-600')
    alert_input3 = (By.CLASS_NAME, 'text-red-500')
    alert_save = (By.CSS_SELECTOR, 'div[style="opacity: 0; transition-property: opacity; transition-duration: 0.75s; '
                                   'transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1);"]')
    alert_correct = (By.CLASS_NAME, 'text-green-800.font-semibold')
    alert_correct1 = (By.CLASS_NAME, "text-green-600")


class REGLOG:
    logo_button = (By.CSS_SELECTOR, 'img[alt="Тандем.Личный кабинет"]')
    registration_button = (By.CSS_SELECTOR, 'a[title="Регистрация"]')
    support_button = (By.XPATH, "//a[contains(text(),'Поддержка')]")
    email = (By.CSS_SELECTOR, 'input[type="email"]')
    last_name = (By.NAME, 'last_name')
    name = (By.NAME, 'first_name')
    middle_name = (By.NAME, 'middle_name')
    password = (By.NAME, 'password')
    password_confirmation = (By.NAME, 'password_confirmation')
    checkbox_rule = (By.NAME, 'pd_agree')
    checkbox_remember = (By.NAME, 'remember')
    enter_button = (By.CSS_SELECTOR, 'button[type="submit"]')
    verification_exit = (By.CSS_SELECTOR, 'button.underline')
    send_new_link = (By.CSS_SELECTOR, 'button.uppercase')
    token = (By.NAME, 'recovery_code')
    birth_date = (By.NAME, 'birth_date')
    last_4_numbers_passport = (By.NAME, 'last_4_numbers_passport')
    snils = (By.NAME, 'snils')
    inn = (By.NAME, 'inn')
    book_number = (By.NAME, 'book_number')
    code = (By.NAME, 'code')
    title = (By.NAME, "title")
    reply_email = (By.NAME, "reply_email")
    content = (By.NAME, "content")
    send_message = (By.XPATH, "//button[contains(text(),'Отправить')]")


class PROFILE:
    # Рабочий стол
    profile_link = (By.CLASS_NAME, 'flex-shrink-0.w-full')
    exit_profile = (By.CSS_SELECTOR, 'button.text-gray-400.rounded-full')
    # Информация профиля
    select_button = (By.CSS_SELECTOR, 'button[type="button"]')
    input_file = (By.CSS_SELECTOR, 'input[type="file"]')
    email = (By.ID, 'email')
    last_name = (By.ID, 'last_name')
    name = (By.ID, 'first_name')
    middle_name = (By.ID, 'middle_name')
    enter_disabled = (By.CSS_SELECTOR, 'button[disabled]')
    save_info = (By.XPATH, '/html/body/div/div/div[3]/main/div[2]/div/div/div[1]//div[2]/button')
    # Обновление пароля
    current_password = (By.ID, 'current_password')
    password = (By.ID, 'password')
    password_confirmation = (By.ID, 'password_confirmation')
    # Двухфакторная аутентификация
    two_fa_widget = (By.XPATH, "//button[contains(text(),'Включить')]")
    password_modal = (By.CSS_SELECTOR, 'input[placeholder="Пароль"]')
    copy_token = (By.XPATH, '//div[5]//div[5]/div[1]')
    regenerate_codes = (By.CLASS_NAME, 'inline-flex.transition.ease-in-out.mr-3')
    # Сессии браузеров
    browser_list = (By.XPATH, '/html/body//div[7]/div/div[2]/div/div[2]/div')
    # Удаление учётной записи
    delete_button = (By.XPATH, "//button[contains(text(),'Удалить')]")
    withdraw_button = (By.XPATH, "//button[contains(text(),'Отозвать')]")


class DASHBOARD:
    dashboard = (By.LINK_TEXT, 'Рабочий стол')
    role_student = (By.LINK_TEXT, 'Обучающийся')
    role_employee = (By.XPATH, "//span[contains(text(),'Сотрудник')]")
    role_parent = (By.LINK_TEXT, 'Родитель')
    block_role = (By.CLASS_NAME, 'bg-gray-200.bg-opacity-25')


class ADMIN:
    # Вкладки на панеле администрирования
    setting = (By.XPATH, "//nav[contains(@class,'bg-white')]//span[contains(text(),'Настройки')]")
    setting_general = (By.LINK_TEXT, 'Общие')
    setting_getting_roles = (By.LINK_TEXT, 'Получение ролей')
    setting_type_chat = (By.XPATH, "//nav[contains(@class,'bg-white')]//span[contains(text(),'Виды обращений')]")
    setting_edu_org = (By.XPATH, "//nav[contains(@class,'bg-white')]//div//span[contains(text(),'Список ОО')]")
    setting_adm_camp = (By.XPATH, "//nav[contains(@class,'bg-white')]//div//span[contains(text(),'Приемная кампания')]")
    setting_reg_cat = (By.XPATH, "//nav[contains(@class,'bg-white')]//div//span[contains(text(),'Нормативные "
                                 "документы')]")
    authorization = (By.XPATH, "//span[contains(text(),'Авторизация')]")
    authorization_permissions = (By.LINK_TEXT, 'Разрешения')
    authorization_roles = (By.LINK_TEXT, 'Роли')
    users = (By.LINK_TEXT, 'Пользователи')
    # /admin/authorization/permissions #################################################################################
    search_permission = (By.CLASS_NAME, "leading-5.text-gray-900")
    # /admin/authorization/roles #######################################################################################
    setting_role = (By.XPATH, "//td[6]/div[1]/a[1]")
    # admin/settings/general ###########################################################################################
    # Общие - Общие настройки
    app_logo = By.ID, 'logoInput'
    app_name = (By.ID, 'app_name')
    app_url = (By.ID, 'app_url')
    app_icon = (By.CSS_SELECTOR, 'input[x-ref="icon"]')
    app_logo_prone = (By.XPATH, "//button[contains(text(),'Обрезать')]")
    is_multiple_org_units = (By.XPATH, "//input[@id='settings.is_multiple_org_units']")
    # Общие - Настройки почты
    mail_server = (By.ID, 'mail_server')
    mail_server_port = (By.ID, 'mail_server_port')
    mail_server_login = (By.ID, 'mail_server_login')
    mail_server_password = (By.ID, 'mail_server_password')
    mail_server_encryption = (By.ID, 'mail_server_encryption')
    mail_server_from_address = (By.ID, 'mail_server_from_address')
    mail_server_from_name = (By.ID, 'mail_server_from_name')
    # Общие - Настройки интерфейса
    show_assign_role_widget = (By.XPATH, '//*[@id="settings.show_assign_role_widget_from_home"]')
    # Общие - Настройки интерфейса
    widget_checkbox = (By.ID, 'settings.show_assign_role_widget')
    # Общие - Управление пользователями
    allow_delete_profile = (By.NAME, 'settings.allow_delete_profile')
    enable_2fa = (By.NAME, "settings.enable_2fa")
    allow_change_fullname = (By.NAME, "settings.allow_change_fullname")
    # Общие - Поддержка пользователей
    support_email = (By.ID, "support_email")
    support_link_name = (By.ID, "support_link_name")
    support_prompt = (By.ID, "support_prompt")
    support_active = (By.ID, "settings.support_active")
    # /admin/settings/assign-roles #####################################################################################
    # Получение ролей
    filter_search = (By.CLASS_NAME, 'form-input.rounded-md.shadow-sm.block.w-full')
    editing = (By.CLASS_NAME, 'bg-white.rounded-full.flex.items-center')
    select_name = (By.CSS_SELECTOR, 'div.leading-5>a')
    input_checkbox = (By.CLASS_NAME, 'align-middle')
    list_roles = (By.XPATH, "//a[contains(text(),'К списку ролей')]")
    assignment_description = (By.NAME, 'assignment_description')
    description = (By.CLASS_NAME, 'mb-4.text-gray-700')
    # /admin/users #####################################################################################################
    # Пользователи
    next_page_button = (By.CSS_SELECTOR, 'a[rel="next"]')
    last_page_button = (By.CSS_SELECTOR, 'span>a')
    # Пользователи - Пользователь
    sub_windows_select = (By.CLASS_NAME, 'ease-in-out.text-gray-600')
    login_as_user = (By.LINK_TEXT, 'Вход под пользователем')
    user_name = (By.CSS_SELECTOR, 'h3[class="text-lg font-medium text-gray-900"]')
    user_data = (By.CSS_SELECTOR, 'div:nth-child(1)>dd')
    # Пользователи - Роли и разрешения
    add_button = (By.XPATH, "//button[contains(text(),'Добавить')]")
    to_appoint = (By.CSS_SELECTOR, 'button.items-center.px-4.py-2')
    cancel_button = (By.CSS_SELECTOR, 'a.leading-6.text-gray-700')
    delete_button = (By.LINK_TEXT, 'Удалить группу')
    select_role = (By.NAME, 'role')
    attached_role = (By.CSS_SELECTOR, '''div[x-show="tab === 'roles'"]>div>div>button.text-gray-600''')
    null_attached_role = (By.XPATH, "//div[contains(text(),'Пока не назначено ни одной роли.')]")
    attached_permission = (By.CSS_SELECTOR, '''div[x-show="tab === 'permissions'"]>div>div>button.text-gray-600''')
    null_attached_permission = (By.XPATH, "//div[contains(text(),'Пока не выдано ни одного разрешения.')]")
    save_button = (By.XPATH, "//button[contains(text(),'Сохранить')]")
    window_permissions = (By.XPATH, "//button[contains(text(),'Разрешения')]")
    select_permission = (By.NAME, 'permission')
    # /admin/settings/assign-roles #####################################################################################
    edit_role = (By.XPATH, "//tbody//*[local-name()='svg']")
    title_edit_role = (By.XPATH, "//h2[contains(text(),'Настройка получения роли')]")
    # /application/settings ############################################################################################
    select_checkbox = (By.CSS_SELECTOR, 'input[type="checkbox"]')
    radiobutton_OPTIONAL = (By.CSS_SELECTOR, 'input.form-radio[value="0"]')
    radiobutton_REQUIRED = (By.CSS_SELECTOR, 'input.form-radio[value="1"]')
    radiobutton_NOT_REQUIRED = (By.CSS_SELECTOR, 'input.form-radio[value="2"]')
    radiobutton_DEFAULT = (By.CSS_SELECTOR, 'input.form-radio[value="2"]')
    select_radiobutton = (By.CSS_SELECTOR, 'input.form-radio')
    setting_EduLevel = (By.XPATH, "//span[contains(text(),'Базовые уровни образования')]")
    setting_RequestType = (By.XPATH, "//span[contains(text(),'Виды заявлений абитуриента')]")
    setting_OrderOfSpecialties = (By.XPATH, "//span[contains(text(),'Порядок следования НПС в списках')]")
    setting_FirstStep = (By.XPATH, "//span[contains(text(),'Шаг 1 - основные данные')]")
    setting_SecondStep = (By.XPATH, "//span[contains(text(),'Шаг 2 - выбор конкурсов и ОП')]")
    setting_ThirdStep = (By.XPATH, "//span[contains(text(),'Шаг 3 - заполнение анкетных данных')]")
    setting_ScanCopies = (By.XPATH, "//span[contains(text(),'Шаг 4 - загрузка скан-копий документов')]")
    setting_CompetitionLists = (By.XPATH, "//span[@class='truncate'][contains(text(),'Конкурсные списки')]")
    setting_EntrantsSearch = (By.XPATH, "//span[contains(text(),'Поиск абитуриентов')]")
    setting_Contracts = (By.XPATH, "//span[@class='truncate'][contains(text(),'Договоры на обучение')]")
    setting_AbiturMain = (By.XPATH, "//span[contains(text(),'Основные настройки')]")
    setting_Regulations = (By.XPATH, "//span[@class='truncate'][contains(text(),'Нормативные документы')]")
    setting_Regulations = (By.XPATH, "//span[@class='truncate'][contains(text(),'Нормативные документы')]")
    setting_TipsApplicant = (By.XPATH, "//span[contains(text(),'Подсказки для абитуриента')]")
    setting_EntrantLists = (By.XPATH, "//span[contains(text(),'Списки поступающих')]")
    setting_DocumentDeadlines = (By.XPATH, "//span[contains(text(),'Сроки приема заявлений')]")
    apply_placement = (By.XPATH, "//button[contains(text(),'Применить расстановку')]")
    default_placement = (By.XPATH, "//button[contains(text(),'Вернуться к расстановке по умолчанию')]")
    # Сроки приема заявлений
    search_form_doc = (By.CSS_SELECTOR, "td:nth-child(2)")
    search_type_doc = (By.CSS_SELECTOR, "td:nth-child(3)")
    input_StartDate = (By.CSS_SELECTOR, "td:nth-child(4)>div>input")
    input_EndDate = (By.CSS_SELECTOR, "td:nth-child(5)>div>input")
    # /tickets/types/ ##################################################################################################
    add_new = (By.CSS_SELECTOR, "a.rounded-md.text-white.bg-indigo-600")
    is_active = (By.ID, 'is_active')
    search_type_chat = (By.CSS_SELECTOR, 'td:nth-child(1)>span')
    page_next = (By.CLASS_NAME, "-ml-px.text-sm.font-medium")
    back_button = (By.XPATH, "//a[contains(text(),'Назад')]")
    # /application/eo-settings/org-units ###############################################################################
    field_input = (By.CSS_SELECTOR, "input.form-input")
    field_textarea = (By.CSS_SELECTOR, 'textarea.form-input')
    field_schedule = (By.XPATH, "//div[6]//input[1]")
    select_user = (By.XPATH, "//span[@class='block py-1 px-5 cursor-pointer hover:bg-indigo-600 hover:text-white']")
    list_user = (By.CLASS_NAME, "bg-indigo-600.rounded")
    info_page = (By.XPATH, "//button[contains(text(),'Информация об организации')]")
    system_page = (By.XPATH, "//button[contains(text(),'Системы')]")
    campaigns_page = (By.XPATH, "//button[contains(text(),'Приемные кампании')]")
    contact_page = (By.XPATH, "//button[contains(text(),'Контакты')]")
    system_create = (By.XPATH, '''//div[@x-show="tab === 'systems'"]//div//a''')
    campaigns_create = (By.XPATH, '''//div[@x-show="tab === 'campaigns'"]//div//a''')
    contact_create = (By.XPATH, '''//div[@x-show="tab === 'contacts'"]//div//a''')
    check_connection = (By.XPATH, "//button[contains(text(),'Проверить подключение')]")
    select_field = (By.CSS_SELECTOR, 'select.form-input')
    stub_in_creation = (By.CLASS_NAME, "overflow-hidden.shadow")
    textarea = (By.CSS_SELECTOR, 'textarea.form-input')
    close_alert = (By.XPATH, "//div[@role='alert']//*[local-name()='svg']")
    # application/regulations/categories
    categories_docs = (By.XPATH, "//button[contains(text(),'Категории нормативных документов')]")
    types_docs = (By.XPATH, "//a[contains(text(),'Типы нормативных документов')]")
    uploadFiles = (By.NAME, "uploadFiles")


class CONTROL:
    # Вкладки
    service = (By.XPATH, '//div/div[2]/div/div//div[6]')
    service_categories = (By.LINK_TEXT, 'Заявки')
    service_applications = (By.LINK_TEXT, 'Категории')
    service_services = (By.LINK_TEXT, 'Услуги')
    service_realization = (By.LINK_TEXT, 'Реализации')
    service_form_editor = (By.LINK_TEXT, 'Редактор форм')
    # Заявки
    search_categories = (By.XPATH, '//tbody/tr/td/a')
    applicants_name = (By.CLASS_NAME, 'max-w-2xl')
    type_application = (By.CLASS_NAME, 'leading-6')
    select_button = (By.CLASS_NAME, 'border.border-gray-300')
    # Категории
    search_applications = (By.CSS_SELECTOR, 'div.leading-5>span')
    remove_button = (By.CLASS_NAME, 'button.bg-white.rounded-full')
    archive_button = (By.CSS_SELECTOR, 'li:nth-child(2)>div>div')
    edit_button = (By.CSS_SELECTOR, 'li:nth-child(1)>div>div')
    send_and_restore = (By.CLASS_NAME, 'bg-indigo-600.ml-2')
    status_archive = (By.CSS_SELECTOR, 'td:nth-child(4)')
    enter_button = (By.XPATH, '//form//div[2]/button[2]')
    wait_enter_button = (By.CSS_SELECTOR, 'button.border.border-transparent[disabled]')
    cancel_modal = (By.CLASS_NAME, 'border-gray-300')
    create_button = (By.CLASS_NAME, 'bg-indigo-600')
    wait_modal = (By.CLASS_NAME, 'transition-all.ease-out.duration-300.opacity-100')
    wait_close_modal = (By.CLASS_NAME, 'transition-all.ease-in.duration-200.opacity-0')
    input_forms = (By.XPATH, '//div[2]/div/input')
    load_pictogram = (By.CLASS_NAME, 'mt-2.text-sm')
    update_pictogram = (By.XPATH, '//div[5]//div[5]/div/div')
    # Услуги
    description = (By.ID, 'description')
    select_category = (By.CSS_SELECTOR, 'div.mt-4>div:nth-child(3)>div>select')
    search_service = (By.CSS_SELECTOR, 'td:nth-child(1)>span')
    # Реализации
    select_service = (By.NAME, 'service')
    select_form = (By.NAME, 'form')
    select_process_type = (By.NAME, 'process_type')
    select_responsible_department = (By.CSS_SELECTOR, 'div:nth-child(7)>div>div>div.choices__inner>div')
    select_place_of_receipt = (By.CSS_SELECTOR, 'div:nth-child(8)>div>div>div.choices__inner>div')
    input_search_select = (By.CSS_SELECTOR, 'div.choices.is-open>div.is-active>input')
    description_name = (By.CSS_SELECTOR, 'trix-editor.form-input')
    description_attach = (By.CSS_SELECTOR, 'button[title="Attach Files"]')
    title_name = (By.NAME, 'title')
    checkbox_electronic = (By.CSS_SELECTOR, 'input[value="1"]')
    checkbox_paper = (By.CSS_SELECTOR, 'input[value="2"]')
    checkbox_document = (By.NAME, 'attachment_required')
    save_button = (By.CSS_SELECTOR, 'button[type="submit"]')
    archive_realization = (By.CSS_SELECTOR, 'li:nth-child(3)>div>div>button')
    remove_realization = (By.CSS_SELECTOR, 'li:nth-child(4)>div>div>button')
    status_archive_realization = (By.CSS_SELECTOR, 'td:nth-child(3)')
    # Редактор форм
    search_constructor = (By.CSS_SELECTOR, 'div.px-6.py-4.bg-white>div>div')
    search_input = (By.CSS_SELECTOR, 'div:nth-child(2)>div>a:nth-child(1)')
    remove_input = (By.CSS_SELECTOR, 'div:nth-child(2)>div>a:nth-child(2)')
    null_form = (By.CLASS_NAME, 'shadow.rounded-lg')
    select_type_input = (By.CSS_SELECTOR, 'select.form-input')
    input_valid_values = (By.CSS_SELECTOR, 'textarea.form-input')
    delete_form = (By.XPATH, '//div[6]/div[2]//button[2]')
    delete_group = (By.XPATH, '//div[3]//div[2]//button[3]')
    delete_input = (By.XPATH, '//div[2]//div[9]//button[2]')
    group_control = (By.CSS_SELECTOR, 'div.bg-gray-50>button')


class STUDENT:
    # Вкладки
    service = (By.LINK_TEXT, 'Услуги')
    estimates = (By.LINK_TEXT, 'Оценки')
    orders = (By.LINK_TEXT, 'Приказы')
    portfolio = (By.LINK_TEXT, 'Портфолио')
    progress = (By.LINK_TEXT, 'Достижения')
    trajectory = (By.LINK_TEXT, 'Траектория')
    record_book = (By.LINK_TEXT, 'Зачетная книжка')
    registration_for_courses = (By.LINK_TEXT, 'Запись на курсы')
    # Услуги
    search_service = (By.CSS_SELECTOR, 'div.text-blue-500>a')


class CHAT:
    # Вкладки
    chat = (By.XPATH, "//nav[contains(@class,'mt-5')]//span[contains(text(),'Обращения')]")
    list_ticket = (By.XPATH, "//nav[contains(@class,'mt-5')]//span[contains(text(),'Список')]")
    archive_ticket = (By.XPATH, "//nav[contains(@class,'mt-5')]//span[contains(text(),'Архив')]")
    type_chat = (By.XPATH, "//nav[contains(@class,'mt-5')]//span[contains(text(),'Виды обращений')]")
    # Информация о тикете
    status_ticket = (By.XPATH, '//td[5]')
    priority_ticket = (By.CSS_SELECTOR, 'td:nth-child(6)>span')
    # Взаимодейтсвие с тикетом
    search_ticket = (By.CLASS_NAME, 'text-indigo-700.cursor-pointer')
    select_author = (By.CLASS_NAME, 'relative.flex.items-stretch.flex-grow')
    input_author = (By.XPATH, "//input[@class='pr-2 appearance-none outline-none w-full text-gray-800']")
    choose_author = (By.CLASS_NAME, 'relative.transition.ease-in-out.duration-150')
    text = (By.NAME, 'text')
    tracker_id = (By.NAME, 'tracker_id')
    type_id = (By.NAME, 'type_id')
    status_id = (By.ID, 'status_id')
    priority_id = (By.ID, 'priority_id')
    send_to_archive = (By.XPATH, "//button[contains(text(),'Отправить')]")
    restore_ticket = (By.XPATH, "//button[contains(text(),'Восстановить')]")
    setting_restore = (By.CLASS_NAME, "border-gray-300.ease-in-out.justify-center")
    # Взаимодействие в чате
    empty_chat = (By.CLASS_NAME, 'text-gray-400.text-center')
    field_message = (By.CSS_SELECTOR, 'textarea[placeholder="Сообщение..."]')
    send_message = (By.CLASS_NAME, 'cursor-pointer.bg-indigo-500')
    load_file = (By.CSS_SELECTOR, 'input[type="file"]')
    check_load_file = (By.CSS_SELECTOR, "svg[style='display: inline-block;']")
    display_file = (By.CSS_SELECTOR, 'div.text-white>a>span:nth-child(2)')
    setting_ticket = (By.CSS_SELECTOR, 'span.rounded-md>button')
    setting_wait = (By.CSS_SELECTOR, 'button[aria-expanded="true"]')
    setting_menu = (By.CSS_SELECTOR, '.shadow-lg>div>div>button')
    messages_container = (By.CSS_SELECTOR, 'div>div>div.px-5')
    messages_archive = (By.CSS_SELECTOR, 'div.bg-indigo-500.text-white')
    back_window = (By.CSS_SELECTOR, 'a.inline-flex')
    ####################################################################################################################
    ticket = (By.XPATH, "//nav[contains(@class,'bg-white')]//span[contains(text(),'Чат')]")
    input_users = (By.XPATH, "//input[@placeholder='Поиск...']")
    select_user = (By.XPATH, "//span[@class='block py-1 px-5 cursor-pointer hover:bg-indigo-600 hover:text-white']")
    list_users = (By.CSS_SELECTOR, 'div.bg-indigo-600')
    delete_users = (By.XPATH, "//button[@class='focus:outline-none']//*[local-name()='svg']")
    leave_chat = (By.XPATH, "//button[contains(text(),'Покинуть')]")
    remove_button = (By.XPATH, "//div[@class='bg-red-500 p-0.5 rounded-full w-7 h-7 text-red-500 hover:bg-red-700 "
                               "hover:text-red-700']")
    delete_button = (By.XPATH, "//button[contains(text(),'Удалить')]")


class ABITUR:
    """Вкладки на панели 'АБИТУРИЕНТ'"""
    applications = (By.XPATH, "//nav[contains(@class,'mt-5')]//span[contains(text(),'Заявления')]")
    entrance_tests = (By.XPATH, "//nav[contains(@class,'mt-5')]//span[contains(text(),'Вступительные испытания')]")
    training_contracts = (By.XPATH, "//nav[contains(@class,'mt-5')]//span[contains(text(),'Договоры на обучение')]")
    competition_lists = (By.XPATH, "//nav[contains(@class,'mt-5')]//span[contains(text(),'Конкурсные списки')]")
    search = (By.XPATH, "//nav[contains(@class,'mt-5')]//span[contains(text(),'Поиск')]")
    regulatory_documents = (By.XPATH, "//nav[contains(@class,'mt-5')]//span[contains(text(),'Нормативные документы')]")
    contacts = (By.XPATH, "//nav[contains(@class,'mt-5')]//span[contains(text(),'Контакты')]")
    ####################################################################################################################
    """########################################### Заявления ########################################################"""
    ####################################################################################################################
    """'Выберите образовательную организацию"""
    submit_new_online_app = (By.XPATH, "//button[contains(text(),'Подать новое онлайн-заявление')]")
    back_button = (By.XPATH, "//a[contains(text(),'Назад')]")
    app_stub = (By.XPATH, "//span[@class='text-sm xl:text-base text-blue-800 font-semibold']")
    search_oo = (By.XPATH, "//input[@placeholder='Введите название образовательной организации']")
    select_oo = (By.CLASS_NAME, "rounded.group.cursor-pointer")
    """ Выберите приемную кампанию"""
    select_pk = (By.CLASS_NAME, "text-indigo-600.truncate")
    """Поданное заявление"""
    delete_application = (By.XPATH, "//button[contains(text(),'Удалить заявление')]")
    edit_application = (By.XPATH, "//a[contains(text(),'Редактировать')]")
    continent_application = (By.XPATH, "//a[contains(text(),'Перейти к заполнению основных данных')]")
    change_RC = (By.XPATH, "//a[contains(text(),'Сменить')]")
    remove_accept = (By.XPATH, "//button[contains(text(),'Удалить')][last()]")
    go_filling_step_1 = (By.XPATH, "//a[contains(text(),'Перейти к заполнению основных данных')]")
    go_filling_step_2 = (By.XPATH, "//a[contains(text(),'Перейти к выбору направлений')]")
    """ШАГ 1 - Основные данные"""
    # Я уже получил(а) образование
    education_1 = (By.XPATH, "//span[contains(text(),'Основное общее образование (9 классов)')]")
    education_2 = (By.XPATH, "//span[contains(text(),'Среднее общее образование (11 классов)')]")
    education_3 = (By.XPATH, "//span[contains(text(),'Среднее профессиональное образование')]")
    education_4 = (By.XPATH, "//span[contains(text(),'Высшее образование - бакалавриат')]")
    education_5 = (By.XPATH, "//span[contains(text(),'Высшее образование - специалитет, магистратура')]")
    education_6 = (By.XPATH, "//span[contains(text(),'Высшее образование - подготовка кадров высшей квал')]")
    # Я хочу поступать на образовательные программы
    program_1 = (By.XPATH, "//span[contains(text(),'Среднее профессиональное образование')]")
    program_2 = (By.XPATH, "//span[contains(text(),'Программы бакалавриата, специалитета')]")
    program_3 = (By.XPATH, "//span[contains(text(),'Программы магистратуры')]")
    program_4 = (By.XPATH, "//span[contains(text(),'Программы аспирантуры (адъюнктуры)')]")
    program_5 = (By.XPATH, "//span[contains(text(),'Программы ординатуры')]")
    # Способ возврата оригиналов документов в случае непоступления
    return_method_1 = (By.XPATH, "//span[contains(text(),'Лично')]")
    return_method_2 = (By.XPATH, "//span[contains(text(),'По почте')]")
    return_method_3 = (By.XPATH, "//span[contains(text(),'По доверенности')]")
    return_method_4 = (By.XPATH, "//span[contains(text(),'В электронной форме')]")
    return_method_5 = (By.XPATH, "//span[contains(text(),'Через портал гос. услуг')]")
    checkbox_haveDisability = (By.XPATH, "//input[@id='haveDisability']")
    # Предпочитаемая форма сдачи вступительных испытаний
    delivery_form_1 = (By.XPATH, "//span[contains(text(),'Очная')]")
    delivery_form_2 = (By.XPATH, "//span[contains(text(),'Дистанционная')]")
    checkbox_aveAgreementTargetEdu = (By.XPATH, "//input[@id='haveAgreementTargetEdu']")
    agreementNumber = (By.CSS_SELECTOR, "div.lg\:col-span-2.space-y-2 > input")
    agreementDate = (By.XPATH, "//input[@placeholder='дд.мм.гггг']")
    agreementOrg = (By.CSS_SELECTOR, "div:nth-child(2)>div:nth-child(2)>input:nth-child(2)")
    agreementEduProgram = (By.CSS_SELECTOR, "div:nth-child(3)>input:nth-child(2)")
    # Я уже сдавал(а) или буду сдавать ЕГЭ по предметам
    EGE_score = (By.CSS_SELECTOR, "input.form-input")
    EGE_year = (By.CSS_SELECTOR, "select.form-input")
    EGE_english_language = (By.XPATH, "//label[@for='subject_8'][contains(text(),'Английский язык')]")
    EGE_biology = (By.XPATH, "//label[@for='subject_5'][contains(text(),'Биология')]")
    EGE_geography = (By.XPATH, "//label[@for='subject_7'][contains(text(),'География')]")
    EGE_computer_science = (By.XPATH, "//label[@for='subject_14'][contains(text(),'Информатика и ИКТ')]")
    EGE_spanish_language = (By.XPATH, "//label[@for='subject_13'][contains(text(),'Испанский язык')]")
    EGE_history = (By.XPATH, "//label[@for='subject_6'][contains(text(),'История')]")
    EGE_literature = (By.XPATH, "//label[@for='subject_12'][contains(text(),'Литература')]")
    EGE_mathematics = (By.XPATH, "//label[@for='subject_2'][contains(text(),'Математика')]")
    EGE_german_language = (By.XPATH, "//label[@for='subject_9'][contains(text(),'Немецкий язык')]")
    EGE_social_studies = (By.XPATH, "//label[@for='subject_11'][contains(text(),'Обществознание')]")
    EGE_russian = (By.XPATH, "//label[@for='subject_1'][contains(text(),'Русский язык')]")
    EGE_physics = (By.XPATH, "//label[@for='subject_3'][contains(text(),'Физика')]")
    EGE_french_language = (By.XPATH, "//label[@for='subject_10'][contains(text(),'Французский язык')]")
    EGE_chemistry = (By.XPATH, "//label[@for='subject_4'][contains(text(),'Химия')]")
    # Прошу допустить к участию во вступительных испытаниях, проводимых образовательной организацией самостоятельно
    agreeEntranceExams = (By.XPATH, "//input[@id='agreeEntranceExams']")
    select_Entrance = (By.XPATH, "//select[@class='form-input rounded-md shadow-sm disabled:opacity-25 block w-full']")
    ET_german_language = (By.XPATH, "//label[@for='internal_discipline_9'][contains(text(),'Немецкий язык')]")
    ET_literature = (By.XPATH, "//label[@for='internal_discipline_12'][contains(text(),'Литература')]")
    ET_spanish = (By.XPATH, "//label[@for='internal_discipline_13'][contains(text(),'Испанский язык')]")
    ET_computer_science = (By.XPATH, "//label[@for='internal_discipline_14'][contains(text(),'Информатика и ИКТ')]")
    ET_mathematics = (By.XPATH, "//label[@for='internal_discipline_2'][contains(text(),'Математика')]")
    ET_russian = (By.XPATH, "//label[@for='internal_discipline_1'][contains(text(),'Русский язык')]")
    ET_chemistry = (By.XPATH, "//label[@for='internal_discipline_4'][contains(text(),'Химия')]")
    ET_french = (By.XPATH, "//label[@for='internal_discipline_10'][contains(text(),'Французский язык')]")
    ET_physics = (By.XPATH, "//label[@for='internal_discipline_3'][contains(text(),'Физика')]")
    ET_social_studies = (By.XPATH, "//label[@for='internal_discipline_11'][contains(text(),'Обществознание')]")
    ET_history = (By.XPATH, "//label[@for='internal_discipline_6'][contains(text(),'История')]")
    ET_biology = (By.XPATH, "//label[@for='internal_discipline_5'][contains(text(),'Биология')]")
    ET_english = (By.XPATH, "//label[@for='internal_discipline_8'][contains(text(),'Английский язык')]")
    ET_geography = (By.XPATH, "//label[@for='internal_discipline_7'][contains(text(),'География')]")
    ET_physical_education = (By.XPATH, "//label[contains(text(),'Физическая культура')]")
    # Я участвовал(а) в олимпиадах школьников
    haveOlympiad = (By.XPATH, "//input[@id='haveOlympiad']")
    inputOlympiad = (By.XPATH, "//input[@class='form-input rounded-md shadow-sm disabled:opacity-25 block w-full "
                               "lg:w-2/3']")
    # У меня есть особые права или преимущественное право зачисления
    exclusiveRights = (By.XPATH, "//input[@id='exclusiveRights']")
    exclusiveRightsWithoutExams = (By.XPATH, "//input[@id='exclusiveRightsWithoutExams']")
    exclusiveRightsBudget = (By.XPATH, "//input[@id='exclusiveRightsBudget']")
    exclusiveRightsEmptiveEnroll = (By.XPATH, "//input[@id='exclusiveRightsEmptiveEnroll']")
    """ШАГ 2 - Выбор конкурсов"""
    # Показать направления подготовки (специальности) и конкурсы
    all_form = (By.XPATH, "//input[@value='all']")
    already_passed_form = (By.XPATH, "//input[@value='already_passed']")
    select_priority = (By.CSS_SELECTOR, "div.flex.items-center.space-x-2>div>input")
    """ШАГ 3 - Анкета абитуриента"""
    # ФИО
    lastName = (By.XPATH, '//*[@id="main"]/div[2]/div/form/div[1]/div/div[1]/input')
    firstName = (By.XPATH, '//*[@id="main"]/div[2]/div/form/div[1]/div/div[2]/input')
    middleName = (By.XPATH, '//*[@id="main"]/div[2]/div/form/div[1]/div/div[3]/input')
    # Удостоверение личности
    cardType = (By.XPATH, '//*[@id="main"]/div[2]/div/form/div[2]/div/div[1]/div[1]/div/select')
    citizenship = (By.XPATH, '//*[@id="main"]/div[2]/div/form/div[2]/div/div[1]/div[2]/div/select')
    seria = (By.XPATH, '//div//div[2]/div/div[2]/div[1]/input')
    number = (By.XPATH, '//div//div[2]/div/div[2]/div[2]/input')
    issuancePlace = (By.XPATH, '//div[2]/div/div[3]/div[1]/input')
    issuanceCode = (By.XPATH, '//div[2]/div/div[3]/div[2]/input')
    issuanceDate = (By.XPATH, '//div//div[2]/div/div[4]/div/input')
    examsWithAnotherIdentityCard = (By.ID, 'examsWithAnotherIdentityCard')
    sex = (By.XPATH, '//div[2]/div/div[6]/div[1]/div/select')
    birthDate = (By.XPATH, '//div[2]/div/div[6]/div[2]/input')
    birthPlace = (By.XPATH, '//div[2]/div/div[7]/input')
    regAddress = (By.XPATH, '//div[2]/div/div[8]/input')
    sameAddress = (By.XPATH, "//input[@id='sameAddress']")  # Адрес проживания
    # СНИЛС
    snils = (By.XPATH, "//input[@id='noSnils']")
    snilsNumber = (By.XPATH, "//div[3]/div/div[2]/input")
    # Документ о полученном образовании
    eduOrganizationCountry = (By.XPATH, "//div[4]/div/div[1]/div[1]/div/select")
    eduOrganizationSettlement = (By.XPATH, "//div[4]/div/div[1]/div[2]/input")
    eduOrganization = (By.XPATH, "//div[4]/div/div[2]/input")
    eduDocumentKind = (By.XPATH, "//div[4]/div/div[3]/div/select")
    qualification = (By.XPATH, "//div[4]/div/div[4]/input")
    seriaEduDocument = (By.XPATH, "//div[4]/div/div[5]/div[1]/input")
    numberEduDocument = (By.XPATH, "//div[4]/div/div[5]/div[2]/input")
    issuanceDateEduDocument = (By.XPATH, "//div[4]/div/div[6]/div[1]/input")
    yearEnd = (By.XPATH, "//div[4]/div/div[6]/div[2]/input")
    eduOrganizationCountry = (By.XPATH, "//div[4]/div/div[7]/div/input")
    # Индивидуальные достижения из SOAP
    # -----
    # Общежитие
    needDorm = (By.ID, "needDorm")
    havePrivilege = (By.ID, "havePrivilege")
    # Ближайшие родственники
    relationDegree = (By.XPATH, "//div[7]/div/div[1]/div[1]/div/select")
    lastNameNextOfKin = (By.XPATH, "//div[7]/div/div[1]/div[2]/input")
    firstNameNextOfKin = (By.XPATH, "//div[7]/div/div[1]/div[3]/input")
    patronymicNextOfKin = (By.XPATH, "//div[7]/div/div[1]/div[4]/input")
    employmentPlace = (By.XPATH, "//div[7]/div/div[2]/div[1]/input")
    post = (By.XPATH, "//div[7]/div/div[2]/div[2]/input")
    phones = (By.XPATH, "//div[7]/div/div[2]/div[3]/input")
    specifySecondRelative = (By.ID, "specifySecondRelative")
    # Контактные данные

    # Дополнительные сведения
    # Спортивные достижения
    add_sport_achievements = (By.XPATH, "//button[contains(text(),'Добавить')]")
    sportType = (By.XPATH, "//div[2]/div[1]/div[2]/div[1]/select[1]")
    sportRank = (By.XPATH, "//div[2]/div[1]/div[3]/div[1]/select[1]")
    # Служба в армии
    """Элементы управления интерфейсом в заявке"""
    next_button = (By.XPATH, "//button[contains(text(),'Далее')]")
    cancel_button = (By.XPATH, "//button[contains(text(),'Отмена')]")
    load_file = (By.CSS_SELECTOR, 'input[type="file"]')
