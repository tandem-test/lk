#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

# https://github.com/liris/websocket-client
# sudo pip install --user websocket-client

from websocket import create_connection


class mailbox(object):
    """10 minute mailbox"""

    def __init__(self):
        super(mailbox, self).__init__()
        self.ws = create_connection("wss://10mail.org/websocket?prefer-domain=emlpro.com")
        self.next = self.ws.recv
        self.close = self.ws.close
        self.email = self.next()[1:].split(":")[0]
        self.next()

    def __del__(self):
        self.ws.close()


def get_mail_name():
    return mailbox()


def main(box):
    import re
    from json import loads
    while True:
        result = box.next()
        print(result)
        try:
            for k in loads(result[1:]).items():
                link_conf = (re.findall(r'(?P<url>https?://[^\s]+)', k))
                new_str = link_conf[2].split(']')[0]
                return new_str
        except:
            link_conf = (re.findall(r'(?P<url>https?://[^\s]+)', result))
            new_str = link_conf[2].split(']')[0]
            return new_str


if __name__ == '__main__':
    import sys

    try:
        box = mailbox()
        main(box)
    except KeyboardInterrupt:
        box.close()
        sys.exit(0)
