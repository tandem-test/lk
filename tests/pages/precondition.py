import random
import string

link = 'http://192.168.1.6:8085'
admin_login = 'mrboog1e@ya.ru'
password = '12345678'
password_encrypt = '$2y$10$v8M38Wc/DteNyatwWsmZQ.O4qs8WSudE6CbPT/nIH0REXEwB2VQc6'
"""                                                 ОО/ПК/СИСТЕМА                                                    """
EDUCATIONAL_ORGANIZATION = 'Тестовая ОО'
RECEPTION_COMPANY = 'Тестовая ПК'
SYSTEM = 'http://192.168.201.5:8080'
LOGIN = 'tandem'
PASSWORD = '123456'
BACH_2018 = '1591104071850266813'
BACH_2017 = '1565636525956272317'
POSTGRADUATE_2018 = '1591104433361522877'


def random_number(x):
    return ''.join(random.choice(string.digits) for _ in range(x))


def random_en(x):
    return ''.join(random.choice(string.ascii_letters) for _ in range(x))


def random_ru(x):
    letters = 'ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ'
    return ''.join(random.choice(letters) for _ in range(x))


def random_ch(x):
    letters = '需要注意的是，在汉语中并没有字母这个概念，汉语拼音及注音符号之类的汉字拉丁化体系都只是一套用字母来标音的符号，而不是字母。'
    return ''.join(random.choice(letters) for _ in range(x))


# Доступные роли #######################################################################################################
class Roles:
    student = 'student'
    employee = 'employee'
    parent = 'parent'
    entrant = 'entrant'


# Основные настройки ###################################################################################################
class AbiturMain:
    TRUE = True
    FALSE = False
    PDF = "pdf"
    JPG = "jpg"
    PNG = "png"
    GIF = "gif"
    DOC = "doc"
    DOCX = "docx"
    RTF = "rtf"
    RAR = "rar"
    ZIP = "zip"
    # Настройки для подготовки ПК в abitur_test.py
    ENABLE_ALL = '":{"pdf":true,"jpg":true,"gif":true,"png":true,"doc":true,"docx":true,"rtf":true,"rar":true,' \
                 '"zip":true}} '
    BY_DEFAULT = '{"107":{"pdf":true,"jpg":true,"gif":false,"png":true,"doc":false,"docx":false,"rtf":false,' \
                 '"rar":false,"zip":false}} '


# Базовые уровни образования############################################################################################
class EduLevel:
    BASIC_GENERAL = "2013.1.3"  # Основное общее образование (9 классов)
    SECONDARY_GENERAL = "2013.1.4"  # Среднее общее образование (11 классов)
    SECONDARY_VOCATIONAL = "2013.2.1"  # Среднее профессиональное образование
    UNDERGRADUATE = "2013.2.2"  # Высшее образование - бакалавриат
    MASTER = "2013.2.3"  # Высшее образование - специалитет, магистратура
    HIGHEST_QUALIFICATION = "2013.2.4"  # Высшее образование - подготовка кадров высшей квалификации
    # Настройки для подготовки ПК в abitur_test.py
    ENABLE_ALL = '":["2013.1.3","2013.1.4","2013.2.1","2013.2.2","2013.2.3","2013.2.4"]}'
    DISABLE_ALL = '":[]}'


# Реализуемые уровни образования########################################################################################
class RequestType:
    SPO = '4'  # Среднее профессиональное образование
    BACH_SPEC = '1'  # Программы бакалавриата, специалитета
    MASTER = '2'  # Программы магистратуры
    POSTGRADUATE = '5'  # Программы аспирантуры (адъюнктуры)
    RESIDENCY = '6'  # Программы ординатуры
    # Настройки для подготовки ПК в abitur_test.py
    ENABLE_ALL = '":["4","1","2","5","6"]}'
    DISABLE_ALL = '":[]}'


# Скан-копии документов#################################################################################################
class ScanCopies:
    DOCUMENT_DEFAULT_SIZE = 5242880
    DOCUMENT_FIELDS_DEFAULT_COUNT = 2
    DOCUMENT_OPTIONAL = 0
    DOCUMENT_REQUIRED = 1
    DOCUMENT_NOT_REQUIRED = 2
    DOCUMENT_SIZE = 'document_size'
    IDENTITY_CARD = 'identity_card'
    IDENTITY_CARD_FIELDS_COUNT = 'identity_card_fields_count'
    EDUCATION_DOCUMENT = 'education_document'
    EDUCATION_DOCUMENT_FIELDS_COUNT = 'education_document_fields_count'
    SNILS = 'snils'
    INDIVIDUAL_ACHIEVEMENTS = 'individual_achievements'
    DOCUMENT_PROVING_DISABILITY = 'document_proving_disability'
    TARGETED_TRAINING_CONTRACT = 'targeted_training_contract'
    BENEFIT_FOR_DORM = 'benefit_for_dorm'
    CONSENT_TO_PERSONAL_DATA = 'consent_to_personal_data'
    APPLICATION = 'application'
    CONSENT_TO_ENROLLMENT = 'consent_to_enrollment'
    OTHER_IDENTITY_CARD = 'other_identity_card'
    OLYMPIAD_DIPLOMA = 'olympiad_diploma'
    SPECIAL_RIGHT_DOCUMENT = 'special_right_document'
    # Настройки для подготовки ПК в abitur_test.py
    OPTIONAL_ALL = '":{"snils":0,"education_document":0,"identity_card":0,"individual_achievements":0,' \
                   '"document_proving_disability":0,"targeted_training_contract":0,"benefit_for_dorm":0,' \
                   '"consent_to_personal_data":0,"application":0,"consent_to_enrollment":0,"document_size":5242880,' \
                   '"identity_card_fields_count":2,"education_document_fields_count":2}} '
    REQUIRED_ALL = '":{"snils":1,"education_document":1,"identity_card":1,"individual_achievements":1,' \
                   '"document_proving_disability":1,"targeted_training_contract":1,"benefit_for_dorm":1,' \
                   '"consent_to_personal_data":1,"application":1,"consent_to_enrollment":1,"document_size":5242880,' \
                   '"identity_card_fields_count":2,"education_document_fields_count":2}} '
    DISABLE_ALL = '":{"snils":2,"education_document":2,"identity_card":2,"individual_achievements":2,' \
                  '"document_proving_disability":2,"targeted_training_contract":2,"benefit_for_dorm":2,' \
                  '"consent_to_personal_data":2,"application":2,"consent_to_enrollment":2,"document_size":5242880,' \
                  '"identity_card_fields_count":2,"education_document_fields_count":2}} '


# Порядок следования НПС в списках #####################################################################################
class OrderOfSpecialties:
    Construction_id = 1  # Строительство
    Informatics_id = 2  # Информатика и вычислительная техника
    Instrumentation_id = 3  # Приборостроение
    Land_management_id = 4  # Землеустройство и кадастры
    Economy_id = 5  # Экономика
    Commercial_business_id = 6  # Торговое дело


# Списки поступающих ###################################################################################################
class EntrantLists:
    ENABLE = '1'
    DISABLE = '0'
    OPTIONAL = '2'
    ENTRANT_LISTS_SECTION = 'entrant_lists_section'
    EDUCATIONAL_PROGRAMS_ENTRANT_LISTS = 'educational_programs_entrant_lists'
    EDUCATIONAL_PROGRAMS_ENTRANT_CARD = 'educational_programs_entrant_card'
    SNILS_OR_UNIQUE_NUMBER = 'snils_or_unique_number'
    COMPETITION_PRIORITY = 'competition_priority'
    TARGETED_ADMISSION = 'targeted_admission'
    WITHOUT_ENTRANCE_EXAMINATIONS = 'without_entrance_examinations'
    ORIGINAL_EDUCATIONAL_DOCUMENT = 'original_educational_document'
    CONSENT_TO_ENROLLMENT = 'consent_to_enrollment'
    REQUEST_NUMBER = 'request_number'


# Сроки приема заявлений ###############################################################################################
class DocumentDeadlines:
    # ФОРМА ОСВОЕНИЯ
    OCHNAYA = '1'
    ZAOCHNAYA = '2'
    OCHNO_ZAOCHNAYA = '3'
    # ВИД ВОЗМЕЩЕНИЯ ЗАТРАТ
    V_RAMKAH_KCP = '1'
    PO_DOGOVORU = '2'


# Шаг 1 (Основные данные)###############################################################################################
class FirstStep:
    # Информация о способе возврата оригиналов документов.
    RETURN_WAY_INFO = 'return_way_info'
    RETURN_WAY_INFO_YES = 'yes'
    RETURN_WAY_INFO_NO = 'no'
    # Ввод балла и года сдачи предмета ЕГЭ.
    EXAM_SCORES_AND_EXAM_YEARS = 'exam_scores_and_exam_years'
    EXAM_SCORES_AND_EXAM_YEARS_NOT_REQUIRED = 'not_required'
    EXAM_SCORES_AND_EXAM_YEARS_REQUIRED = 'required'
    EXAM_SCORES_AND_EXAM_YEARS_NO = 'no'
    # Проверять балл ЕГЭ, что он больше минимального значения.
    CHECK_EXAM_SCORES = 'check_exam_scores'
    CHECK_EXAM_SCORES_DISALLOW = 'disallow'
    CHECK_EXAM_SCORES_WARN = 'warn'
    CHECK_EXAM_SCORES_NO = 'no'
    # Флаг о допуске к участию во вступительных испытаниях, проводимых вузом самостоятельно.
    PARTICIPATION_IN_TESTS = 'participation_in_tests'
    PARTICIPATION_IN_TESTS_AVAILABLE = 'available'
    PARTICIPATION_IN_TESTS_NOT_AVAILABLE = 'not_available'
    # Выбор категории лиц, обладающих правом сдавать ВИ по материалам вуза.
    BASED_ON_UNIVERSITY_MATERIALS = 'based_on_university_materials'
    BASED_ON_UNIVERSITY_MATERIALS_YES = 'yes'
    BASED_ON_UNIVERSITY_MATERIALS_NO = 'no'
    # Выбор вступительных испытаний, проводимых вузом самостоятельно.
    SELECTING_TESTS = 'selecting_tests'
    SELECTING_TESTS_YES = 'yes'
    SELECTING_TESTS_NO = 'no'
    # Информация о необходимости специальных условий для проведения ВИ.
    NEED_FOR_SPECIAL_CONDITIONS_INFO = 'need_for_special_conditions_info'
    NEED_FOR_SPECIAL_CONDITIONS_INFO_YES = 'yes'
    NEED_FOR_SPECIAL_CONDITIONS_INFO_NO = 'no'
    # Информация о предпочитаемой форме сдачи ВИ.
    PREFERRED_FORM_INFO = 'preferred_form_info'
    PREFERRED_FORM_INFO_YES = 'yes'
    PREFERRED_FORM_INFO_NO = 'no'
    # Информация об участии в олимпиадах.
    PARTICIPATION_IN_OLYMPIADS_INFO = 'participation_in_olympiads_info'
    PARTICIPATION_IN_OLYMPIADS_INFO_YES = 'yes'
    PARTICIPATION_IN_OLYMPIADS_INFO_NO = 'no'
    # Информация об особых правах или о преимущественном праве зачисления.
    SPECIAL_RIGHTS_INFO = 'special_rights_info'
    SPECIAL_RIGHTS_INFO_YES = 'yes'
    SPECIAL_RIGHTS_INFO_NO = 'no'
    # Информация о заключении договора о целевом обучении.
    CONCLUDED_AGREEMENT_INFO = 'concluded_agreement_info'
    CONCLUDED_AGREEMENT_INFO_YES = 'yes'
    CONCLUDED_AGREEMENT_INFO_NO = 'no'
    # Настройки для подготовки ПК в abitur_test.py
    REQUIRED_ALL = '":{"concluded_agreement_info":"yes","special_rights_info":"yes",' \
                   '"participation_in_olympiads_info":"yes","preferred_form_info":"yes",' \
                   '"need_for_special_conditions_info":"yes","selecting_tests":"yes",' \
                   '"based_on_university_materials":"yes","participation_in_tests":"available",' \
                   '"check_exam_scores":"disallow","exam_scores_and_exam_years":"required","return_way_info":"yes"}} '
    OPTIONAL_ALL = '":{"concluded_agreement_info":"yes","special_rights_info":"yes",' \
                   '"participation_in_olympiads_info":"yes","preferred_form_info":"yes",' \
                   '"need_for_special_conditions_info":"yes","selecting_tests":"yes",' \
                   '"based_on_university_materials":"yes","participation_in_tests":"available",' \
                   '"check_exam_scores":"warn","exam_scores_and_exam_years":"not_required","return_way_info":"yes"}} '
    DISABLE_ALL = '":{"concluded_agreement_info":"no","special_rights_info":"no",' \
                  '"participation_in_olympiads_info":"no","preferred_form_info":"no",' \
                  '"need_for_special_conditions_info":"no","selecting_tests":"no",' \
                  '"based_on_university_materials":"no","participation_in_tests":"not_available",' \
                  '"check_exam_scores":"no","exam_scores_and_exam_years":"no","return_way_info":"no"}} '
    FINISH_BACH_APP = '{"step":2,"haveEduLevel":"2013.2.2","wantRequestType":"1","originalReturnWay":"4",' \
                      '"haveDisability":true,"preferredForm":"1","haveAgreementTargetEdu":true,' \
                      '"agreementNumber":"3414417403","agreementDate":"22.04.2021","agreementOrg":"TEST ' \
                      'agreementOrg","agreementEduProgram":"TEST agreementEduProgram","stateExamCodes":[],' \
                      '"stateExamMarks":[],"stateExamYears":[],"agreeEntranceExams":false,"entranceExamsReason":"2",' \
                      '"internalDisciplineCodes":[],"haveOlympiad":false,"olympiadInfo":"","exclusiveRights":false,' \
                      '"exclusiveRightsWithoutExams":false,"exclusiveRightsBudget":false,' \
                      '"exclusiveRightsEmptiveEnroll":false} '


# Способ возврата оригиналов документов в случае непоступления
class ReturnWay:
    PERSONALLY = '1'  # Лично
    BY_MAIL = '3'  # По почте
    BY_PROXY = '2'  # По доверенности
    IN_ELECTRONIC_FORM = '4'  # В электронной форме
    VIA_THE_SERVICE_PORTAL = '5'  # Через портал гос. услуг


# Предпочитаемая форма сдачи вступительных испытаний
class PreferredForm:
    FULL_TIME = '0'  # Очная
    REMOTE = '1'  # Дистанционная


# Я уже сдавал(а) или буду сдавать ЕГЭ по предметам
class Exam:
    ENGLISH_LANGUAGE = "8"
    BIOLOGY = "5"
    GEOGRAPHY = "7"
    COMPUTER_SCIENCE = "14"
    SPANISH_LANGUAGE = "13"
    HISTORY = "6"
    LITERATURE = "12"
    MATHEMATICS = "2"
    GERMAN_LANGUAGE = "9"
    SOCIAL_STUDIES = "11"
    RUSSIAN = "1"
    PHYSICS = "3"
    FRENCH_LANGUAGE = "10"
    CHEMISTRY = "4"


# Прошу допустить к участию во вступительных испытаниях
class EntranceExams:
    FOREIGN_CITIZENS = "2"  # Иностранные граждане
    LIMITED_OPTIONS = "1"  # Лица с ограниченными возможностями здоровья, дети-инвалиды, инвалиды
    PAST_GIA = "8"  # Лица, прошедшие ГИА по программам СОО не в форме ЕГЭ
    CERTIFICATE_IN_CRIMEA = "9"  # Лица получившие аттестат СОО в Крыму
    VOCATIONAL_EDU = "10"  # Лица, поступающие на базе профессионального образования
    #############################
    GERMAN_LANGUAGE = "9"
    LITERATURE = "12"
    SPANISH_LANGUAGE = "13"
    COMPUTER_SCIENCE = "14"
    MATHEMATICS = "2"
    RUSSIAN = "4"
    CHEMISTRY = "1"
    FRENCH_LANGUAGE = "10"
    PHYSICS = "3"
    SOCIAL_STUDIES = "11"
    HISTORY = "6"
    BIOLOGY = "5"
    ENGLISH_LANGUAGE = "8"
    GEOGRAPHY = "7"
    PHYSICAL_CULTURE = "23"


# Шаг 2 (Выбор конурсов) ###############################################################################################
class SecondStep:
    # Образовательные программы в списке конкурсов для выбора.
    EDUCATIONAL_PROGRAMS_CHOICE = 'educational_programs_choice'
    EDUCATIONAL_PROGRAMS_CHOICE_YES = 'yes'
    EDUCATIONAL_PROGRAMS_CHOICE_NO = 'no'
    # Графа с числом мест.
    PLACES_COLUMN = 'places_column'
    PLACES_COLUMN_YES = 'yes'
    PLACES_COLUMN_NO = 'no'
    # Графа с числом поданных заявлений.
    REQUESTS_COLUMN = 'requests_column'
    REQUESTS_COLUMN_YES = 'yes'
    REQUESTS_COLUMN_NO = 'no'
    # Выбор в рамках одного заявления конкурсов на бюджет и на договор.
    CHOICE_BUDGET_AND_CONTRACT = 'choice_budget_and_contract'
    CHOICE_BUDGET_AND_CONTRACT_YES = 'yes'
    CHOICE_BUDGET_AND_CONTRACT_NOT_NO = 'no'
    # В списке для выбора скрывать конкурсы, для которых введенные результаты ЕГЭ меньше минимальных баллов.
    HIDE_CONTESTS = 'hide_contests'
    HIDE_CONTESTS_YES = 'yes'
    HIDE_CONTESTS_NO = 'no'
    # Выбор приоритетов для выбранных конкурсов.
    PRIORITIES_CHOICE = 'priorities_choice'
    PRIORITIES_CHOICE_YES = 'yes'
    PRIORITIES_CHOICE_NO = 'no'
    # Выбор образовательных программ и их приоритетов для выбранных конкурсов.
    EDUCATIONAL_PROGRAMS_AND_PRIORITIES_CHOICE = 'educational_programs_and_priorities_choice'
    EDUCATIONAL_PROGRAMS_AND_PRIORITIES_CHOICE_NOT_REQUIRED = 'not_required'
    EDUCATIONAL_PROGRAMS_AND_PRIORITIES_CHOICE_REQUIRED = 'required'
    EDUCATIONAL_PROGRAMS_AND_PRIORITIES_CHOICE_NO = 'no'
    # Настройки для подготовки ПК в abitur_test.py
    REQUIRED_ALL = '":{"educational_programs_choice":"yes","places_column":"yes","requests_column":"yes",' \
                   '"choice_budget_and_contract":"yes","priorities_choice":"yes",' \
                   '"educational_programs_and_priorities_choice":"required","hide_contests":"yes"}} '
    DISABLE_ALL = '":{"educational_programs_choice":"no","places_column":"no","requests_column":"no",' \
                  '"choice_budget_and_contract":"no","priorities_choice":"no",' \
                  '"educational_programs_and_priorities_choice":"no","hide_contests":"yes"}} '
    OPTIONAL_ALL = '":{"educational_programs_choice":"yes","places_column":"yes","requests_column":"yes",' \
                   '"choice_budget_and_contract":"yes","priorities_choice":"yes",' \
                   '"educational_programs_and_priorities_choice":"not_required","hide_contests":"yes"}} '
    FINISH_BACH_APP = '{"step":2,"haveEduLevel":"2013.2.2","wantRequestType":"1","originalReturnWay":"4",' \
                      '"haveDisability":true,"preferredForm":"1","haveAgreementTargetEdu":true,' \
                      '"agreementNumber":"3414417403","agreementDate":"22.04.2021","agreementOrg":"TEST ' \
                      'agreementOrg","agreementEduProgram":"TEST agreementEduProgram","stateExamCodes":[],' \
                      '"stateExamMarks":[],"stateExamYears":[],"agreeEntranceExams":false,"entranceExamsReason":"2",' \
                      '"internalDisciplineCodes":[],"haveOlympiad":false,"olympiadInfo":"","exclusiveRights":false,' \
                      '"exclusiveRightsWithoutExams":false,"exclusiveRightsBudget":false,' \
                      '"exclusiveRightsEmptiveEnroll":false,"competitions":["1569549480418614582"],' \
                      '"competitionPriorities":{"1569549480418614582":"1"},"requestedProgramPriorities":{' \
                      '"1569549480418614582":{"1565636529315911834":"1"}}} '


#  Шаг 3 (Заполнение анкетных данных) ##################################################################################
class ThirdStep:
    OPTIONAL = 0
    REQUIRED = 1
    NOT_REQUIRED = 2
    REQUIRED_FOR_RF = 3
    REQUIRED_FOR_UNDERAGE = 4
    SNILS = 'snils'
    EDUCATIONAL_PROGRAM_RESULTS = 'educational_program_results'
    RELATIVE_DATA = 'relative_data'
    SECOND_RELATIVE_DATA = 'second_relative_data'
    RELATIVE_CONTACT_PHONE = 'relative_contact_phone'
    MAIN_FOREIGN_LANGUAGE = 'main_foreign_language'
    WORK_POSITION = 'work_position'
    INN = 'inn'
    COURSES_INFO = 'courses_info'
    EDUCATIONAL_ORGANIZATION_INFO_SOURCES = 'educational_organization_info_sources'
    SPORTS_ACHIEVEMENTS_INFO = 'sports_achievements_info'
    MILITARY_INFO = 'military_info'


#  Поиск абитуриентов ##################################################################################################
class EntrantsSearch:
    RECEIVE = True
    NOT_RECEIVE = False
    DATA = 'data'
    SEARCH_SECTION = 'search_section'
    PROMPT_TEXT = 'prompt_text'
    SNILS_OR_UNIQUE_NUMBER = 'snils_or_unique_number'
    ENTRANCE_EXAMINATIONS_SCHEDULE = 'entrance_examinations_schedule'


# Конкурсные списки ####################################################################################################
class CompetitionLists:
    NOT_SHOW = 0
    SHOW = 1
    SHOW_APPLICATION_NUMBER = 2
    COMPETITION_LISTS_SECTION = 'competition_lists_section'
    EDUCATIONAL_PROGRAMS_COMPETITION_LISTS = 'educational_programs_competition_lists'
    EDUCATIONAL_PROGRAMS_COMPETITION_CARD = 'educational_programs_competition_card'
    COMPETITION_CARD_LINKS = 'competition_card_links'
    SNILS_OR_UNIQUE_NUMBER = 'snils_or_unique_number'
    TARGETED_ADMISSION = 'targeted_admission'
    WITHOUT_ENTRANCE_EXAMINATIONS = 'without_entrance_examinations'
    AVERAGE_MARK = 'average_mark'
    COMPETITION_POINTS_SUM = 'competition_points_sum'
    ENTRANCE_EXAMINATIONS_POINTS_SUM = 'entrance_examinations_points_sum'
    INDIVIDUAL_ACHIEVEMENTS_POINTS_SUM = 'individual_achievements_points_sum'
    ENTRANCE_EXAMINATIONS_POINTS_DETAILING = 'entrance_examinations_points_detailing'
    INDIVIDUAL_ACHIEVEMENTS_POINTS_DETAILING = 'individual_achievements_points_detailing'
    PREEMPTIVE_RIGHTS = 'preemptive_rights'
    ORIGINAL_EDUCATIONAL_DOCUMENT = 'original_educational_document'
    CONSENT_TO_ENROLLMENT = 'consent_to_enrollment'
    STATEMENT_STATE = 'statement_state'
    # Настройки для подготовки ПК в abitur_test.py
    DISABLE_ALL = '":{"competition_lists_section":0}}'
    ENABLE_ALL = '":{"competition_lists_section":1,"educational_programs_competition_lists":1,' \
                 '"educational_programs_competition_card":1,"competition_card_links":1,"snils_or_unique_number":1,' \
                 '"targeted_admission":1,"without_entrance_examinations":1,"average_mark":1,' \
                 '"competition_points_sum":1,"entrance_examinations_points_sum":1,' \
                 '"individual_achievements_points_sum":1,"entrance_examinations_points_detailing":1,' \
                 '"individual_achievements_points_detailing":1,"preemptive_rights":1,' \
                 '"original_educational_document":1,"consent_to_enrollment":1,"statement_state":1}} '


# Договоры на обучение #################################################################################################
# Действия в разделе "Договоры на обучение"
class ContractAction:
    REQUIRED = "required"
    NOT_REQUIRED = "not_required"
    NO = "no"
    YES = "yes"


# Договоры на обучение
class ContractCommon:
    CONTRACT_REQUEST_COUNT = "contract_request_count"
    MOTHER_CERT_SCAN = "mother_cert_scan"
    # Настройки для подготовки ПК в abitur_test.py
    OPTIONAL_ALL = '":{"mother_cert_scan":"not_required","contract_request_count":"1"}}'
    REQUEST_ALL = '":{"mother_cert_scan":"required","contract_request_count":"1"}}'


# Заполнение полей заявки типа заказчика «Физическое лицо (абитуриент)»
class ContractEntrantPersonType:
    PERSONAL_DATA_AGREEMENT = "personal_data_agreement"
    # Настройки для подготовки ПК в abitur_test.py
    DISABLE_ALL = '":{"personal_data_agreement":"no"}}'
    ENABLE_ALL = '":{"personal_data_agreement":"yes"}}'


# Заполнение полей заявки типа заказчика «Физическое лицо (иное)»
class ContractOtherPersonType:
    PERSONAL_DATA_AGREEMENT = "personal_data_agreement"
    PHONE = "phone"
    SNILS = "snils"
    INN = "inn"
    BANK_NAME = "bank_name"
    BIK = "bik"
    BANK_ACCOUNT = "bank_account"
    WORK = "work"
    POSITION = "position"
    PASSPORT_SCAN = "passport_scan"
    # Настройки для подготовки ПК в abitur_test.py
    OPTIONAL_ALL = '":{"phone":"not_required","personal_data_agreement":"no","snils":"not_required",' \
                   '"inn":"not_required","bank_name":"not_required","bik":"not_required",' \
                   '"bank_account":"not_required","work":"not_required","position":"not_required",' \
                   '"passport_scan":"not_required"}} '
    REQUEST_ALL = '":{"phone":"required","personal_data_agreement":"yes","snils":"required","inn":"required",' \
                  '"bank_name":"required","bik":"required","bank_account":"required","work":"required",' \
                  '"position":"required","passport_scan":"required"}} '


# Заполнение полей заявки типа заказчика «Юридическое лицо»
class ContractLegalPersonType:
    REGISTRATION_COUNTRY = "registration_country"
    REGISTRATION_DATE = "registration_date"
    AUTHORIZED_PERSON = "authorized_person"
    INN = "inn"
    KPP = "kpp"
    OGRN = "ogrn"
    OKPO = "okpo"
    CARD_LINK = "card_link"
    LEGAL_ADDRESS = "legal_address"
    REAL_ADDRESS = "real_address"
    BANK_NAME = "bank_name"
    BIK = "bik"
    CHECKING_ACCOUNT = "checking_account"
    CORRESPONDENT_ACCOUNT = "correspondent_account"
    PHONE = "phone"
    GUARANTEE_LETTER_SCAN = "guarantee_letter_scan"
    CARD_SCAN = "card_scan"
    CREDENTIAL_PROOF_SCAN = "credential_proof_scan"
    # Настройки для подготовки ПК в abitur_test.py
    OPTIONAL_ALL = '":{"registration_country":"not_required","registration_date":"not_required",' \
                   '"authorized_person":"not_required","inn":"not_required","kpp":"not_required",' \
                   '"ogrn":"not_required","okpo":"not_required","card_link":"not_required",' \
                   '"legal_address":"not_required","real_address":"not_required","bank_name":"not_required",' \
                   '"bik":"not_required","checking_account":"not_required","correspondent_account":"not_required",' \
                   '"phone":"not_required","guarantee_letter_scan":"not_required","card_scan":"not_required",' \
                   '"credential_proof_scan":"not_required"}} '
    REQUEST_ALL = '":{"registration_country":"required","registration_date":"required",' \
                  '"authorized_person":"required","inn":"required","kpp":"required","ogrn":"required",' \
                  '"okpo":"required","card_link":"required","legal_address":"required","real_address":"required",' \
                  '"bank_name":"required","bik":"required","checking_account":"required",' \
                  '"correspondent_account":"required","phone":"required","guarantee_letter_scan":"required",' \
                  '"card_scan":"required","credential_proof_scan":"required"}} '
