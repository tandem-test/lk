from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


class Database(object):
    session = None
    conn = None

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(Database, cls).__new__(cls)
        return cls.instance

    def __init__(self):
        if Database.session is None:
            self.__connect_to_db()

    def __connect_to_db(self):
        if Database.session is not None:
            return 1
        try:
            engine = create_engine('mysql+pymysql://admin:12345678@192.168.1.6:3306/abitur', isolation_level='READ COMMITTED')
            session = sessionmaker(bind=engine)
            Database.session = session()
            Database.conn = engine.connect()
            return 1
        except Exception as e:
            return 0

    def disconnect(self):
        Database.conn.close()
