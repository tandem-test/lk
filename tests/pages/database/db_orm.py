__author__ = 'zbirko'

from sqlalchemy import Column, Integer, MetaData, Text, String, Date, TIMESTAMP, JSON
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
Base.metadata = MetaData(schema='abitur')


class Users(Base):
    __tablename__ = 'users'
    id = Column('id', Integer, primary_key=True)
    uuid = Column('uuid', String)
    human_id = Column('human_id', String)
    last_name = Column('last_name', String)
    first_name = Column('first_name', String)
    middle_name = Column('middle_name', String)
    birth_date = Column('birth_date', Date)
    sex = Column('sex', Integer)
    email = Column('email', String)
    email_verified_at = Column('email_verified_at', TIMESTAMP)
    phone = Column('phone', String)
    phone_verified_at = Column('phone_verified_at', TIMESTAMP)
    password = Column('password', String)
    two_factor_secret = Column('two_factor_secret', Text)
    two_factor_recovery_codes = Column('two_factor_recovery_codes', Text)
    remember_token = Column('remember_token', String)
    api_token = Column('api_token', String)
    avatar = Column('avatar', String)
    current_team_id = Column('current_team_id', Integer)
    profile_photo_path = Column('profile_photo_path', Text)
    created_at = Column('created_at', TIMESTAMP)
    updated_at = Column('updated_at', TIMESTAMP)
    username = Column('username', String)
    domain = Column('domain', String)
    deleted_at = Column('deleted_at', TIMESTAMP)


class Admins(Base):
    __tablename__ = 'admins'
    id = Column('id', Integer, primary_key=True)
    created_at = Column('created_at', TIMESTAMP)
    updated_at = Column('updated_at', TIMESTAMP)


class Roles(Base):
    __tablename__ = 'roles'
    id = Column('id', Integer, primary_key=True)
    name = Column('name', String)
    guard_name = Column('guard_name', String)
    title = Column('title', String)
    description = Column('description', String)
    created_at = Column('created_at', TIMESTAMP)
    updated_at = Column('updated_at', TIMESTAMP)
    assignment_description = Column('assignment_description', Text)


class Model_has_roles(Base):
    __tablename__ = 'model_has_roles'
    role_id = Column('role_id', Integer, primary_key=True)
    model_type = Column('model_type', String)
    model_id = Column('model_id', Integer)


class Permissions(Base):
    __tablename__ = 'permissions'
    id = Column('id', Integer, primary_key=True)
    name = Column('name', String)
    guard_name = Column('guard_name', String)
    title = Column('title', String)
    description = Column('description', String)
    created_at = Column('created_at', TIMESTAMP)
    updated_at = Column('updated_at', TIMESTAMP)


class Model_has_permissions(Base):
    __tablename__ = 'model_has_permissions'
    permission_id = Column('permission_id', Integer, primary_key=True)
    model_type = Column('model_type', String)
    model_id = Column('model_id', Integer)


class Assign_role_by_columns(Base):
    __tablename__ = 'assign_role_by_columns'
    id = Column('id', String, primary_key=True)
    role_id = Column('role_id', Integer)
    column_id = Column('column_id', String)
    created_at = Column('created_at', TIMESTAMP)
    updated_at = Column('updated_at', TIMESTAMP)


class Find_human_columns(Base):
    __tablename__ = 'find_human_columns'
    id = Column('id', String, primary_key=True)
    table = Column('table', String)
    human_id_column = Column('human_id_column', String)
    column = Column('column', String)
    title = Column('title', String)
    type = Column('type', String)
    placeholder = Column('placeholder', String)
    required = Column('required', Integer)
    group = Column('group', String)
    order = Column('order', Integer)
    created_at = Column('created_at', TIMESTAMP)
    updated_at = Column('updated_at', TIMESTAMP)


class Settings(Base):
    __tablename__ = 'settings'
    id = Column('id', Integer, primary_key=True)
    name = Column('name', String)
    value = Column('value', Text)
    created_at = Column('created_at', TIMESTAMP)
    updated_at = Column('updated_at', TIMESTAMP)


########################################################################################################################
class Ticket_types(Base):
    __tablename__ = 'ticket_types'
    id = Column('id', Integer, primary_key=True)
    title = Column('title', String)
    description = Column('description', Text)
    created_at = Column('created_at', TIMESTAMP)
    updated_at = Column('updated_at', TIMESTAMP)
    is_active = Column('is_active', Integer)


class Tickets(Base):
    __tablename__ = 'tickets'
    id = Column('id', Integer, primary_key=True)
    abstract_chat_id = Column('abstract_chat_id', Integer)
    responsible_id = Column('responsible_id', Integer)
    status_id = Column('status_id', Integer)
    priority_id = Column('priority_id', Integer)
    type_id = Column('type_id', Integer)
    number = Column('number', String)
    text = Column('text', Text)
    finished_at = Column('finished_at', TIMESTAMP)
    deleted_at = Column('deleted_at', TIMESTAMP)
    created_at = Column('created_at', TIMESTAMP)
    updated_at = Column('updated_at', TIMESTAMP)


class Ticket_statuses(Base):
    __tablename__ = 'ticket_statuses'
    id = Column('id', Integer, primary_key=True)
    title = Column('title', String)
    description = Column('description', Text)
    color = Column('color', String)
    created_at = Column('created_at', TIMESTAMP)
    updated_at = Column('updated_at', TIMESTAMP)


class Abstract_chats(Base):
    __tablename__ = 'abstract_chats'
    id = Column('id', Integer, primary_key=True)
    creator_id = Column('creator_id', Integer)
    title = Column('title', String)
    deleted_at = Column('deleted_at', TIMESTAMP)
    created_at = Column('created_at', TIMESTAMP)
    updated_at = Column('updated_at', TIMESTAMP)


class Chat_users(Base):
    __tablename__ = 'chat_users'
    id = Column('id', Integer, primary_key=True)
    abstract_chat_id = Column('abstract_chat_id', Integer)
    user_id = Column('user_id', Integer)
    deleted_at = Column('deleted_at', TIMESTAMP)
    created_at = Column('created_at', TIMESTAMP)
    updated_at = Column('updated_at', TIMESTAMP)


class Chat_messages(Base):
    __tablename__ = 'chat_messages'
    id = Column('id', Integer, primary_key=True)
    abstract_chat_id = Column('abstract_chat_id', Integer)
    sender_id = Column('sender_id', Integer)
    type = Column('type', String)
    data = Column('data', Text)
    deleted_at = Column('deleted_at', TIMESTAMP)
    created_at = Column('created_at', TIMESTAMP)
    updated_at = Column('updated_at', TIMESTAMP)


class Chats(Base):
    __tablename__ = 'chats'
    id = Column('id', Integer, primary_key=True)
    abstract_chat_id = Column('abstract_chat_id', Integer)
    deleted_at = Column('deleted_at', TIMESTAMP)
    created_at = Column('created_at', TIMESTAMP)
    updated_at = Column('updated_at', TIMESTAMP)


########################################################################################################################
class External_org_units(Base):
    __tablename__ = 'external_org_units'
    id = Column('id', Integer, primary_key=True)
    title = Column('title', String)
    short_title = Column('short_title', String)
    logo = Column('logo', String)
    legal_form_id = Column('legal_form_id', Integer)  # Данная колонка не задействована
    parent_id = Column('parent_id', Integer)
    legal_address = Column('legal_address', String)
    fact_address = Column('fact_address', String)
    description = Column('description', Text)
    created_at = Column('created_at', TIMESTAMP)
    updated_at = Column('updated_at', TIMESTAMP)


class External_org_contacts(Base):
    __tablename__ = 'external_org_contacts'
    id = Column('id', Integer, primary_key=True)
    external_org_unit_id = Column('external_org_unit_id', Integer)
    enrollment_campaign_id = Column('enrollment_campaign_id', Integer)
    address = Column('address', String)
    how_to_get_to = Column('how_to_get_to', Text)
    phones = Column('phones', JSON)
    faxes = Column('faxes', JSON)
    emails = Column('emails', JSON)
    enrollment_schedule = Column('enrollment_schedule', JSON)
    secretary = Column('secretary', JSON)
    created_at = Column('created_at', TIMESTAMP)
    updated_at = Column('updated_at', TIMESTAMP)


class Enrollment_campaigns(Base):
    __tablename__ = 'enrollment_campaigns'
    id = Column('id', Integer, primary_key=True)
    campaign_id = Column('campaign_id', Integer)
    external_org_unit_id = Column('external_org_unit_id', String)
    type_id = Column('type_id', Integer)
    installation_id = Column('installation_id', Integer)
    title = Column('title', String)
    original_title = Column('original_title', String)
    disabled_at = Column('disabled_at', TIMESTAMP)
    created_at = Column('created_at', TIMESTAMP)
    updated_at = Column('updated_at', TIMESTAMP)
    checked_at = Column('checked_at', TIMESTAMP)


class User_enrollment_campaigns(Base):
    __tablename__ = 'user_enrollment_campaigns'
    user_id = Column('user_id', Integer, primary_key=True)
    campaign_id = Column('campaign_id', Integer)


class Enrollment_campaign_types(Base):
    __tablename__ = 'enrollment_campaign_types'
    id = Column('id', Integer, primary_key=True)
    code = Column('code', String)
    title = Column('title', String)
    short_title = Column('short_title', String)


class Uni_installations(Base):
    __tablename__ = 'uni_installations'
    id = Column('id', Integer, primary_key=True)
    external_org_unit_id = Column('external_org_unit_id', Integer)
    url = Column('url', String)
    login = Column('login', String)
    password = Column('password', String)
    last_pinged_at = Column('last_pinged_at', TIMESTAMP)
    available = Column('available', Integer)
    created_at = Column('created_at', TIMESTAMP)
    updated_at = Column('updated_at', TIMESTAMP)


########################################################################################################################

class Online_entrant_requests(Base):
    __tablename__ = 'online_entrant_requests'
    id = Column('id', Integer, primary_key=True)
    user_id = Column('user_id', Integer)
    data = Column('data', Text)
    state = Column('state', String)
    campaign_id = Column('campaign_id', String)


class Attached_files(Base):
    __tablename__ = 'attached_files'
    id = Column('id', Integer, primary_key=True)
    user_id = Column('user_id', Integer)
    attacheable_type = Column('attacheable_type', String)
    type_code = Column('type_code', String)
    path = Column('path', String)
    filename = Column('filename', String)
    deleted_at = Column('deleted_at', TIMESTAMP)
    created_at = Column('created_at', TIMESTAMP)
    updated_at = Column('updated_at', TIMESTAMP)


# Нормативные документы ################################################################################################
class Regulation_categories(Base):
    __tablename__ = 'regulation_categories'
    id = Column('id', Integer, primary_key=True)
    title = Column('title', String)
    short_title = Column('short_title', String)
    active = Column('active', Integer)
    created_at = Column('created_at', TIMESTAMP)
    updated_at = Column('updated_at', TIMESTAMP)


class Regulation_types(Base):
    __tablename__ = 'regulation_types'
    id = Column('id', Integer, primary_key=True)
    title = Column('title', String)
    short_title = Column('short_title', String)
    regulation_category_id = Column('regulation_category_id', Integer)
    active = Column('active', Integer)
    created_at = Column('created_at', TIMESTAMP)
    updated_at = Column('updated_at', TIMESTAMP)


class Regulations(Base):
    __tablename__ = 'regulations'
    id = Column('id', Integer, primary_key=True)
    title = Column('title', String)
    short_title = Column('short_title', String)
    regulation_type_id = Column('regulation_type_id', Integer)
    description = Column('description', Text)
    link = Column('link', String)
    active = Column('active', Integer)
    external_org_unit_id = Column('external_org_unit_id', Integer)
    enrollment_campaign_id = Column('enrollment_campaign_id', Integer)
    created_at = Column('created_at', TIMESTAMP)
    updated_at = Column('updated_at', TIMESTAMP)


class Accepting_document_deadlines(Base):
    __tablename__ = 'accepting_document_deadlines'
    id = Column('id', Integer, primary_key=True)
    campaign_id = Column('campaign_id', Integer)
    request_type_id = Column('request_type_id', Integer)
    edu_program_form_id = Column('edu_program_form_id', Integer)
    compensation_type_id = Column('compensation_type_id', Integer)
    starts_at = Column('starts_at', TIMESTAMP)
    ends_in = Column('ends_in', TIMESTAMP)
    created_at = Column('created_at', TIMESTAMP)
    updated_at = Column('updated_at', TIMESTAMP)



