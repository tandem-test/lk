import json
from . import database as DB
import datetime
from .db_orm import *
from ..precondition import random_number, random_en, admin_login, password_encrypt, OrderOfSpecialties, \
    ContractLegalPersonType, ContractOtherPersonType, ContractEntrantPersonType, ContractCommon, AbiturMain, \
    ThirdStep, EntrantsSearch, EntrantLists

db = DB.Database()
now = datetime.datetime.now()


# Получить текущую дату и время для записи
def date():
    time = datetime.datetime.now()
    date_pull = time.year, time.month, time.day, time.hour - 3, time.minute, time.second
    date_type = '%d-%d-%d %d:%d:%d' % date_pull
    return date_type


def date_record_db():
    result = datetime.datetime(now.year, now.month, now.day, 0, 0)
    return result


class DATABASE:
    def __init__(self):
        self.db = DB.Database()


class USERS(DATABASE):
    # Создать новую верифицированную учетную запись
    def create_a_new_verified_user(self, last_name, first_name, middle_name, email):
        uuid = random_en(8) + '-' + random_number(4) + '-' + random_number(4) + random_en(12)
        user = Users(uuid=uuid, last_name=last_name, first_name=first_name, middle_name=middle_name, email=email,
                     email_verified_at=date(), password=password_encrypt, created_at=date(), updated_at=date())
        self.db.session.add(user)  # Добавляем запись о новом пользователе
        self.db.session.commit()  # Добавляем данные а таблицу

    # Получить данные о новом пользователе по {email}
    def get_data_about_a_new_user_by_email(self, email):
        data = self.db.session.query(Users.email, Users.last_name, Users.first_name, Users.middle_name) \
            .filter(Users.email == email).first()
        return data

    # Получить {id} пользователя по {email}
    def get_user_id_by_email(self, email):
        user_id = self.db.session.query(Users.id).filter(Users.email == email).first()
        data = str(user_id).replace('(', '').replace(')', '').replace(',', '')
        return data

    # Получить {deleted_at} пользователя по {email}
    def get_user_deleted_at_by_email(self, email):
        deleted_at = self.db.session.query(Users.deleted_at).filter(Users.email == email).first()
        data = str(deleted_at).replace('(', '').replace(')', '').replace(',', '')
        return data

    # Получить {avatar} пользователя по {email}
    def get_avatar_by_email(self, email):
        avatar = self.db.session.query(Users.avatar).filter(Users.email == email).first()
        data = str(avatar).replace('(', '').replace(')', '').replace(',', '')
        return data

    # Получить {password} пользователя по {email}
    def get_password_by_email(self, email):
        password = self.db.session.query(Users.password).filter(Users.email == email).first()
        data = str(password).replace('(', '').replace(')', '').replace(',', '')
        return data


class ADMINS(DATABASE):
    def create_a_user_with_administrator_rights(self):
        uuid = random_en(8) + '-' + random_number(4) + '-' + random_number(4) + random_en(12)
        try:
            user = Users(uuid=uuid, last_name='Збирко', first_name='Дмитрий', middle_name='Николаевич',
                         email=admin_login, email_verified_at=date(), password=password_encrypt,
                         created_at=date(), updated_at=date())
            self.db.session.add(user)
            self.db.session.commit()
            admin = Admins(id=USERS().get_user_id_by_email(admin_login), created_at=date(), updated_at=date())
            self.db.session.add(admin)  # Добавляем запись о новом пользователе
            self.db.session.commit()  # Добавляем данные а таблицу
        except:
            self.db.session.close()
            'Пользователь уже создан'

    def get_id_admins_by_id(self, email):
        admin_id = self.db.session.query(Admins.id).filter(Admins.id == USERS().get_user_id_by_email(email)).first()
        data = str(admin_id).replace('(', '').replace(')', '').replace(',', '')
        return data


class ROLES(DATABASE):
    # Получить данные о назначении роли по {model_id}
    def get_data_on_role_assignment_by_model_id(self, email):
        data = self.db.session.query(Model_has_roles.role_id, Model_has_roles.model_id). \
            filter(Model_has_roles.model_id == USERS().get_user_id_by_email(email))
        return data

    # Получить {id} роли по названию {name}
    def get_id_by_name(self, role):
        user_id = self.db.session.query(Roles.id).filter(Roles.name == role).first()
        data = str(user_id).replace('(', '').replace(')', '').replace(',', '')
        return data

    # Получить {id} роли по названию {title}
    def get_id_by_title(self, role):
        user_id = self.db.session.query(Roles.id).filter(Roles.title == role).first()
        data = str(user_id).replace('(', '').replace(')', '').replace(',', '').replace("'", "")
        return data

    # Назначить пользователю роль по {email}
    def assign_role_to_user(self, email, role):
        role = Model_has_roles(role_id=ROLES().get_id_by_name(role), model_type='Tandem\\Base\\User\\Models\\User',
                               model_id=USERS().get_user_id_by_email(email))
        self.db.session.add(role)
        self.db.session.commit()

    # Получить описание к виджету
    def get_assignment_description_by_title(self, role):
        data = self.db.session.query(Roles.assignment_description).filter(Roles.title == role).first()
        result = str(data).replace('(', '').replace(')', '').replace(',', '').replace("'", "")
        return result

    # Обновить описание выбранной роле
    def update_assignment_description_in_role(self, role, description):
        data = self.db.session.query(Roles).filter(Roles.title == role).first()
        data.assignment_description = description
        self.db.session.add(data)
        self.db.session.commit()


class PERMISSION(DATABASE):
    # Получить {id} по {name}
    def get_id_permission_by_name(self, permission):
        permission = self.db.session.query(Permissions.id).filter(Permissions.name == permission).first()
        data = str(permission).replace('(', '').replace(')', '').replace(',', '')
        return data

    # Получить данные о назначении разрешений по {model_id}
    def get_data_on_role_permission_by_model_id(self, email):
        data = self.db.session.query(Model_has_permissions.permission_id, Model_has_permissions.model_id). \
            filter(Model_has_permissions.model_id == USERS().get_user_id_by_email(email)).first()
        return data

    # Назначить пользователю разрешение по {email}
    def assign_permission_to_user(self, email, permission):
        role = Model_has_permissions(permission_id=PERMISSION().get_id_permission_by_name(permission),
                                     model_type='Tandem\\Base\\User\\Models\\User',
                                     model_id=USERS().get_user_id_by_email(email))
        self.db.session.add(role)
        self.db.session.commit()

    # Добавить новую запись в abitur.permissions
    def add_new_permission(self, name, title):
        data = Permissions(name=name, guard_name='web', title=title, description='test', created_at=date())
        self.db.session.add(data)
        self.db.session.commit()

    # Удалить ранее созданное разрешение
    def deleted_new_permission(self, name):
        permission = self.db.session.query(Permissions).filter(Permissions.name == name).first()
        self.db.session.delete(permission)
        self.db.session.commit()

    def get_data_model_has_roles(self, permission):
        data = self.db.session.query(Model_has_permissions.model_id, Model_has_permissions.permission_id). \
            filter(Model_has_permissions.permission_id == PERMISSION().get_id_permission_by_name(permission))
        return data


class SETTING(DATABASE):
    # /settings/assign-roles/ ##########################################################################################
    # Получить {id} find_human_columns по {column}
    def get_id_find_human_by_human_id_column(self, form):
        column = self.db.session.query(Find_human_columns.id).filter(Find_human_columns.column == form).first()
        data = str(column).replace('(', '').replace(')', '').replace(',', '').replace("'", '')
        return data

    # Удалить все записи в таблице abitur.assign_role_by_columns
    def delete_entries_in_assign_role_by_columns(self):
        while True:
            try:
                column = self.db.session.query(Assign_role_by_columns).first()
                self.db.session.delete(column)
                self.db.session.commit()
            except:
                break

    # Получить запись о присвоении поля роли
    def get_a_record_of_assigning_a_field_to_a_role(self, form):
        data = self.db.session.query(Assign_role_by_columns.role_id, Assign_role_by_columns.column_id).filter(
            Assign_role_by_columns.column_id == SETTING().get_id_find_human_by_human_id_column(form)).first()
        return data

    # Создать запись о присвоении формы ввода для роли
    def add_form_assignment_record_for_role(self, form, role):
        uuid = random_en(8) + '-' + random_number(4) + '-' + random_number(4) + random_en(12)
        data = Assign_role_by_columns(id=uuid, role_id=ROLES().get_id_by_title(role),
                                      column_id=SETTING().get_id_find_human_by_human_id_column(form),
                                      created_at=date(), updated_at=date())
        self.db.session.add(data)
        self.db.session.commit()

    # Получить {value} settings по {name}
    def get_settings_app_by_name(self, setting):
        data = self.db.session.query(Settings.value).filter(Settings.name == setting).first()
        result = str(data).replace('(', '').replace(')', '').replace(',', '').replace("'", '')
        return result

    # Обновить {value} settings по {name}
    def update_settings_app(self, setting, value):
        try:
            data = self.db.session.query(Settings).filter(Settings.name == setting).first()
            data.value = value
            self.db.session.add(data)
            self.db.session.commit()
        except:
            data = Settings(name=setting, value=value, created_at=date(), updated_at=date())
            self.db.session.add(data)
            self.db.session.commit()

    # /application/eo-settings/org-units ##############################################################################
    # Получить данные о компании по {title}
    def get_data_external_org_units(self, title):
        data = self.db.session.query(External_org_units.title, External_org_units.short_title,
                                     External_org_units.parent_id, External_org_units.legal_address,
                                     External_org_units.fact_address, External_org_units.description). \
            filter(External_org_units.title == title).first()
        return data

    # Получить {logo} External_org_units по {title}
    def get_logo_org_by_title(self, title):
        data = self.db.session.query(External_org_units.logo).filter(External_org_units.title == title).first()
        result = str(data).replace('(', '').replace(')', '').replace(',', '')
        return result

    # Добавить новую ОО в External_org_units
    def add_educational_organization(self, title, parent_id):
        if parent_id is None:
            parent_ids = None
        else:
            parent_ids = SETTING().get_id_org_by_title(parent_id)
        data = External_org_units(title=title, short_title=random_en(8), parent_id=parent_ids,
                                  legal_address=random_en(8), fact_address=random_en(8), description=random_en(8),
                                  created_at=date(), updated_at=date())
        self.db.session.add(data)
        self.db.session.commit()

    # Получить {parent_id} External_org_units по {title}
    def get_parent_id_by_title(self, title):
        data = self.db.session.query(External_org_units.parent_id).filter(External_org_units.title == title).first()
        result = str(data).replace('(', '').replace(')', '').replace(',', '')
        return result

    # Получить {id} External_org_units по {title}
    def get_id_org_by_title(self, title):
        data = self.db.session.query(External_org_units.id).filter(External_org_units.title == title).first()
        result = str(data).replace('(', '').replace(')', '').replace(',', '')
        return result

    # Получить данные uni_installations по {external_org_unit_id}
    def get_data_uni_by_org_id(self, title, login):
        data = self.db.session.query(Uni_installations.url, Uni_installations.login, Uni_installations.password,
                                     Uni_installations.available).filter(Uni_installations.external_org_unit_id ==
                                                                         SETTING().get_id_org_by_title(title) or
                                                                         Uni_installations.login == login).first()
        return data

    # Добавить запись в Uni_installations
    def add_uni_installations(self, title, url, login, password, available):
        data = Uni_installations(external_org_unit_id=SETTING().get_id_org_by_title(title), url=url, login=login,
                                 password=password, available=available, created_at=date(), updated_at=date())
        self.db.session.add(data)
        self.db.session.commit()

    # Получить {id} enrollment_campaign_types по {title}
    def get_id_campaign_types_by_title(self, title):
        data = self.db.session.query(Enrollment_campaign_types.id).filter(Enrollment_campaign_types.title
                                                                          == title).first()
        result = str(data).replace('(', '').replace(')', '').replace(',', '')
        return result

    # Получить {id} uni_installations.id по {url}
    def get_id_uni_installations_by_url(self, url):
        data = self.db.session.query(Uni_installations.id).filter(Uni_installations.url == url).all()[-1]
        result = str(data).replace('(', '').replace(')', '').replace(',', '')
        return result

    # Получить данные enrollment_campaigns по {title}
    def get_data_enrollment_campaigns(self, org, pk):
        data = self.db.session.query(Enrollment_campaigns.type_id, Enrollment_campaigns.installation_id,
                                     Enrollment_campaigns.title, Enrollment_campaigns.original_title). \
            filter(Enrollment_campaigns.external_org_unit_id == SETTING().
                   get_id_org_by_title(org) or Enrollment_campaigns.title == pk).first()
        return data

    # Получить {disabled_at} enrollment_campaigns по {title}
    def get_disabled_at_enrollment_campaigns_by_title(self, org, pk):
        data = self.db.session.query(Enrollment_campaigns.disabled_at).filter(
            Enrollment_campaigns.external_org_unit_id == SETTING().get_id_org_by_title(
                org) or Enrollment_campaigns.title == pk).first()
        message = str(data).replace('(', '').replace(')', '').replace(',', '')
        return message

    # Добавить новую ПК
    def add_enrollment_campaigns(self, campaign_id, org, title, original_title, type_id, system, status):
        try:
            if status == 'inactive':
                result = date()
            else:
                result = None
            data = Enrollment_campaigns(campaign_id=campaign_id,
                                        external_org_unit_id=SETTING().get_id_org_by_title(org),
                                        type_id=SETTING().get_id_campaign_types_by_title(type_id),
                                        installation_id=SETTING().get_id_uni_installations_by_url(system), title=title,
                                        original_title=original_title, disabled_at=result,
                                        created_at=date(), updated_at=date(), checked_at=date())
            self.db.session.add(data)
            self.db.session.commit()
        except:
            self.db.session.close()

    # Обновить тип приёмной компании
    def update_type_enrollment_campaigns(self, title_pk, type_id):
        data = self.db.session.query(Enrollment_campaigns).filter(Enrollment_campaigns.title == title_pk).first()
        data.type_id = SETTING().get_id_campaign_types_by_title(type_id)
        self.db.session.add(data)
        self.db.session.commit()

    # Получить campaigns_id ПК по ОО или ПК
    def get_enrollment_campaigns_id_by_org_or_pk(self, org, pk):
        data = self.db.session.query(Enrollment_campaigns.campaign_id).filter(Enrollment_campaigns.external_org_unit_id
                                                                              == SETTING().get_id_org_by_title(org) or
                                                                              Enrollment_campaigns.title == pk).first()
        result = str(data).replace('(', '').replace(')', '').replace(',', '')
        return result

    # Получить id ПК
    def get_enrollment_campaigns_id(self, title_pk):
        data = self.db.session.query(Enrollment_campaigns.id).filter(Enrollment_campaigns.title == title_pk).first()
        result = str(data).replace('(', '').replace(')', '').replace(',', '')
        return result

    # Получить данные external_org_contacts по ОО//ПК
    def get_data_general_settings_external_org_contacts(self, org, pk):
        data = self.db.session.query(External_org_contacts.address, External_org_contacts.how_to_get_to,
                                     External_org_contacts.phones, External_org_contacts.faxes,
                                     External_org_contacts.emails).filter(
            External_org_contacts.external_org_unit_id == SETTING().get_id_org_by_title(org) or
            External_org_contacts.enrollment_campaign_id == SETTING().get_enrollment_campaigns_id(pk)).first()
        return data

    # Получить данные {executive_external_org_contacts} External_org_contacts и распарсить
    def get_data_executive_external_org_contacts(self, org, pk):
        data = self.db.session.query(External_org_contacts.secretary).filter(
            External_org_contacts.external_org_unit_id == SETTING().get_id_org_by_title(org) or
            External_org_contacts.enrollment_campaign_id == SETTING().get_enrollment_campaigns_id(pk)).first()
        name = data[0][0]["name"]
        email = data[0][0]["email"]
        phone = data[0][0]["phone"]
        office = data[0][0]["office"]
        position = data[0][0]["position"]
        return (name, email, phone, office, position)

    # Получить данные {executive_external_org_contacts} External_org_contacts и распарсить
    def get_data_working_hours_external_org_contacts(self, org, pk, day):
        data = self.db.session.query(External_org_contacts.enrollment_schedule).filter(
            External_org_contacts.external_org_unit_id == SETTING().get_id_org_by_title(org) or
            External_org_contacts.enrollment_campaign_id == SETTING().get_enrollment_campaigns_id(pk)).first()
        with_time = data[0][day]["start"]
        by_time = data[0][day]["end"]
        return (with_time, by_time)

    # Добавить данные в {executive_external_org_contacts} External_org_contacts
    def add_data_external_org_contacts(self, org, pk):
        data = External_org_contacts(external_org_unit_id=SETTING().get_id_org_by_title(org),
                                     enrollment_campaign_id=SETTING().get_enrollment_campaigns_id(pk),
                                     address=random_en(8), how_to_get_to=random_en(8),
                                     phones=random_number(10), faxes=random_number(8),
                                     emails=random_en(8) + '@gmail.com', enrollment_schedule=[],
                                     secretary=[{"name": "", "email": "", "phone": "", "office": "", "position": ""}],
                                     created_at=date(), updated_at=date())
        self.db.session.add(data)
        self.db.session.commit()

    # Получить данные {basic_education_levels} Settings и распарсить
    def get_data_basic_education_levels(self, title_pk):
        data = self.db.session.query(Settings.value).filter(Settings.name == 'basic_education_levels').first()
        dictData = json.loads(data[0])
        campaign_id = SETTING().get_enrollment_campaigns_id(title_pk)
        basic_education_levels = dictData[campaign_id]
        return basic_education_levels

    # Получить данные {implemented_education_levels} Settings и распарсить
    def get_data_implemented_education_levels(self, title_pk):
        data = self.db.session.query(Settings.value).filter(Settings.name == 'implemented_education_levels').first()
        dictData = json.loads(data[0])
        campaign_id = SETTING().get_enrollment_campaigns_id(title_pk)
        implemented_education_levels = dictData[campaign_id]
        return implemented_education_levels

    # Получить данные {abitur_scan_copies} Settings и распарсить
    def get_data_abitur_scan_copies(self, title_pk):
        data = self.db.session.query(Settings.value).filter(Settings.name == 'abitur_scan_copies').first()
        dictData = json.loads(data[0])
        campaign_id = SETTING().get_enrollment_campaigns_id(title_pk)
        identity_card = dictData[campaign_id]["identity_card"]
        education_document = dictData[campaign_id]["education_document"]
        snils = dictData[campaign_id]["snils"]
        individual_achievements = dictData[campaign_id]["individual_achievements"]
        document_proving_disability = dictData[campaign_id]["document_proving_disability"]
        targeted_training_contract = dictData[campaign_id]["targeted_training_contract"]
        benefit_for_dorm = dictData[campaign_id]["benefit_for_dorm"]
        consent_to_personal_data = dictData[campaign_id]["consent_to_personal_data"]
        application = dictData[campaign_id]["application"]
        consent_to_enrollment = dictData[campaign_id]["consent_to_enrollment"]
        identity_card_fields_count = dictData[campaign_id]["identity_card_fields_count"]
        education_document_fields_count = dictData[campaign_id]["education_document_fields_count"]
        document_size = dictData[campaign_id]["document_size"]
        return (identity_card, education_document, snils, individual_achievements, document_proving_disability,
                targeted_training_contract, benefit_for_dorm, consent_to_personal_data, application,
                consent_to_enrollment, document_size, identity_card_fields_count, education_document_fields_count)

    # Получить данные {order_of_specialties} Settings и распарсить
    def get_data_order_of_specialties(self, title_pk):
        data = self.db.session.query(Settings.value).filter(Settings.name == 'order_of_specialties').first()
        dictData = json.loads(data[0])
        campaign_id = SETTING().get_enrollment_campaigns_id(title_pk)
        construction = dictData[campaign_id][OrderOfSpecialties.Construction_id - 1]["order"]
        informatics = dictData[campaign_id][OrderOfSpecialties.Informatics_id - 1]["order"]
        instrumentation = dictData[campaign_id][OrderOfSpecialties.Instrumentation_id - 1]["order"]
        land_management = dictData[campaign_id][OrderOfSpecialties.Land_management_id - 1]["order"]
        economy = dictData[campaign_id][OrderOfSpecialties.Economy_id - 1]["order"]
        commercial_business = dictData[campaign_id][OrderOfSpecialties.Commercial_business_id - 1]["order"]
        return (construction, informatics, instrumentation, land_management, economy, commercial_business)

    # Получить данные по умолчанию {order_of_specialties} Settings и распарсить
    def get_default_data_order_of_specialties(self, title_pk):
        data = self.db.session.query(Settings.value).filter(Settings.name == 'order_of_specialties').first()
        dictData = json.loads(data[0])
        campaign_id = SETTING().get_enrollment_campaigns_id(title_pk)
        default_data = dictData[campaign_id]
        return default_data

    # Получить данные {abitur_first_step} Settings и распарсить
    def get_data_abitur_first_step(self, title_pk):
        data = self.db.session.query(Settings.value).filter(Settings.name == 'abitur_first_step').first()
        dictData = json.loads(data[0])
        campaign_id = SETTING().get_enrollment_campaigns_id(title_pk)
        return_way_info = dictData[campaign_id]["return_way_info"]
        exam_scores_and_exam_years = dictData[campaign_id]["exam_scores_and_exam_years"]
        check_exam_scores = dictData[campaign_id]["check_exam_scores"]
        participation_in_tests = dictData[campaign_id]["participation_in_tests"]
        based_on_university_materials = dictData[campaign_id]["based_on_university_materials"]
        selecting_tests = dictData[campaign_id]["selecting_tests"]
        need_for_special_conditions_info = dictData[campaign_id]["need_for_special_conditions_info"]
        preferred_form_info = dictData[campaign_id]["preferred_form_info"]
        participation_in_olympiads_info = dictData[campaign_id]["participation_in_olympiads_info"]
        special_rights_info = dictData[campaign_id]["special_rights_info"]
        concluded_agreement_info = dictData[campaign_id]["concluded_agreement_info"]
        return (return_way_info, exam_scores_and_exam_years, check_exam_scores, participation_in_tests,
                based_on_university_materials, selecting_tests, need_for_special_conditions_info, preferred_form_info,
                participation_in_olympiads_info, special_rights_info, concluded_agreement_info)

    # Получить данные {abitur_first_step} Settings и распарсить
    def get_data_abitur_second_step(self, title_pk):
        data = self.db.session.query(Settings.value).filter(Settings.name == 'abitur_second_step').first()
        dictData = json.loads(data[0])
        campaign_id = SETTING().get_enrollment_campaigns_id(title_pk)
        educational_programs_choice = dictData[campaign_id]["educational_programs_choice"]
        places_column = dictData[campaign_id]["places_column"]
        requests_column = dictData[campaign_id]["requests_column"]
        choice_budget_and_contract = dictData[campaign_id]["choice_budget_and_contract"]
        hide_contests = dictData[campaign_id]["hide_contests"]
        priorities_choice = dictData[campaign_id]["priorities_choice"]
        educational_programs_and_priorities_choice = dictData[campaign_id]["educational_programs_and_priorities_choice"]
        return (educational_programs_choice, places_column, requests_column, choice_budget_and_contract, hide_contests,
                priorities_choice, educational_programs_and_priorities_choice)

    # Получить данные {abitur_competition_lists} Settings и распарсить
    def get_data_abitur_competition_lists(self, title_pk):
        data = self.db.session.query(Settings.value).filter(Settings.name == 'abitur_competition_lists').first()
        dictData = json.loads(data[0])
        campaign_id = SETTING().get_enrollment_campaigns_id(title_pk)
        # competition_lists_section = dictData[campaign_id]["competition_lists_section"]
        educational_programs_competition_lists = dictData[campaign_id]["educational_programs_competition_lists"]
        educational_programs_competition_card = dictData[campaign_id]["educational_programs_competition_card"]
        # competition_card_links = dictData[campaign_id]["competition_card_links"]
        snils_or_unique_number = dictData[campaign_id]["snils_or_unique_number"]
        statement_state = dictData[campaign_id]["statement_state"]
        consent_to_enrollment = dictData[campaign_id]["consent_to_enrollment"]
        original_educational_document = dictData[campaign_id]["original_educational_document"]
        preemptive_rights = dictData[campaign_id]["preemptive_rights"]
        individual_achievements_points_detailing = dictData[campaign_id]["individual_achievements_points_detailing"]
        entrance_examinations_points_detailing = dictData[campaign_id]["entrance_examinations_points_detailing"]
        individual_achievements_points_sum = dictData[campaign_id]["individual_achievements_points_sum"]
        entrance_examinations_points_sum = dictData[campaign_id]["entrance_examinations_points_sum"]
        competition_points_sum = dictData[campaign_id]["competition_points_sum"]
        average_mark = dictData[campaign_id]["average_mark"]
        without_entrance_examinations = dictData[campaign_id]["without_entrance_examinations"]
        targeted_admission = dictData[campaign_id]["targeted_admission"]
        return (educational_programs_competition_lists, educational_programs_competition_card, snils_or_unique_number,
                statement_state, consent_to_enrollment, original_educational_document, preemptive_rights,
                individual_achievements_points_detailing, entrance_examinations_points_detailing,
                individual_achievements_points_sum, entrance_examinations_points_sum, competition_points_sum,
                average_mark, without_entrance_examinations, targeted_admission)

    # Получить данные {contract_common} Settings и распарсить
    def get_data_contract_common(self, title_pk):
        data = self.db.session.query(Settings.value).filter(Settings.name == 'contract_common').first()
        dictData = json.loads(data[0])
        campaign_id = SETTING().get_enrollment_campaigns_id(title_pk)
        contract_request_count = dictData[campaign_id][ContractCommon.CONTRACT_REQUEST_COUNT]
        mother_cert_scan = dictData[campaign_id][ContractCommon.MOTHER_CERT_SCAN]
        return (contract_request_count, mother_cert_scan)

    # Получить данные {contract_entrant_person_type} Settings и распарсить
    def get_data_contract_entrant_person_type(self, title_pk):
        data = self.db.session.query(Settings.value).filter(Settings.name == 'contract_entrant_person_type').first()
        dictData = json.loads(data[0])
        campaign_id = SETTING().get_enrollment_campaigns_id(title_pk)
        personal_data_agreement = dictData[campaign_id][ContractEntrantPersonType.PERSONAL_DATA_AGREEMENT]
        return (personal_data_agreement)

    # Получить данные {contract_other_person_type} Settings и распарсить
    def get_data_contract_other_person_type(self, title_pk):
        data = self.db.session.query(Settings.value).filter(Settings.name == 'contract_other_person_type').first()
        dictData = json.loads(data[0])
        campaign_id = SETTING().get_enrollment_campaigns_id(title_pk)
        personal_data_agreement = dictData[campaign_id][ContractOtherPersonType.PERSONAL_DATA_AGREEMENT]
        phone = dictData[campaign_id][ContractOtherPersonType.PHONE]
        snils = dictData[campaign_id][ContractOtherPersonType.SNILS]
        inn = dictData[campaign_id][ContractOtherPersonType.INN]
        bank_name = dictData[campaign_id][ContractOtherPersonType.BANK_NAME]
        bik = dictData[campaign_id][ContractOtherPersonType.BIK]
        bank_account = dictData[campaign_id][ContractOtherPersonType.BANK_NAME]
        work = dictData[campaign_id][ContractOtherPersonType.WORK]
        position = dictData[campaign_id][ContractOtherPersonType.POSITION]
        passport_scan = dictData[campaign_id][ContractOtherPersonType.PASSPORT_SCAN]
        return (personal_data_agreement, phone, snils, inn, bank_name, bik, bank_account, work, position, passport_scan)

    # Получить данные {contract_legal_person_type} Settings и распарсить
    def get_data_contract_legal_person_type(self, title_pk):
        data = self.db.session.query(Settings.value).filter(Settings.name == 'contract_legal_person_type').first()
        dictData = json.loads(data[0])
        campaign_id = SETTING().get_enrollment_campaigns_id(title_pk)
        registration_country = dictData[campaign_id][ContractLegalPersonType.REGISTRATION_COUNTRY]
        registration_date = dictData[campaign_id][ContractLegalPersonType.REGISTRATION_DATE]
        authorized_person = dictData[campaign_id][ContractLegalPersonType.AUTHORIZED_PERSON]
        inn = dictData[campaign_id][ContractLegalPersonType.INN]
        kpp = dictData[campaign_id][ContractLegalPersonType.KPP]
        ogrn = dictData[campaign_id][ContractLegalPersonType.OGRN]
        okpo = dictData[campaign_id][ContractLegalPersonType.OKPO]
        card_link = dictData[campaign_id][ContractLegalPersonType.CARD_LINK]
        legal_address = dictData[campaign_id][ContractLegalPersonType.LEGAL_ADDRESS]
        real_address = dictData[campaign_id][ContractLegalPersonType.REAL_ADDRESS]
        bank_name = dictData[campaign_id][ContractLegalPersonType.BANK_NAME]
        bik = dictData[campaign_id][ContractLegalPersonType.BIK]
        checking_account = dictData[campaign_id][ContractLegalPersonType.CHECKING_ACCOUNT]
        correspondent_account = dictData[campaign_id][ContractLegalPersonType.CORRESPONDENT_ACCOUNT]
        phone = dictData[campaign_id][ContractLegalPersonType.PHONE]
        guarantee_letter_scan = dictData[campaign_id][ContractLegalPersonType.GUARANTEE_LETTER_SCAN]
        card_scan = dictData[campaign_id][ContractLegalPersonType.CARD_SCAN]
        credential_proof_scan = dictData[campaign_id][ContractLegalPersonType.CREDENTIAL_PROOF_SCAN]
        return (registration_country, registration_date, authorized_person, inn, kpp, ogrn, okpo, card_link,
                legal_address, real_address, bank_name, bik, checking_account, correspondent_account, phone,
                guarantee_letter_scan, card_scan, credential_proof_scan)

    # Получить данные {abitur_main} Settings и распарсить
    def get_data_abitur_main(self, title_pk):
        data = self.db.session.query(Settings.value).filter(Settings.name == 'abitur_main').first()
        dictData = json.loads(data[0])
        campaign_id = SETTING().get_enrollment_campaigns_id(title_pk)
        PDF = dictData[campaign_id][AbiturMain.PDF]
        JPG = dictData[campaign_id][AbiturMain.JPG]
        PNG = dictData[campaign_id][AbiturMain.PNG]
        GIF = dictData[campaign_id][AbiturMain.GIF]
        DOC = dictData[campaign_id][AbiturMain.DOC]
        DOCX = dictData[campaign_id][AbiturMain.DOCX]
        RTF = dictData[campaign_id][AbiturMain.RTF]
        RAR = dictData[campaign_id][AbiturMain.RAR]
        ZIP = dictData[campaign_id][AbiturMain.ZIP]
        online_requests_count = dictData[campaign_id]["online_requests_count"]
        return (ZIP, PDF, JPG, PNG, GIF, DOC, DOCX, RTF, RAR, online_requests_count)

    # Получить данные {abitur_third_step} Settings и распарсить
    def get_data_abitur_third_step(self, title_pk):
        data = self.db.session.query(Settings.value).filter(Settings.name == 'abitur_third_step').first()
        dictData = json.loads(data[0])
        campaign_id = SETTING().get_enrollment_campaigns_id(title_pk)
        snils = dictData[campaign_id][ThirdStep.SNILS]
        educational_program_results = dictData[campaign_id][ThirdStep.EDUCATIONAL_PROGRAM_RESULTS]
        relative_data = dictData[campaign_id][ThirdStep.RELATIVE_DATA]
        second_relative_data = dictData[campaign_id][ThirdStep.SECOND_RELATIVE_DATA]
        relative_contact_phone = dictData[campaign_id][ThirdStep.RELATIVE_CONTACT_PHONE]
        main_foreign_language = dictData[campaign_id][ThirdStep.MAIN_FOREIGN_LANGUAGE]
        work_position = dictData[campaign_id][ThirdStep.WORK_POSITION]
        inn = dictData[campaign_id][ThirdStep.INN]
        courses_info = dictData[campaign_id][ThirdStep.COURSES_INFO]
        educational_organization_info_sources = dictData[campaign_id][ThirdStep.EDUCATIONAL_ORGANIZATION_INFO_SOURCES]
        sports_achievements_info = dictData[campaign_id][ThirdStep.SPORTS_ACHIEVEMENTS_INFO]
        military_info = dictData[campaign_id][ThirdStep.MILITARY_INFO]
        return (snils, educational_program_results, relative_data, second_relative_data, relative_contact_phone,
                main_foreign_language, work_position, inn, courses_info, educational_organization_info_sources,
                sports_achievements_info, military_info)

    # Получить данные {abitur_entrants_search} Settings и распарсить
    def get_data_abitur_entrants_search(self, title_pk):
        data = self.db.session.query(Settings.value).filter(Settings.name == 'abitur_entrants_search').first()
        dictData = json.loads(data[0])
        campaign_id = SETTING().get_enrollment_campaigns_id(title_pk)
        data = dictData[campaign_id][EntrantsSearch.DATA]
        search_section = dictData[campaign_id][EntrantsSearch.SEARCH_SECTION]
        prompt_text = dictData[campaign_id][EntrantsSearch.PROMPT_TEXT]
        snils_or_unique_number = dictData[campaign_id][EntrantsSearch.SNILS_OR_UNIQUE_NUMBER]
        entrance_examinations_schedule = dictData[campaign_id][EntrantsSearch.ENTRANCE_EXAMINATIONS_SCHEDULE]
        return (data, search_section, prompt_text, snils_or_unique_number, entrance_examinations_schedule)

    # Получить данные {abitur_entrant_list} Settings и распарсить
    def get_data_abitur_entrant_list(self, title_pk):
        data = self.db.session.query(Settings.value).filter(Settings.name == 'abitur_entrant_list').first()
        dictData = json.loads(data[0])
        campaign_id = SETTING().get_enrollment_campaigns_id(title_pk)
        entrant_lists_section = dictData[campaign_id][EntrantLists.ENTRANT_LISTS_SECTION]
        educational_programs_entrant_lists = dictData[campaign_id][EntrantLists.EDUCATIONAL_PROGRAMS_ENTRANT_LISTS]
        educational_programs_entrant_card = dictData[campaign_id][EntrantLists.EDUCATIONAL_PROGRAMS_ENTRANT_CARD]
        snils_or_unique_number = dictData[campaign_id][EntrantLists.SNILS_OR_UNIQUE_NUMBER]
        competition_priority = dictData[campaign_id][EntrantLists.COMPETITION_PRIORITY]
        targeted_admission = dictData[campaign_id][EntrantLists.TARGETED_ADMISSION]
        without_entrance_examinations = dictData[campaign_id][EntrantLists.WITHOUT_ENTRANCE_EXAMINATIONS]
        original_educational_document = dictData[campaign_id][EntrantLists.ORIGINAL_EDUCATIONAL_DOCUMENT]
        consent_to_enrollment = dictData[campaign_id][EntrantLists.CONSENT_TO_ENROLLMENT]
        request_number = dictData[campaign_id][EntrantLists.REQUEST_NUMBER]
        return (entrant_lists_section, educational_programs_entrant_lists, educational_programs_entrant_card,
                snils_or_unique_number, competition_priority, targeted_admission, without_entrance_examinations,
                original_educational_document, consent_to_enrollment, request_number)

    # Получить данные {tips_for_applicant} Settings и распарсить
    def get_data_tips_for_applicant(self, title_pk):
        data = self.db.session.query(Settings.value).filter(Settings.name == 'tips_for_applicant').first()
        dictData = json.loads(data[0])
        campaign_id = SETTING().get_enrollment_campaigns_id(title_pk)
        haveEduLevel = dictData[campaign_id]["haveEduLevel"]
        wantRequestType = dictData[campaign_id]["wantRequestType"]
        originalReturnWay = dictData[campaign_id]["originalReturnWay"]
        stateExamCodes = dictData[campaign_id]["stateExamCodes"]
        agreeEntranceExams = dictData[campaign_id]["agreeEntranceExams"]
        haveDisability = dictData[campaign_id]["haveDisability"]
        haveOlympiad = dictData[campaign_id]["haveOlympiad"]
        exclusiveRights = dictData[campaign_id]["exclusiveRights"]
        haveAgreementTargetEdu = dictData[campaign_id]["haveAgreementTargetEdu"]
        competitionParams = dictData[campaign_id]["competitionParams"]
        competitions = dictData[campaign_id]["competitions"]
        competitionPriorities = dictData[campaign_id]["competitionPriorities"]
        cardType = dictData[campaign_id]["cardType"]
        citizenship = dictData[campaign_id]["citizenship"]
        issuancePlace = dictData[campaign_id]["issuancePlace"]
        birthPlace = dictData[campaign_id]["birthPlace"]
        regAddress = dictData[campaign_id]["regAddress"]
        factAddress = dictData[campaign_id]["factAddress"]
        snilsNumber = dictData[campaign_id]["snilsNumber"]
        eduOrganizationCountry = dictData[campaign_id]["eduOrganizationCountry"]
        eduOrganizationSettlement = dictData[campaign_id]["eduOrganizationSettlement"]
        eduOrganization = dictData[campaign_id]["eduOrganization"]
        eduDocumentKind = dictData[campaign_id]["eduDocumentKind"]
        seriaEduDocument = dictData[campaign_id]["seriaEduDocument"]
        mark3 = dictData[campaign_id]["mark3"]
        mark4 = dictData[campaign_id]["mark4"]
        mark5 = dictData[campaign_id]["mark5"]
        avgMarkAsLong = dictData[campaign_id]["avgMarkAsLong"]
        avgScoreAsLong = dictData[campaign_id]["avgScoreAsLong"]
        marks = dictData[campaign_id]["marks"]
        individualAchievements = dictData[campaign_id]["individualAchievements"]
        needDorm = dictData[campaign_id]["needDorm"]
        relatives = dictData[campaign_id]["relatives"]
        foreignLanguage = dictData[campaign_id]["foreignLanguage"]
        identityCardPagesScans = dictData[campaign_id]["identityCardPagesScans"]
        otherIdentityCardForPassExamsPagesScans = dictData[campaign_id]["otherIdentityCardForPassExamsPagesScans"]
        educationDocumentTitlePageScans = dictData[campaign_id]["educationDocumentTitlePageScans"]
        snilsScans = dictData[campaign_id]["snilsScans"]
        individualAchievementsScans = dictData[campaign_id]["individualAchievementsScans"]
        benefitForDormScans = dictData[campaign_id]["benefitForDormScans"]
        consentToPersonalDataScans = dictData[campaign_id]["consentToPersonalDataScans"]
        applicationScans = dictData[campaign_id]["applicationScans"]
        consentToEnrollmentScans = dictData[campaign_id]["consentToEnrollmentScans"]
        submittingConsentForEnrollment = dictData[campaign_id]["submittingConsentForEnrollment"]
        enrollmentConsentStatementScan = dictData[campaign_id]["enrollmentConsentStatementScan"]
        submittingRefusalToEnroll = dictData[campaign_id]["submittingRefusalToEnroll"]
        enrollmentRefusalApplicationScan = dictData[campaign_id]["enrollmentRefusalApplicationScan"]
        withdrawalOfApplication = dictData[campaign_id]["withdrawalOfApplication"]
        applicationToWithdrawApplicationScan = dictData[campaign_id]["applicationToWithdrawApplicationScan"]
        return (haveEduLevel, wantRequestType, originalReturnWay, stateExamCodes, agreeEntranceExams, haveDisability,
                haveOlympiad, exclusiveRights, haveAgreementTargetEdu, competitionParams, competitions,
                competitionPriorities, cardType, citizenship, issuancePlace, birthPlace, regAddress, factAddress,
                snilsNumber, eduOrganizationCountry, eduOrganizationSettlement, eduOrganization, eduDocumentKind,
                seriaEduDocument, mark3, mark4, mark5, avgMarkAsLong, avgScoreAsLong, marks, individualAchievements,
                needDorm, relatives, foreignLanguage, identityCardPagesScans, otherIdentityCardForPassExamsPagesScans,
                educationDocumentTitlePageScans, snilsScans, individualAchievementsScans, benefitForDormScans,
                consentToPersonalDataScans, applicationScans, consentToEnrollmentScans, submittingConsentForEnrollment,
                enrollmentConsentStatementScan, submittingRefusalToEnroll, enrollmentRefusalApplicationScan,
                withdrawalOfApplication, applicationToWithdrawApplicationScan)

    # Получить данные {accepting_document_deadlines}
    def get_data_accepting_document_deadlines(self, title_pk):
        TABLE = Accepting_document_deadlines
        data = self.db.session.query(TABLE.request_type_id, TABLE.edu_program_form_id, TABLE.compensation_type_id,
                                     TABLE.starts_at, TABLE.ends_in).filter(
            Accepting_document_deadlines.campaign_id == SETTING().get_enrollment_campaigns_id(title_pk)).all()[-1]
        return data

    def add_record_accepting_document_deadlines(self, title_pk, request_type, edu_program,
                                                compensation_type, starts_at, ends_in):
        data = Accepting_document_deadlines(campaign_id=SETTING().get_enrollment_campaigns_id(title_pk),
                                            request_type_id=request_type, edu_program_form_id=edu_program,
                                            compensation_type_id=compensation_type, starts_at=starts_at,
                                            ends_in=ends_in, created_at=date(), updated_at=date())
        self.db.session.add(data)
        self.db.session.commit()

    # Удалить запись в таблице enrollment_campaigns
    def delete_record_enrollment_campaigns(self, title_pk):
        while True:
            try:
                if title_pk == (None):
                    column = self.db.session.query(Enrollment_campaigns).first()
                else:
                    column = self.db.session.query(Enrollment_campaigns).filter(
                        Enrollment_campaigns.title == title_pk).all()[-1]
                self.db.session.delete(column)
                self.db.session.commit()
            except:
                break

    def delete_record_regulations(self, title_pk):
        while True:
            try:
                if title_pk == (None):
                    column = self.db.session.query(Regulations).first()
                else:
                    column = self.db.session.query(Regulations).filter(
                        Regulations.enrollment_campaign_id == SETTING().get_enrollment_campaigns_id(title_pk)).all()[-1]
                self.db.session.delete(column)
                self.db.session.commit()
            except:
                break

    def delete_record_accepting_document_deadlines(self, title_pk):
        while True:
            try:
                if title_pk == (None):
                    column = self.db.session.query(Accepting_document_deadlines).first()
                else:
                    column = self.db.session.query(Accepting_document_deadlines).filter(
                        Accepting_document_deadlines == SETTING().get_enrollment_campaigns_id(title_pk)).all()[-1]
                self.db.session.delete(column)
                self.db.session.commit()
            except:
                break

    # Добавить запись в user_enrollment_campaigns
    def add_record_user_enrollment_campaigns(self, email_abitur, title_pk):
        data = User_enrollment_campaigns(user_id=USERS().get_user_id_by_email(email_abitur),
                                         campaign_id=SETTING().get_enrollment_campaigns_id(title_pk))
        self.db.session.add(data)
        self.db.session.commit()

    # Удалить запись в таблице user_enrollment_campaigns
    def delete_record_user_enrollment_campaigns(self, title_pk):
        while True:
            try:
                if title_pk is None:
                    column = self.db.session.query(User_enrollment_campaigns).first()
                else:
                    column = self.db.session.query(User_enrollment_campaigns).filter(
                        User_enrollment_campaigns.campaign_id == SETTING().get_enrollment_campaigns_id(
                            title_pk)).first()
                self.db.session.delete(column)
                self.db.session.commit()
            except:
                break

    # Удалить запись в таблице uni_installations
    def delete_record_uni_installations(self, system):
        while True:
            try:
                if system is None:
                    column = self.db.session.query(Uni_installations).first()
                else:
                    column = self.db.session.query(Uni_installations).filter(Uni_installations.url == system).all()[-1]
                self.db.session.delete(column)
                self.db.session.commit()
            except:
                break

    # Удалить запись в таблице external_org_units
    def delete_record_external_org_units(self, title_org):
        while True:
            try:
                if title_org is None:
                    column = self.db.session.query(External_org_units).first()
                else:
                    column = self.db.session.query(External_org_units).filter(
                        External_org_units.title == title_org).first()
                self.db.session.delete(column)
                self.db.session.commit()
            except:
                break

    # Получить данные в regulation_categories
    def get_data_regulation_categories(self, title):
        data = self.db.session.query(Regulation_categories.title, Regulation_categories.short_title,
                                     Regulation_categories.active).filter(Regulation_categories.title == title).first()
        return data

    # Получить данные в regulation_types
    def get_data_regulation_types(self, title):
        data = self.db.session.query(Regulation_types.regulation_category_id, Regulation_types.title,
                                     Regulation_types.short_title, Regulation_types.active).filter(
            Regulation_types.title == title).first()
        return data

    # Получить {id} regulation_categories по {title}
    def get_id_regulation_categories_by_title(self, title):
        data = self.db.session.query(Regulation_categories.id).filter(Regulation_categories.title == title).first()
        result = str(data).replace('(', '').replace(')', '').replace(',', '')
        return result

    # Получить {id} regulation_types по {title}
    def get_id_regulation_types_by_title(self, title):
        data = self.db.session.query(Regulation_types.id).filter(Regulation_types.title == title).first()
        result = str(data).replace('(', '').replace(')', '').replace(',', '')
        return result

    # Добавить данные в regulation_categories
    def add_data_regulation_categories(self, title, short_title, actives):
        if actives is True:
            active = 1
        else:
            active = 0
        data = Regulation_categories(title=title, short_title=short_title, active=active,
                                     created_at=date(), updated_at=date())
        self.db.session.add(data)
        self.db.session.commit()

    # Добавить данные в regulation_types
    def add_data_regulation_types(self, title, short_title, title_category, actives):
        if actives is True:
            active = 1
        else:
            active = 0
        data = Regulation_types(title=title, short_title=short_title,
                                regulation_category_id=SETTING().get_id_regulation_categories_by_title(title_category),
                                active=active, created_at=date(), updated_at=date())
        self.db.session.add(data)
        self.db.session.commit()

    # Добавить данные в regulations
    def add_data_regulations(self, title, short_title, title_category, description, link, actives, title_org, title_pk):
        if actives is True:
            active = 1
        else:
            active = 0
        data = Regulations(title=title, short_title=short_title, description=description, link=link,
                           regulation_type_id=SETTING().get_id_regulation_types_by_title(title_category),
                           active=active, external_org_unit_id=SETTING().get_id_org_by_title(title_org),
                           enrollment_campaign_id=SETTING().get_enrollment_campaigns_id(title_pk),
                           created_at=date(), updated_at=date())
        self.db.session.add(data)
        self.db.session.commit()

    def get_data_regulations(self, title_regulations):
        data = self.db.session.query(Regulations.title, Regulations.short_title, Regulations.regulation_type_id,
                                     Regulations.description, Regulations.link, Regulations.active,
                                     Regulations.external_org_unit_id, Regulations.enrollment_campaign_id).filter(
            Regulations.title == title_regulations).first()
        return data


class CHATS(DATABASE):
    # Добавить вид обращения в abitur.ticket_types
    def add_ticket_types(self, title, description, is_active):
        data = Ticket_types(title=title, description=description,
                            is_active=is_active, created_at=date(), updated_at=date())
        self.db.session.add(data)
        self.db.session.commit()

    # Получить {title}, {description}, {is_active} по {title}
    def get_data_type_chat_by_title(self, title):
        data = self.db.session.query(Ticket_types.title, Ticket_types.description, Ticket_types.is_active).filter(
            Ticket_types.title == title).first()
        return data

    # Получить {id} статуса по {title}
    def get_ticket_statuses_id_by_title(self, status):
        column = self.db.session.query(Ticket_statuses.id).filter(Ticket_statuses.title == status).first()
        data = str(column).replace('(', '').replace(')', '').replace(',', '').replace("'", '')
        return data

    # Получить {id} вида обращения по {title}
    def get_ticket_type_id_by_title(self, title):
        column = self.db.session.query(Ticket_types.id).filter(Ticket_types.title == title).first()
        data = str(column).replace('(', '').replace(')', '').replace(',', '').replace("'", '')
        return data

    # Получить данные {id} Abstract_chats по {title}
    def get_id_abstract_chats_by_title(self, title):
        data = self.db.session.query(Abstract_chats.id).filter(Abstract_chats.title == title).first()
        message = str(data).replace('(', '').replace(')', '').replace(',', '')
        return message

    # Добавить запись в Tickets
    def add_new_ticket(self, title, text, responsible, status, type_id, deleted):
        if deleted == 'yes':
            result = date()
        else:
            result = None
        data = Tickets(abstract_chat_id=CHATS().get_id_abstract_chats_by_title(title), responsible_id=responsible,
                       status_id=CHATS().get_ticket_statuses_id_by_title(status), number=random_number(10), text=text,
                       type_id=CHATS().get_ticket_type_id_by_title(type_id), created_at=date(), deleted_at=result)
        self.db.session.add(data)
        self.db.session.commit()

    # Добавить запись в Abstract_chats
    def add_new_abstract_chats(self, title, creator_id, deleted):
        if deleted == 'yes':
            result = date()
        else:
            result = None
        data = Abstract_chats(creator_id=creator_id, title=title, created_at=date(),
                              updated_at=date(), deleted_at=result)
        self.db.session.add(data)
        self.db.session.commit()

    # Добавить запись в Chat_users
    def add_new_chat_users(self, email, title):
        data = Chat_users(abstract_chat_id=CHATS().get_id_abstract_chats_by_title(title),
                          user_id=USERS().get_user_id_by_email(email),
                          created_at=date(), updated_at=date())
        self.db.session.add(data)
        self.db.session.commit()

    # Добавить запись в Chats
    def add_new_chats(self, title):
        data = Chats(abstract_chat_id=CHATS().get_id_abstract_chats_by_title(title))
        self.db.session.add(data)
        self.db.session.commit()

    # Добавить запись в Chat_message
    def add_data_chat_message(self, title_chat, email, message):
        data = Chat_messages(abstract_chat_id=CHATS().get_id_abstract_chats_by_title(title_chat),
                             sender_id=USERS().get_user_id_by_email(email),
                             type='Tandem\\Chat\\Catalogs\\ChatMessageType\\TextMessage', data=message,
                             created_at=date(), updated_at=date())
        self.db.session.add(data)
        self.db.session.commit()

    # Получить данные {responsible_id/status_id/type_id/text} tickets по {abstract_chat_id}
    def get_data_ticket_by_abstract_chat_id(self, title):
        data = self.db.session.query(Tickets.responsible_id, Tickets.status_id, Tickets.type_id, Tickets.text). \
            filter(Tickets.abstract_chat_id == CHATS().get_id_abstract_chats_by_title(title)).first()
        return data

    # Получить данные {creator_id/title} Abstract_chats по {abstract_chat_id}
    def get_data_abstract_chat_by_title(self, title):
        data = self.db.session.query(Abstract_chats.creator_id, Abstract_chats.title).filter(Abstract_chats.title
                                                                                             == title).first()
        return data

    # Получить данные {abstract_chat_id/user_id} Chat_users по {abstract_chat_id}
    def get_data_chat_users_by_title(self, title):
        data = self.db.session.query(Chat_users.abstract_chat_id, Chat_users.user_id).filter(
            Chat_users.abstract_chat_id == CHATS().get_id_abstract_chats_by_title(title)).first()
        return data

    # Получить данные {abstract_chat_id/user_id} Chat_users по {user_id}
    def get_data_chats_users_by_title(self, email):
        data = self.db.session.query(Chat_users.abstract_chat_id, Chat_users.user_id).filter(
            Chat_users.user_id == USERS().get_user_id_by_email(email)).first()
        return data

    # Получить данные {data} chat_message по {sender_id}
    def get_data_chat_message_by_sender_id(self, email, title_chat):
        data = self.db.session.query(Chat_messages.data).filter(
            Chat_messages.sender_id == USERS().get_user_id_by_email(email) or
            Chat_messages.abstract_chat_id == CHATS().get_id_abstract_chats_by_title(title_chat)).first()
        data = str(data).replace('(', '').replace(')', '').replace(',', '')
        return data

    # Получить данные {status_id} ticket по {abstract_chat_id}
    def get_status_ticket_by_abstract_chat_id(self, title):
        data = self.db.session.query(Tickets.status_id).filter(Tickets.abstract_chat_id
                                                               == CHATS().get_id_abstract_chats_by_title(title)).first()
        message = str(data).replace('(', '').replace(')', '').replace(',', '')
        return message

    # Получить данные {deleted_at} ticket по {abstract_chat_id}
    def get_deleted_at_ticket_by_abstract_chat_id(self, title):
        data = self.db.session.query(Tickets.deleted_at).filter(
            Tickets.abstract_chat_id == CHATS().get_id_abstract_chats_by_title(title)).first()
        message = str(data).replace('(', '').replace(')', '').replace(',', '')
        return message

    # Получить данные {abstract_chat_id} chats по {title_chat}
    def get_chats_abstract_chat_id_by_abstract_chat_id(self, title):
        data = self.db.session.query(Chats.abstract_chat_id).filter(
            Chats.abstract_chat_id == CHATS().get_id_abstract_chats_by_title(title)).first()
        message = str(data).replace('(', '').replace(')', '').replace(',', '')
        return message

    # Получить данные {deleted_at} chat_user по {abstract_chat_id/user_id}
    def get_chat_user_deleted_at_by_user_id(self, email, title):
        data = self.db.session.query(Chat_users.deleted_at).filter(
            Chat_users.abstract_chat_id == CHATS().get_id_abstract_chats_by_title(title) and
            Chat_users.user_id == USERS().get_user_id_by_email(email)).first()
        result = str(data).replace('(', '').replace(')', '').replace(',', '')
        return result

    # Получить данные {deleted_at} abstract_chat по {title}
    def get_deleted_abstract_chat_by_title(self, title):
        data = self.db.session.query(Chats.deleted_at).filter(
            Chats.abstract_chat_id == CHATS().get_id_abstract_chats_by_title(title)).first()
        result = str(data).replace('(', '').replace(')', '').replace(',', '')
        return result


class ABITUR_DB(DATABASE):
    # Добавить запись о прохождение заявления у абитуриента
    def add_online_entrant_requests(self, abitur_email, data, title_pk, state):
        data = Online_entrant_requests(user_id=USERS().get_user_id_by_email(abitur_email), data=data, state=state,
                                       campaign_id=SETTING().get_enrollment_campaigns_id(title_pk))
        self.db.session.add(data)
        self.db.session.commit()

    # Добавить запись о том, что пользователь ранее использовал сервис
    def add_user_enrollment_campaigns(self, abitur_email, title_pk):
        data = User_enrollment_campaigns(user_id=USERS().get_user_id_by_email(abitur_email),
                                         campaign_id=SETTING().get_enrollment_campaigns_id(title_pk))
        self.db.session.add(data)
        self.db.session.commit()

    # Удалить записи созданные в ПК (PRECONDITION) таблица online_entrant_requests
    def delete_record_online_entrant_requests(self, title_pk):
        while True:
            try:
                if title_pk is None:
                    column = self.db.session.query(Online_entrant_requests).first()
                else:
                    column = self.db.session.query(Online_entrant_requests).filter(
                        Online_entrant_requests.campaign_id == SETTING().get_enrollment_campaigns_id(title_pk)).first()
                self.db.session.delete(column)
                self.db.session.commit()
            except:
                break

    # Удалить все записи в таблице attached_files
    def delete_all_attached_files(self):
        while True:
            try:
                column = self.db.session.query(Attached_files).first()
                self.db.session.delete(column)
                self.db.session.commit()
            except:
                break

    # Удалить все записи в таблице external_org_contacts
    def delete_all_external_org_contacts(self):
        while True:
            try:
                column = self.db.session.query(External_org_contacts).first()
                self.db.session.delete(column)
                self.db.session.commit()
            except:
                break

    # Получить данные {data} online_entrant_requests по 1 шагу
    def get_data_online_entrant_requests_step_1(self, email_abitur, Disability, preForm):
        data = self.db.session.query(Online_entrant_requests.data).filter(
            Online_entrant_requests.user_id == USERS().get_user_id_by_email(email_abitur)).first()
        dictData = json.loads(data[0])
        step = dictData["step"]
        haveEduLevel = dictData["haveEduLevel"]
        wantRequestType = dictData["wantRequestType"]
        originalReturnWay = dictData["originalReturnWay"]
        if None is not Disability:
            haveDisability = dictData["haveDisability"]
        else:
            haveDisability = None
        if None is not preForm:
            preferredForm = dictData["preferredForm"]
        else:
            preferredForm = None
        aveAgreementTargetEdu = dictData["haveAgreementTargetEdu"]
        agreementNumber = dictData["agreementNumber"]
        agreementDate = dictData["agreementDate"]
        agreementOrg = dictData["agreementOrg"]
        agreementEduProgram = dictData["agreementEduProgram"]
        return (step, haveEduLevel, wantRequestType, originalReturnWay, haveDisability, preferredForm,
                aveAgreementTargetEdu, agreementNumber, agreementDate, agreementOrg, agreementEduProgram)

    # Получить данные {data} online_entrant_requests по 2 шагу
    def get_data_online_entrant_requests_step_2(self, email_abitur):
        data = self.db.session.query(Online_entrant_requests.data).filter(
            Online_entrant_requests.user_id == USERS().get_user_id_by_email(email_abitur)).first()
        dictData = json.loads(data[0])
        step = dictData["step"]
        competitions = dictData["competitions"]
        result1 = str(competitions).replace('[', '').replace(']', '').replace("'", "")
        competitionPriorities = dictData["competitionPriorities"][result1]
        requestedProgramPriorities = dictData["requestedProgramPriorities"][result1]
        result2 = str(requestedProgramPriorities).replace('{', '').replace('}', '')
        return (step, result1, competitionPriorities, result2)

    # Получить данные {data - stateExam} online_entrant_requests по 1 шагу
    def get_stateExam_online_entrant_requests_step_1(self, email_abitur, Exam):
        data = self.db.session.query(Online_entrant_requests.data).filter(
            Online_entrant_requests.user_id == USERS().get_user_id_by_email(email_abitur)).first()
        dictData = json.loads(data[0])
        ExamCodes = dictData["stateExamCodes"]
        assert Exam in ExamCodes, 'Запись "ExamCodes" отсутвует в json "stateExamCodes"'
        ExamMarks = dictData["stateExamMarks"][Exam]
        ExamYears = dictData["stateExamYears"][Exam]
        return (ExamMarks, ExamYears)

    # Получить данные {data - DisciplineCodes} online_entrant_requests по 1 шагу
    def get_DisciplineCodes_online_entrant_requests_step_1(self, email_abitur, DisciplineCodes):
        data = self.db.session.query(Online_entrant_requests.data).filter(
            Online_entrant_requests.user_id == USERS().get_user_id_by_email(email_abitur)).first()
        dictData = json.loads(data[0])
        agreeEntranceExams = dictData["agreeEntranceExams"]
        entranceExamsReason = dictData["entranceExamsReason"]
        internalDisciplineCodes = dictData["internalDisciplineCodes"]
        assert DisciplineCodes in internalDisciplineCodes, 'Запись "DisciplineCodes" отсутвует в json ' \
                                                           '"internalDisciplineCodes" '
        return (agreeEntranceExams, entranceExamsReason)

    # Получить данные {data - haveOlympiad} online_entrant_requests по 1 шагу
    def get_haveOlympiad_online_entrant_requests_step_1(self, email_abitur):
        data = self.db.session.query(Online_entrant_requests.data).filter(
            Online_entrant_requests.user_id == USERS().get_user_id_by_email(email_abitur)).first()
        dictData = json.loads(data[0])
        haveOlympiad = dictData["haveOlympiad"]
        olympiadInfo = dictData["olympiadInfo"]
        return (haveOlympiad, olympiadInfo)

    # Получить данные {data - exclusiveRights} online_entrant_requests по 1 шагу
    def get_exclusiveRights_online_entrant_requests_step_1(self, email_abitur):
        data = self.db.session.query(Online_entrant_requests.data).filter(
            Online_entrant_requests.user_id == USERS().get_user_id_by_email(email_abitur)).first()
        dictData = json.loads(data[0])
        exclusiveRights = dictData["exclusiveRights"]
        if exclusiveRights:
            exclusiveRightsWithoutExams = dictData["exclusiveRightsWithoutExams"]
            exclusiveRightsBudget = dictData["exclusiveRightsBudget"]
            exclusiveRightsEmptiveEnroll = dictData["exclusiveRightsEmptiveEnroll"]
            return (exclusiveRightsWithoutExams, exclusiveRightsBudget, exclusiveRightsEmptiveEnroll)
        else:
            return None
