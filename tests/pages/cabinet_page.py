import os
import time
from .base_page import BasePage
from .locators.LOCATORS import PROFILE, REGLOG, ALERT, DASHBOARD, ADMIN
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait


# noinspection PyGlobalUndefined
class Profile(BasePage):
    def open_profile(self):
        WebDriverWait(self.browser, 5).until(EC.element_to_be_clickable((PROFILE.profile_link))).click()

    def exit_profile(self):
        WebDriverWait(self.browser, 5).until(EC.element_to_be_clickable((PROFILE.exit_profile))).click()
        WebDriverWait(self.browser, 5).until(EC.element_to_be_clickable((REGLOG.enter_button)))
        time.sleep(0.5)

    def profile_edit_avatar(self, avatar):
        element = self.browser.find_element(*PROFILE.input_file)
        current_dir = os.path.abspath(os.path.dirname(__file__))
        file_path = os.path.join(current_dir, avatar)
        element.send_keys(file_path)

    def save_results_of_changes_avatar(self, result):
        if result == 'Correct':
            if WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((PROFILE.enter_disabled))):
                assert WebDriverWait(self.browser, 8).until(EC.element_to_be_clickable((PROFILE.save_info)))
                self.browser.find_element(*PROFILE.save_info).click()
                WebDriverWait(self.browser, 5).until(EC.element_to_be_clickable((PROFILE.profile_link)))
            else:
                print('Ошибка при загрузке аватара')
        elif result == 'Incorrect':
            WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((PROFILE.enter_disabled)))
            WebDriverWait(self.browser, 8).until(EC.element_to_be_clickable((PROFILE.save_info))).click()
            alert = len(self.browser.find_elements(*ALERT.alert_input2))
            assert alert == 1, 'ERROR: Предупреждение об ограничении в поле "Аватар" не вылезло'

    def save_results_of_changes_profile(self, result):
        if result == 'Correct':
            if WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((PROFILE.enter_disabled))):
                time.sleep(0.5)
                self.browser.find_elements(*REGLOG.enter_button)[0].click()
                assert WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ALERT.alert_save)))
            else:
                self.browser.find_elements(*REGLOG.enter_button)[0].click()
                assert WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ALERT.alert_save)))
        elif result == 'CorrectEmail':
            self.browser.find_elements(*REGLOG.enter_button)[0].click()
            assert WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ALERT.alert_save)))
            self.browser.refresh()
            assert '/email/verify' in self.browser.current_url, 'ERROR: В URL должен быть /email/verify'
        elif result == 'Incorrect':
            self.browser.find_elements(*REGLOG.enter_button)[0].click()
            time.sleep(0.5)
            WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ALERT.alert_input2)))
            alert = len(self.browser.find_elements(*ALERT.alert_input2))
            assert alert == 1, 'ERROR: Предупреждение об ограничении не вылезло'

    def input_email(self, email):
        self.browser.find_element(*PROFILE.email).clear()
        self.browser.find_element(*PROFILE.email).send_keys(email)

    def input_last_name(self, last_name):
        self.browser.find_element(*PROFILE.last_name).clear()
        self.browser.find_element(*PROFILE.last_name).send_keys(last_name)

    def input_name(self, name):
        self.browser.find_element(*PROFILE.name).clear()
        self.browser.find_element(*PROFILE.name).send_keys(name)

    def input_middle_name(self, middle_name):
        self.browser.find_element(*PROFILE.middle_name).clear()
        self.browser.find_element(*PROFILE.middle_name).send_keys(middle_name)

    def input_current_password(self, current_password):
        self.browser.find_element(*PROFILE.current_password).clear()
        self.browser.find_element(*PROFILE.current_password).send_keys(current_password)

    def input_password(self, password):
        self.browser.find_element(*PROFILE.password).clear()
        self.browser.find_element(*PROFILE.password).send_keys(password)

    def input_password_confirmation(self, password_confirmation):
        self.browser.find_element(*PROFILE.password_confirmation).clear()
        self.browser.find_element(*PROFILE.password_confirmation).send_keys(password_confirmation)

    def save_results_of_changes_password(self, result, button):
        if result == 'Correct':
            self.browser.find_elements(*REGLOG.enter_button)[button].click()
            assert WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ALERT.alert_save)))
        elif result == 'Incorrect':
            self.browser.find_elements(*REGLOG.enter_button)[button].click()
            time.sleep(0.5)
            WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ALERT.alert_input2)))
            alert = len(self.browser.find_elements(*ALERT.alert_input2))
            assert alert == 1, 'ERROR: Предупреждение об ограничении не вылезло'

    def click_select_button(self, button):
        self.browser.find_elements(*PROFILE.select_button)[button].click()

    def input_password_modal_windows(self, password, modal):
        self.browser.find_elements(*PROFILE.password_modal)[modal].send_keys(password)

    def click_enter_button(self, button):
        self.browser.find_elements(*REGLOG.enter_button)[button].click()

    def copy_token_two_factor_authentication(self):
        global token_user
        WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((PROFILE.copy_token)))
        token_user = self.browser.find_element(*PROFILE.copy_token).text

    def input_global_token(self):
        self.browser.find_element(*REGLOG.token).clear()
        self.browser.find_element(*REGLOG.token).send_keys(token_user)

    def input_token(self, token):
        self.browser.find_element(*REGLOG.token).clear()
        self.browser.find_element(*REGLOG.token).send_keys(token)

    def delete_profile_account(self, delete, button):
        self.browser.find_elements(*PROFILE.delete_button)[button].click()
        if delete == 'Correct':
            assert '/login' in self.browser.current_url, 'ERROR: В URL должен быть /login'
        elif delete == 'Incorrect':
            alert = len(self.browser.find_elements(*ALERT.alert_input2))
            assert alert == 1, 'ERROR: Предупреждение об ограничении не вылезло'
        elif delete == 'Skip':
            pass

    # Виджет удаления профиля выключен
    def not_delete_profile_button(self, button):
        self.browser.refresh()
        time.sleep(0.5)
        assert not self.browser.find_elements(*PROFILE.delete_button)[button].click(), 'Виджет удаления профиля включен'

    # Виджет 2FA профиля выключен
    def search_2fa_widget(self, result):
        self.browser.refresh()
        time.sleep(0.5)
        if result == 'Correct':
            assert WebDriverWait(self.browser, 1).until(EC.presence_of_element_located((PROFILE.two_fa_widget))), \
                'Виджет 2FA профиля выключен'
        elif result == 'Incorrect':
            assert WebDriverWait(self.browser, 1).until(EC.invisibility_of_element_located((PROFILE.two_fa_widget))), \
                'Виджет 2FA профиля включен'

    def search_change_full_name_widget(self, result):
        self.browser.refresh()
        time.sleep(0.5)
        if result == 'Correct':
            assert WebDriverWait(self.browser, 1).until(EC.presence_of_element_located((PROFILE.name))) and \
                   WebDriverWait(self.browser, 1).until(EC.presence_of_element_located((PROFILE.last_name))) and \
                   WebDriverWait(self.browser, 1).until(EC.presence_of_element_located((PROFILE.middle_name))), \
                'Виджет allow_change_fullname выключен'
        elif result == 'Incorrect':
            assert WebDriverWait(self.browser, 1).until(EC.invisibility_of_element_located((PROFILE.name))) and \
                   WebDriverWait(self.browser, 1).until(EC.invisibility_of_element_located((PROFILE.last_name))) and \
                   WebDriverWait(self.browser, 1).until(EC.invisibility_of_element_located((PROFILE.middle_name))), \
                'Виджет allow_change_fullname включен'

    def regenerate_recovery_codes(self):
        WebDriverWait(self.browser, 8).until(EC.element_to_be_clickable((PROFILE.regenerate_codes))).click()

    def checking_the_list_of_browsers(self):
        assert WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((ALERT.alert_save)))
        WebDriverWait(self.browser, 3).until(EC.presence_of_element_located((PROFILE.browser_list)))
        browsers = len(self.browser.find_elements(*PROFILE.browser_list))
        assert browsers == 1, 'ERROR: Должна остаться только одна сессия браузера'


class Dashboard(BasePage):
    def open_window_desktop(self):
        WebDriverWait(self.browser, 8).until(EC.element_to_be_clickable((DASHBOARD.dashboard))).click()
        time.sleep(0.5)

    def select_role(self, role, description):
        if role == 'Студент':
            WebDriverWait(self.browser, 8).until(EC.element_to_be_clickable((DASHBOARD.role_student))).click()
        elif role == 'Сотрудник':
            WebDriverWait(self.browser, 8).until(EC.element_to_be_clickable((DASHBOARD.role_employee))).click()
        elif role == 'Родитель':
            WebDriverWait(self.browser, 8).until(EC.element_to_be_clickable((DASHBOARD.role_parent))).click()
        time.sleep(0.5)
        if description != 'no':
            description_displayed = self.browser.find_element(*ADMIN.description).text
            assert description == description_displayed, 'ERROR: Текст описания не совпадает'

    def role_selection_widget_disabled(self):
        wait = WebDriverWait(self.browser, 3)
        assert wait.until(EC.invisibility_of_element_located((DASHBOARD.block_role))), 'ERROR: Виджет отображения ' \
                                                                                       'ролей не выключен '

    def input_birth_date(self, birth_date):
        self.browser.find_element(*REGLOG.birth_date).clear()
        self.browser.find_element(*REGLOG.birth_date).send_keys(birth_date)

    def input_last_4_numbers_passport(self, last_4_numbers_passport):
        self.browser.find_element(*REGLOG.last_4_numbers_passport).clear()
        self.browser.find_element(*REGLOG.last_4_numbers_passport).send_keys(last_4_numbers_passport)

    def input_snils(self, snils):
        self.browser.find_element(*REGLOG.snils).clear()
        self.browser.find_element(*REGLOG.snils).send_keys(snils)

    def input_inn(self, inn):
        self.browser.find_element(*REGLOG.inn).clear()
        self.browser.find_element(*REGLOG.inn).send_keys(inn)

    def input_book_number(self, book_number):
        self.browser.find_element(*REGLOG.book_number).clear()
        self.browser.find_element(*REGLOG.book_number).send_keys(book_number)

    def input_code(self, code):
        self.browser.find_element(*REGLOG.code).clear()
        self.browser.find_element(*REGLOG.code).send_keys(code)
