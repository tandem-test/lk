import pytest
import os
import allure
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver import DesiredCapabilities


def pytest_addoption(parser):
    parser.addoption('--browser_name', action='store', default="chrome", help="Choose browser: chrome or firefox")
    parser.addoption('--start_maximized', action='store', default=None, help="full_size browser window")


@pytest.fixture(scope='function')
def browser(request, browser_name=None):
    if browser_name is None:
        browser_name = request.config.getoption("browser_name")
    browser = None
    options = Options()
    options.add_argument("window-size=1300,900")
    options.add_argument('--headless')
    options.add_argument("--no-sandbox")
    if browser_name == "chrome":
        caps = DesiredCapabilities().CHROME
        caps["pageLoadStrategy"] = "normal"
        browser = webdriver.Remote(desired_capabilities=caps,
                                   command_executor="http://selenium__standalone-chrome:4444/wd/hub", options=options)
    elif browser_name == "firefox":
        caps = DesiredCapabilities().FIREFOX
        caps["pageLoadStrategy"] = "normal"
        browser = webdriver.Remote(desired_capabilities=caps,
                                   command_executor="http://selenium__standalone-firefox:4444/wd/hub")
        browser.maximize_window()
    else:
        assert browser_name == "chrome" or browser_name == "firefox", "You should choose browser: chrome or firefox"
    yield browser
    browser.quit()


@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item, call):
    outcome = yield
    rep = outcome.get_result()
    if rep.when == 'call' and rep.failed:
        mode = 'a' if os.path.exists('failures') else 'w'
        try:
            with open('failures', mode) as f:
                if 'browser' in item.fixturenames:
                    web_driver = item.funcargs['browser']
                else:
                    print('Fail to take screen-shot')
                    return
            allure.attach(
                web_driver.get_screenshot_as_png(),
                name='screenshot',
                attachment_type=allure.attachment_type.PNG
            )
        except Exception as e:
            print('Fail to take screen-shot: {}'.format(e))