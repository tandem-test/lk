import pytest
import allure

from .pages.student_page import Student
from .pages.precondition import link, password, admin_login
from .pages.authentication_page import RegLogin


@pytest.mark.student
@allure.link(link + '/services/requests', name='ТАНДЕМ Личный кабинет')
@allure.epic('Функциональное тестирование панели обучающегося')
@allure.feature('Проверка функциональности модуля "Услуги"')
@pytest.mark.skip(reason='Модуль "Услуги" находиться в разработке')
class TestStudent_Service:
    @allure.story('Позитивная проверка функциональности модуля')
    @pytest.mark.smoke
    @allure.severity(allure.severity_level.CRITICAL)
    def testing_the_functionality_of_sending_an_application(self, browser):
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Student(browser, link)
        with allure.step('Открыть вкладку "Категории"'):
            page.open_window_student('Услуги')
        with allure.step('Открыть услугу "Тестовая услуга"'):
            page.open_test_service()

