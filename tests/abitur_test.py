import datetime

import pytest
import allure

from .pages.soap.getEnrollmentEnvironment import EnrollmentEnvironment
from .pages.cabinet_page import Profile
from .pages.admin_page import Setting
from .pages.abitur_page import Applications
from .pages.authentication_page import RegLogin
from .pages.database.db_logic import USERS, ROLES, SETTING, ABITUR_DB, ADMINS, now
from .pages.precondition import random_en, random_number, Roles, password, link, EduLevel, RequestType, ReturnWay, \
    PreferredForm, random_ru, AbiturMain, ScanCopies, FirstStep, SecondStep, CompetitionLists, ContractCommon, \
    ContractEntrantPersonType, ContractOtherPersonType, ContractLegalPersonType, admin_login, ContractAction, \
    OrderOfSpecialties, ThirdStep, EntrantsSearch, Exam, EntranceExams, BACH_2017, EDUCATIONAL_ORGANIZATION, SYSTEM, \
    RECEPTION_COMPANY, LOGIN, PASSWORD, POSTGRADUATE_2018, BACH_2018, EntrantLists, DocumentDeadlines


@pytest.mark.abitur
@allure.epic('Функциональное тестирование модуля "ABITUR"')
@allure.feature('Предварительные условия для выполнения тест-кейсов')
class TestAbitur_PRECONDITION:
    @staticmethod
    def test_precondition_clear_previously_created_OO_RC_Systems():
        with allure.step('Удалить записи созданные в ПК (PRECONDITION) таблица online_entrant_requests'):
            ABITUR_DB().delete_record_online_entrant_requests(None)
        with allure.step('Удалить все записи в таблице user_enrollment_campaigns'):
            SETTING().delete_record_user_enrollment_campaigns(None)
        with allure.step('Удалить все записи в таблице attached_files'):
            ABITUR_DB().delete_all_attached_files()
        with allure.step('Удалить все записи в таблице external_org_contacts'):
            ABITUR_DB().delete_all_external_org_contacts()
        with allure.step('Удалить все записи в таблице regulations'):
            SETTING().delete_record_regulations(None)
        with allure.step('Удалить все записи в таблице accepting_document_deadlines'):
            SETTING().delete_record_accepting_document_deadlines(None)
        with allure.step('Удалить все записи в таблице enrollment_campaigns'):
            SETTING().delete_record_enrollment_campaigns(None)
        with allure.step('Удалить все записи в таблице uni_installations'):
            SETTING().delete_record_uni_installations(None)
        with allure.step('Удалить все записи в таблице external_org_units'):
            SETTING().delete_record_external_org_units(None)

    @staticmethod
    def test_create_a_RC():
        type_pk = 'Прием иностранных абитуриентов по направлениям Минобрнауки РФ'
        precondition_campaign_id = BACH_2017
        with allure.step('Добавить ОО в external_org_units'):
            SETTING().add_educational_organization(EDUCATIONAL_ORGANIZATION, None)
        with allure.step('Добавить систему в uni_installations'):
            SETTING().add_uni_installations(EDUCATIONAL_ORGANIZATION, SYSTEM, 'tandem', '123456', 1)
        with allure.step('Добавить ПК в enrollment_campaigns'):
            SETTING().add_enrollment_campaigns(precondition_campaign_id, EDUCATIONAL_ORGANIZATION, RECEPTION_COMPANY,
                                               random_en(8), type_pk, SYSTEM, 'active')

    @staticmethod
    def set_settings_RC(abitur_main, basic_education_levels, implemented_education_levels, abitur_scan_copies,
                        abitur_first_step, abitur_second_step, abitur_competition_lists, contract_common,
                        contract_other_person_type, contract_legal_person_type, contract_entrant_person_type):
        with allure.step('Установить настройки для abitur_main'):
            SETTING().update_settings_app('abitur_main', '{"' + SETTING().get_enrollment_campaigns_id(RECEPTION_COMPANY)
                                          + abitur_main)
        with allure.step('Установить настройки для basic_education_levels'):
            SETTING().update_settings_app('basic_education_levels', '{"' + SETTING().get_enrollment_campaigns_id(
                RECEPTION_COMPANY) + basic_education_levels)
        with allure.step('Установить настройки для implemented_education_levels'):
            SETTING().update_settings_app('implemented_education_levels', '{"' + SETTING(
            ).get_enrollment_campaigns_id(RECEPTION_COMPANY) + implemented_education_levels)
        with allure.step('Установить настройки для abitur_scan_copies'):
            SETTING().update_settings_app('abitur_scan_copies', '{"' + SETTING().get_enrollment_campaigns_id(
                RECEPTION_COMPANY) + abitur_scan_copies)
        with allure.step('Установить настройки для abitur_first_step'):
            SETTING().update_settings_app('abitur_first_step', '{"' + SETTING().get_enrollment_campaigns_id(
                RECEPTION_COMPANY) + abitur_first_step)
        with allure.step('Установить настройки для abitur_second_step'):
            SETTING().update_settings_app('abitur_second_step', '{"' + SETTING().get_enrollment_campaigns_id(
                RECEPTION_COMPANY) + abitur_second_step)
        with allure.step('Установить настройки для abitur_competition_lists'):
            SETTING().update_settings_app('abitur_competition_lists', '{"' + SETTING().get_enrollment_campaigns_id(
                RECEPTION_COMPANY) + abitur_competition_lists)
        with allure.step('Установить настройки для contract_common'):
            SETTING().update_settings_app('contract_common', '{"' + SETTING().get_enrollment_campaigns_id(
                RECEPTION_COMPANY) + contract_common)
        with allure.step('Установить настройки для contract_entrant_person_type'):
            SETTING().update_settings_app('contract_entrant_person_type', '{"' + SETTING().get_enrollment_campaigns_id(
                RECEPTION_COMPANY) + contract_entrant_person_type)
        with allure.step('Установить настройки для contract_other_person_type'):
            SETTING().update_settings_app('contract_other_person_type', '{"' + SETTING().get_enrollment_campaigns_id(
                RECEPTION_COMPANY) + contract_other_person_type)
        with allure.step('Установить настройки для contract_legal_person_type'):
            SETTING().update_settings_app('contract_legal_person_type', '{"' + SETTING().get_enrollment_campaigns_id(
                RECEPTION_COMPANY) + contract_legal_person_type)


@pytest.mark.abitur
@allure.link(link + '/application/filing', name='ТАНДЕМ ЛК - Заявления')
@allure.epic('Функциональное тестирование модуля "ABITUR"')
@allure.feature('Проверка функциональности раздела "Заявления"')
class TestAbitur_APPLICATIONS:
    @allure.story('Проверка функциональности нулевого шага')
    def testing_the_changing_the_RC_in_the_application_application_with_the_one_company_mode(self, browser):
        type_pk = 'Прием иностранных абитуриентов по направлениям Минобрнауки РФ'
        email_abitur = random_en(8) + '@abitur.test'
        NEW_RECEPTION_COMPANY = 'Тестовая ПК (TEST)'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись поступающего'):
                with allure.step('Создать верефицированную учётную запись'):
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email_abitur)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_abitur, Roles.entrant)
            with allure.step('Режим работы ЛКА - Одна организация'):
                SETTING().update_settings_app('is_multiple_org_units', '')
            with allure.step('Добавить вторую ПК в enrollment_campaigns'):
                SETTING().add_enrollment_campaigns(POSTGRADUATE_2018, EDUCATIONAL_ORGANIZATION,
                                                   NEW_RECEPTION_COMPANY, random_en(8), type_pk, SYSTEM, 'active')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition - Поступающий)'):
            page.input_email(email_abitur)
        with allure.step('Ввести в поле "Пароль" (Precondition - Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Applications(browser, link)
        with allure.step('Открыть вкладку "Заявления"'):
            page.open_page_applications()
        with allure.step('Нажать кнопку "Подать новое онлайн-заявление"'):
            page.submit_a_new_application_in_a_single_company_mode('new', 'yes', 'Correct')
        with allure.step('Выбрать ранее созданную ПК'):
            page.select_a_reception_company(RECEPTION_COMPANY)
        with allure.step('Нажать кнопку "Отмена"'):
            page.cancel_the_application()
        with allure.step('Сменить ПК на вторую'):
            page.change_the_receiving_campaign(NEW_RECEPTION_COMPANY, 'Correct')
        with allure.step('Нажать кнопку "Подать новое онлайн-заявление"'):
            page.submit_a_new_application_in_a_single_company_mode('', 'no', 'Correct')
        with allure.step('Нажать кнопку "Отмена"'):
            page.cancel_the_application()
        with allure.step('Удалить записи созданные в ПК (PRECONDITION) таблица online_entrant_requests'):
            ABITUR_DB().delete_record_online_entrant_requests(NEW_RECEPTION_COMPANY)
        with allure.step('Удалить все записи в таблице user_enrollment_campaigns'):
            SETTING().delete_record_user_enrollment_campaigns(NEW_RECEPTION_COMPANY)
        with allure.step('Удалить запись о второй ПК в таблице enrollment_campaigns'):
            SETTING().delete_record_enrollment_campaigns(NEW_RECEPTION_COMPANY)

    @allure.story('Проверка функциональности нулевого шага')
    def testing_resetting_the_application_widget_when_logged_out_in_single_organization_mode(self, browser):
        email_abitur = random_en(8) + '@abitur.test'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись поступающего'):
                with allure.step('Создать верефицированную учётную запись'):
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email_abitur)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_abitur, Roles.entrant)
            with allure.step('Режим работы ЛКА - Одна организация'):
                SETTING().update_settings_app('is_multiple_org_units', '')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition - Поступающий)'):
            page.input_email(email_abitur)
        with allure.step('Ввести в поле "Пароль" (Precondition - Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Applications(browser, link)
        with allure.step('Открыть вкладку "Заявления"'):
            page.open_page_applications()
        with allure.step('Нажать кнопку "Подать новое онлайн-заявление"'):
            page.submit_a_new_application_in_a_single_company_mode('new', 'yes', 'Correct')
        with allure.step('Выбрать ранее созданную ПК'):
            page.select_a_reception_company(RECEPTION_COMPANY)
        with allure.step('Нажать кнопку "Отмена"'):
            page.cancel_the_application()
        page = Profile(browser, link)
        with allure.step('Разлогиниться с учётной записи'):
            page.exit_profile()
        page = RegLogin(browser, link)
        with allure.step('Ввести в поле "E-mail" (Precondition - Поступающий)'):
            page.input_email(email_abitur)
        with allure.step('Ввести в поле "Пароль" (Precondition - Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Applications(browser, link)
        with allure.step('Открыть вкладку "Заявления"'):
            page.open_page_applications()
        with allure.step('Нажать кнопку "Подать новое онлайн-заявление"'):
            page.submit_a_new_application_in_a_single_company_mode('new', 'yes', 'Correct')
        with allure.step('Выбрать ранее созданную ПК'):
            page.select_a_reception_company(RECEPTION_COMPANY)

    @allure.story('Проверка функциональности нулевого шага')
    def testing_resetting_the_application_widget_when_logging_out_in_multiple_organization_mode(self, browser):
        email_abitur = random_en(8) + '@abitur.test'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись поступающего'):
                with allure.step('Создать верефицированную учётную запись'):
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email_abitur)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_abitur, Roles.entrant)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition - Поступающий)'):
            page.input_email(email_abitur)
        with allure.step('Ввести в поле "Пароль" (Precondition - Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Applications(browser, link)
        with allure.step('Открыть вкладку "Заявления"'):
            page.open_page_applications()
        with allure.step('Нажать кнопку "Подать новое онлайн-заявление"'):
            page.submit_a_new_application_in_multi_company_mode('new')
        with allure.step('Ввести в поиске ранее созданную ОО'):
            page.search_educational_organization(EDUCATIONAL_ORGANIZATION)
        with allure.step('Открыть найденную ОО'):
            page.open_educational_organization()
        with allure.step('Выбрать ранее созданную ПК'):
            page.select_a_reception_company(RECEPTION_COMPANY)
        with allure.step('Нажать кнопку "Отмена"'):
            page.cancel_the_application()
        page = Profile(browser, link)
        with allure.step('Разлогиниться с учётной записи'):
            page.exit_profile()
        page = RegLogin(browser, link)
        with allure.step('Ввести в поле "E-mail" (Precondition - Поступающий)'):
            page.input_email(email_abitur)
        with allure.step('Ввести в поле "Пароль" (Precondition - Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Applications(browser, link)
        with allure.step('Открыть вкладку "Заявления"'):
            page.open_page_applications()
        with allure.step('Нажать кнопку "Подать новое онлайн-заявление"'):
            page.submit_a_new_application_in_multi_company_mode('new')
        with allure.step('Ввести в поиске ранее созданную ОО'):
            page.search_educational_organization(EDUCATIONAL_ORGANIZATION)

    @allure.story('Проверка функциональности нулевого шага')
    def testing_the_creation_of_a_new_application_with_an_unsent_first_one(self, browser):
        email_abitur = random_en(8) + '@abitur.test'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись поступающего'):
                with allure.step('Создать верефицированную учётную запись'):
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email_abitur)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_abitur, Roles.entrant)
                with allure.step('Создать запись о начале прохождения заполнения заявления'):
                    ABITUR_DB().add_online_entrant_requests(email_abitur, '{"step":1}', RECEPTION_COMPANY, 'pending')
            with allure.step('Режим работы ЛКА - Одна организация'):
                SETTING().update_settings_app('is_multiple_org_units', '')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition - Поступающий)'):
            page.input_email(email_abitur)
        with allure.step('Ввести в поле "Пароль" (Precondition - Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Applications(browser, link)
        with allure.step('Открыть вкладку "Заявления"'):
            page.open_page_applications()
        with allure.step('Нажать кнопку "Подать новое онлайн-заявление"'):
            page.submit_a_new_application_in_a_single_company_mode('', 'yes', 'Incorrect')

    @allure.story('Проверка функциональности нулевого шага')
    def testing_a_RC_change_in_the_one_company_mode_with_an_unsent_application(self, browser):
        email_abitur = random_en(8) + '@abitur.test'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись поступающего'):
                with allure.step('Создать верефицированную учётную запись'):
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email_abitur)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_abitur, Roles.entrant)
                with allure.step('Создать запись о начале прохождения заполнения заявления'):
                    ABITUR_DB().add_online_entrant_requests(email_abitur, '{"step":1}', RECEPTION_COMPANY, 'pending')
                with allure.step('Добавить запись в user_enrollment_campaigns'):
                    ABITUR_DB().add_user_enrollment_campaigns(email_abitur, RECEPTION_COMPANY)
            with allure.step('Режим работы ЛКА - Одна организация'):
                SETTING().update_settings_app('is_multiple_org_units', '')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition - Поступающий)'):
            page.input_email(email_abitur)
        with allure.step('Ввести в поле "Пароль" (Precondition - Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Applications(browser, link)
        with allure.step('Открыть вкладку "Заявления"'):
            page.open_page_applications()
        with allure.step('Нажать кнопку "Сменить" в блоке ПК'):
            page.change_the_receiving_campaign('', 'Incorrect')

    @allure.story('Проверка функциональности нулевого шага')
    @pytest.mark.abitur1
    def testing_the_changing_the_RC_in_the_application_application_with_multiple_company_mode(self, browser):
        type_pk = 'Прием иностранных абитуриентов по направлениям Минобрнауки РФ'
        email_abitur = random_en(8) + '@abitur.test'
        NEW_EDUCATIONAL_ORGANIZATION = 'Тестовая ОО (2)'
        NEW_RECEPTION_COMPANY = 'Тестовая ПК (2)'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись поступающего'):
                with allure.step('Создать верефицированную учётную запись'):
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email_abitur)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_abitur, Roles.entrant)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
            with allure.step('Добавить вторую ОО в external_org_units'):
                SETTING().add_educational_organization(NEW_EDUCATIONAL_ORGANIZATION, None)
            with allure.step('Закрепить ПК ко второй ОО в enrollment_campaigns'):
                SETTING().add_enrollment_campaigns(BACH_2018, NEW_EDUCATIONAL_ORGANIZATION,
                                                   NEW_RECEPTION_COMPANY, random_en(8), type_pk, SYSTEM, 'active')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition - Поступающий)'):
            page.input_email(email_abitur)
        with allure.step('Ввести в поле "Пароль" (Precondition - Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Applications(browser, link)
        with allure.step('Открыть вкладку "Заявления"'):
            page.open_page_applications()
        with allure.step('Нажать кнопку "Подать новое онлайн-заявление"'):
            page.submit_a_new_application_in_multi_company_mode('new')
        with allure.step('Ввести в поиске ранее созданную ОО'):
            page.search_educational_organization(EDUCATIONAL_ORGANIZATION)
        with allure.step('Открыть найденную ОО'):
            page.open_educational_organization()
        with allure.step('Выбрать ранее созданную ПК'):
            page.select_a_reception_company(RECEPTION_COMPANY)
        with allure.step('Нажать кнопку "Отмена"'):
            page.cancel_the_application()
        with allure.step('Нажать кнопку "Сменить" в блоке ОО'):
            page.change_the_education_organization('Correct')
        with allure.step('Ввести в поиске ранее созданную ОО'):
            page.search_educational_organization(NEW_EDUCATIONAL_ORGANIZATION)
        with allure.step('Открыть найденную ОО'):
            page.open_educational_organization()
        with allure.step('Выбрать привязанную ПК'):
            page.change_the_receiving_campaign(NEW_RECEPTION_COMPANY, 'SkipChange')
        with allure.step('Нажать кнопку "Подать новое онлайн-заявление"'):
            page.submit_a_new_application_in_a_single_company_mode('', 'no', 'Correct')
        with allure.step('Нажать кнопку "Отмена"'):
            page.cancel_the_application()
        with allure.step('Удалить записи созданные в ПК (PRECONDITION) таблица online_entrant_requests'):
            ABITUR_DB().delete_record_online_entrant_requests(NEW_RECEPTION_COMPANY)
        with allure.step('Удалить все записи в таблице user_enrollment_campaigns'):
            SETTING().delete_record_user_enrollment_campaigns(NEW_RECEPTION_COMPANY)
        with allure.step('Удалить запись о второй ПК в таблице enrollment_campaigns'):
            SETTING().delete_record_enrollment_campaigns(NEW_RECEPTION_COMPANY)
        with allure.step('Удалить запись о второй ОО в таблице external_org_units'):
            SETTING().delete_record_external_org_units(NEW_EDUCATIONAL_ORGANIZATION)

    @allure.story('Проверка функциональности нулевого шага')
    def testing_the_change_of_organization_in_the_multiple_companies_mode_with_an_unsent_application(self, browser):
        email_abitur = random_en(8) + '@abitur.test'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись поступающего'):
                with allure.step('Создать верефицированную учётную запись'):
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email_abitur)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_abitur, Roles.entrant)
                with allure.step('Создать запись о начале прохождения заполнения заявления'):
                    ABITUR_DB().add_online_entrant_requests(email_abitur, '{"step":1}', RECEPTION_COMPANY, 'pending')
                with allure.step('Добавить запись в user_enrollment_campaigns'):
                    ABITUR_DB().add_user_enrollment_campaigns(email_abitur, RECEPTION_COMPANY)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition - Поступающий)'):
            page.input_email(email_abitur)
        with allure.step('Ввести в поле "Пароль" (Precondition - Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Applications(browser, link)
        with allure.step('Открыть вкладку "Заявления"'):
            page.open_page_applications()
        with allure.step('Нажать кнопку "Сменить" в блоке ОО'):
            page.change_the_education_organization('Incorrect')

    @allure.story('Проверка функциональности первого шага')
    def testing_the_first_step_with_the_RC_type_Admission_of_foreign_applicants(self, browser):
        type_pk = 'Прием иностранных абитуриентов по направлениям Минобрнауки РФ'
        email_abitur = random_en(8) + '@abitur.test'
        step = 1
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись поступающего'):
                with allure.step('Создать верефицированную учётную запись'):
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email_abitur)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_abitur, Roles.entrant)
            with allure.step('Установить настройки ПК - "Включить все поля/необязательно"'):
                TestAbitur_PRECONDITION().set_settings_RC(AbiturMain.ENABLE_ALL, EduLevel.ENABLE_ALL,
                                                          RequestType.ENABLE_ALL, ScanCopies.OPTIONAL_ALL,
                                                          FirstStep.OPTIONAL_ALL, SecondStep.OPTIONAL_ALL,
                                                          CompetitionLists.ENABLE_ALL, ContractCommon.OPTIONAL_ALL,
                                                          ContractEntrantPersonType.ENABLE_ALL, ContractOtherPersonType.
                                                          OPTIONAL_ALL, ContractLegalPersonType.OPTIONAL_ALL)
            with allure.step('Обновить тип ПК - "Прием иностранных абитуриентов по направлениям Минобрнауки РФ"'):
                SETTING().update_type_enrollment_campaigns(RECEPTION_COMPANY, type_pk)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition - Поступающий)'):
            page.input_email(email_abitur)
        with allure.step('Ввести в поле "Пароль" (Precondition - Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Applications(browser, link)
        with allure.step('Открыть вкладку "Заявления"'):
            page.open_page_applications()
        with allure.step('Нажать кнопку "Подать новое онлайн-заявление"'):
            page.submit_a_new_application_in_multi_company_mode('new')
        with allure.step('Ввести в поиске ранее созданную ОО'):
            page.search_educational_organization(EDUCATIONAL_ORGANIZATION)
        with allure.step('Открыть найденную ОО'):
            page.open_educational_organization()
        with allure.step('Выбрать ранее созданную ПК'):
            page.select_a_reception_company(RECEPTION_COMPANY)
        with allure.step('Выбрать в поле "Я уже получил(а) образование" - "Высшее образование - бакалавриат"'):
            haveEduLevel = EduLevel.UNDERGRADUATE
            page.select_haveEduLevel(haveEduLevel)
        with allure.step('Выбрать в поле "Я хочу поступать на" - "Программы магистратуры"'):
            wantRequestType = RequestType.MASTER
            page.select_wantRequestType(wantRequestType)
        with allure.step('Выбрать в поле "Способ возврата оригиналов документов" - "В электронной форме"'):
            originalReturnWay = ReturnWay.IN_ELECTRONIC_FORM
            page.select_originalReturnWay(originalReturnWay)
        with allure.step('Включить чекбокс "Мне необходимы специальные условия"'):
            haveDisability = True
            page.select_haveDisability()
        with allure.step('Выбрать в поле "Предпочитаемая форма сдачи вступительных испытаний" - "Дистанционная"'):
            preferredForm = PreferredForm.REMOTE
            page.select_preferredForm(preferredForm)
        with allure.step('Включить чекбокс "Я заключил(а) договор о целевом обучении"'):
            aveAgreementTargetEdu = True
            page.select_aveAgreementTargetEdu('on')
        with allure.step('Ввести в поле "Номер договора о целевом обучении" 10 цифр'):
            agreementNumber = random_number(10)
            page.input_agreementNumber(agreementNumber)
        with allure.step('Ввести в поле "Дата заключения договора" - 22.04.2021'):
            agreementDate = '22.04.2021'
            page.input_agreementDate(agreementDate)
        with allure.step('Ввести в поле "Данные об организации по договору о целевом обучении" 8 символов'):
            agreementOrg = random_ru(8)
            page.input_agreementOrg(agreementOrg)
        with allure.step('Ввести в поле "Данные об образовательной программе по договору о целевом" 8 символов'):
            agreementEduProgram = random_ru(8)
            page.input_agreementEduProgram(agreementEduProgram)
        with allure.step('Проверка записи данных в столбец online_entrant_requests.data'):
            page.check_record_data_step_1(email_abitur, step, haveEduLevel, wantRequestType, originalReturnWay,
                                          haveDisability, preferredForm, aveAgreementTargetEdu, agreementNumber,
                                          agreementDate, agreementOrg, agreementEduProgram)

    @allure.story('Проверка функциональности первого шага')
    def testing_the_first_step_with_the_RC_type_Admission_to_bachelors_programs(self, browser):
        type_pk = 'Прием на обучение по программам бакалавриата, специалитета'
        email_abitur = random_en(8) + '@abitur.test'
        step = 1
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись поступающего'):
                with allure.step('Создать верефицированную учётную запись'):
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email_abitur)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_abitur, Roles.entrant)
            with allure.step('Установить настройки ПК - "Включить все поля/необязательно"'):
                TestAbitur_PRECONDITION().set_settings_RC(AbiturMain.ENABLE_ALL, EduLevel.ENABLE_ALL,
                                                          RequestType.ENABLE_ALL, ScanCopies.OPTIONAL_ALL,
                                                          FirstStep.OPTIONAL_ALL, SecondStep.OPTIONAL_ALL,
                                                          CompetitionLists.ENABLE_ALL, ContractCommon.OPTIONAL_ALL,
                                                          ContractEntrantPersonType.ENABLE_ALL,
                                                          ContractOtherPersonType.
                                                          OPTIONAL_ALL, ContractLegalPersonType.OPTIONAL_ALL)
            with allure.step('Обновить тип ПК - "Прием на обучение по программам бакалавриата, специалитета"'):
                SETTING().update_type_enrollment_campaigns(RECEPTION_COMPANY, type_pk)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition - Поступающий)'):
            page.input_email(email_abitur)
        with allure.step('Ввести в поле "Пароль" (Precondition - Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Applications(browser, link)
        with allure.step('Открыть вкладку "Заявления"'):
            page.open_page_applications()
        with allure.step('Нажать кнопку "Подать новое онлайн-заявление"'):
            page.submit_a_new_application_in_multi_company_mode('new')
        with allure.step('Ввести в поиске ранее созданную ОО'):
            page.search_educational_organization(EDUCATIONAL_ORGANIZATION)
        with allure.step('Открыть найденную ОО'):
            page.open_educational_organization()
        with allure.step('Выбрать ранее созданную ПК'):
            page.select_a_reception_company(RECEPTION_COMPANY)
        with allure.step('Выбрать в поле "Я уже получил(а) образование" - "Среднее общее образование (11 классов)"'):
            haveEduLevel = EduLevel.SECONDARY_GENERAL
            page.select_haveEduLevel(haveEduLevel)
        with allure.step('Выбрать в поле "Я хочу поступать на" - "Программы бакалавриата, специалитета"'):
            wantRequestType = RequestType.BACH_SPEC
            page.select_wantRequestType(wantRequestType)
        with allure.step('Выбрать в поле "Способ возврата оригиналов документов" - "В электронной форме"'):
            originalReturnWay = ReturnWay.IN_ELECTRONIC_FORM
            page.select_originalReturnWay(originalReturnWay)
        with allure.step('Выбрать в поле "Я уже сдавал(а) или буду сдавать ЕГЭ" - Биология/60 баллов/2020'):
            ExamCodes_1 = Exam.BIOLOGY
            ExamMarks_1 = '60'
            ExamYears_1 = '2020'
            page.select_Exam(ExamCodes_1, ExamMarks_1, ExamYears_1, 'on')
        with allure.step('Выбрать в поле "Я уже сдавал(а) или буду сдавать ЕГЭ" - Информатика/80 баллов/2021'):
            ExamCodes_2 = Exam.COMPUTER_SCIENCE
            ExamMarks_2 = '80'
            ExamYears_2 = '2021'
            page.select_Exam(ExamCodes_2, ExamMarks_2, ExamYears_2, 'on')
        with allure.step('Включить чекбокс "Прошу допустить к ВИ" - Иностранные/Лица с ограниченными возможностями'):
            selectEntranceExams = True
            ExamsReason = EntranceExams.LIMITED_OPTIONS
            DisciplineCodes = EntranceExams.LITERATURE
            page.select_internalDisciplineCodes('on', ExamsReason, DisciplineCodes)
        with allure.step('Включить чекбокс "Мне необходимы специальные условия"'):
            haveDisability = True
            page.select_haveDisability()
        with allure.step('Выбрать в поле "Предпочитаемая форма сдачи вступительных испытаний" - "Дистанционная"'):
            preferredForm = PreferredForm.REMOTE
            page.select_preferredForm(preferredForm)
        with allure.step('Включить чекбокс "Я участвовал(а) в олимпиадах школьников"'):
            haveOlympiad = True
            olympiadInfo = random_en(8)
            page.select_checkbox_haveOlympiad(olympiadInfo, 'on')
        with allure.step('Включить чекбокс "У меня есть особые права или преимущественное право зачисления"'):
            Rights = True
            page.select_checkbox_exclusiveRights(Rights, Rights, Rights)
        with allure.step('Включить чекбокс "Я заключил(а) договор о целевом обучении"'):
            aveAgreementTargetEdu = True
            page.select_aveAgreementTargetEdu('on')
        with allure.step('Ввести в поле "Номер договора о целевом обучении" 10 цифр'):
            agreementNumber = random_number(10)
            page.input_agreementNumber(agreementNumber)
        with allure.step('Ввести в поле "Дата заключения договора" - 22.04.2021'):
            agreementDate = '22.04.2021'
            page.input_agreementDate(agreementDate)
        with allure.step('Ввести в поле "Данные об организации по договору о целевом обучении" 8 символов'):
            agreementOrg = random_ru(8)
            page.input_agreementOrg(agreementOrg)
        with allure.step('Ввести в поле "Данные об образовательной программе по договору о целевом" 8 символов'):
            agreementEduProgram = random_ru(8)
            page.input_agreementEduProgram(agreementEduProgram)
        with allure.step('Проверка общих данных в столбец online_entrant_requests.data'):
            page.check_record_data_step_1(email_abitur, step, haveEduLevel, wantRequestType, originalReturnWay,
                                          haveDisability, preferredForm, aveAgreementTargetEdu, agreementNumber,
                                          agreementDate, agreementOrg, agreementEduProgram)
        with allure.step('Проверка записи баллов ЕГЭ (Биология) в столбец online_entrant_requests.data'):
            page.check_records_Exam(email_abitur, ExamCodes_1, ExamMarks_1, ExamYears_1)
        with allure.step('Проверка записи баллов ЕГЭ (Информатика) в столбец online_entrant_requests.data'):
            page.check_records_Exam(email_abitur, ExamCodes_2, ExamMarks_2, ExamYears_2)
        with allure.step('Проверка записи основания ВИ (Литература) в столбец online_entrant_requests.data'):
            page.check_records_internalDisciplineCodes(email_abitur, selectEntranceExams, ExamsReason, DisciplineCodes)
        with allure.step('Проверка записи участия в олимпиаде в столбец online_entrant_requests.data'):
            page.check_records_Olympiad(email_abitur, haveOlympiad, olympiadInfo)
        with allure.step('Проверка записи особых прав в столбец online_entrant_requests.data'):
            page.check_records_exclusiveRights(email_abitur, Rights, Rights, Rights)

    @allure.story('Проверка функциональности первого шага')
    def testing_the_first_step_with_the_RC_type_Admission_to_secondary_education_programs(self, browser):
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        email_abitur = random_en(8) + '@abitur.test'
        step = 1
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись поступающего'):
                with allure.step('Создать верефицированную учётную запись'):
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email_abitur)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_abitur, Roles.entrant)
            with allure.step('Установить настройки ПК - "Включить все поля/необязательно"'):
                TestAbitur_PRECONDITION().set_settings_RC(AbiturMain.ENABLE_ALL, EduLevel.ENABLE_ALL,
                                                          RequestType.ENABLE_ALL, ScanCopies.OPTIONAL_ALL,
                                                          FirstStep.OPTIONAL_ALL, SecondStep.OPTIONAL_ALL,
                                                          CompetitionLists.ENABLE_ALL, ContractCommon.OPTIONAL_ALL,
                                                          ContractEntrantPersonType.ENABLE_ALL, ContractOtherPersonType.
                                                          OPTIONAL_ALL, ContractLegalPersonType.OPTIONAL_ALL)
            with allure.step('Обновить тип ПК - "Прием на обучение по программам среднего профессионального"'):
                SETTING().update_type_enrollment_campaigns(RECEPTION_COMPANY, type_pk)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition - Поступающий)'):
            page.input_email(email_abitur)
        with allure.step('Ввести в поле "Пароль" (Precondition - Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Applications(browser, link)
        with allure.step('Открыть вкладку "Заявления"'):
            page.open_page_applications()
        with allure.step('Нажать кнопку "Подать новое онлайн-заявление"'):
            page.submit_a_new_application_in_multi_company_mode('new')
        with allure.step('Ввести в поиске ранее созданную ОО'):
            page.search_educational_organization(EDUCATIONAL_ORGANIZATION)
        with allure.step('Открыть найденную ОО'):
            page.open_educational_organization()
        with allure.step('Выбрать ранее созданную ПК'):
            page.select_a_reception_company(RECEPTION_COMPANY)
        with allure.step('Выбрать в поле "Я уже получил(а) образование" - "Среднее профессиональное образование"'):
            haveEduLevel = EduLevel.SECONDARY_VOCATIONAL
            page.select_haveEduLevel(haveEduLevel)
        with allure.step('Выбрать в поле "Я хочу поступать на" - "Среднее профессиональное образование"'):
            wantRequestType = RequestType.SPO
            page.select_wantRequestType(wantRequestType)
        with allure.step('Выбрать в поле "Способ возврата оригиналов документов" - "По почте"'):
            originalReturnWay = ReturnWay.BY_MAIL
            page.select_originalReturnWay(originalReturnWay)
        with allure.step('Включить чекбокс "Мне необходимы специальные условия"'):
            haveDisability = True
            page.select_haveDisability()
        with allure.step('Выбрать в поле "Предпочитаемая форма сдачи вступительных испытаний" - "Дистанционная"'):
            preferredForm = PreferredForm.REMOTE
            page.select_preferredForm(preferredForm)
        with allure.step('Включить чекбокс "Я заключил(а) договор о целевом обучении"'):
            aveAgreementTargetEdu = True
            page.select_aveAgreementTargetEdu('on')
        with allure.step('Ввести в поле "Номер договора о целевом обучении" 10 цифр'):
            agreementNumber = random_number(10)
            page.input_agreementNumber(agreementNumber)
        with allure.step('Ввести в поле "Дата заключения договора" - 22.04.2021'):
            agreementDate = '22.04.2021'
            page.input_agreementDate(agreementDate)
        with allure.step('Ввести в поле "Данные об организации по договору о целевом обучении" 8 символов'):
            agreementOrg = random_ru(8)
            page.input_agreementOrg(agreementOrg)
        with allure.step('Ввести в поле "Данные об образовательной программе по договору о целевом" 8 символов'):
            agreementEduProgram = random_ru(8)
            page.input_agreementEduProgram(agreementEduProgram)
        with allure.step('Проверка записи данных в столбец online_entrant_requests.data'):
            page.check_record_data_step_1(email_abitur, step, haveEduLevel, wantRequestType, originalReturnWay,
                                          haveDisability, preferredForm, aveAgreementTargetEdu, agreementNumber,
                                          agreementDate, agreementOrg, agreementEduProgram)

    @allure.story('Проверка функциональности первого шага')
    def testing_the_first_step_with_the_RC_type_Admission_to_Masters_degree_programs(self, browser):
        type_pk = 'Прием на обучение по программам магистратуры'
        email_abitur = random_en(8) + '@abitur.test'
        step = 1
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись поступающего'):
                with allure.step('Создать верефицированную учётную запись'):
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email_abitur)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_abitur, Roles.entrant)
            with allure.step('Установить настройки ПК - "Включить все поля/необязательно"'):
                TestAbitur_PRECONDITION().set_settings_RC(AbiturMain.ENABLE_ALL, EduLevel.ENABLE_ALL,
                                                          RequestType.ENABLE_ALL, ScanCopies.OPTIONAL_ALL,
                                                          FirstStep.OPTIONAL_ALL, SecondStep.OPTIONAL_ALL,
                                                          CompetitionLists.ENABLE_ALL, ContractCommon.OPTIONAL_ALL,
                                                          ContractEntrantPersonType.ENABLE_ALL, ContractOtherPersonType.
                                                          OPTIONAL_ALL, ContractLegalPersonType.OPTIONAL_ALL)
            with allure.step('Обновить тип ПК - "Прием на обучение по программам магистратуры"'):
                SETTING().update_type_enrollment_campaigns(RECEPTION_COMPANY, type_pk)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition - Поступающий)'):
            page.input_email(email_abitur)
        with allure.step('Ввести в поле "Пароль" (Precondition - Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Applications(browser, link)
        with allure.step('Открыть вкладку "Заявления"'):
            page.open_page_applications()
        with allure.step('Нажать кнопку "Подать новое онлайн-заявление"'):
            page.submit_a_new_application_in_multi_company_mode('new')
        with allure.step('Ввести в поиске ранее созданную ОО'):
            page.search_educational_organization(EDUCATIONAL_ORGANIZATION)
        with allure.step('Открыть найденную ОО'):
            page.open_educational_organization()
        with allure.step('Выбрать ранее созданную ПК'):
            page.select_a_reception_company(RECEPTION_COMPANY)
        with allure.step('Выбрать в поле "Я уже получил(а) образование" - "Высшее образование - высшей квалификации"'):
            haveEduLevel = EduLevel.HIGHEST_QUALIFICATION
            page.select_haveEduLevel(haveEduLevel)
        with allure.step('Выбрать в поле "Я хочу поступать на" - "Программы магистратуры"'):
            wantRequestType = RequestType.MASTER
            page.select_wantRequestType(wantRequestType)
        with allure.step('Выбрать в поле "Способ возврата оригиналов документов" - "По почте"'):
            originalReturnWay = ReturnWay.BY_MAIL
            page.select_originalReturnWay(originalReturnWay)
        with allure.step('Включить чекбокс "Мне необходимы специальные условия"'):
            haveDisability = True
            page.select_haveDisability()
        with allure.step('Выбрать в поле "Предпочитаемая форма сдачи вступительных испытаний" - "Дистанционная"'):
            preferredForm = PreferredForm.REMOTE
            page.select_preferredForm(preferredForm)
        with allure.step('Включить чекбокс "Я заключил(а) договор о целевом обучении"'):
            aveAgreementTargetEdu = True
            page.select_aveAgreementTargetEdu('on')
        with allure.step('Ввести в поле "Номер договора о целевом обучении" 10 цифр'):
            agreementNumber = random_number(10)
            page.input_agreementNumber(agreementNumber)
        with allure.step('Ввести в поле "Дата заключения договора" - 22.04.2021'):
            agreementDate = '22.04.2021'
            page.input_agreementDate(agreementDate)
        with allure.step('Ввести в поле "Данные об организации по договору о целевом обучении" 8 символов'):
            agreementOrg = random_ru(8)
            page.input_agreementOrg(agreementOrg)
        with allure.step('Ввести в поле "Данные об образовательной программе по договору о целевом" 8 символов'):
            agreementEduProgram = random_ru(8)
            page.input_agreementEduProgram(agreementEduProgram)
        with allure.step('Проверка записи данных в столбец online_entrant_requests.data'):
            page.check_record_data_step_1(email_abitur, step, haveEduLevel, wantRequestType, originalReturnWay,
                                          haveDisability, preferredForm, aveAgreementTargetEdu, agreementNumber,
                                          agreementDate, agreementOrg, agreementEduProgram)

    @allure.story('Проверка функциональности первого шага')
    def testing_of_the_first_step_with_the_RC_type_Admission_to_higher_qualification_programs(self, browser):
        type_pk = 'Прием на обучение по программам подготовки кадров высшей квалификации'
        email_abitur = random_en(8) + '@abitur.test'
        step = 1
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись поступающего'):
                with allure.step('Создать верефицированную учётную запись'):
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email_abitur)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_abitur, Roles.entrant)
            with allure.step('Установить настройки ПК - "Включить все поля/необязательно"'):
                TestAbitur_PRECONDITION().set_settings_RC(AbiturMain.ENABLE_ALL, EduLevel.ENABLE_ALL,
                                                          RequestType.ENABLE_ALL, ScanCopies.OPTIONAL_ALL,
                                                          FirstStep.OPTIONAL_ALL, SecondStep.OPTIONAL_ALL,
                                                          CompetitionLists.ENABLE_ALL, ContractCommon.OPTIONAL_ALL,
                                                          ContractEntrantPersonType.ENABLE_ALL, ContractOtherPersonType.
                                                          OPTIONAL_ALL, ContractLegalPersonType.OPTIONAL_ALL)
            with allure.step('Обновить тип ПК - "Прием на обучение по программам кадров высшей квалификации"'):
                SETTING().update_type_enrollment_campaigns(RECEPTION_COMPANY, type_pk)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition - Поступающий)'):
            page.input_email(email_abitur)
        with allure.step('Ввести в поле "Пароль" (Precondition - Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Applications(browser, link)
        with allure.step('Открыть вкладку "Заявления"'):
            page.open_page_applications()
        with allure.step('Нажать кнопку "Подать новое онлайн-заявление"'):
            page.submit_a_new_application_in_multi_company_mode('new')
        with allure.step('Ввести в поиске ранее созданную ОО'):
            page.search_educational_organization(EDUCATIONAL_ORGANIZATION)
        with allure.step('Открыть найденную ОО'):
            page.open_educational_organization()
        with allure.step('Выбрать ранее созданную ПК'):
            page.select_a_reception_company(RECEPTION_COMPANY)
        with allure.step('Выбрать в поле "Я уже получил(а) образование" - "Высшее образование - специалитет"'):
            haveEduLevel = EduLevel.MASTER
            page.select_haveEduLevel(haveEduLevel)
        with allure.step('Выбрать в поле "Я хочу поступать на" - "Программы ординатуры"'):
            wantRequestType = RequestType.RESIDENCY
            page.select_wantRequestType(wantRequestType)
        with allure.step('Выбрать в поле "Способ возврата оригиналов документов" - "Через портал гос. услуг"'):
            originalReturnWay = ReturnWay.VIA_THE_SERVICE_PORTAL
            page.select_originalReturnWay(originalReturnWay)
        with allure.step('Включить чекбокс "Мне необходимы специальные условия"'):
            haveDisability = True
            page.select_haveDisability()
        with allure.step('Выбрать в поле "Предпочитаемая форма сдачи вступительных испытаний" - "Дистанционная"'):
            preferredForm = PreferredForm.REMOTE
            page.select_preferredForm(preferredForm)
        with allure.step('Включить чекбокс "Я заключил(а) договор о целевом обучении"'):
            aveAgreementTargetEdu = True
            page.select_aveAgreementTargetEdu('on')
        with allure.step('Ввести в поле "Номер договора о целевом обучении" 10 цифр'):
            agreementNumber = random_number(10)
            page.input_agreementNumber(agreementNumber)
        with allure.step('Ввести в поле "Дата заключения договора" - 22.04.2021'):
            agreementDate = '22.04.2021'
            page.input_agreementDate(agreementDate)
        with allure.step('Ввести в поле "Данные об организации по договору о целевом обучении" 8 символов'):
            agreementOrg = random_ru(8)
            page.input_agreementOrg(agreementOrg)
        with allure.step('Ввести в поле "Данные об образовательной программе по договору о целевом" 8 символов'):
            agreementEduProgram = random_ru(8)
            page.input_agreementEduProgram(agreementEduProgram)
        with allure.step('Проверка записи данных в столбец online_entrant_requests.data'):
            page.check_record_data_step_1(email_abitur, step, haveEduLevel, wantRequestType, originalReturnWay,
                                          haveDisability, preferredForm, aveAgreementTargetEdu, agreementNumber,
                                          agreementDate, agreementOrg, agreementEduProgram)

    @allure.story('Проверка функциональности первого шага')
    def testing_the_first_step_with_entering_limit_values(self, browser):
        type_pk = 'Прием на обучение по программам бакалавриата, специалитета'
        email_abitur = random_en(8) + '@abitur.test'
        step = 1
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись поступающего'):
                with allure.step('Создать верефицированную учётную запись'):
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email_abitur)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_abitur, Roles.entrant)
            with allure.step('Установить настройки ПК - "Включить все поля/необязательно"'):
                TestAbitur_PRECONDITION().set_settings_RC(AbiturMain.ENABLE_ALL, EduLevel.ENABLE_ALL,
                                                          RequestType.ENABLE_ALL, ScanCopies.OPTIONAL_ALL,
                                                          FirstStep.OPTIONAL_ALL, SecondStep.OPTIONAL_ALL,
                                                          CompetitionLists.ENABLE_ALL, ContractCommon.OPTIONAL_ALL,
                                                          ContractEntrantPersonType.ENABLE_ALL,
                                                          ContractOtherPersonType.
                                                          OPTIONAL_ALL, ContractLegalPersonType.OPTIONAL_ALL)
            with allure.step('Обновить тип ПК - "Прием на обучение по программам бакалавриата, специалитета"'):
                SETTING().update_type_enrollment_campaigns(RECEPTION_COMPANY, type_pk)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition - Поступающий)'):
            page.input_email(email_abitur)
        with allure.step('Ввести в поле "Пароль" (Precondition - Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Applications(browser, link)
        with allure.step('Открыть вкладку "Заявления"'):
            page.open_page_applications()
        with allure.step('Нажать кнопку "Подать новое онлайн-заявление"'):
            page.submit_a_new_application_in_multi_company_mode('new')
        with allure.step('Ввести в поиске ранее созданную ОО'):
            page.search_educational_organization(EDUCATIONAL_ORGANIZATION)
        with allure.step('Открыть найденную ОО'):
            page.open_educational_organization()
        with allure.step('Выбрать ранее созданную ПК'):
            page.select_a_reception_company(RECEPTION_COMPANY)
        with allure.step('Выбрать в поле "Я уже получил(а) образование" - "Высшее образование - бакалавриат"'):
            haveEduLevel = EduLevel.UNDERGRADUATE
            page.select_haveEduLevel(haveEduLevel)
        with allure.step('Выбрать в поле "Я хочу поступать на" - "Программы бакалавриата, специалитета"'):
            wantRequestType = RequestType.BACH_SPEC
            page.select_wantRequestType(wantRequestType)
        with allure.step('Выбрать в поле "Способ возврата оригиналов документов" - "По доверенности"'):
            originalReturnWay = ReturnWay.BY_PROXY
            page.select_originalReturnWay(originalReturnWay)
        with allure.step('Выбрать в поле "Я уже сдавал(а) или буду сдавать ЕГЭ" - Физика/100 баллов/2021'):
            ExamCodes = Exam.PHYSICS
            ExamMarks = '100'
            ExamYears = '2021'
            page.select_Exam(ExamCodes, ExamMarks, ExamYears, 'on')
        # with allure.step('Выбрать в поле "Предпочитаемая форма сдачи вступительных испытаний" - "Дистанционная"'):
        #     preferredForm = PreferredForm.REMOTE
        #     page.select_preferredForm(preferredForm)
        with allure.step('Включить чекбокс "Я участвовал(а) в олимпиадах школьников", Название - 255 символов'):
            haveOlympiad = True
            olympiadInfo = random_en(255)
            page.select_checkbox_haveOlympiad(olympiadInfo, 'on')
        with allure.step('Включить чекбокс "Я заключил(а) договор о целевом обучении"'):
            aveAgreementTargetEdu = True
            page.select_aveAgreementTargetEdu('on')
        with allure.step('Ввести в поле "Номер договора о целевом обучении" 255 цифр'):
            agreementNumber = random_number(255)
            page.input_agreementNumber(agreementNumber)
        with allure.step('Ввести в поле "Дата заключения договора" - 01.02.1000'):
            agreementDate = '01.02.1000'
            page.input_agreementDate(agreementDate)
        with allure.step('Ввести в поле "Данные об организации по договору о целевом обучении" 255 символов'):
            agreementOrg = random_ru(255)
            page.input_agreementOrg(agreementOrg)
        with allure.step('Ввести в поле "Данные об образовательной программе по договору о целевом" 255 символов'):
            agreementEduProgram = random_ru(255)
            page.input_agreementEduProgram(agreementEduProgram)
        with allure.step('Проверка общих данных в столбец online_entrant_requests.data'):
            page.check_record_data_step_1(email_abitur, step, haveEduLevel, wantRequestType, originalReturnWay,
                                          None, None, aveAgreementTargetEdu, agreementNumber,
                                          agreementDate, agreementOrg, agreementEduProgram)
        with allure.step('Проверка записи баллов ЕГЭ (Биология) в столбец online_entrant_requests.data'):
            page.check_records_Exam(email_abitur, ExamCodes, ExamMarks, ExamYears)
        with allure.step('Проверка записи участия в олимпиаде в столбец online_entrant_requests.data'):
            page.check_records_Olympiad(email_abitur, haveOlympiad, olympiadInfo)

    @allure.story('Проверка функциональности первого шага')
    def testing_the_first_step_with_the_input_of_exorbitant_values_in_the_form_Exam(self, browser):
        type_pk = 'Прием на обучение по программам бакалавриата, специалитета'
        email_abitur = random_en(8) + '@abitur.test'
        ExamMarks = random_number(256)
        ExamYears = '2021'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись поступающего'):
                with allure.step('Создать верефицированную учётную запись'):
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email_abitur)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_abitur, Roles.entrant)
            with allure.step('Установить настройки ПК - "Включить все поля/необязательно"'):
                TestAbitur_PRECONDITION().set_settings_RC(AbiturMain.ENABLE_ALL, EduLevel.ENABLE_ALL,
                                                          RequestType.ENABLE_ALL, ScanCopies.OPTIONAL_ALL,
                                                          FirstStep.OPTIONAL_ALL, SecondStep.OPTIONAL_ALL,
                                                          CompetitionLists.ENABLE_ALL, ContractCommon.OPTIONAL_ALL,
                                                          ContractEntrantPersonType.ENABLE_ALL,
                                                          ContractOtherPersonType.
                                                          OPTIONAL_ALL, ContractLegalPersonType.OPTIONAL_ALL)
            with allure.step('Обновить тип ПК - "Прием на обучение по программам бакалавриата, специалитета"'):
                SETTING().update_type_enrollment_campaigns(RECEPTION_COMPANY, type_pk)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition - Поступающий)'):
            page.input_email(email_abitur)
        with allure.step('Ввести в поле "Пароль" (Precondition - Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Applications(browser, link)
        with allure.step('Открыть вкладку "Заявления"'):
            page.open_page_applications()
        with allure.step('Нажать кнопку "Подать новое онлайн-заявление"'):
            page.submit_a_new_application_in_multi_company_mode('new')
        with allure.step('Ввести в поиске ранее созданную ОО'):
            page.search_educational_organization(EDUCATIONAL_ORGANIZATION)
        with allure.step('Открыть найденную ОО'):
            page.open_educational_organization()
        with allure.step('Выбрать ранее созданную ПК'):
            page.select_a_reception_company(RECEPTION_COMPANY)
        with allure.step('Выбрать в поле "Я уже получил(а) образование" - "Высшее образование - бакалавриат"'):
            haveEduLevel = EduLevel.UNDERGRADUATE
            page.select_haveEduLevel(haveEduLevel)
        with allure.step('Выбрать в поле "Я хочу поступать на" - "Программы бакалавриата, специалитета"'):
            wantRequestType = RequestType.BACH_SPEC
            page.select_wantRequestType(wantRequestType)
        with allure.step('Выбрать в поле "Способ возврата оригиналов документов" - "По почте"'):
            originalReturnWay = ReturnWay.BY_MAIL
            page.select_originalReturnWay(originalReturnWay)
        with allure.step('Проверка валидации поля "ЕГЭ - Английский язык" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Балл ЕГЭ" 256 символов'):
                ExamCodes_1 = Exam.ENGLISH_LANGUAGE
                page.select_Exam(ExamCodes_1, ExamMarks, ExamYears, 'on')
            with allure.step('Нажать кнопку "Далее"'):
                page.go_to_the_NextStep('Incorrect', 2, 'Поле «баллы ЕГЭ» должно быть целым числом.')
            with allure.step('Вернуть поле в исходное состояние'):
                page.select_Exam(ExamCodes_1, '100', '2021', 'off')
        with allure.step('Проверка валидации поля "ЕГЭ - Биология" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Балл ЕГЭ" 256 символов'):
                ExamCodes_2 = Exam.BIOLOGY
                page.select_Exam(ExamCodes_2, ExamMarks, ExamYears, 'on')
            with allure.step('Нажать кнопку "Далее"'):
                page.go_to_the_NextStep('Incorrect', 2, 'Поле «баллы ЕГЭ» должно быть целым числом.')
            with allure.step('Вернуть поле в исходное состояние'):
                page.select_Exam(ExamCodes_2, '100', '2021', 'off')
        with allure.step('Проверка валидации поля "ЕГЭ - География" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Балл ЕГЭ" 256 символов'):
                ExamCodes_3 = Exam.GEOGRAPHY
                page.select_Exam(ExamCodes_3, ExamMarks, ExamYears, 'on')
            with allure.step('Нажать кнопку "Далее"'):
                page.go_to_the_NextStep('Incorrect', 2, 'Поле «баллы ЕГЭ» должно быть целым числом.')
            with allure.step('Вернуть поле в исходное состояние'):
                page.select_Exam(ExamCodes_3, '100', '2021', 'off')
        with allure.step('Проверка валидации поля "ЕГЭ - Информатика и ИКТ" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Балл ЕГЭ" 256 символов'):
                ExamCodes_4 = Exam.COMPUTER_SCIENCE
                page.select_Exam(ExamCodes_4, ExamMarks, ExamYears, 'on')
            with allure.step('Нажать кнопку "Далее"'):
                page.go_to_the_NextStep('Incorrect', 2, 'Поле «баллы ЕГЭ» должно быть целым числом.')
            with allure.step('Вернуть поле в исходное состояние'):
                page.select_Exam(ExamCodes_4, '100', '2021', 'off')
        with allure.step('Проверка валидации поля "ЕГЭ - Испанский язык" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Балл ЕГЭ" 256 символов'):
                ExamCodes_5 = Exam.SPANISH_LANGUAGE
                page.select_Exam(ExamCodes_5, ExamMarks, ExamYears, 'on')
            with allure.step('Нажать кнопку "Далее"'):
                page.go_to_the_NextStep('Incorrect', 2, 'Поле «баллы ЕГЭ» должно быть целым числом.')
            with allure.step('Вернуть поле в исходное состояние'):
                page.select_Exam(ExamCodes_5, '100', '2021', 'off')
        with allure.step('Проверка валидации поля "ЕГЭ - История" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Балл ЕГЭ" 256 символов'):
                ExamCodes_6 = Exam.HISTORY
                page.select_Exam(ExamCodes_6, ExamMarks, ExamYears, 'on')
            with allure.step('Нажать кнопку "Далее"'):
                page.go_to_the_NextStep('Incorrect', 2, 'Поле «баллы ЕГЭ» должно быть целым числом.')
            with allure.step('Вернуть поле в исходное состояние'):
                page.select_Exam(ExamCodes_6, '100', '2021', 'off')
        with allure.step('Проверка валидации поля "ЕГЭ - Литература" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Балл ЕГЭ" 256 символов'):
                ExamCodes_7 = Exam.LITERATURE
                page.select_Exam(ExamCodes_7, ExamMarks, ExamYears, 'on')
            with allure.step('Нажать кнопку "Далее"'):
                page.go_to_the_NextStep('Incorrect', 2, 'Поле «баллы ЕГЭ» должно быть целым числом.')
            with allure.step('Вернуть поле в исходное состояние'):
                page.select_Exam(ExamCodes_7, '100', '2021', 'off')
        with allure.step('Проверка валидации поля "ЕГЭ - Математика" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Балл ЕГЭ" 256 символов'):
                ExamCodes_8 = Exam.MATHEMATICS
                page.select_Exam(ExamCodes_8, ExamMarks, ExamYears, 'on')
            with allure.step('Нажать кнопку "Далее"'):
                page.go_to_the_NextStep('Incorrect', 2, 'Поле «баллы ЕГЭ» должно быть целым числом.')
            with allure.step('Вернуть поле в исходное состояние'):
                page.select_Exam(ExamCodes_8, '100', '2021', 'off')
        with allure.step('Проверка валидации поля "ЕГЭ - Немецкий язык" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Балл ЕГЭ" 256 символов'):
                ExamCodes_9 = Exam.GERMAN_LANGUAGE
                page.select_Exam(ExamCodes_9, ExamMarks, ExamYears, 'on')
            with allure.step('Нажать кнопку "Далее"'):
                page.go_to_the_NextStep('Incorrect', 2, 'Поле «баллы ЕГЭ» должно быть целым числом.')
            with allure.step('Вернуть поле в исходное состояние'):
                page.select_Exam(ExamCodes_9, '100', '2021', 'off')
        with allure.step('Проверка валидации поля "ЕГЭ - Обществознание" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Балл ЕГЭ" 256 символов'):
                ExamCodes_10 = Exam.SOCIAL_STUDIES
                page.select_Exam(ExamCodes_10, ExamMarks, ExamYears, 'on')
            with allure.step('Нажать кнопку "Далее"'):
                page.go_to_the_NextStep('Incorrect', 2, 'Поле «баллы ЕГЭ» должно быть целым числом.')
            with allure.step('Вернуть поле в исходное состояние'):
                page.select_Exam(ExamCodes_10, '100', '2021', 'off')
        with allure.step('Проверка валидации поля "ЕГЭ - Русский язык" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Балл ЕГЭ" 256 символов'):
                ExamCodes_11 = Exam.RUSSIAN
                page.select_Exam(ExamCodes_11, ExamMarks, ExamYears, 'on')
            with allure.step('Нажать кнопку "Далее"'):
                page.go_to_the_NextStep('Incorrect', 2, 'Поле «баллы ЕГЭ» должно быть целым числом.')
            with allure.step('Вернуть поле в исходное состояние'):
                page.select_Exam(ExamCodes_11, '100', '2021', 'off')
        with allure.step('Проверка валидации поля "ЕГЭ - Физика" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Балл ЕГЭ" 256 символов'):
                ExamCodes_12 = Exam.PHYSICS
                page.select_Exam(ExamCodes_12, ExamMarks, ExamYears, 'on')
            with allure.step('Нажать кнопку "Далее"'):
                page.go_to_the_NextStep('Incorrect', 2, 'Поле «баллы ЕГЭ» должно быть целым числом.')
            with allure.step('Вернуть поле в исходное состояние'):
                page.select_Exam(ExamCodes_12, '100', '2021', 'off')
        with allure.step('Проверка валидации поля "ЕГЭ - Французский язык" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Балл ЕГЭ" 256 символов'):
                ExamCodes_13 = Exam.FRENCH_LANGUAGE
                page.select_Exam(ExamCodes_13, ExamMarks, ExamYears, 'on')
            with allure.step('Нажать кнопку "Далее"'):
                page.go_to_the_NextStep('Incorrect', 2, 'Поле «баллы ЕГЭ» должно быть целым числом.')
            with allure.step('Вернуть поле в исходное состояние'):
                page.select_Exam(ExamCodes_13, '100', '2021', 'off')
        with allure.step('Проверка валидации поля "ЕГЭ - Химия" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Балл ЕГЭ" 256 символов'):
                ExamCodes_14 = Exam.CHEMISTRY
                page.select_Exam(ExamCodes_14, ExamMarks, ExamYears, 'on')
            with allure.step('Нажать кнопку "Далее"'):
                page.go_to_the_NextStep('Incorrect', 2, 'Поле «баллы ЕГЭ» должно быть целым числом.')
            with allure.step('Вернуть поле в исходное состояние'):
                page.select_Exam(ExamCodes_14, '100', '2021', 'off')

    @allure.story('Проверка функциональности первого шага')
    def testing_the_first_step_with_the_input_of_exorbitant_values_in_the_form_olympiadInfo(self, browser):
        type_pk = 'Прием на обучение по программам бакалавриата, специалитета'
        email_abitur = random_en(8) + '@abitur.test'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись поступающего'):
                with allure.step('Создать верефицированную учётную запись'):
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email_abitur)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_abitur, Roles.entrant)
            with allure.step('Установить настройки ПК - "Включить все поля/необязательно"'):
                TestAbitur_PRECONDITION().set_settings_RC(AbiturMain.ENABLE_ALL, EduLevel.ENABLE_ALL,
                                                          RequestType.ENABLE_ALL, ScanCopies.OPTIONAL_ALL,
                                                          FirstStep.OPTIONAL_ALL, SecondStep.OPTIONAL_ALL,
                                                          CompetitionLists.ENABLE_ALL, ContractCommon.OPTIONAL_ALL,
                                                          ContractEntrantPersonType.ENABLE_ALL,
                                                          ContractOtherPersonType.
                                                          OPTIONAL_ALL, ContractLegalPersonType.OPTIONAL_ALL)
            with allure.step('Обновить тип ПК - "Прием на обучение по программам бакалавриата, специалитета"'):
                SETTING().update_type_enrollment_campaigns(RECEPTION_COMPANY, type_pk)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition - Поступающий)'):
            page.input_email(email_abitur)
        with allure.step('Ввести в поле "Пароль" (Precondition - Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Applications(browser, link)
        with allure.step('Открыть вкладку "Заявления"'):
            page.open_page_applications()
        with allure.step('Нажать кнопку "Подать новое онлайн-заявление"'):
            page.submit_a_new_application_in_multi_company_mode('new')
        with allure.step('Ввести в поиске ранее созданную ОО'):
            page.search_educational_organization(EDUCATIONAL_ORGANIZATION)
        with allure.step('Открыть найденную ОО'):
            page.open_educational_organization()
        with allure.step('Выбрать ранее созданную ПК'):
            page.select_a_reception_company(RECEPTION_COMPANY)
        with allure.step('Выбрать в поле "Я уже получил(а) образование" - "Высшее образование - бакалавриат"'):
            haveEduLevel = EduLevel.UNDERGRADUATE
            page.select_haveEduLevel(haveEduLevel)
        with allure.step('Выбрать в поле "Я хочу поступать на" - "Программы бакалавриата, специалитета"'):
            wantRequestType = RequestType.BACH_SPEC
            page.select_wantRequestType(wantRequestType)
        with allure.step('Выбрать в поле "Способ возврата оригиналов документов" - "По почте"'):
            originalReturnWay = ReturnWay.BY_MAIL
            page.select_originalReturnWay(originalReturnWay)
        with allure.step('Проверка валидации поля "Название олимпиады" на ввод запредельных значений'):
            with allure.step('Включить чекбокс "Я участвовал(а) в олимпиадах школьников", Название - 256 символов'):
                olympiadInfo = random_en(256)
                page.select_checkbox_haveOlympiad(olympiadInfo, 'on')
            with allure.step('Нажать кнопку "Далее"'):
                page.go_to_the_NextStep('Incorrect', 2, '«название олимпиады» не может превышать')
            with allure.step('Вернуть поле в исходное состояние'):
                page.select_checkbox_haveOlympiad('', 'off')

    @allure.story('Проверка функциональности первого шага')
    def testing_the_first_step_with_the_input_of_exorbitant_values_in_the_form_Agreement(self, browser):
        type_pk = 'Прием на обучение по программам бакалавриата, специалитета'
        email_abitur = random_en(8) + '@abitur.test'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись поступающего'):
                with allure.step('Создать верефицированную учётную запись'):
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email_abitur)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_abitur, Roles.entrant)
            with allure.step('Установить настройки ПК - "Включить все поля/необязательно"'):
                TestAbitur_PRECONDITION().set_settings_RC(AbiturMain.ENABLE_ALL, EduLevel.ENABLE_ALL,
                                                          RequestType.ENABLE_ALL, ScanCopies.OPTIONAL_ALL,
                                                          FirstStep.OPTIONAL_ALL, SecondStep.OPTIONAL_ALL,
                                                          CompetitionLists.ENABLE_ALL, ContractCommon.OPTIONAL_ALL,
                                                          ContractEntrantPersonType.ENABLE_ALL,
                                                          ContractOtherPersonType.
                                                          OPTIONAL_ALL, ContractLegalPersonType.OPTIONAL_ALL)
            with allure.step('Обновить тип ПК - "Прием на обучение по программам бакалавриата, специалитета"'):
                SETTING().update_type_enrollment_campaigns(RECEPTION_COMPANY, type_pk)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition - Поступающий)'):
            page.input_email(email_abitur)
        with allure.step('Ввести в поле "Пароль" (Precondition - Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Applications(browser, link)
        with allure.step('Открыть вкладку "Заявления"'):
            page.open_page_applications()
        with allure.step('Нажать кнопку "Подать новое онлайн-заявление"'):
            page.submit_a_new_application_in_multi_company_mode('new')
        with allure.step('Ввести в поиске ранее созданную ОО'):
            page.search_educational_organization(EDUCATIONAL_ORGANIZATION)
        with allure.step('Открыть найденную ОО'):
            page.open_educational_organization()
        with allure.step('Выбрать ранее созданную ПК'):
            page.select_a_reception_company(RECEPTION_COMPANY)
        with allure.step('Выбрать в поле "Я уже получил(а) образование" - "Высшее образование - бакалавриат"'):
            haveEduLevel = EduLevel.UNDERGRADUATE
            page.select_haveEduLevel(haveEduLevel)
        with allure.step('Выбрать в поле "Я хочу поступать на" - "Программы бакалавриата, специалитета"'):
            wantRequestType = RequestType.BACH_SPEC
            page.select_wantRequestType(wantRequestType)
        with allure.step('Выбрать в поле "Способ возврата оригиналов документов" - "По почте"'):
            originalReturnWay = ReturnWay.BY_MAIL
            page.select_originalReturnWay(originalReturnWay)
        with allure.step('Включить чекбокс "Я заключил(а) договор о целевом обучении"'):
            page.select_aveAgreementTargetEdu('on')
        with allure.step('Ввести в поле "Дата заключения договора" 17.10.1799'):
            agreementDate = '17.10.1799'
            page.input_agreementDate(agreementDate)
        with allure.step('Проверка валидации поля "Номер договора о целевом обучении" на ввод запредельных значений'):
            with allure.step('Ввести в поле 256 символов'):
                agreementNumber = random_number(256)
                page.input_agreementNumber(agreementNumber)
            with allure.step('Нажать кнопку "Далее"'):
                page.go_to_the_NextStep('Incorrect', 2, '«номер договора о целевом обучении» не может превышать')
            with allure.step('Вернуть поле в исходное состояние'):
                page.input_agreementNumber(random_number(8))
        with allure.step('Проверка валидации поля "Данные об организации по договору" на ввод запредельных значений'):
            with allure.step('Ввести в поле 256 символов'):
                agreementOrg = random_ru(256)
                page.input_agreementOrg(agreementOrg)
            with allure.step('Нажать кнопку "Далее"'):
                page.go_to_the_NextStep('Incorrect', 2, '«данные об организации» не может превышать')
            with allure.step('Вернуть поле в исходное состояние'):
                page.input_agreementOrg(random_number(8))
        with allure.step('Проверка валидации поля "Данные об образовательной программе" на ввод запредельных значений'):
            with allure.step('Ввести в поле 256 символов'):
                agreementEduProgram = random_ru(256)
                page.input_agreementEduProgram(agreementEduProgram)
            with allure.step('Нажать кнопку "Далее"'):
                page.go_to_the_NextStep('Incorrect', 2, '«данные об образовательной программе» не может превышать')
            with allure.step('Вернуть поле в исходное состояние'):
                page.input_agreementEduProgram(random_number(8))

    @allure.story('Проверка функциональности первого шага')
    def testing_the_first_step_with_omitting_the_required_fields(self, browser):
        type_pk = 'Прием на обучение по программам бакалавриата, специалитета'
        email_abitur = random_en(8) + '@abitur.test'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись поступающего'):
                with allure.step('Создать верефицированную учётную запись'):
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email_abitur)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_abitur, Roles.entrant)
            with allure.step('Установить настройки ПК - "Включить все поля/необязательно"'):
                TestAbitur_PRECONDITION().set_settings_RC(AbiturMain.ENABLE_ALL, EduLevel.ENABLE_ALL,
                                                          RequestType.ENABLE_ALL, ScanCopies.OPTIONAL_ALL,
                                                          FirstStep.OPTIONAL_ALL, SecondStep.OPTIONAL_ALL,
                                                          CompetitionLists.ENABLE_ALL, ContractCommon.OPTIONAL_ALL,
                                                          ContractEntrantPersonType.ENABLE_ALL,
                                                          ContractOtherPersonType.
                                                          OPTIONAL_ALL, ContractLegalPersonType.OPTIONAL_ALL)
            with allure.step('Обновить тип ПК - "Прием на обучение по программам бакалавриата, специалитета"'):
                SETTING().update_type_enrollment_campaigns(RECEPTION_COMPANY, type_pk)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition - Поступающий)'):
            page.input_email(email_abitur)
        with allure.step('Ввести в поле "Пароль" (Precondition - Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Applications(browser, link)
        with allure.step('Открыть вкладку "Заявления"'):
            page.open_page_applications()
        with allure.step('Нажать кнопку "Подать новое онлайн-заявление"'):
            page.submit_a_new_application_in_multi_company_mode('new')
        with allure.step('Ввести в поиске ранее созданную ОО'):
            page.search_educational_organization(EDUCATIONAL_ORGANIZATION)
        with allure.step('Открыть найденную ОО'):
            page.open_educational_organization()
        with allure.step('Выбрать ранее созданную ПК'):
            page.select_a_reception_company(RECEPTION_COMPANY)
        with allure.step('Проверка валидации обязательного поля "Я уже получил(а) образование"'):
            with allure.step('Нажать кнопку "Далее"'):
                page.go_to_the_NextStep('Incorrect', 2, '«уровень образования» обязательно')
            with allure.step('Выбрать в поле "Высшее образование - бакалавриат"'):
                haveEduLevel = EduLevel.UNDERGRADUATE
                page.select_haveEduLevel(haveEduLevel)
        with allure.step('Проверка валидации обязательного поля "Я хочу поступать на"'):
            with allure.step('Нажать кнопку "Далее"'):
                page.go_to_the_NextStep('Incorrect', 2, '«желаемая образовательная программа» обязательно')
            with allure.step('Выбрать в поле "Программы бакалавриата, специалитета"'):
                wantRequestType = RequestType.BACH_SPEC
                page.select_wantRequestType(wantRequestType)
        with allure.step('Проверка валидации обязательного поля "Способ возврата"'):
            with allure.step('Нажать кнопку "Далее"'):
                page.go_to_the_NextStep('Incorrect', 2, '«способ возврата документов» обязательно')
            with allure.step('Выбрать в поле "По почте"'):
                originalReturnWay = ReturnWay.BY_MAIL
                page.select_originalReturnWay(originalReturnWay)
        with allure.step('Проверка валидации обязательного поля "Предпочитаемая форма сдачи вступительных испытаний"'):
            with allure.step('Включить чекбокс "Прошу допустить к ВИ" - Иностранные/Лица с огр. возможностями'):
                ExamsReason = EntranceExams.LIMITED_OPTIONS
                DisciplineCodes = EntranceExams.LITERATURE
                page.select_internalDisciplineCodes('on', ExamsReason, DisciplineCodes)
            with allure.step('Нажать кнопку "Далее"'):
                page.go_to_the_NextStep('Incorrect', 2, 'форма сдачи вступительных испытаний» обязательно')
            with allure.step('Выбрать в поле "Дистанционная"'):
                preferredForm = PreferredForm.REMOTE
                page.select_preferredForm(preferredForm)

    @allure.story('Проверка функциональности второго шага')
    def testing_the_second_step_with_the_RC_type_Admission_to_bachelors_programs(self, browser):
        type_pk = 'Прием на обучение по программам бакалавриата, специалитета'
        email_abitur = random_en(8) + '@abitur.test'
        step = 2
        competitions = EnrollmentEnvironment.getEnrollmentEnvironmentCompetition(RequestType.BACH_SPEC, BACH_2017)
        edu_program = EnrollmentEnvironment.getEnrollmentEnvironmentEDUProgram(RequestType.BACH_SPEC, BACH_2017)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись поступающего'):
                with allure.step('Создать верефицированную учётную запись'):
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email_abitur)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_abitur, Roles.entrant)
                with allure.step('Создать запись с пройденным первым шагом заявления'):
                    ABITUR_DB().add_online_entrant_requests(email_abitur, FirstStep.FINISH_BACH_APP,
                                                            RECEPTION_COMPANY, 'pending')
                with allure.step('Добавить запись в user_enrollment_campaigns'):
                    SETTING().add_record_user_enrollment_campaigns(email_abitur, RECEPTION_COMPANY)
            with allure.step('Установить настройки ПК - "Включить все поля/необязательно"'):
                TestAbitur_PRECONDITION().set_settings_RC(AbiturMain.ENABLE_ALL, EduLevel.ENABLE_ALL,
                                                          RequestType.ENABLE_ALL, ScanCopies.OPTIONAL_ALL,
                                                          FirstStep.OPTIONAL_ALL, SecondStep.OPTIONAL_ALL,
                                                          CompetitionLists.ENABLE_ALL, ContractCommon.OPTIONAL_ALL,
                                                          ContractEntrantPersonType.ENABLE_ALL,
                                                          ContractOtherPersonType.
                                                          OPTIONAL_ALL, ContractLegalPersonType.OPTIONAL_ALL)
            with allure.step('Обновить тип ПК - "Прием на обучение по программам бакалавриата, специалитета"'):
                SETTING().update_type_enrollment_campaigns(RECEPTION_COMPANY, type_pk)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition - Поступающий)'):
            page.input_email(email_abitur)
        with allure.step('Ввести в поле "Пароль" (Precondition - Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Applications(browser, link)
        with allure.step('Открыть вкладку "Заявления"'):
            page.open_page_applications()
        with allure.step('Нажать кнопку "Перейти к выбору направлений"'):
            page.go_to_filling_in_the_master_data(2)
        with allure.step('Выбрать любой доступный конкурс в блоке'):
            page.choose_a_contest(competitions)
        with allure.step('Указать приоритет выбранному конкурсу'):
            competitionPriorities = '1'
            page.give_priority_to_the_competition(0, competitionPriorities)
        with allure.step('Указать приоритет выбранному направлению'):
            requestedProgramPriorities = '1'
            page.give_priority_to_the_competition(1, requestedProgramPriorities)
        with allure.step('Проверка записи данных в столбец online_entrant_requests.data'):
            page.check_record_data_step_2(email_abitur, step, competitions,
                                          competitionPriorities, edu_program, competitionPriorities)

    @allure.story('Проверка функциональности третьего шага')
    def testing_the_third_step_with_the_RC_type_Admission_to_bachelors_programs(self, browser):
        type_pk = 'Прием на обучение по программам бакалавриата, специалитета'
        email_abitur = random_en(8) + '@abitur.test'
        step = 3
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись поступающего'):
                with allure.step('Создать верефицированную учётную запись'):
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email_abitur)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_abitur, Roles.entrant)
                with allure.step('Создать запись с пройденным первым/вторым шагом заявления'):
                    ABITUR_DB().add_online_entrant_requests(email_abitur, FirstStep.FINISH_BACH_APP,
                                                            RECEPTION_COMPANY, 'pending')
                with allure.step('Добавить запись в user_enrollment_campaigns'):
                    SETTING().add_record_user_enrollment_campaigns(email_abitur, RECEPTION_COMPANY)
            with allure.step('Установить настройки ПК - "Включить все поля/необязательно"'):
                TestAbitur_PRECONDITION().set_settings_RC(AbiturMain.ENABLE_ALL, EduLevel.ENABLE_ALL,
                                                          RequestType.ENABLE_ALL, ScanCopies.OPTIONAL_ALL,
                                                          FirstStep.OPTIONAL_ALL, SecondStep.OPTIONAL_ALL,
                                                          CompetitionLists.ENABLE_ALL, ContractCommon.OPTIONAL_ALL,
                                                          ContractEntrantPersonType.ENABLE_ALL,
                                                          ContractOtherPersonType.
                                                          OPTIONAL_ALL, ContractLegalPersonType.OPTIONAL_ALL)
            with allure.step('Обновить тип ПК - "Прием на обучение по программам бакалавриата, специалитета"'):
                SETTING().update_type_enrollment_campaigns(RECEPTION_COMPANY, type_pk)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition - Поступающий)'):
            page.input_email(email_abitur)
        with allure.step('Ввести в поле "Пароль" (Precondition - Поступающий)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Applications(browser, link)
        with allure.step('Открыть вкладку "Заявления"'):
            page.open_page_applications()
        with allure.step('Нажать кнопку "Перейти к выбору направлений"'):
            page.go_to_filling_in_the_master_data(2)
        with allure.step('Нажать кнопку "Далее"'):
            page.go_to_the_NextStep('Correct', 3, '')


@pytest.mark.abitur
@allure.link(link + '/application/regulations/categories', name='ТАНДЕМ ЛК - Нормативные документы')
@allure.epic('Функциональное тестирование модуля "ABITUR"')
@allure.feature('Проверка функциональности раздела "Нормативные документы"')
class TestAbitur_REGULATIONS_CATEGORIES:
    @pytest.mark.smoke
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.story('Позитивная проверка функциональности')
    def testing_the_addition_of_a_regulatory_document_category_with_valid_data(self, browser):
        title = random_en(8)
        short_title = random_en(8)
        active = 0
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (Precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Нормативные документы"'):
            page.open_window_setting('Нормативные документы')
        with allure.step('Нажать кнопку "Новая категория"'):
            page.create_new_company('Correct')
        with allure.step('Ввести в поле "Название" 8 символов'):
            page.input_field(0, title, '')
        with allure.step('Ввести в поле "Сокращенное название" 8 символов'):
            page.input_field(1, short_title, '')
        with allure.step('Выключить чекбокс "Используется ли категория"'):
            page.select_checkbox(0)
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_validators('Correct', '')
        with allure.step('Проверка записи в regulation_categories'):
            page.check_record_regulation_categories(title, short_title, active)
        with allure.step('Удалить ранее созданную категорию'):
            page.perform_an_action_on_an_document(title, 'remove', 'Correct')

    @allure.story('Позитивная проверка функциональности')
    def testing_the_addition_of_a_regulatory_document_category_with_limit_values(self, browser):
        title = random_en(255)
        short_title = random_en(255)
        active = 1
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (Precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Нормативные документы"'):
            page.open_window_setting('Нормативные документы')
        with allure.step('Нажать кнопку "Новая категория"'):
            page.create_new_company('Correct')
        with allure.step('Ввести в поле "Название" 8 символов'):
            page.input_field(0, title, '')
        with allure.step('Ввести в поле "Сокращенное название" 8 символов'):
            page.input_field(1, short_title, '')
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_validators('Correct', '')
        with allure.step('Проверка записи в regulation_categories'):
            page.check_record_regulation_categories(title, short_title, active)
        with allure.step('Удалить ранее созданную категорию'):
            page.perform_an_action_on_an_document(title, 'remove', 'Correct')

    @allure.story('Негативная проверка функциональности')
    def testing_the_addition_of_a_regulatory_document_category_with_exorbitant_values(self, browser):
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (Precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Нормативные документы"'):
            page.open_window_setting('Нормативные документы')
        with allure.step('Нажать кнопку "Новая категория"'):
            page.create_new_company('Correct')
        with allure.step('Проверка валидации поля "Название" на ввод запредельных значений'):
            with allure.step('Ввести в поле 256 символов'):
                page.input_field(0, random_en(256), '')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«название» не может превышать')
            with allure.step('Вернуть в исходное состояние поле "Название"'):
                page.input_field(0, random_en(8), 'clear')
        with allure.step('Проверка валидации поля "Сокращенное название" на ввод запредельных значений'):
            with allure.step('Ввести в поле 256 символов'):
                page.input_field(1, random_en(256), '')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«сокращенное название» не может превышать')
            with allure.step('Вернуть в исходное состояние поле "Название"'):
                page.input_field(1, random_en(8), 'clear')

    @allure.story('Негативная проверка функциональности')
    def testing_adding_a_regulatory_document_with_category_empty_values(self, browser):
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (Precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Нормативные документы"'):
            page.open_window_setting('Нормативные документы')
        with allure.step('Нажать кнопку "Новая категория"'):
            page.create_new_company('Correct')
        with allure.step('Проверка валидации обязательного поля "Название"'):
            with allure.step('Оставить поле пустым'):
                page.input_field(0, ' ', '')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«название» обязательно')
            with allure.step('Вернуть в исходное состояние поле "Название"'):
                page.input_field(0, random_en(8), 'clear')

    @allure.story('Негативная проверка функциональности')
    def testing_the_deletion_of_a_regulatory_document_category_linked_to_a_type(self, browser):
        title_category = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Добавить запись в regulation_categories'):
                SETTING().add_data_regulation_categories(title_category, '', True)
            with allure.step('Добавить запись в regulation_types, и привязать ранее созданную категорию'):
                SETTING().add_data_regulation_types(random_en(8), random_en(8), title_category, True)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (Precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Нормативные документы"'):
            page.open_window_setting('Нормативные документы')
        with allure.step('Удалить ранее созданную категорию'):
            page.perform_an_action_on_an_document(title_category, 'remove', 'Incorrect')

    @allure.story('Негативная проверка функциональности')
    def testing_the_functionality_of_adding_a_disabled_category_to_a_type(self, browser):
        title_category = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Добавить запись в regulation_categories'):
                SETTING().add_data_regulation_categories(title_category, title_category, False)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (Precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Нормативные документы"'):
            page.open_window_setting('Нормативные документы')
        with allure.step('Перейти в раздел "Типы нормативных документов"'):
            page.switch_the_section_of_the_regulatory_document('Типы')
        with allure.step('Нажать кнопку "Новый тип"'):
            page.create_new_company('Correct')
        with allure.step('Раскрыть селект "Категория"'):
            page.select_disable_categories_regulatory_document(title_category)

    @allure.story('Позитивная проверка функциональности')
    def testing_the_editing_of_a_previously_created_category_of_a_regulatory_document(self, browser):
        old_title = random_en(8)
        new_title = random_en(8)
        short_title = random_en(8)
        active = 1
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Добавить запись в regulation_categories'):
                SETTING().add_data_regulation_categories(old_title, random_en(8), False)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (Precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Нормативные документы"'):
            page.open_window_setting('Нормативные документы')
        with allure.step('Редактировать ранее созданную категорию'):
            page.perform_an_action_on_an_document(old_title, 'edit', 'Correct')
        with allure.step('Ввести в поле "Название" 8 символов'):
            page.input_field(0, new_title, 'clear')
        with allure.step('Ввести в поле "Сокращенное название" 8 символов'):
            page.input_field(1, short_title, 'clear')
        with allure.step('Включить чекбокс "Используется ли категория"'):
            page.select_checkbox(0)
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_validators('Correct', '')
        with allure.step('Проверка записи в regulation_categories'):
            page.check_record_regulation_categories(new_title, short_title, active)
        with allure.step('Удалить ранее созданную категорию'):
            page.perform_an_action_on_an_document(new_title, 'remove', 'Correct')

    @pytest.mark.smoke
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.story('Позитивная проверка функциональности')
    def testing_the_addition_of_a_regulatory_document_types_with_valid_data(self, browser):
        title_category = random_en(8)
        title_type = random_en(8)
        short_title_type = random_en(8)
        active = 0
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Добавить запись в regulation_categories'):
                SETTING().add_data_regulation_categories(title_category, '', True)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (Precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Нормативные документы"'):
            page.open_window_setting('Нормативные документы')
        with allure.step('Перейти в раздел "Типы нормативных документов"'):
            page.switch_the_section_of_the_regulatory_document('Типы')
        with allure.step('Нажать кнопку "Новая категория"'):
            page.create_new_company('Correct')
        with allure.step('Выбрать в селекте "Категория"'):
            regulation_category_id = SETTING().get_id_regulation_categories_by_title(title_category)
            page.select_categories_regulatory_document(regulation_category_id)
        with allure.step('Ввести в поле "Название" 8 символов'):
            page.input_field(0, title_type, '')
        with allure.step('Ввести в поле "Сокращенное название" 8 символов'):
            page.input_field(1, short_title_type, '')
        with allure.step('Выключить чекбокс "Используется ли тип"'):
            page.select_checkbox(0)
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_validators('Correct', '')
        with allure.step('Проверка записи в regulation_types'):
            page.check_record_regulation_types(regulation_category_id, title_type, short_title_type, active)
        with allure.step('Удалить ранее созданную категорию'):
            page.perform_an_action_on_an_document(title_category, 'remove', 'Correct')

    @allure.story('Позитивная проверка функциональности')
    def testing_the_addition_of_a_regulatory_document_types_with_limit_values(self, browser):
        title_category = random_en(255)
        title_type = random_en(255)
        short_title_type = random_en(255)
        active = 1
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Добавить запись в regulation_categories'):
                SETTING().add_data_regulation_categories(title_category, '', True)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (Precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Нормативные документы"'):
            page.open_window_setting('Нормативные документы')
        with allure.step('Перейти в раздел "Типы нормативных документов"'):
            page.switch_the_section_of_the_regulatory_document('Типы')
        with allure.step('Нажать кнопку "Новая категория"'):
            page.create_new_company('Correct')
        with allure.step('Выбрать в селекте "Категория"'):
            regulation_category_id = SETTING().get_id_regulation_categories_by_title(title_category)
            page.select_categories_regulatory_document(regulation_category_id)
        with allure.step('Ввести в поле "Название" 8 символов'):
            page.input_field(0, title_type, '')
        with allure.step('Ввести в поле "Сокращенное название" 8 символов'):
            page.input_field(1, short_title_type, '')
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_validators('Correct', '')
        with allure.step('Проверка записи в regulation_types'):
            page.check_record_regulation_types(regulation_category_id, title_type, short_title_type, active)
        with allure.step('Удалить ранее созданную категорию'):
            page.perform_an_action_on_an_document(title_category, 'remove', 'Correct')

    @allure.story('Негативная проверка функциональности')
    def testing_the_addition_of_a_regulatory_document_types_with_exorbitant_values(self, browser):
        title_category = random_en(255)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Добавить запись в regulation_categories'):
                SETTING().add_data_regulation_categories(title_category, '', True)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (Precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Нормативные документы"'):
            page.open_window_setting('Нормативные документы')
        with allure.step('Перейти в раздел "Типы нормативных документов"'):
            page.switch_the_section_of_the_regulatory_document('Типы')
        with allure.step('Нажать кнопку "Новая категория"'):
            page.create_new_company('Correct')
        with allure.step('Выбрать в селекте "Категория"'):
            regulation_category_id = SETTING().get_id_regulation_categories_by_title(title_category)
            page.select_categories_regulatory_document(regulation_category_id)
        with allure.step('Проверка валидации поля "Название" на ввод запредельных значений'):
            with allure.step('Ввести в поле 256 символов'):
                page.input_field(0, random_en(256), '')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«название» не может превышать')
            with allure.step('Вернуть в исходное состояние поле "Название"'):
                page.input_field(0, random_en(8), 'clear')
        with allure.step('Проверка валидации поля "Сокращенное название" на ввод запредельных значений'):
            with allure.step('Ввести в поле 256 символов'):
                page.input_field(1, random_en(256), '')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«сокращенное название» не может превышать')
            with allure.step('Вернуть в исходное состояние поле "Название"'):
                page.input_field(1, random_en(8), 'clear')

    @allure.story('Негативная проверка функциональности')
    def testing_adding_a_regulatory_document_with_types_empty_values(self, browser):
        title_category = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Добавить запись в regulation_categories'):
                SETTING().add_data_regulation_categories(title_category, '', True)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (Precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Нормативные документы"'):
            page.open_window_setting('Нормативные документы')
        with allure.step('Перейти в раздел "Типы нормативных документов"'):
            page.switch_the_section_of_the_regulatory_document('Типы')
        with allure.step('Нажать кнопку "Новая категория"'):
            page.create_new_company('Correct')
        with allure.step('Проверка валидации обязательного поля "Категория"'):
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«категория» обязательно')
            with allure.step('Вернуть поле в исходное состояние'):
                regulation_category_id = SETTING().get_id_regulation_categories_by_title(title_category)
                page.select_categories_regulatory_document(regulation_category_id)
        with allure.step('Проверка валидации обязательного поля "Название"'):
            with allure.step('Оставить поле пустым'):
                page.input_field(0, ' ', '')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«название» обязательно')
            with allure.step('Вернуть в исходное состояние поле "Название"'):
                page.input_field(0, random_en(8), 'clear')

    @allure.story('Негативная проверка функциональности')
    def testing_the_deletion_of_a_type_linked_to_a_regulatory_document(self, browser):
        title_category = random_en(8)
        title_type = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Добавить запись в regulation_categories'):
                SETTING().add_data_regulation_categories(title_category, '', True)
            with allure.step('Добавить запись в regulation_types, и привязать ранее созданную категорию'):
                SETTING().add_data_regulation_types(title_type, random_en(8), title_category, True)
            with allure.step('Добавить запись в regulations, и привязать к ранее созданному типу'):
                SETTING().add_data_regulations(random_en(8), random_en(8), title_type, random_en(8), random_en(8),
                                               True, EDUCATIONAL_ORGANIZATION, RECEPTION_COMPANY)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (Precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Нормативные документы"'):
            page.open_window_setting('Нормативные документы')
        with allure.step('Перейти в раздел "Типы нормативных документов"'):
            page.switch_the_section_of_the_regulatory_document('Типы')
        with allure.step('Удалить ранее созданную категорию'):
            page.perform_an_action_on_an_document(title_category, 'remove', 'Incorrect')

    @allure.story('Позитивная проверка функциональности')
    def testing_the_editing_of_a_previously_created_types_of_a_regulatory_document(self, browser):
        old_title_category = random_en(8)
        new_title_category = random_en(8)
        old_title_type = random_en(8)
        new_title_type = random_en(8)
        short_title = random_en(8)
        active = 1
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Добавить запись в regulation_categories'):
                SETTING().add_data_regulation_categories(old_title_category, '', True)
            with allure.step('Добавить вторую запись в regulation_categories'):
                SETTING().add_data_regulation_categories(new_title_category, '', True)
            with allure.step('Добавить запись в regulation_types, и привязать ранее созданную первую категорию'):
                SETTING().add_data_regulation_types(old_title_type, random_en(8), old_title_category, True)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (Precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Нормативные документы"'):
            page.open_window_setting('Нормативные документы')
        with allure.step('Перейти в раздел "Типы нормативных документов"'):
            page.switch_the_section_of_the_regulatory_document('Типы')
        with allure.step('Редактировать ранее созданный тип'):
            page.perform_an_action_on_an_document(old_title_category, 'edit', 'Correct')
        with allure.step('Выбрать в селекте "Категория"'):
            regulation_category_id = SETTING().get_id_regulation_categories_by_title(new_title_category)
            page.select_categories_regulatory_document(regulation_category_id)
        with allure.step('Ввести в поле "Название" 8 символов'):
            page.input_field(0, new_title_type, 'clear')
        with allure.step('Ввести в поле "Сокращенное название" 8 символов'):
            page.input_field(1, short_title, 'clear')
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_validators('Correct', '')
        with allure.step('Проверка записи в regulation_categories'):
            page.check_record_regulation_types(regulation_category_id, new_title_type, short_title, active)
        with allure.step('Удалить ранее созданный тип'):
            page.perform_an_action_on_an_document(new_title_category, 'remove', 'Correct')


@pytest.mark.abitur
@allure.link(link + '/application/eo-settings/org-units', name='ТАНДЕМ ЛК - Список ОО')
@allure.epic('Функциональное тестирование модуля "ABITUR"')
@allure.feature('Проверка функциональности раздела "Список ОО"')
class TestAbitur_ORG_UNITS:
    @staticmethod
    @pytest.mark.smoke
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.story('Позитивная проверка функциональности')
    def test_precondition_clear_previously_created_OO_RC_Systems():
        with allure.step('Удалить записи созданные в ПК (PRECONDITION) таблица online_entrant_requests'):
            ABITUR_DB().delete_record_online_entrant_requests(None)
        with allure.step('Удалить все записи в таблице attached_files'):
            ABITUR_DB().delete_all_attached_files()
        with allure.step('Удалить все записи в таблице external_org_contacts'):
            ABITUR_DB().delete_all_external_org_contacts()
        with allure.step('Удалить все записи в таблице enrollment_campaigns'):
            SETTING().delete_record_enrollment_campaigns(None)
        with allure.step('Удалить все записи в таблице uni_installations'):
            SETTING().delete_record_uni_installations(None)
        with allure.step('Удалить все записи в таблице external_org_units'):
            SETTING().delete_record_external_org_units(None)

    @pytest.mark.smoke
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.story('Негативная проверка функциональности')
    def testing_the_functionality_of_creating_a_second_organization_in_one_organization_mode(self, browser):
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Режим работы ЛКА - Одна организация'):
                SETTING().update_settings_app('is_multiple_org_units', '')
            with allure.step('Добавить ОО в external_org_units'):
                title_org1 = random_en(8)
                SETTING().add_educational_organization(title_org1, None)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Нажать кнопку "Новая организация"'):
            page.create_new_company('Incorrect')
        with allure.step('Удалить записи ранее созданных ОО в таблице external_org_units'):
            SETTING().delete_record_external_org_units(title_org1)

    @pytest.mark.smoke
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_creating_a_company_with_valid_data(self, browser):
        title = random_en(8)
        short_title = random_en(8)
        ur_address = random_en(8)
        fac_address = random_en(8)
        description = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Нажать кнопку "Новая организация"'):
            page.create_new_company('Correct')
        with allure.step('Загрузить логотип в формате .png'):
            page.change_logo_app('files/images/photo.png')
        with allure.step('Ввести в поле "Название" 8 символов'):
            page.input_field(0, title, '')
        with allure.step('Ввести в поле "Сокращенное название" 8 символов'):
            page.input_field(1, short_title, '')
        with allure.step('Ввести в поле "Юридический адрес" 8 символов'):
            page.input_field(3, ur_address, '')
        with allure.step('Ввести в поле "Фактический адрес" 8 символов'):
            page.input_field(4, fac_address, '')
        with allure.step('Ввести в поле "Описание" 8 символов'):
            page.input_description_org(description, '')
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_validators('Correct', '')
        with allure.step('Проверка записи в abitur.external_org_units'):
            page.check_record_org(title, short_title, None, ur_address, fac_address, description)
        with allure.step('Проверка записи логотипа в abitur.external_org_units'):
            page.check_record_logo_org(title, 'yes')
        with allure.step('Нажать кнопку "Назад"'):
            page.click_back_button()
        with allure.step('Удалить ранее созданную запись'):
            page.perform_an_action_on_an_organization(title, 'remove', 'Correct')

    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_creating_a_company_with_limit_values(self, browser):
        title = random_en(255)
        short_title = random_en(255)
        ur_address = random_en(255)
        fac_address = random_en(255)
        description = random_en(255)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Нажать кнопку "Новая организация"'):
            page.create_new_company('Correct')
        with allure.step('Загрузить логотип в формате .png'):
            page.change_logo_app('files/images/photo.png')
        with allure.step('Ввести в поле "Название" 255 символов'):
            page.input_field(0, title, '')
        with allure.step('Ввести в поле "Сокращенное название" 255 символов'):
            page.input_field(1, short_title, '')
        with allure.step('Ввести в поле "Юридический адрес" 255 символов'):
            page.input_field(3, ur_address, '')
        with allure.step('Ввести в поле "Фактический адрес" 255 символов'):
            page.input_field(4, fac_address, '')
        with allure.step('Ввести в поле "Описание" 255 символов'):
            page.input_description_org(description, '')
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_validators('Correct', '')
        with allure.step('Проверка записи в abitur.external_org_units'):
            page.check_record_org(title, short_title, None, ur_address, fac_address, description)
        with allure.step('Проверка записи логотипа в abitur.external_org_units'):
            page.check_record_logo_org(title, 'yes')
        with allure.step('Нажать кнопку "Назад"'):
            page.click_back_button()
        with allure.step('Удалить ранее созданную запись'):
            page.perform_an_action_on_an_organization(title, 'remove', 'Correct')

    @allure.story('Негативная проверка функциональности')
    def testing_the_functionality_of_creating_a_company_with_out_of_range_values(self, browser):
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Нажать кнопку "Новая организация"'):
            page.create_new_company('Correct')
        with allure.step('Проверка валидации поля "Название" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Название" 256 символов'):
                page.input_field(0, random_en(256), '')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«название» не может превышать')
            with allure.step('Вернуть в исходное состояние поле "Название"'):
                page.input_field(0, random_en(8), 'clear')
        with allure.step('Проверка валидации поля "Сокращенное название" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Сокращенное название" 256 символов'):
                page.input_field(1, random_en(256), '')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«сокращенное название» не может превышать')
            with allure.step('Вернуть в исходное состояние поле "Сокращенное название"'):
                page.input_field(1, random_en(8), 'clear')
        with allure.step('Проверка валидации поля "Юридический адрес" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Юридический адрес" 256 символов'):
                page.input_field(3, random_en(256), '')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«юридический адрес» не может превышать')
            with allure.step('Вернуть в исходное состояние поле "Юридический адрес"'):
                page.input_field(3, random_en(8), 'clear')
        with allure.step('Проверка валидации поля "Фактический адрес" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Фактический адрес" 256 символов'):
                page.input_field(4, random_en(256), '')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«фактический адрес» не может превышать')
            with allure.step('Вернуть в исходное состояние поле "Фактический адрес"'):
                page.input_field(4, random_en(8), 'clear')

    @allure.story('Негативная проверка функциональности')
    def testing_the_functionality_of_creating_a_company_without_filling_in_the_required_field(self, browser):
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Нажать кнопку "Новая организация"'):
            page.create_new_company('Correct')
        with allure.step('Проверка валидации обязательного поля "Название"'):
            with allure.step('Оставить поле "Название" пустым'):
                page.input_field(0, ' ', '')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«название» обязательно')
            with allure.step('Вернуть в исходное состояние поле "Название"'):
                page.input_field(0, random_en(8), 'clear')

    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_loading_an_organizations_logo_with_a_valid_file(self, browser):
        title = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Нажать кнопку "Новая организация"'):
            page.create_new_company('Correct')
        with allure.step('Ввести в поле "Название" 8 символов'):
            page.input_field(0, title, '')
        with allure.step('Проверка валидации на загрузку фотографии в формате .png'):
            with allure.step('Загрузить логотип в формате .png'):
                page.change_logo_app('files/images/photo.png')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Correct', '')
            with allure.step('Проверка записи логотипа в abitur.external_org_units'):
                page.check_record_logo_org(title, 'yes')
        with allure.step('Проверка валидации на загрузку фотографии в формате .jpg'):
            with allure.step('Загрузить логотип в формате .jpg'):
                page.change_logo_app('files/images/photo.jpg')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Correct', '')
            with allure.step('Проверка записи логотипа в abitur.external_org_units'):
                page.check_record_logo_org(title, 'yes')
        with allure.step('Проверка валидации на загрузку фотографии в формате .bmp'):
            with allure.step('Загрузить логотип в формате .bmp'):
                page.change_logo_app('files/images/photo.bmp')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Correct', '')
            with allure.step('Проверка записи логотипа в abitur.external_org_units'):
                page.check_record_logo_org(title, 'yes')
        with allure.step('Проверка валидации на загрузку фотографии в формате .gif'):
            with allure.step('Загрузить логотип в формате .gif'):
                page.change_logo_app('files/images/photo.gif')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Correct', '')
            with allure.step('Проверка записи логотипа в abitur.external_org_units'):
                page.check_record_logo_org(title, 'yes')
        with allure.step('Проверка валидации на загрузку фотографии в формате .jpeg'):
            with allure.step('Загрузить логотип в формате .jpeg'):
                page.change_logo_app('files/images/photo.jpeg')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Correct', '')
            with allure.step('Проверка записи логотипа в abitur.external_org_units'):
                page.check_record_logo_org(title, 'yes')
        with allure.step('Проверка валидации на загрузку фотографии в формате .webp'):
            with allure.step('Загрузить логотип в формате .webp'):
                page.change_logo_app('files/images/photo.webp')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Correct', '')
            with allure.step('Проверка записи логотипа в abitur.external_org_units'):
                page.check_record_logo_org(title, 'yes')
        with allure.step('Проверка валидации на загрузку фотографии в формате .tiff'):
            with allure.step('Загрузить логотип в формате .tiff'):
                page.change_logo_app('files/images/photo.tiff')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Correct', '')
            with allure.step('Проверка записи логотипа в abitur.external_org_units'):
                page.check_record_logo_org(title, 'yes')

    @allure.story('Негативная проверка функциональности')
    def testing_the_functionality_of_loading_an_organizations_logo_with_a_invalid_file(self, browser):
        title = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Нажать кнопку "Новая организация"'):
            page.create_new_company('Correct')
        with allure.step('Ввести в поле "Название" 8 символов'):
            page.input_field(0, title, '')
        with allure.step('Проверка валидации на загрузку файла в формате .ico'):
            with allure.step('Загрузить логотип в формате .ico'):
                page.change_logo_app('files/images/photo.ico')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«логотип» должно быть изображением')
            with allure.step('Проверка отсутствия записи логотипа в abitur.external_org_units'):
                page.check_record_logo_org(title, 'no')
        with allure.step('Проверка валидации на загрузку файла в формате .docx'):
            with allure.step('Загрузить логотип в формате .docx'):
                page.change_logo_app('files/doc/doc.docx')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«логотип» должно быть изображением')
            with allure.step('Проверка отсутствия записи логотипа в abitur.external_org_units'):
                page.check_record_logo_org(title, 'no')
        with allure.step('Проверка валидации на загрузку файла в формате .pdf'):
            with allure.step('Загрузить логотип в формате .pdf'):
                page.change_logo_app('files/doc/doc.pdf')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«логотип» должно быть изображением')
            with allure.step('Проверка отсутствия записи логотипа в abitur.external_org_units'):
                page.check_record_logo_org(title, 'no')
        with allure.step('Проверка валидации на загрузку файла в формате .xlsx'):
            with allure.step('Загрузить логотип в формате .xlsx'):
                page.change_logo_app('files/doc/doc.xlsx')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«логотип» должно быть изображением')
            with allure.step('Проверка отсутствия записи логотипа в abitur.external_org_units'):
                page.check_record_logo_org(title, 'no')
        with allure.step('Проверка валидации на загрузку файла в формате .txt'):
            with allure.step('Загрузить логотип в формате .txt'):
                page.change_logo_app('files/doc/doc.txt')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«логотип» должно быть изображением')
            with allure.step('Проверка отсутствия записи логотипа в abitur.external_org_units'):
                page.check_record_logo_org(title, 'no')

    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_deleting_a_previously_created_organization(self, browser):
        title = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить запись в external_org_units'):
                    SETTING().add_educational_organization(title, None)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Удалить ранее созданную запись'):
            page.perform_an_action_on_an_organization(title, 'remove', 'Correct')

    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_linking_and_deleting_a_subsidiary(self, browser):
        parent_title = random_en(8)
        title = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить запись в external_org_units'):
                    SETTING().add_educational_organization(parent_title, None)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Нажать кнопку "Новая организация"'):
            page.create_new_company('Correct')
        with allure.step('Ввести в поле "Название" 8 символов'):
            page.input_field(0, title, '')
        with allure.step('Ввести в поле "Родительская организация" ранее созданную ОО'):
            page.input_parent_organization(parent_title)
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_validators('Correct', '')
        with allure.step('Проверка привязки организации в abitur.external_org_units'):
            page.check_record_parent_org(title, parent_title)
        with allure.step('Удалить ранее привязанную родительскую организацию'):
            page.delete_parent_organization()
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_validators('Correct', '')
        with allure.step('Проверка отвязки организации в abitur.external_org_units'):
            page.check_record_parent_org(title, None)

    @allure.story('Позитивная проверка функциональности')
    def testing_the_editing_functionality_of_a_previously_created_educational_organization(self, browser):
        parent_title = random_en(8)
        title = random_en(8)
        short_title = random_en(8)
        ur_address = random_en(8)
        fac_address = random_en(8)
        description = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить запись в external_org_units'):
                    SETTING().add_educational_organization(parent_title, None)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Редактировать ранее созданную запись'):
            page.perform_an_action_on_an_organization(parent_title, 'edit', 'Correct')
        with allure.step('Загрузить логотип в формате .png'):
            page.change_logo_app('files/images/photo.png')
        with allure.step('Ввести в поле "Название" 8 символов'):
            page.input_field(0, title, 'clear')
        with allure.step('Ввести в поле "Сокращенное название" 8 символов'):
            page.input_field(1, short_title, 'clear')
        with allure.step('Ввести в поле "Юридический адрес" 8 символов'):
            page.input_field(3, ur_address, 'clear')
        with allure.step('Ввести в поле "Фактический адрес" 8 символов'):
            page.input_field(4, fac_address, 'clear')
        with allure.step('Ввести в поле "Описание" 8 символов'):
            page.input_description_org(description, 'clear')
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_validators('Correct', '')
        with allure.step('Проверка записи в abitur.external_org_units'):
            page.check_record_org(title, short_title, None, ur_address, fac_address, description)
        with allure.step('Проверка записи логотипа в abitur.external_org_units'):
            page.check_record_logo_org(title, 'yes')

    @allure.story('Негативная проверка функциональности')
    def testing_the_functionality_of_deleting_a_parent_organization_through_actions_in_the_list(self, browser):
        parent_title = random_en(8)
        title = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить в запись в external_org_units'):
                    SETTING().add_educational_organization(parent_title, None)
                with allure.step('Привязать ранее созданную организацию к новой "ОО" в external_org_units'):
                    SETTING().add_educational_organization(title, parent_title)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Удалить ранее созданную родительскую организацию'):
            page.perform_an_action_on_an_organization(parent_title, 'remove', 'Incorrect')

    @allure.story('Негативная проверка функциональности')
    def testing_validation_functionality_when_assigning_a_parent_as_a_child(self, browser):
        parent_title = random_en(8)
        title = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить запись в external_org_units'):
                    SETTING().add_educational_organization(parent_title, None)
                with allure.step('Привязать ранее созданную организацию к новой "ОО" в external_org_units'):
                    SETTING().add_educational_organization(title, parent_title)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Редактировать ранее созданную родительскую ОО'):
            page.perform_an_action_on_an_organization(parent_title, 'edit', 'Correct')
        with allure.step('Ввести в поле "Родительская организация" родителькую ОО'):
            page.input_parent_organization(title)
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_validators('Incorrect', '«родительская организация» ошибочно')

    @allure.story('Негативная проверка функциональности')
    def testing_the_functionality_of_creating_a_company_available_in_the_list(self, browser):
        title = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить запись в external_org_units'):
                    SETTING().add_educational_organization(title, None)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Нажать кнопку "Новая организация"'):
            page.create_new_company('Correct')
        with allure.step('Ввести в поле "Название" название ранее созданной организации'):
            page.input_field(0, title, '')
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_validators('Incorrect', '«название» уже существует')

    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_creating_a_company_by_filling_in_only_the_required_fields(self, browser):
        title = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Нажать кнопку "Новая организация"'):
            page.create_new_company('Correct')
        with allure.step('Ввести в поле "Название" 8 символов'):
            page.input_field(0, title, '')
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_validators('Correct', '')
        with allure.step('Проверка записи в abitur.external_org_units'):
            page.check_record_org(title, None, None, None, None, None)
        with allure.step('Нажать кнопку "Назад"'):
            page.click_back_button()
        with allure.step('Удалить ранее созданную запись'):
            page.perform_an_action_on_an_organization(title, 'remove', 'Correct')

    @allure.story('Негативная проверка функциональности')
    def testing_the_functionality_of_deleting_an_educational_organization_with_an_active_application(self, browser):
        title_org = random_en(8)
        system = random_en(8)
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        campaign_id = random_number(10)
        title_pk = random_en(8)
        email_abitur = random_en(8) + '@test.com'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
                with allure.step('Добавить ПК в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(campaign_id, title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
            with allure.step('Создать учетную запись поступающего с начатым заявлением'):
                with allure.step('Создать верефицированную учётную запись'):
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email_abitur)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_abitur, Roles.entrant)
                with allure.step('Создать запись о начале прохождения заполнения заявления'):
                    ABITUR_DB().add_online_entrant_requests(email_abitur, '{"step":1}', title_pk, 'pending')
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Удалить ранее созданную запись'):
            page.perform_an_action_on_an_organization(title_org, 'remove', 'Incorrect')

    @allure.story('Негативная проверка функциональности')
    def testing_the_deleting_an_educational_organization_with_an_associated_regulatory_doc(self, browser):
        title_org = random_en(8)
        system = random_en(8)
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        campaign_id = random_number(10)
        title_pk = random_en(8)
        title_category = random_en(8)
        title_type = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
                with allure.step('Добавить ПК в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(campaign_id, title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
                with allure.step('Добавить нормативный документ, и привязать к ранее созданной ПК'):
                    with allure.step('Добавить запись в regulation_categories'):
                        SETTING().add_data_regulation_categories(title_category, '', True)
                    with allure.step('Добавить запись в regulation_types, и привязать ранее созданную категорию'):
                        SETTING().add_data_regulation_types(title_type, random_en(8), title_category, True)
                    with allure.step('Добавить запись в regulations, и привязать к ранее созданному типу'):
                        SETTING().add_data_regulations(random_en(8), random_en(8), title_type, random_en(8),
                                                       random_en(8), True, title_org, title_pk)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Удалить ранее созданную запись'):
            page.perform_an_action_on_an_organization(title_org, 'remove', 'Incorrect')

    @allure.story('Негативная проверка функциональности')
    def testing_the_deletion_of_an_educational_organization_with_associated_deadlines(self, browser):
        title_org = random_en(8)
        system = random_en(8)
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        campaign_id = random_number(10)
        title_pk = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
                with allure.step('Добавить ПК в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(campaign_id, title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
            with allure.step('Добавить настройку сроков ПК, и привязать к ранее созданной ПК'):
                with allure.step('Добавить запись в accepting_document_deadlines'):
                    DATE = datetime.datetime(now.year, now.month, now.day, 0, 0)
                    SETTING().add_record_accepting_document_deadlines(title_pk, RequestType.BACH_SPEC,
                                                                      DocumentDeadlines.OCHNAYA,
                                                                      DocumentDeadlines.PO_DOGOVORU, DATE, DATE)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Удалить ранее созданную запись'):
            page.perform_an_action_on_an_organization(title_org, 'remove', 'Correct')

    @pytest.mark.smoke
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_adding_a_system_with_valid_data(self, browser):
        title = random_en(8)
        setting = 'Системы'
        url = 'http://test-trunk.tandemservice.ru'
        login = '1'
        passwords = '123456'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить запись в external_org_units'):
                    SETTING().add_educational_organization(title, None)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Редактировать ранее созданную ОО'):
            page.perform_an_action_on_an_organization(title, 'edit', 'Correct')
        with allure.step('Открыть раздел "Система"'):
            page.open_org_setting(setting)
        with allure.step('Нажать кнопку "+ Новая система"'):
            page.create_new_org_setting(setting)
        with allure.step('Ввести в поле "Адрес стенда" валидный URL'):
            page.input_field(0, url, '')
        with allure.step('Ввести в поле "Логин" валидный логин'):
            page.input_field(1, login, '')
        with allure.step('Ввести в поле "Пароль" валидный пароль'):
            page.input_field(2, passwords, '')
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_validators('Correct', '')
        with allure.step('Проверка записи в uni_installations'):
            page.check_record_uni(title, url, login, passwords, 1)

    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_adding_a_system_with_limit_values(self, browser):
        title = random_en(255)
        setting = 'Системы'
        url = 'http://test.com/' + random_en(239)
        login = random_en(255)
        passwords = random_en(255)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить запись в external_org_units'):
                    SETTING().add_educational_organization(title, None)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Редактировать ранее созданную ОО'):
            page.perform_an_action_on_an_organization(title, 'edit', 'Correct')
        with allure.step('Открыть раздел "Система"'):
            page.open_org_setting(setting)
        with allure.step('Нажать кнопку "+ Новая система"'):
            page.create_new_org_setting(setting)
        with allure.step('Ввести в поле "Адрес стенда" 255 символов'):
            page.input_field(0, url, '')
        with allure.step('Ввести в поле "Логин" 255 символов'):
            page.input_field(1, login, '')
        with allure.step('Ввести в поле "Пароль" 255 символов'):
            page.input_field(2, passwords, '')
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_validators('Correct', '')
        with allure.step('Проверка записи в uni_installations'):
            page.check_record_uni(title, url, login, passwords, 1)

    @allure.story('Негативная проверка функциональности')
    def testing_the_functionality_of_adding_a_system_with_out_of_range_values(self, browser):
        title = random_en(8)
        setting = 'Системы'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить запись в external_org_units'):
                    SETTING().add_educational_organization(title, None)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Редактировать ранее созданную ОО'):
            page.perform_an_action_on_an_organization(title, 'edit', 'Correct')
        with allure.step('Открыть раздел "Система"'):
            page.open_org_setting(setting)
        with allure.step('Нажать кнопку "+ Новая система"'):
            page.create_new_org_setting(setting)
        with allure.step('Проверка валидации поля "Адрес стенда" на ввод запредельных значений'):
            with allure.step('Ввести в поле 256 символов'):
                page.input_field(0, 'http://test.com/' + random_en(240), '')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«адрес стенда» не может превышать')
            with allure.step('Вернуть поле в исходное состояние'):
                page.input_field(0, 'http://test.com/', 'clear')
        with allure.step('Проверка валидации поля "Логин" на ввод запредельных значений'):
            with allure.step('Ввести в поле 256 символов'):
                page.input_field(1, random_en(256), '')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«логин» не может превышать')
            with allure.step('Вернуть поле в исходное состояние'):
                page.input_field(1, random_en(8), 'clear')
        with allure.step('Проверка валидации поля "Пароль" на ввод запредельных значений'):
            with allure.step('Ввести в поле 256 символов'):
                page.input_field(2, random_en(256), '')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«пароль» не может превышать')
            with allure.step('Вернуть поле в исходное состояние'):
                page.input_field(2, random_en(8), 'clear')

    @allure.story('Негативная проверка функциональности')
    def testing_the_functionality_of_adding_a_system_without_filling_in_the_required_field(self, browser):
        title = random_en(8)
        setting = 'Системы'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить запись в external_org_units'):
                    SETTING().add_educational_organization(title, None)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Редактировать ранее созданную ОО'):
            page.perform_an_action_on_an_organization(title, 'edit', 'Correct')
        with allure.step('Открыть раздел "Система"'):
            page.open_org_setting(setting)
        with allure.step('Нажать кнопку "+ Новая система"'):
            page.create_new_org_setting(setting)
        with allure.step('Проверка валидации обязательного поля "Адрес стенда"'):
            with allure.step('Оставить поле пустым'):
                page.input_field(0, ' ', '')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«адрес стенда» обязательно')
            with allure.step('Вернуть поле в исходное состояние'):
                page.input_field(0, 'http://test.com/', 'clear')
        with allure.step('Проверка валидации обязательного поля "Логин"'):
            with allure.step('Оставить поле пустым'):
                page.input_field(1, ' ', '')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«логин» обязательно')
            with allure.step('Вернуть поле в исходное состояние'):
                page.input_field(1, random_en(8), 'clear')
        with allure.step('Проверка валидации обязательного поля "Пароль"'):
            with allure.step('Оставить поле пустым'):
                page.input_field(2, ' ', '')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«пароль» обязательно')
            with allure.step('Вернуть поле в исходное состояние'):
                page.input_field(2, random_en(8), 'clear')

    @allure.story('Негативная проверка функциональности')
    def testing_the_functionality_of_adding_a_system_with_some_urls(self, browser):
        title = random_en(8)
        setting = 'Системы'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить запись в external_org_units'):
                    SETTING().add_educational_organization(title, None)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Редактировать ранее созданную ОО'):
            page.perform_an_action_on_an_organization(title, 'edit', 'Correct')
        with allure.step('Открыть раздел "Система"'):
            page.open_org_setting(setting)
        with allure.step('Нажать кнопку "+ Новая система"'):
            page.create_new_org_setting(setting)
        with allure.step('Проверка валидации поля "Адрес стенда" с некорректным адрессом'):
            with allure.step('Ввести в поле 8 символов'):
                page.input_field(0, random_en(8), '')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«адрес стенда» имеет ошибочный формат URL')
            with allure.step('Вернуть поле в исходное состояние'):
                page.input_field(0, 'http://test.com/', 'clear')

    @allure.story('Позитивная проверка функциональности')
    def testing_the_ping_functionality_in_a_previously_created_system(self, browser):
        title = random_en(8)
        setting = 'Системы'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить запись в external_org_units'):
                    SETTING().add_educational_organization(title, None)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Редактировать ранее созданную ОО'):
            page.perform_an_action_on_an_organization(title, 'edit', 'Correct')
        with allure.step('Открыть раздел "Система"'):
            page.open_org_setting(setting)
        with allure.step('Проверка добавления корректной системы'):
            url = 'http://test-trunk.tandemservice.ru'
            login = '1'
            passwords = '123456'
            with allure.step('Нажать кнопку "+ Новая система"'):
                page.create_new_org_setting(setting)
            with allure.step('Ввести в поле "Адрес стенда" валидный URL'):
                page.input_field(0, url, '')
            with allure.step('Ввести в поле "Логин" валидный логин'):
                page.input_field(1, login, '')
            with allure.step('Ввести в поле "Пароль" валидный пароль'):
                page.input_field(2, passwords, '')
            with allure.step('Нажать кнопку "Проверить подключение"'):
                page.click_check_connection('Correct')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Correct', '')
            with allure.step('Проверка записи в uni_installations'):
                page.check_record_uni(title, url, login, passwords, 1)
        with allure.step('Проверка добавления некорректной системы'):
            url_1 = 'http://test-trunk.tandemservice.com'
            login_1 = random_en(8)
            password_1 = random_en(8)
            with allure.step('Нажать кнопку "+ Новая система"'):
                page.create_new_org_setting(setting)
            with allure.step('Ввести в поле "Адрес стенда" валидный URL'):
                page.input_field(0, url_1, '')
            with allure.step('Ввести в поле "Логин" валидный логин'):
                page.input_field(1, login_1, '')
            with allure.step('Ввести в поле "Пароль" валидный пароль'):
                page.input_field(2, password_1, '')
            with allure.step('Нажать кнопку "Проверить подключение"'):
                page.click_check_connection('Incorrect')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Correct', '')
            with allure.step('Проверка записи в uni_installations'):
                page.check_record_uni(title, url_1, login_1, password_1, 0)

    @allure.story('Позитивная проверка функциональности')
    def testing_the_editing_functionality_of_a_previously_created_system(self, browser):
        title = random_en(8)
        url_before = random_en(8)
        setting = 'Системы'
        url = 'http://test-trunk.tandemservice.ru'
        login = '1'
        passwords = '123456'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить в external_org_units'):
                    SETTING().add_educational_organization(title, None)
                with allure.step('Добавить запись в uni_installations'):
                    SETTING().add_uni_installations(title, url_before, random_en(8), random_en(8), 1)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Редактировать ранее созданную ОО'):
            page.perform_an_action_on_an_organization(title, 'edit', 'Correct')
        with allure.step('Открыть раздел "Система"'):
            page.open_org_setting(setting)
        with allure.step('Редактировать ранее созданную систему'):
            page.perform_an_action_on_an_system(url_before, 'edit', 'Correct')
        with allure.step('Ввести в поле "Адрес стенда" валидный URL'):
            page.input_field(0, url, 'clear')
        with allure.step('Ввести в поле "Логин" валидный логин'):
            page.input_field(1, login, 'clear')
        with allure.step('Ввести в поле "Пароль" валидный пароль'):
            page.input_field(2, passwords, 'clear')
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_validators('Correct', '')
        with allure.step('Проверка записи в uni_installations'):
            page.check_record_uni(title, url, login, passwords, 1)

    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_deleting_a_previously_created_system(self, browser):
        title = random_en(8)
        url_before = random_en(8)
        setting = 'Системы'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить в external_org_units'):
                    SETTING().add_educational_organization(title, None)
                with allure.step('Добавить запись в uni_installations'):
                    SETTING().add_uni_installations(title, url_before, random_en(8), random_en(8), 1)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Редактировать ранее созданную ОО'):
            page.perform_an_action_on_an_organization(title, 'edit', 'Correct')
        with allure.step('Открыть раздел "Система"'):
            page.open_org_setting(setting)
        with allure.step('Удалить ранее созданную систему'):
            page.perform_an_action_on_an_system(url_before, 'remove', 'Correct')

    @allure.story('Негативная проверка функциональности')
    def testing_the_functionality_of_deleting_a_system_with_an_active_application(self, browser):
        title_org = random_en(8)
        system = random_en(8)
        setting = 'Системы'
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        campaign_id = random_number(10)
        title_pk = random_en(8)
        email_abitur = random_en(8) + '@test.com'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
                with allure.step('Добавить ПК в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(campaign_id, title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
            with allure.step('Создать учетную запись поступающего с начатым заявлением'):
                with allure.step('Создать верефицированную учётную запись'):
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email_abitur)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_abitur, Roles.entrant)
                with allure.step('Создать запись о начале прохождения заполнения заявления'):
                    ABITUR_DB().add_online_entrant_requests(email_abitur, '{"step":1}', title_pk, 'pending')
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Редактировать ранее созданную ОО'):
            page.perform_an_action_on_an_organization(title_org, 'edit', 'Correct')
        with allure.step('Открыть раздел "Системы"'):
            page.open_org_setting(setting)
        with allure.step('Удалить ранее созданную систему'):
            page.perform_an_action_on_an_system(system, 'remove', 'Incorrect')

    @pytest.mark.smoke
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_creating_a_receiving_company_with_valid_data(self, browser):
        title = random_en(8)
        setting = 'Приемные кампании'
        title_pk = random_en(8)
        original_pk = 'Бакалавриат-2018'
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить в external_org_units'):
                    SETTING().add_educational_organization(title, None)
                with allure.step('Добавить запись в uni_installations'):
                    SETTING().add_uni_installations(title, SYSTEM, LOGIN, PASSWORD, 1)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Редактировать ранее созданную ОО'):
            page.perform_an_action_on_an_organization(title, 'edit', 'Correct')
        with allure.step('Открыть раздел "Приемные кампании"'):
            page.open_org_setting(setting)
        with allure.step('Нажать кнопку "+ Новая ПК"'):
            page.create_new_org_setting(setting)
        with allure.step('Выбрать в поле "Приемная кампания" - Бакалавриат-2018'):
            page.select_field('Приемная кампания', original_pk)
        with allure.step('Ввести в поле "Название, выводимое в интерфейсе" 8 символов'):
            page.input_field(-1, title_pk, 'clear')
        with allure.step('Выбрать в поле "Тип" - Прием на обучение по программам среднего...'):
            page.select_field('Тип', type_pk)
        with allure.step('Выбрать в поле "Система" - ранее созданную систему'):
            page.select_field('Система', SYSTEM)
        with allure.step('Выключить чекбокс "Используется ли приемная кампания"'):
            page.select_checkbox(0)
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_validators('Correct', '')
        with allure.step('Проверка записи в enrollment_campaigns'):
            page.check_record_enrollment_campaigns(title, type_pk, SYSTEM, title_pk, original_pk)
        with allure.step('Проверка записи {disabled_at} в enrollment_campaigns'):
            page.check_record_enrollment_campaigns_disabled_at(title, title_pk, 'inactive')
        with allure.step('Нажать кнопку "Назад"'):
            page.click_back_button()
        with allure.step('Удалить ранее созданную ОО'):
            page.perform_an_action_on_an_organization(title, 'remove', 'Correct')

    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_creating_a_receiving_company_with_limit_value(self, browser):
        title = random_en(255)
        setting = 'Приемные кампании'
        title_pk = random_en(255)
        original_pk = 'Бакалавриат-2018'
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить в external_org_units'):
                    SETTING().add_educational_organization(title, None)
                with allure.step('Добавить запись в uni_installations'):
                    SETTING().add_uni_installations(title, SYSTEM, LOGIN, PASSWORD, 1)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Редактировать ранее созданную ОО'):
            page.perform_an_action_on_an_organization(title, 'edit', 'Correct')
        with allure.step('Открыть раздел "Приемные кампании"'):
            page.open_org_setting(setting)
        with allure.step('Нажать кнопку "+ Новая ПК"'):
            page.create_new_org_setting(setting)
        with allure.step('Выбрать в поле "Приемная кампания" - Бакалавриат-2018'):
            page.select_field('Приемная кампания', original_pk)
        with allure.step('Ввести в поле "Название, выводимое в интерфейсе" 8 символов'):
            page.input_field(-1, title_pk, 'clear')
        with allure.step('Выбрать в поле "Тип" - Прием на обучение по программам среднего...'):
            page.select_field('Тип', type_pk)
        with allure.step('Выбрать в поле "Система" - ранее созданную систему'):
            page.select_field('Система', SYSTEM)
        with allure.step('Выключить чекбокс "Используется ли приемная кампания"'):
            page.select_checkbox(0)
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_validators('Correct', '')
        with allure.step('Проверка записи в enrollment_campaigns'):
            page.check_record_enrollment_campaigns(title, type_pk, SYSTEM, title_pk, original_pk)
        with allure.step('Проверка записи {disabled_at} в enrollment_campaigns'):
            page.check_record_enrollment_campaigns_disabled_at(title, title_pk, 'inactive')
        with allure.step('Нажать кнопку "Назад"'):
            page.click_back_button()
        with allure.step('Удалить ранее созданную ОО'):
            page.perform_an_action_on_an_organization(title, 'remove', 'Correct')

    @allure.story('Негативная проверка функциональности')
    def testing_the_functionality_of_creating_a_receiving_company_with_out_of_range_values(self, browser):
        title = random_en(8)
        setting = 'Приемные кампании'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить в external_org_units'):
                    SETTING().add_educational_organization(title, None)
                with allure.step('Добавить запись в uni_installations'):
                    SETTING().add_uni_installations(title, SYSTEM, LOGIN, PASSWORD, 1)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Редактировать ранее созданную ОО'):
            page.perform_an_action_on_an_organization(title, 'edit', 'Correct')
        with allure.step('Открыть раздел "Приемные кампании"'):
            page.open_org_setting(setting)
        with allure.step('Нажать кнопку "+ Новая ПК"'):
            page.create_new_org_setting(setting)
        with allure.step('Проверка валидации поля "Название, выводимое в интерфейсе" на ввод запредельных значений'):
            with allure.step('Ввести в поле 256 символов'):
                page.input_field(-1, random_en(256), 'clear')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«название» не может превышать')
            with allure.step('Вернуть поле в исходное состояние'):
                page.input_field(-1, random_en(8), 'clear')
        with allure.step('Нажать кнопку "Назад"'):
            page.click_back_button()
        with allure.step('Нажать кнопку "Назад"'):
            page.click_back_button()
        with allure.step('Удалить ранее созданную ОО'):
            page.perform_an_action_on_an_organization(title, 'remove', 'Correct')

    @allure.story('Негативная проверка функциональности')
    def testing_the_functionality_of_creating_a_receiving_company_without_filling_in_the_required_field(self, browser):
        title = random_en(8)
        setting = 'Приемные кампании'
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить в external_org_units'):
                    SETTING().add_educational_organization(title, None)
                with allure.step('Добавить запись в uni_installations'):
                    SETTING().add_uni_installations(title, SYSTEM, LOGIN, PASSWORD, 1)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Редактировать ранее созданную ОО'):
            page.perform_an_action_on_an_organization(title, 'edit', 'Correct')
        with allure.step('Открыть раздел "Приемные кампании"'):
            page.open_org_setting(setting)
        with allure.step('Нажать кнопку "+ Новая ПК"'):
            page.create_new_org_setting(setting)
        with allure.step('Проверка валидации обязательного поля "Название, выводимое в интерфейсе"'):
            with allure.step('Оставить поле пустым'):
                page.input_field(-1, ' ', 'clear')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«название» обязательно')
            with allure.step('Вернуть поле в исходное состояние'):
                page.input_field(-1, random_en(8), 'clear')
        with allure.step('Проверка валидации обязательного поля "Тип"'):
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«тип» обязательно')
            with allure.step('Вернуть поле в исходное состояние'):
                page.select_field('Тип', type_pk)
        with allure.step('Нажать кнопку "Назад"'):
            page.click_back_button()
        with allure.step('Нажать кнопку "Назад"'):
            page.click_back_button()
        with allure.step('Удалить ранее созданную ОО'):
            page.perform_an_action_on_an_organization(title, 'remove', 'Correct')

    @allure.story('Позитивная проверка функциональности')
    def testing_the_editing_functionality_of_a_previously_created_receiving_company(self, browser):
        title_org = random_en(8)
        setting = 'Приемные кампании'
        campaign_id = '1591104433361522877'
        title_pk = random_en(8)
        new_title_pk = random_en(8)
        original_pk = random_en(8)
        new_type_pk = 'Прием на обучение по программам магистратуры'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить запись в uni_installations'):
                    SETTING().add_uni_installations(title_org, SYSTEM, LOGIN, PASSWORD, 1)
                with allure.step('Добавить запись в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(campaign_id, title_org, title_pk, original_pk,
                                                       'Прием на обучение по программам среднего профессионального '
                                                       'образования', SYSTEM, 'active')
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Редактировать ранее созданную ОО'):
            page.perform_an_action_on_an_organization(title_org, 'edit', 'Correct')
        with allure.step('Открыть раздел "Приемные кампании"'):
            page.open_org_setting(setting)
        with allure.step('Редактировать ранее созданную систему'):
            page.perform_an_action_on_an_pk(title_pk, 'edit', 'Correct')
        with allure.step('Ввести в поле "Название, выводимое в интерфейсе" 8 символов'):
            page.input_field(-1, new_title_pk, 'clear')
        with allure.step('Выбрать в поле "Тип" - Прием на обучение по программам магистратуры'):
            page.select_field('Тип(Редактирование)', new_type_pk)
        with allure.step('Выключить чекбокс "Используется ли приемная кампания"'):
            page.select_checkbox(0)
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_validators('Correct', '')
        with allure.step('Проверка записи в enrollment_campaigns'):
            page.check_record_enrollment_campaigns(title_org, new_type_pk, SYSTEM, new_title_pk, original_pk)
        with allure.step('Проверка записи {disabled_at} в enrollment_campaigns'):
            page.check_record_enrollment_campaigns_disabled_at(title_org, new_title_pk, 'inactive')
        with allure.step('Нажать кнопку "Назад"'):
            page.click_back_button()
        with allure.step('Удалить ранее созданную ОО'):
            page.perform_an_action_on_an_organization(title_org, 'remove', 'Correct')

    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_deleting_a_receiving_company(self, browser):
        title_org = random_en(8)
        system = random_en(8)
        setting = 'Приемные кампании'
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        campaign_id = random_number(10)
        title_pk = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить запись в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
                with allure.step('Добавить запись в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(campaign_id, title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Редактировать ранее созданную ОО'):
            page.perform_an_action_on_an_organization(title_org, 'edit', 'Correct')
        with allure.step('Открыть раздел "Приемные кампании"'):
            page.open_org_setting(setting)
        with allure.step('Удалить ранее созданную ПК'):
            page.perform_an_action_on_an_pk(title_pk, 'remove', 'Correct')

    @allure.story('Негативная проверка функциональности')
    def testing_the_functionality_of_deleting_a_receiving_company_with_an_active_application(self, browser):
        title_org = random_en(8)
        system = random_en(8)
        setting = 'Приемные кампании'
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        campaign_id = random_number(10)
        title_pk = random_en(8)
        email_abitur = random_en(8) + '@test.com'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
                with allure.step('Добавить ПК в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(campaign_id, title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
            with allure.step('Создать учетную запись поступающего с начатым заявлением'):
                with allure.step('Создать верефицированную учётную запись'):
                    USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email_abitur)
                with allure.step('Назначить роль "Поступающий"'):
                    ROLES().assign_role_to_user(email_abitur, Roles.entrant)
                with allure.step('Создать запись о начале прохождения заполнения заявления'):
                    ABITUR_DB().add_online_entrant_requests(email_abitur, '{"step":1}', title_pk, 'pending')
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Редактировать ранее созданную ОО'):
            page.perform_an_action_on_an_organization(title_org, 'edit', 'Correct')
        with allure.step('Открыть раздел "Приемные кампании"'):
            page.open_org_setting(setting)
        with allure.step('Удалить ранее созданную ПК'):
            page.perform_an_action_on_an_pk(title_pk, 'remove', 'Incorrect')

    @allure.story('Негативная проверка функциональности')
    def testing_the_removal_of_the_reception_company_with_an_associated_regulatory_document(self, browser):
        title_org = random_en(8)
        system = random_en(8)
        setting = 'Приемные кампании'
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        campaign_id = random_number(10)
        title_pk = random_en(8)
        title_category = random_en(8)
        title_type = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
                with allure.step('Добавить ПК в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(campaign_id, title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
            with allure.step('Добавить нормативный документ, и привязать к ранее созданной ПК'):
                with allure.step('Добавить запись в regulation_categories'):
                    SETTING().add_data_regulation_categories(title_category, '', True)
                with allure.step('Добавить запись в regulation_types, и привязать ранее созданную категорию'):
                    SETTING().add_data_regulation_types(title_type, random_en(8), title_category, True)
                with allure.step('Добавить запись в regulations, и привязать к ранее созданному типу'):
                    SETTING().add_data_regulations(random_en(8), random_en(8), title_type, random_en(8),
                                                   random_en(8), True, title_org, title_pk)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Редактировать ранее созданную ОО'):
            page.perform_an_action_on_an_organization(title_org, 'edit', 'Correct')
        with allure.step('Открыть раздел "Приемные кампании"'):
            page.open_org_setting(setting)
        with allure.step('Удалить ранее созданную ПК'):
            page.perform_an_action_on_an_pk(title_pk, 'remove', 'Incorrect')

    @allure.story('Позитивная проверка функциональности')
    def testing_the_removal_of_the_reception_company_with_linked_time_settings(self, browser):
        title_org = random_en(8)
        system = random_en(8)
        setting = 'Приемные кампании'
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        campaign_id = random_number(10)
        title_pk = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
                with allure.step('Добавить ПК в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(campaign_id, title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
            with allure.step('Добавить настройку сроков ПК, и привязать к ранее созданной ПК'):
                with allure.step('Добавить запись в accepting_document_deadlines'):
                    DATE = datetime.datetime(now.year, now.month, now.day, 0, 0)
                    SETTING().add_record_accepting_document_deadlines(title_pk, RequestType.BACH_SPEC,
                                                                      DocumentDeadlines.OCHNAYA,
                                                                      DocumentDeadlines.PO_DOGOVORU, DATE, DATE)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Редактировать ранее созданную ОО'):
            page.perform_an_action_on_an_organization(title_org, 'edit', 'Correct')
        with allure.step('Открыть раздел "Приемные кампании"'):
            page.open_org_setting(setting)
        with allure.step('Удалить ранее созданную ПК'):
            page.perform_an_action_on_an_pk(title_pk, 'remove', 'Correct')

    @allure.story('Негативная проверка функциональности')
    def testing_the_functionality_of_adding_a_receiving_company_without_a_system(self, browser):
        title = random_en(8)
        setting = 'Приемные кампании'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить в external_org_units'):
                    SETTING().add_educational_organization(title, None)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Редактировать ранее созданную ОО'):
            page.perform_an_action_on_an_organization(title, 'edit', 'Correct')
        with allure.step('Открыть раздел "Приемные кампании"'):
            page.open_org_setting(setting)
        with allure.step('Нажать кнопку "+ Новая ПК"'):
            page.checking_the_stub_at_creation(setting, 'Система отсутствует')

    @allure.story('Негативная проверка функциональности')
    def testing_the_functionality_of_adding_a_receiving_company_with_a_non_working_system(self, browser):
        title = random_en(8)
        setting = 'Приемные кампании'
        system = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить в external_org_units'):
                    SETTING().add_educational_organization(title, None)
                with allure.step('Добавить запись в uni_installations'):
                    SETTING().add_uni_installations(title, system, random_en(8), random_en(8), 1)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Редактировать ранее созданную ОО'):
            page.perform_an_action_on_an_organization(title, 'edit', 'Correct')
        with allure.step('Открыть раздел "Приемные кампании"'):
            page.open_org_setting(setting)
        with allure.step('Нажать кнопку "+ Новая ПК"'):
            page.checking_the_stub_at_creation(setting, 'Система недоступна')

    #################################################
    @pytest.mark.smoke
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_adding_RC_contacts_in_the_general_settings_section(self, browser):
        setting = 'Контакты'
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        title_org = random_en(8)
        system = random_en(8)
        campaign_id = random_number(10)
        # Общие настройки
        title_pk = random_en(8)
        address = random_en(8)
        description = random_en(8)
        phone = random_number(10)
        faxes = random_number(8)
        email_pk = random_en(8) + '@test-pk.com'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
                with allure.step('Добавить ПК в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(campaign_id, title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Редактировать ранее созданную ОО'):
            page.perform_an_action_on_an_organization(title_org, 'edit', 'Correct')
        with allure.step('Открыть раздел "Контакты"'):
            page.open_org_setting(setting)
        with allure.step('Нажать кнопку "+ Новые контакты"'):
            page.create_new_org_setting(setting)
        with allure.step('Выбрать в поле "Приемная кампания" - ранее созданную ПК'):
            page.select_field('Приемная кампания', title_pk)
        with allure.step('Ввести в поле "Адрес" 8 символов'):
            page.input_field(0, address, '')
        with allure.step('Ввести в поле "Как проехать" 8 символов'):
            page.input_description_org(description, '')
        with allure.step('Ввести в поле "Телефон" 10 цифр'):
            page.input_field(1, phone, '')
        with allure.step('Ввести в поле "Факс" 8 цифр'):
            page.input_field(2, faxes, '')
        with allure.step('Ввести в поле "Эл. почта" валидный адрес'):
            page.input_field(3, email_pk, '')
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_validators('Correct', '')
        with allure.step('Проверка записи "Общие настройки" в external_org_contacts'):
            page.check_record_general_external_org_contacts(title_org, title_pk, address,
                                                            description, phone, faxes, email_pk)

    @pytest.mark.smoke
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_adding_RC_contacts_in_the_contact_persons_section(self, browser):
        setting = 'Контакты'
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        title_org = random_en(8)
        system = random_en(8)
        campaign_id = random_number(10)
        # Общие настройки
        title_pk = random_en(8)
        # Ответственный секретарь
        name_sec = random_en(8)
        phone_sec = random_number(10)
        email_sec = random_en(8) + '@test-sec.com'
        position_sec = random_en(8)
        office_sec = random_number(3)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
                with allure.step('Добавить ПК в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(campaign_id, title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Редактировать ранее созданную ОО'):
            page.perform_an_action_on_an_organization(title_org, 'edit', 'Correct')
        with allure.step('Открыть раздел "Контакты"'):
            page.open_org_setting(setting)
        with allure.step('Нажать кнопку "+ Новые контакты"'):
            page.create_new_org_setting(setting)
        with allure.step('Выбрать в поле "Приемная кампания" - ранее созданную ПК'):
            page.select_field('Приемная кампания', title_pk)
        with allure.step('Ввести в поле "Имя" 8 символов'):
            page.input_field(4, name_sec, '')
        with allure.step('Ввести в поле "Телефон" 10 цифр'):
            page.input_field(5, phone_sec, '')
        with allure.step('Ввести в поле "Эл. почта" валидный адрес'):
            page.input_field(6, email_sec, '')
        with allure.step('Ввести в поле "Должность" 8 символов'):
            page.input_field(7, position_sec, '')
        with allure.step('Ввести в поле "Кабинет" 3 цифры'):
            page.input_field(8, office_sec, '')
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_validators('Correct', '')
        with allure.step('Проверка записи "Ответственный секретарь" в external_org_contacts'):
            page.check_record_executive_external_org_contacts(title_org, title_pk, name_sec,
                                                              email_sec, phone_sec, office_sec, position_sec)

    @pytest.mark.smoke
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_adding_pc_contacts_in_the_work_schedule_section(self, browser):
        setting = 'Контакты'
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        title_org = random_en(8)
        system = random_en(8)
        campaign_id = random_number(10)
        # Общие настройки####################
        title_pk = random_en(255)  # Селект приёмной компании
        #  График работы приемной комиссии
        with_time = '08:30'
        by_time = '17:30'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
                with allure.step('Добавить ПК в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(campaign_id, title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Редактировать ранее созданную ОО'):
            page.perform_an_action_on_an_organization(title_org, 'edit', 'Correct')
        with allure.step('Открыть раздел "Контакты"'):
            page.open_org_setting(setting)
        with allure.step('Нажать кнопку "+ Новые контакты"'):
            page.create_new_org_setting(setting)
        with allure.step('Выбрать в поле "Приемная кампания" - ранее созданную ПК'):
            page.select_field('Приемная кампания', title_pk)
        with allure.step('Ввести в поле "Пн - с" - 09:00'):
            page.input_field(9, with_time, '')
        with allure.step('Ввести в поле "Пн - до" - 18:00'):
            page.input_field(10, by_time, '')
        with allure.step('Ввести в поле "Вт - с" - 09:00'):
            page.input_field(11, with_time, '')
        with allure.step('Ввести в поле "Вт - до" - 18:00'):
            page.input_field(12, by_time, '')
        with allure.step('Ввести в поле "Ср - с" - 09:00'):
            page.input_field(13, with_time, '')
        with allure.step('Ввести в поле "Ср - до" - 18:00'):
            page.input_field(14, by_time, '')
        with allure.step('Ввести в поле "Чт - с" - 09:00'):
            page.input_field(15, with_time, '')
        with allure.step('Ввести в поле "Чт - до" - 18:00'):
            page.input_field(16, by_time, '')
        with allure.step('Ввести в поле "Пт - с" - 09:00'):
            page.input_field(17, with_time, '')
        with allure.step('Ввести в поле "Пт - до" - 18:00'):
            page.input_field(18, by_time, '')
        with allure.step('Ввести в поле "Сб - с" - 09:00'):
            page.input_field(19, with_time, '')
        with allure.step('Ввести в поле "Сб - до" - 18:00'):
            page.input_field(20, by_time, '')
        with allure.step('Ввести в поле "Вс - с" - 09:00'):
            page.input_field(21, with_time, '')
        with allure.step('Ввести в поле "Вс - до" - 18:00'):
            page.input_field(22, by_time, '')
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_validators('Correct', '')
        with allure.step('Проверка записи "График работы приемной комиссии" в external_org_contacts'):
            with allure.step('Понедельник'):
                page.check_record_working_hours_external_org_contacts(title_org, title_pk, with_time, by_time, 0)
            with allure.step('Вторник'):
                page.check_record_working_hours_external_org_contacts(title_org, title_pk, with_time, by_time, 1)
            with allure.step('Среда'):
                page.check_record_working_hours_external_org_contacts(title_org, title_pk, with_time, by_time, 2)
            with allure.step('Четверг'):
                page.check_record_working_hours_external_org_contacts(title_org, title_pk, with_time, by_time, 3)
            with allure.step('Пятница'):
                page.check_record_working_hours_external_org_contacts(title_org, title_pk, with_time, by_time, 4)
            with allure.step('Суббота'):
                page.check_record_working_hours_external_org_contacts(title_org, title_pk, with_time, by_time, 5)
            with allure.step('Воскресенье'):
                page.check_record_working_hours_external_org_contacts(title_org, title_pk, with_time, by_time, 6)

    @pytest.mark.skip(reason='https://tandemservice.atlassian.net/browse/LK-165')
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_adding_contacts_with_valid_data(self, browser):
        setting = 'Контакты'
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        title_org = random_en(8)
        system = random_en(8)
        campaign_id = random_number(10)
        # Общие настройки####################
        title_pk = random_en(8)  # Селект приёмной компании
        address = random_en(8)  # Поле Адрес
        description = random_en(8)
        phone = random_number(10)
        faxes = random_number(8)
        email_pk = random_en(8) + '@test-pk.com'
        # Ответственный секретарь#############
        name_sec = random_en(8)
        phone_sec = random_number(10)
        email_sec = random_en(8) + '@test-sec.com'
        office_sec = random_number(3)
        #  График работы приемной комиссии ###
        with_time = '09:00'
        by_time = '18:00'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
                with allure.step('Добавить ПК в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(campaign_id, title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Редактировать ранее созданную ОО'):
            page.perform_an_action_on_an_organization(title_org, 'edit', 'Correct')
        with allure.step('Открыть раздел "Контакты"'):
            page.open_org_setting(setting)
        with allure.step('Нажать кнопку "+ Новые контакты"'):
            page.create_new_org_setting(setting)
        with allure.step('Выбрать в поле "Приемная кампания" - ранее созданную ПК'):
            page.select_field('Приемная кампания', title_pk)
        with allure.step('Ввести в поле "Адрес" 8 символов'):
            page.input_field(0, address, '')
        with allure.step('Ввести в поле "Как проехать" 8 символов'):
            page.input_description_org(description, '')
        with allure.step('Ввести в поле "Телефон" 10 цифр'):
            page.input_field(1, phone, '')
        with allure.step('Ввести в поле "Факс" 8 цифр'):
            page.input_field(2, faxes, '')
        with allure.step('Ввести в поле "Эл. почта" валидный адрес'):
            page.input_field(3, email_pk, '')
        with allure.step('Ввести в поле "Имя" 8 символов'):
            page.input_field(4, name_sec, '')
        with allure.step('Ввести в поле "Телефон" 10 цифр'):
            page.input_field(5, phone_sec, '')
        with allure.step('Ввести в поле "Эл. почта секретаря" валидный адрес'):
            page.input_field(6, email_sec, '')
        with allure.step('Ввести в поле "Кабинет" 3 цифры'):
            page.input_field(7, office_sec, '')
        with allure.step('Ввести в поле "Пн - с" - 09:00'):
            page.input_field(14, with_time, '')
        with allure.step('Ввести в поле "Пн - до" - 18:00'):
            page.input_field(15, by_time, '')
        with allure.step('Ввести в поле "Вт - с" - 09:00'):
            page.input_field(16, with_time, '')
        with allure.step('Ввести в поле "Вт - до" - 18:00'):
            page.input_field(17, by_time, '')
        with allure.step('Ввести в поле "Ср - с" - 09:00'):
            page.input_field(18, with_time, '')
        with allure.step('Ввести в поле "Ср - до" - 18:00'):
            page.input_field(19, by_time, '')
        with allure.step('Ввести в поле "Чт - с" - 09:00'):
            page.input_field(20, with_time, '')
        with allure.step('Ввести в поле "Чт - до" - 18:00'):
            page.input_field(21, by_time, '')
        with allure.step('Ввести в поле "Пт - с" - 09:00'):
            page.input_field(22, with_time, '')
        with allure.step('Ввести в поле "Пт - до" - 18:00'):
            page.input_field(23, by_time, '')
        with allure.step('Ввести в поле "Сб - с" - 09:00'):
            page.input_field(24, with_time, '')
        with allure.step('Ввести в поле "Сб - до" - 18:00'):
            page.input_field(25, by_time, '')
        with allure.step('Ввести в поле "Вс - с" - 09:00'):
            page.input_field(26, with_time, '')
        with allure.step('Ввести в поле "Вс - до" - 18:00'):
            page.input_field(27, by_time, '')
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_validators('Correct', '')
        with allure.step('Проверка записи "Общие настройки" в external_org_contacts'):
            page.check_record_general_external_org_contacts(title_org, title_pk, address,
                                                            description, phone, faxes, email_pk)

    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_adding_RC_contacts_in_the_general_setting_section_with_limit_values(self, browser):
        setting = 'Контакты'
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        title_org = random_en(8)
        system = random_en(8)
        campaign_id = random_number(10)
        # Общие настройки
        title_pk = random_en(255)
        address = random_en(255)
        description = random_en(255)
        phone = random_number(255)
        faxes = random_number(255)
        email_pk = random_en(243) + '@test-pk.com'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
                with allure.step('Добавить ПК в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(campaign_id, title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Редактировать ранее созданную ОО'):
            page.perform_an_action_on_an_organization(title_org, 'edit', 'Correct')
        with allure.step('Открыть раздел "Контакты"'):
            page.open_org_setting(setting)
        with allure.step('Нажать кнопку "+ Новые контакты"'):
            page.create_new_org_setting(setting)
        with allure.step('Выбрать в поле "Приемная кампания" - ранее созданную ПК'):
            page.select_field('Приемная кампания', title_pk)
        with allure.step('Ввести в поле "Адрес" 8 символов'):
            page.input_field(0, address, '')
        with allure.step('Ввести в поле "Как проехать" 8 символов'):
            page.input_description_org(description, '')
        with allure.step('Ввести в поле "Телефон" 10 цифр'):
            page.input_field(1, phone, '')
        with allure.step('Ввести в поле "Факс" 8 цифр'):
            page.input_field(2, faxes, '')
        with allure.step('Ввести в поле "Эл. почта" валидный адрес'):
            page.input_field(3, email_pk, '')
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_validators('Correct', '')
        with allure.step('Проверка записи "Общие настройки" в external_org_contacts'):
            page.check_record_general_external_org_contacts(title_org, title_pk, address,
                                                            description, phone, faxes, email_pk)

    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_adding_RC_contacts_in_the_contact_persons_section_with_limit_values(self, browser):
        setting = 'Контакты'
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        title_org = random_en(8)
        system = random_en(8)
        campaign_id = random_number(10)
        # Общие настройки
        title_pk = random_en(8)
        # Ответственный секретарь
        name_sec = random_en(255)
        phone_sec = random_number(255)
        email_sec = random_en(242) + '@test-sec.com'
        office_sec = random_number(255)
        position_sec = random_en(255)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
                with allure.step('Добавить ПК в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(campaign_id, title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Редактировать ранее созданную ОО'):
            page.perform_an_action_on_an_organization(title_org, 'edit', 'Correct')
        with allure.step('Открыть раздел "Контакты"'):
            page.open_org_setting(setting)
        with allure.step('Нажать кнопку "+ Новые контакты"'):
            page.create_new_org_setting(setting)
        with allure.step('Выбрать в поле "Приемная кампания" - ранее созданную ПК'):
            page.select_field('Приемная кампания', title_pk)
        with allure.step('Ввести в поле "Имя" 8 символов'):
            page.input_field(4, name_sec, '')
        with allure.step('Ввести в поле "Телефон" 10 цифр'):
            page.input_field(5, phone_sec, '')
        with allure.step('Ввести в поле "Эл. почта" валидный адрес'):
            page.input_field(6, email_sec, '')
        with allure.step('Ввести в поле "Должность" 8 символов'):
            page.input_field(7, position_sec, '')
        with allure.step('Ввести в поле "Кабинет" 3 цифры'):
            page.input_field(8, office_sec, '')
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_validators('Correct', '')
        with allure.step('Проверка записи "Ответственный секретарь" в external_org_contacts'):
            page.check_record_executive_external_org_contacts(title_org, title_pk, name_sec,
                                                              email_sec, phone_sec, office_sec, position_sec)

    @allure.story('Негативная проверка функциональности')
    def testing_the_functionality_of_adding_contacts_with_an_outrageous_value(self, browser):
        setting = 'Контакты'
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        title_org = random_en(8)
        title_pk = random_en(8)
        system = random_en(8)
        campaign_id = random_number(10)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
                with allure.step('Добавить ПК в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(campaign_id, title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Редактировать ранее созданную ОО'):
            page.perform_an_action_on_an_organization(title_org, 'edit', 'Correct')
        with allure.step('Открыть раздел "Контакты"'):
            page.open_org_setting(setting)
        with allure.step('Нажать кнопку "+ Новые контакты"'):
            page.create_new_org_setting(setting)
        with allure.step('Проверка валидации поля "Адрес" на ввод запредельных значений'):
            with allure.step('Ввести в поле 256 символов'):
                page.input_field(0, random_en(256), '')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«адрес» не может превышать')
            with allure.step('Вернуть поле в исходное состояние'):
                page.input_field(0, ' ', 'clear')
        with allure.step('Проверка валидации поля "Телефон" на ввод запредельных значений'):
            with allure.step('Ввести в поле 256 символов'):
                page.input_field(1, random_en(256), '')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«телефон» не может превышать')
            with allure.step('Вернуть поле в исходное состояние'):
                page.input_field(2, ' ', 'clear')
        with allure.step('Проверка валидации поля "Факс" на ввод запредельных значений'):
            with allure.step('Ввести в поле 256 символов'):
                page.input_field(3, random_en(256), '')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«факс» не может превышать')
            with allure.step('Вернуть поле в исходное состояние'):
                page.input_field(4, ' ', 'clear')
        with allure.step('Проверка валидации поля "Эл. почта" на ввод запредельных значений'):
            with allure.step('Ввести в поле 256 символов'):
                page.input_field(5, random_en(256), '')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«эл. почта» должно быть действительным ')
            with allure.step('Вернуть поле в исходное состояние'):
                page.input_field(6, ' ', 'clear')
        with allure.step('Проверка валидации поля "Имя" на ввод запредельных значений'):
            with allure.step('Ввести в поле 256 символов'):
                page.input_field(7, random_en(256), '')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«имя» не может превышать')
            with allure.step('Вернуть поле в исходное состояние'):
                page.input_field(12, random_en(8), 'clear')
        with allure.step('Проверка валидации поля "Телефон" на ввод запредельных значений'):
            with allure.step('Ввести в поле 256 символов'):
                page.input_field(13, random_en(256), '')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«телефон» не может превышать')
            with allure.step('Вернуть поле в исходное состояние'):
                page.input_field(13, ' ', 'clear')
        with allure.step('Проверка валидации поля "Эл. почта" на ввод запредельных значений'):
            with allure.step('Ввести в поле 256 символов'):
                page.input_field(14, random_en(256), '')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«эл. почта» должно быть действительным')
            with allure.step('Вернуть поле в исходное состояние'):
                page.input_field(14, ' ', 'clear')
        with allure.step('Проверка валидации поля "Должность" на ввод запредельных значений'):
            with allure.step('Ввести в поле 256 символов'):
                page.input_field(15, random_en(256), '')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«должность» не может превышать')
            with allure.step('Вернуть поле в исходное состояние'):
                page.input_field(15, random_en(8), 'clear')
        with allure.step('Проверка валидации поля "Кабинет" на ввод запредельных значений'):
            with allure.step('Ввести в поле 256 символов'):
                page.input_field(16, random_en(256), '')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«кабинет» не может превышать')
            with allure.step('Вернуть поле в исходное состояние'):
                page.input_field(16, ' ', 'clear')

    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_editing_previously_created_contact_data(self, browser):
        setting = 'Контакты'
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        title_org = random_en(8)
        title_pk = random_en(8)
        system = random_en(8)
        campaign_id = random_number(10)
        # Общие настройки
        address = random_en(8)
        description = random_en(8)
        phone = random_number(10)
        faxes = random_number(8)
        email_pk = random_en(8) + '@test-pk.com'
        # Ответственный секретарь
        name_sec = random_en(8)
        phone_sec = random_number(10)
        email_sec = random_en(8) + '@test-sec.com'
        office_sec = random_number(3)
        position_sec = random_en(8)
        #  График работы приемной комиссии
        with_time = '09:00'
        by_time = '18:00'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
                with allure.step('Добавить ПК в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(campaign_id, title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
                with allure.step('Добавить контактные данные ПК в external_org_contacts'):
                    SETTING().add_data_external_org_contacts(title_org, title_pk)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Редактировать ранее созданную ОО'):
            page.perform_an_action_on_an_organization(title_org, 'edit', 'Correct')
        with allure.step('Открыть раздел "Контакты"'):
            page.open_org_setting(setting)
        with allure.step('Редактировать ранее созданные контактные данные'):
            page.perform_an_action_on_an_contact(title_pk, 'edit', 'Correct')
        with allure.step('Ввести в поле "Адрес" 8 символов'):
            page.input_field(0, address, 'clear')
        with allure.step('Ввести в поле "Как проехать" 8 символов'):
            page.input_description_org(description, 'clear')
        with allure.step('Ввести в поле "Телефон" 10 цифр'):
            page.input_field(2, phone, 'clear')
        with allure.step('Ввести в поле "Факс" 8 цифр'):
            page.input_field(4, faxes, 'clear')
        with allure.step('Ввести в поле "Эл. почта" валидный адрес'):
            page.input_field(6, email_pk, 'clear')
        with allure.step('Ввести в поле "Имя" 8 символов'):
            page.input_field(12, name_sec, '')
        with allure.step('Ввести в поле "Телефон" 10 цифр'):
            page.input_field(13, phone_sec, '')
        with allure.step('Ввести в поле "Эл. почта секретаря" валидный адрес'):
            page.input_field(14, email_sec, '')
        with allure.step('Ввести в поле "Должность" валидный адрес'):
            page.input_field(15, position_sec, '')
        with allure.step('Ввести в поле "Кабинет" 3 цифры'):
            page.input_field(16, office_sec, '')
        with allure.step('Ввести в поле "Пн - с" - 09:00'):
            page.input_field(17, with_time, '')
        with allure.step('Ввести в поле "Пн - до" - 18:00'):
            page.input_field(18, by_time, '')
        with allure.step('Ввести в поле "Вт - с" - 09:00'):
            page.input_field(19, with_time, '')
        with allure.step('Ввести в поле "Вт - до" - 18:00'):
            page.input_field(20, by_time, '')
        with allure.step('Ввести в поле "Ср - с" - 09:00'):
            page.input_field(21, with_time, '')
        with allure.step('Ввести в поле "Ср - до" - 18:00'):
            page.input_field(22, by_time, '')
        with allure.step('Ввести в поле "Чт - с" - 09:00'):
            page.input_field(23, with_time, '')
        with allure.step('Ввести в поле "Чт - до" - 18:00'):
            page.input_field(24, by_time, '')
        with allure.step('Ввести в поле "Пт - с" - 09:00'):
            page.input_field(25, with_time, '')
        with allure.step('Ввести в поле "Пт - до" - 18:00'):
            page.input_field(26, by_time, '')
        with allure.step('Ввести в поле "Сб - с" - 09:00'):
            page.input_field(27, with_time, '')
        with allure.step('Ввести в поле "Сб - до" - 18:00'):
            page.input_field(28, by_time, '')
        with allure.step('Ввести в поле "Вс - с" - 09:00'):
            page.input_field(29, with_time, '')
        with allure.step('Ввести в поле "Вс - до" - 18:00'):
            page.input_field(30, by_time, '')
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_validators('Correct', '')
        with allure.step('Проверка записи "Общие настройки" в external_org_contacts'):
            page.check_record_general_external_org_contacts(title_org, title_pk, address,
                                                            description, phone, faxes, email_pk)
        with allure.step('Проверка записи "Ответственный секретарь" в external_org_contacts'):
            page.check_record_executive_external_org_contacts(title_org, title_pk, name_sec,
                                                              email_sec, phone_sec, office_sec, position_sec)
        with allure.step('Проверка записи "График работы приемной комиссии" в external_org_contacts'):
            with allure.step('Понедельник'):
                page.check_record_working_hours_external_org_contacts(title_org, title_pk, with_time, by_time, 0)
            with allure.step('Вторник'):
                page.check_record_working_hours_external_org_contacts(title_org, title_pk, with_time, by_time, 1)
            with allure.step('Среда'):
                page.check_record_working_hours_external_org_contacts(title_org, title_pk, with_time, by_time, 2)
            with allure.step('Четверг'):
                page.check_record_working_hours_external_org_contacts(title_org, title_pk, with_time, by_time, 3)
            with allure.step('Пятница'):
                page.check_record_working_hours_external_org_contacts(title_org, title_pk, with_time, by_time, 4)
            with allure.step('Суббота'):
                page.check_record_working_hours_external_org_contacts(title_org, title_pk, with_time, by_time, 5)
            with allure.step('Воскресенье'):
                page.check_record_working_hours_external_org_contacts(title_org, title_pk, with_time, by_time, 6)

    @allure.story('Позитивная проверка функциональности')
    def testing_the_functionality_of_deleting_previously_created_contact_data(self, browser):
        setting = 'Контакты'
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        title_org = random_en(8)
        title_pk = random_en(8)
        system = random_en(8)
        campaign_id = random_number(10)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
                with allure.step('Добавить ПК в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(campaign_id, title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
                with allure.step('Добавить контактные данные ПК в external_org_contacts'):
                    SETTING().add_data_external_org_contacts(title_org, title_pk)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Редактировать ранее созданную ОО'):
            page.perform_an_action_on_an_organization(title_org, 'edit', 'Correct')
        with allure.step('Открыть раздел "Контакты"'):
            page.open_org_setting(setting)
        with allure.step('Удалить ранее созданные контактные данные'):
            page.perform_an_action_on_an_contact(title_pk, 'remove', 'Correct')

    @allure.story('Негативная проверка функциональности')
    def testing_the_functionality_of_adding_contacts_without_a_receiving_company(self, browser):
        setting = 'Контакты'
        title_org = random_en(8)
        system = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
            with allure.step('Режим работы ЛКА - Несколько организаций'):
                SETTING().update_settings_app('is_multiple_org_units', '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Список ОО"'):
            page.open_window_setting('Список ОО')
        with allure.step('Редактировать ранее созданную ОО'):
            page.perform_an_action_on_an_organization(title_org, 'edit', 'Correct')
        with allure.step('Открыть раздел "Контакты"'):
            page.open_org_setting(setting)
        with allure.step('Нажать кнопку "+ Новые контакты"'):
            page.checking_the_stub_at_creation(setting, '')


@pytest.mark.abitur
@allure.link(link + '/application/settings/', name='ТАНДЕМ ЛК - Приемная кампания')
@allure.epic('Функциональное тестирование модуля "ABITUR"')
@allure.feature('Проверка функциональности раздела "Приемная кампания"')
@allure.severity(allure.severity_level.CRITICAL)
class TestAbitur_APPLICATION_SETTING:
    def testing_of_the_RC_settings_basic_education_levels(self, browser):
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        title_org = random_en(8)
        title_pk = random_en(8)
        system = random_en(8)
        campaign_id = random_number(10)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
                with allure.step('Добавить ПК в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(campaign_id, title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Приемная кампания"'):
            page.open_window_setting('Приемная кампания')
        with allure.step('Выбрать ранее созданную ОО'):
            page.select_field('Настройки ОО', title_org)
        with allure.step('Выбрать ранее созданную ПК'):
            page.select_field('Настройки ПК', title_pk)
        with allure.step('Открыть настройки "Базовые уровни образования"'):
            page.open_setting_pk('EduLevel')
        with allure.step('Включить чекбокс "Основное общее образование (9 классов)"'):
            page.select_checkbox_setting_EduLevel('BASIC_GENERAL')
        with allure.step('Включить чекбокс "Основное общее образование (9 классов)"'):
            page.select_checkbox_setting_EduLevel('SECONDARY_GENERAL')
        with allure.step('Включить чекбокс "Основное общее образование (9 классов)"'):
            page.select_checkbox_setting_EduLevel('SECONDARY_VOCATIONAL')
        with allure.step('Включить чекбокс "Основное общее образование (9 классов)"'):
            page.select_checkbox_setting_EduLevel('UNDERGRADUATE')
        with allure.step('Включить чекбокс "Основное общее образование (9 классов)"'):
            page.select_checkbox_setting_EduLevel('MASTER')
        with allure.step('Включить чекбокс "Основное общее образование (9 классов)"'):
            page.select_checkbox_setting_EduLevel('HIGHEST_QUALIFICATION')
        with allure.step('Проверка записи настроек в settings.basic_education_levels'):
            page.check_records_EduLevel(title_pk, EduLevel.BASIC_GENERAL, EduLevel.SECONDARY_GENERAL,
                                        EduLevel.SECONDARY_VOCATIONAL, EduLevel.UNDERGRADUATE,
                                        EduLevel.MASTER, EduLevel.HIGHEST_QUALIFICATION)

    def testing_of_the_RC_settings_implemented_education_levels(self, browser):
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        title_org = random_en(8)
        title_pk = random_en(8)
        system = random_en(8)
        campaign_id = random_number(10)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
                with allure.step('Добавить ПК в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(campaign_id, title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Приемная кампания"'):
            page.open_window_setting('Приемная кампания')
        with allure.step('Выбрать ранее созданную ОО'):
            page.select_field('Настройки ОО', title_org)
        with allure.step('Выбрать ранее созданную ПК'):
            page.select_field('Настройки ПК', title_pk)
        with allure.step('Открыть настройки "Реализуемые уровни образования"'):
            page.open_setting_pk('RequestType')
        with allure.step('Включить чекбокс "Среднее профессиональное образовани"'):
            page.select_checkbox_setting_RequestType('SPO')
        with allure.step('Включить чекбокс "Программы бакалавриата, специалитета"'):
            page.select_checkbox_setting_RequestType('BACH_SPEC')
        with allure.step('Включить чекбокс "Программы магистратуры"'):
            page.select_checkbox_setting_RequestType('MASTER')
        with allure.step('Включить чекбокс "Программы аспирантуры (адъюнктуры)"'):
            page.select_checkbox_setting_RequestType('POSTGRADUATE')
        with allure.step('Включить чекбокс "Программы ординатуры"'):
            page.select_checkbox_setting_RequestType('RESIDENCY')
        with allure.step('Проверка записи настроек в settings.implemented_education_levels'):
            page.check_records_RequestType(title_pk, RequestType.SPO, RequestType.BACH_SPEC, RequestType.MASTER,
                                           RequestType.POSTGRADUATE, RequestType.RESIDENCY)

    def testing_of_the_setting_up_a_RC_scan_copy_of_documents(self, browser):
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        title_org = random_en(8)
        title_pk = random_en(8)
        system = random_en(8)
        campaign_id = random_number(10)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
                with allure.step('Добавить ПК в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(campaign_id, title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Приемная кампания"'):
            page.open_window_setting('Приемная кампания')
        with allure.step('Выбрать ранее созданную ОО'):
            page.select_field('Настройки ОО', title_org)
        with allure.step('Выбрать ранее созданную ПК'):
            page.select_field('Настройки ПК', title_pk)
        with allure.step('Открыть настройки "Скан-копии документов"'):
            page.open_setting_pk('ScanCopies')
        with allure.step('Установить тип документа "Удостоверение личности" в "Прикладывается обязательно'):
            IDENTITY_CARD = ScanCopies.DOCUMENT_REQUIRED
            page.radiobutton_setting_ScanCopies(ScanCopies.IDENTITY_CARD, IDENTITY_CARD, '')
        with allure.step('Установить тип документа "Документ об образовании" в "Не прикладывается'):
            EDUCATION_DOCUMENT = ScanCopies.DOCUMENT_NOT_REQUIRED
            page.radiobutton_setting_ScanCopies(ScanCopies.EDUCATION_DOCUMENT, EDUCATION_DOCUMENT, '')
        with allure.step('Установить тип документа "СНИЛС" в "Прикладывается обязательно'):
            SNILS = ScanCopies.DOCUMENT_REQUIRED
            page.radiobutton_setting_ScanCopies(ScanCopies.SNILS, SNILS, '')
        with allure.step('Установить тип документа "Индивидуальные достижения" в "Прикладывается обязательно'):
            INDIVIDUAL_ACHIEVEMENTS = ScanCopies.DOCUMENT_REQUIRED
            page.radiobutton_setting_ScanCopies(ScanCopies.INDIVIDUAL_ACHIEVEMENTS, INDIVIDUAL_ACHIEVEMENTS, '')
        with allure.step('Установить тип документа "Подтверждающий инвалидность" в "Прикладывается обязательно'):
            DOCUMENT_PROVING_DISABILITY = ScanCopies.DOCUMENT_REQUIRED
            page.radiobutton_setting_ScanCopies(ScanCopies.DOCUMENT_PROVING_DISABILITY, DOCUMENT_PROVING_DISABILITY, '')
        with allure.step('Установить тип документа "Договор о целевом обучении" в "Прикладывается обязательно'):
            TARGETED_TRAINING_CONTRACT = ScanCopies.DOCUMENT_REQUIRED
            page.radiobutton_setting_ScanCopies(ScanCopies.TARGETED_TRAINING_CONTRACT, TARGETED_TRAINING_CONTRACT, '')
        with allure.step('Установить тип документа "Льгота для заселения" в "Не прикладывается'):
            BENEFIT_FOR_DORM = ScanCopies.DOCUMENT_NOT_REQUIRED
            page.radiobutton_setting_ScanCopies(ScanCopies.BENEFIT_FOR_DORM, BENEFIT_FOR_DORM, '')
        with allure.step('Установить тип документа "Согласие на обработку" в "Прикладывается обязательно'):
            CONSENT_TO_PERSONAL_DATA = ScanCopies.DOCUMENT_REQUIRED
            page.radiobutton_setting_ScanCopies(ScanCopies.CONSENT_TO_PERSONAL_DATA, CONSENT_TO_PERSONAL_DATA, '')
        with allure.step('Установить тип документа "Заявление" в "Прикладывается обязательно'):
            APPLICATION = ScanCopies.DOCUMENT_REQUIRED
            page.radiobutton_setting_ScanCopies(ScanCopies.APPLICATION, APPLICATION, '')
        with allure.step('Установить тип документа "Согласие на зачисление" в "Прикладывается обязательно'):
            CONSENT_TO_ENROLLMENT = ScanCopies.DOCUMENT_REQUIRED
            page.radiobutton_setting_ScanCopies(ScanCopies.CONSENT_TO_ENROLLMENT, CONSENT_TO_ENROLLMENT, '')
        with allure.step('Ввести в поле "Размер файла" 8 цифр'):
            FILE_SIZE = random_number(8)
            page.radiobutton_setting_ScanCopies(ScanCopies.DOCUMENT_SIZE, 'Correct', FILE_SIZE)
        with allure.step('Установить тип документа "Для удостоверения личности" в "Одно поле'):
            IDENTITY_CARD_FIELDS_COUNT = ScanCopies.DOCUMENT_REQUIRED
            page.radiobutton_setting_ScanCopies(ScanCopies.IDENTITY_CARD_FIELDS_COUNT, IDENTITY_CARD_FIELDS_COUNT, '')
        with allure.step('Установить тип документа "Для удостоверения личности" в "Одно поле'):
            EDUCATION_DOCUMENT_FIELDS_COUNT = ScanCopies.DOCUMENT_REQUIRED
            page.radiobutton_setting_ScanCopies(ScanCopies.EDUCATION_DOCUMENT_FIELDS_COUNT,
                                                EDUCATION_DOCUMENT_FIELDS_COUNT, '')
        with allure.step('Проверка записи настроек в settings.implemented_education_levels'):
            page.check_records_ScanCopies(title_pk, IDENTITY_CARD, EDUCATION_DOCUMENT, SNILS,
                                          INDIVIDUAL_ACHIEVEMENTS, DOCUMENT_PROVING_DISABILITY,
                                          TARGETED_TRAINING_CONTRACT, BENEFIT_FOR_DORM, CONSENT_TO_PERSONAL_DATA,
                                          APPLICATION, CONSENT_TO_ENROLLMENT, FILE_SIZE, IDENTITY_CARD_FIELDS_COUNT,
                                          EDUCATION_DOCUMENT_FIELDS_COUNT)

    @pytest.mark.skip(reason='Тест-кейс отложен из-за трудоёмкости')
    def testing_of_the_RC_settings_the_order_of_the_NPCs_in_the_lists(self, browser):
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        title_org = 'Тестовая NPCs'
        title_pk = 'Тестовая NPCs'
        system = 'http://192.168.201.5:8080'
        campaign_id = "1565636525956272317"
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, 'tandem', '123456', 1)
                with allure.step('Добавить ПК в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(campaign_id, title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Приемная кампания"'):
            page.open_window_setting('Приемная кампания')
        with allure.step('Выбрать ранее созданную ОО'):
            page.select_field('Настройки ОО', title_org)
        with allure.step('Выбрать ранее созданную ПК'):
            page.select_field('Настройки ПК', title_pk)
        with allure.step('Открыть настройки "Порядок следования НПС в списках"'):
            page.open_setting_pk('OrderOfSpecialties')
        with allure.step('Указазать "08.03.01 Строительство" позицию 6'):
            Construction = '6'  # Позиция
            page.select_position_OrderOfSpecialties(OrderOfSpecialties.Construction_id, Construction)
        with allure.step('Указазать "09.03.01 Информатика и вычислительная техника" позицию 5'):
            Informatics = '5'  # Позиция
            page.select_position_OrderOfSpecialties(OrderOfSpecialties.Informatics_id, Informatics)
        with allure.step('Указазать "12.03.01 Приборостроение" позицию 4'):
            Instrumentation = '4'  # Позиция
            page.select_position_OrderOfSpecialties(OrderOfSpecialties.Instrumentation_id, Instrumentation)
        with allure.step('Указазать "21.03.02 Землеустройство и кадастры" позицию 3'):
            Land_management = '3'  # Позиция
            page.select_position_OrderOfSpecialties(OrderOfSpecialties.Land_management_id, Land_management)
        with allure.step('Указазать "38.03.01 Экономика" позицию 2'):
            Economy = '2'  # Позиция
            page.select_position_OrderOfSpecialties(OrderOfSpecialties.Economy_id, Economy)
        with allure.step('Указазать "38.03.06 Торговое дело" позицию 1'):
            Commercial_business = '1'  # Позиция
            page.select_position_OrderOfSpecialties(OrderOfSpecialties.Commercial_business_id, Commercial_business)
        with allure.step('Нажать кнопку "Применить расстановку"'):
            page.check_validation_OrderOfSpecialties('Correct', '')
        with allure.step('Проверка записи настроек в settings.order_of_specialties'):
            page.check_records_position_OrderOfSpecialties(title_pk, Construction, Informatics, Instrumentation,
                                                           Land_management, Economy, Commercial_business)
        with allure.step('Нажать кнопку "Вернуться к расстановке по умолчанию"'):
            page.return_default_OrderOfSpecialties()
        with allure.step('Проверка записи настроек в settings.order_of_specialties'):
            page.check_records_default_position_OrderOfSpecialties(title_pk)

    def testing_of_the_RC_settings_step_first(self, browser):
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        title_org = random_en(8)
        title_pk = random_en(8)
        system = random_en(8)
        campaign_id = random_number(10)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
                with allure.step('Добавить ПК в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(campaign_id, title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Приемная кампания"'):
            page.open_window_setting('Приемная кампания')
        with allure.step('Выбрать ранее созданную ОО'):
            page.select_field('Настройки ОО', title_org)
        with allure.step('Выбрать ранее созданную ПК'):
            page.select_field('Настройки ПК', title_pk)
        with allure.step('Открыть настройки "Шаг 1 (Основные данные)"'):
            page.open_setting_pk('FirstStep')
        with allure.step('Установить тип поля "Способе возврата" в "Не заполняется'):
            RETURN_WAY_INFO = FirstStep.RETURN_WAY_INFO_NO
            page.select_radiobutton_FirstStep(FirstStep.RETURN_WAY_INFO, RETURN_WAY_INFO)
        with allure.step('Установить тип поля "Ввод балла и года сдачи предмета ЕГЭ" в "Не осуществляется'):
            EXAM_SCORES_AND_EXAM_YEARS = FirstStep.EXAM_SCORES_AND_EXAM_YEARS_NO
            page.select_radiobutton_FirstStep(FirstStep.EXAM_SCORES_AND_EXAM_YEARS, EXAM_SCORES_AND_EXAM_YEARS)
        with allure.step('Установить тип поля "Проверять балл ЕГЭ" в "Да, запрещать'):
            CHECK_EXAM_SCORES = FirstStep.CHECK_EXAM_SCORES_DISALLOW
            page.select_radiobutton_FirstStep(FirstStep.CHECK_EXAM_SCORES, CHECK_EXAM_SCORES)
        with allure.step('Установить тип поля "Флаг о допуске" в "Не доступен'):
            PARTICIPATION_IN_TESTS = FirstStep.PARTICIPATION_IN_TESTS_NOT_AVAILABLE
            page.select_radiobutton_FirstStep(FirstStep.PARTICIPATION_IN_TESTS, PARTICIPATION_IN_TESTS)
        with allure.step('Установить тип поля "Выбор категории лиц" в "Не осуществляется'):
            BASED_ON_UNIVERSITY_MATERIALS = FirstStep.BASED_ON_UNIVERSITY_MATERIALS_NO
            page.select_radiobutton_FirstStep(FirstStep.BASED_ON_UNIVERSITY_MATERIALS, BASED_ON_UNIVERSITY_MATERIALS)
        with allure.step('Установить тип поля "Выбор вступительных испытаний" в "Не осуществляется'):
            SELECTING_TESTS = FirstStep.SELECTING_TESTS_NO
            page.select_radiobutton_FirstStep(FirstStep.SELECTING_TESTS, SELECTING_TESTS)
        with allure.step('Установить тип поля "Информация о необходимости" в "Не заполняется'):
            NEED_FOR_SPECIAL_CONDITIONS_INFO = FirstStep.NEED_FOR_SPECIAL_CONDITIONS_INFO_NO
            page.select_radiobutton_FirstStep(FirstStep.NEED_FOR_SPECIAL_CONDITIONS_INFO,
                                              NEED_FOR_SPECIAL_CONDITIONS_INFO)
        with allure.step('Установить тип поля "Информация о предпочитаемой форме сдачи ВИ" в "Не заполняется'):
            PREFERRED_FORM_INFO = FirstStep.PREFERRED_FORM_INFO_NO
            page.select_radiobutton_FirstStep(FirstStep.PREFERRED_FORM_INFO, PREFERRED_FORM_INFO)
        with allure.step('Установить тип поля "Информация об участии" в "Не заполняется'):
            PARTICIPATION_IN_OLYMPIADS_INFO = FirstStep.PARTICIPATION_IN_OLYMPIADS_INFO_NO
            page.select_radiobutton_FirstStep(FirstStep.PARTICIPATION_IN_OLYMPIADS_INFO,
                                              PARTICIPATION_IN_OLYMPIADS_INFO)
        with allure.step('Установить тип поля "Информация об особых правах" в "Не заполняется'):
            SPECIAL_RIGHTS_INFO = FirstStep.SPECIAL_RIGHTS_INFO_NO
            page.select_radiobutton_FirstStep(FirstStep.SPECIAL_RIGHTS_INFO, SPECIAL_RIGHTS_INFO)
        with allure.step('Установить тип поля "Информация о заключении" в "Не заполняется'):
            CONCLUDED_AGREEMENT_INFO = FirstStep.CONCLUDED_AGREEMENT_INFO_NO
            page.select_radiobutton_FirstStep(FirstStep.CONCLUDED_AGREEMENT_INFO, CONCLUDED_AGREEMENT_INFO)
        with allure.step('Проверка записи настроек в settings.abitur_first_step'):
            page.check_records_FirstStep(title_pk, RETURN_WAY_INFO, EXAM_SCORES_AND_EXAM_YEARS, CHECK_EXAM_SCORES,
                                         PARTICIPATION_IN_TESTS, BASED_ON_UNIVERSITY_MATERIALS, SELECTING_TESTS,
                                         NEED_FOR_SPECIAL_CONDITIONS_INFO, PREFERRED_FORM_INFO,
                                         PARTICIPATION_IN_OLYMPIADS_INFO, SPECIAL_RIGHTS_INFO, CONCLUDED_AGREEMENT_INFO)

    def testing_of_the_the_RC_settings_step_second(self, browser):
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        title_org = random_en(8)
        title_pk = random_en(8)
        system = random_en(8)
        campaign_id = random_number(10)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
                with allure.step('Добавить ПК в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(campaign_id, title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Приемная кампания"'):
            page.open_window_setting('Приемная кампания')
        with allure.step('Выбрать ранее созданную ОО'):
            page.select_field('Настройки ОО', title_org)
        with allure.step('Выбрать ранее созданную ПК'):
            page.select_field('Настройки ПК', title_pk)
        with allure.step('Открыть настройки "Шаг 2 (Выбор конурсов)"'):
            page.open_setting_pk('SecondStep')
        with allure.step('Установить тип поля "Образовательные программы" в "Не отображаются'):
            EDUCATIONAL_PROGRAMS_CHOICE = SecondStep.EDUCATIONAL_PROGRAMS_CHOICE_NO
            page.select_radiobutton_SecondStep(SecondStep.EDUCATIONAL_PROGRAMS_CHOICE, EDUCATIONAL_PROGRAMS_CHOICE)
        with allure.step('Установить тип поля "Графа с числом мест" в "Не отображаются'):
            PLACES_COLUMN = SecondStep.PLACES_COLUMN_NO
            page.select_radiobutton_SecondStep(SecondStep.PLACES_COLUMN, PLACES_COLUMN)
        with allure.step('Установить тип поля "Графа с числом поданных" в "Не отображаются'):
            REQUESTS_COLUMN = SecondStep.REQUESTS_COLUMN_NO
            page.select_radiobutton_SecondStep(SecondStep.REQUESTS_COLUMN, REQUESTS_COLUMN)
        with allure.step('Установить тип поля "Выбор в рамках одного" в "Не допускается'):
            CHOICE_BUDGET_AND_CONTRACT = SecondStep.CHOICE_BUDGET_AND_CONTRACT_NOT_NO
            page.select_radiobutton_SecondStep(SecondStep.CHOICE_BUDGET_AND_CONTRACT, CHOICE_BUDGET_AND_CONTRACT)
        with allure.step('Установить тип поля "В списке для выбора" в "Да'):
            HIDE_CONTESTS = SecondStep.HIDE_CONTESTS_YES
            page.select_radiobutton_SecondStep(SecondStep.HIDE_CONTESTS, HIDE_CONTESTS)
        with allure.step('Установить тип поля "Выбор приоритетов" в "Не осуществляется'):
            PRIORITIES_CHOICE = SecondStep.PRIORITIES_CHOICE_NO
            page.select_radiobutton_SecondStep(SecondStep.PRIORITIES_CHOICE, PRIORITIES_CHOICE)
        with allure.step('Установить тип поля "Выбор образовательных программ" в "Осуществляется, обязательно'):
            EDUCATIONAL_PROGRAMS_AND_PRIORITIES_CHOICE = SecondStep.EDUCATIONAL_PROGRAMS_AND_PRIORITIES_CHOICE_REQUIRED
            page.select_radiobutton_SecondStep(SecondStep.EDUCATIONAL_PROGRAMS_AND_PRIORITIES_CHOICE,
                                               EDUCATIONAL_PROGRAMS_AND_PRIORITIES_CHOICE)
        with allure.step('Проверка записи настроек в settings.abitur_second_step'):
            page.check_records_SecondStep(title_pk, EDUCATIONAL_PROGRAMS_CHOICE, PLACES_COLUMN, REQUESTS_COLUMN,
                                          CHOICE_BUDGET_AND_CONTRACT, HIDE_CONTESTS, PRIORITIES_CHOICE,
                                          EDUCATIONAL_PROGRAMS_AND_PRIORITIES_CHOICE)

    def testing_of_the_RC_settings_competitive_lists(self, browser):
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        title_org = random_en(8)
        title_pk = random_en(8)
        system = random_en(8)
        campaign_id = random_number(10)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
                with allure.step('Добавить ПК в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(campaign_id, title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Приемная кампания"'):
            page.open_window_setting('Приемная кампания')
        with allure.step('Выбрать ранее созданную ОО'):
            page.select_field('Настройки ОО', title_org)
        with allure.step('Выбрать ранее созданную ПК'):
            page.select_field('Настройки ПК', title_pk)
        with allure.step('Открыть настройки "Конкурсные списки"'):
            page.open_setting_pk('CompetitionLists')
        with allure.step('Установить тип поля "Образовательные программы в списке конкурсов»" в "Не отображать"'):
            EDUCATIONAL_PROGRAMS_COMPETITION_LISTS = CompetitionLists.NOT_SHOW
            page.select_radiobutton_CompetitionLists(CompetitionLists.EDUCATIONAL_PROGRAMS_COMPETITION_LISTS,
                                                     EDUCATIONAL_PROGRAMS_COMPETITION_LISTS)
        with allure.step('Установить тип поля "Образовательные программы на карточке конкурса" в "Не отображать"'):
            EDUCATIONAL_PROGRAMS_COMPETITION_CARD = CompetitionLists.NOT_SHOW
            page.select_radiobutton_CompetitionLists(CompetitionLists.EDUCATIONAL_PROGRAMS_COMPETITION_CARD,
                                                     EDUCATIONAL_PROGRAMS_COMPETITION_CARD)
        with allure.step('Установить тип поля "Отображать вместо ФИО" в "Не отображать"'):
            SNILS_OR_UNIQUE_NUMBER = CompetitionLists.NOT_SHOW
            page.select_radiobutton_CompetitionLists(CompetitionLists.SNILS_OR_UNIQUE_NUMBER, SNILS_OR_UNIQUE_NUMBER)
        with allure.step('Установить тип поля "Списки по виду ЦП" в "Не отображать"'):
            TARGETED_ADMISSION = CompetitionLists.NOT_SHOW
            page.select_radiobutton_CompetitionLists(CompetitionLists.TARGETED_ADMISSION, TARGETED_ADMISSION)
        with allure.step('Установить тип поля "Основание для приема" в "Не отображать"'):
            WITHOUT_ENTRANCE_EXAMINATIONS = CompetitionLists.NOT_SHOW
            page.select_radiobutton_CompetitionLists(CompetitionLists.WITHOUT_ENTRANCE_EXAMINATIONS,
                                                     WITHOUT_ENTRANCE_EXAMINATIONS)
        with allure.step('Установить тип поля "Средний балл" в "Не отображать"'):
            AVERAGE_MARK = CompetitionLists.NOT_SHOW
            page.select_radiobutton_CompetitionLists(CompetitionLists.AVERAGE_MARK, AVERAGE_MARK)
        with allure.step('Установить тип поля "Сумма конкурсных баллов" в "Не отображать"'):
            COMPETITION_POINTS_SUM = CompetitionLists.NOT_SHOW
            page.select_radiobutton_CompetitionLists(CompetitionLists.COMPETITION_POINTS_SUM, COMPETITION_POINTS_SUM)
        with allure.step('Установить тип поля "Сумма баллов за вступительные" в "Не отображать"'):
            ENTRANCE_EXAMINATIONS_POINTS_SUM = CompetitionLists.NOT_SHOW
            page.select_radiobutton_CompetitionLists(CompetitionLists.ENTRANCE_EXAMINATIONS_POINTS_SUM,
                                                     ENTRANCE_EXAMINATIONS_POINTS_SUM)
        with allure.step('Установить тип поля "Сумма баллов за индивидуальные" в "Не отображать"'):
            INDIVIDUAL_ACHIEVEMENTS_POINTS_SUM = CompetitionLists.NOT_SHOW
            page.select_radiobutton_CompetitionLists(CompetitionLists.INDIVIDUAL_ACHIEVEMENTS_POINTS_SUM,
                                                     INDIVIDUAL_ACHIEVEMENTS_POINTS_SUM)
        with allure.step('Установить тип поля "Детализация баллов за вступительные" в "Не отображать"'):
            ENTRANCE_EXAMINATIONS_POINTS_DETAILING = CompetitionLists.NOT_SHOW
            page.select_radiobutton_CompetitionLists(CompetitionLists.ENTRANCE_EXAMINATIONS_POINTS_DETAILING,
                                                     ENTRANCE_EXAMINATIONS_POINTS_DETAILING)
        with allure.step('Установить тип поля "Детализация баллов за индивидуальные" в "Не отображать"'):
            INDIVIDUAL_ACHIEVEMENTS_POINTS_DETAILING = CompetitionLists.NOT_SHOW
            page.select_radiobutton_CompetitionLists(CompetitionLists.INDIVIDUAL_ACHIEVEMENTS_POINTS_DETAILING,
                                                     INDIVIDUAL_ACHIEVEMENTS_POINTS_DETAILING)
        with allure.step('Установить тип поля "Преимущественные права" в "Не отображать"'):
            PREEMPTIVE_RIGHTS = CompetitionLists.NOT_SHOW
            page.select_radiobutton_CompetitionLists(CompetitionLists.PREEMPTIVE_RIGHTS, PREEMPTIVE_RIGHTS)
        with allure.step('Установить тип поля "Оригинал документа об образовании" в "Не отображать"'):
            ORIGINAL_EDUCATIONAL_DOCUMENT = CompetitionLists.NOT_SHOW
            page.select_radiobutton_CompetitionLists(CompetitionLists.ORIGINAL_EDUCATIONAL_DOCUMENT,
                                                     ORIGINAL_EDUCATIONAL_DOCUMENT)
        with allure.step('Установить тип поля "Согласие на зачисление" в "Не отображать"'):
            CONSENT_TO_ENROLLMENT = CompetitionLists.NOT_SHOW
            page.select_radiobutton_CompetitionLists(CompetitionLists.CONSENT_TO_ENROLLMENT, CONSENT_TO_ENROLLMENT)
        with allure.step('Установить тип поля "Состояние заявления" в "Не отображать"'):
            STATEMENT_STATE = CompetitionLists.NOT_SHOW
            page.select_radiobutton_CompetitionLists(CompetitionLists.STATEMENT_STATE, STATEMENT_STATE)
        with allure.step('Проверка записи настроек в settings.abitur_competition_lists'):
            page.check_records_CompetitionLists(title_pk, EDUCATIONAL_PROGRAMS_COMPETITION_LISTS,
                                                EDUCATIONAL_PROGRAMS_COMPETITION_CARD, SNILS_OR_UNIQUE_NUMBER,
                                                TARGETED_ADMISSION, WITHOUT_ENTRANCE_EXAMINATIONS, AVERAGE_MARK,
                                                COMPETITION_POINTS_SUM, ENTRANCE_EXAMINATIONS_POINTS_SUM,
                                                INDIVIDUAL_ACHIEVEMENTS_POINTS_SUM,
                                                ENTRANCE_EXAMINATIONS_POINTS_DETAILING,
                                                INDIVIDUAL_ACHIEVEMENTS_POINTS_DETAILING, PREEMPTIVE_RIGHTS,
                                                ORIGINAL_EDUCATIONAL_DOCUMENT, CONSENT_TO_ENROLLMENT, STATEMENT_STATE)

    def testing_of_the_RC_settings_contracts(self, browser):
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        title_org = random_en(8)
        title_pk = random_en(8)
        system = random_en(8)
        campaign_id = random_number(10)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
                with allure.step('Добавить ПК в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(campaign_id, title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Приемная кампания"'):
            page.open_window_setting('Приемная кампания')
        with allure.step('Выбрать ранее созданную ОО'):
            page.select_field('Настройки ОО', title_org)
        with allure.step('Выбрать ранее созданную ПК'):
            page.select_field('Настройки ПК', title_pk)
        with allure.step('Открыть настройки "Договоры на обучение"'):
            page.open_setting_pk('Contracts')
        with allure.step('Проверка функциональности настроек - Общие'):
            with allure.step('Ввести в поле "Количество заявок на заключение договора" - "10"'):
                CONTRACT_REQUEST_COUNT = '10'
                page.select_radiobutton_ContractCommon(ContractCommon.CONTRACT_REQUEST_COUNT, '',
                                                       CONTRACT_REQUEST_COUNT)
            with allure.step('Установить тип поля "Файл скан-копии сертификата" в "Прикладывается обязательно"'):
                MOTHER_CERT_SCAN = ContractAction.REQUIRED
                page.select_radiobutton_ContractCommon(ContractCommon.MOTHER_CERT_SCAN, MOTHER_CERT_SCAN, '')
            with allure.step('Проверка записи настроек в settings.contract_common'):
                page.check_records_ContractCommon(title_pk, CONTRACT_REQUEST_COUNT, MOTHER_CERT_SCAN)
        with allure.step('Проверка функциональности настроек - «Физическое лицо (абитуриент)»'):
            with allure.step('Установить тип поля "Согласие на обработку персональных данных" в "Не заполняется"'):
                PERSONAL_DATA_AGREEMENT = ContractAction.NO
                page.select_radiobutton_ContractEntrantPersonType(ContractEntrantPersonType.PERSONAL_DATA_AGREEMENT,
                                                                  PERSONAL_DATA_AGREEMENT)
            with allure.step('Проверка записи настроек в settings.contract_entrant_person_type'):
                page.check_records_ContractEntrantPersonType(title_pk, PERSONAL_DATA_AGREEMENT)
        with allure.step('Проверка функциональности настроек - «Физическое лицо (иное)»'):
            with allure.step('Установить тип поля "Согласие на обработку персональных" в "Не заполняется"'):
                PERSONAL_DATA_AGREEMENT = ContractAction.NO
                page.select_radiobutton_ContractOtherPersonType(ContractOtherPersonType.PERSONAL_DATA_AGREEMENT,
                                                                PERSONAL_DATA_AGREEMENT)
            with allure.step('Установить тип поля "Заполнение поля «Телефон»" в "Обязательно"'):
                PHONE = ContractAction.REQUIRED
                page.select_radiobutton_ContractOtherPersonType(ContractOtherPersonType.PHONE, PHONE)
            with allure.step('Установить тип поля "Заполнение поля «СНИЛС»" в "Обязательно"'):
                SNILS = ContractAction.REQUIRED
                page.select_radiobutton_ContractOtherPersonType(ContractOtherPersonType.SNILS, SNILS)
            with allure.step('Установить тип поля "Заполнение поля «ИНН»" в "Обязательно"'):
                INN = ContractAction.REQUIRED
                page.select_radiobutton_ContractOtherPersonType(ContractOtherPersonType.INN, INN)
            with allure.step('Установить тип поля "Заполнение поля «Наименование банка заказчика»" в "Обязательно"'):
                BANK_NAME = ContractAction.REQUIRED
                page.select_radiobutton_ContractOtherPersonType(ContractOtherPersonType.BANK_NAME, BANK_NAME)
            with allure.step('Установить тип поля "Заполнение поля «БИК»" в "Обязательно"'):
                BIK = ContractAction.REQUIRED
                page.select_radiobutton_ContractOtherPersonType(ContractOtherPersonType.BIK, BIK)
            with allure.step('Установить тип поля "Заполнение поля «Лицевой счет заказчика»" в "Обязательно"'):
                BANK_ACCOUNT = ContractAction.REQUIRED
                page.select_radiobutton_ContractOtherPersonType(ContractOtherPersonType.BANK_ACCOUNT, BANK_ACCOUNT)
            with allure.step('Установить тип поля "Заполнение поля «Место работы»" в "Обязательно"'):
                WORK = ContractAction.REQUIRED
                page.select_radiobutton_ContractOtherPersonType(ContractOtherPersonType.WORK, WORK)
            with allure.step('Установить тип поля "Заполнение поля «Должность»" в "Обязательно"'):
                POSITION = ContractAction.REQUIRED
                page.select_radiobutton_ContractOtherPersonType(ContractOtherPersonType.POSITION, POSITION)
            with allure.step('Установить тип поля "Файл скан-копии удостоверения" в "Прикладывается обязательно"'):
                PASSPORT_SCAN = ContractAction.REQUIRED
                page.select_radiobutton_ContractOtherPersonType(ContractOtherPersonType.PASSPORT_SCAN, PASSPORT_SCAN)
            with allure.step('Проверка записи настроек в settings.contract_other_person_type'):
                page.check_records_ContractOtherPersonType(title_pk, PERSONAL_DATA_AGREEMENT, PHONE, SNILS, INN,
                                                           BANK_NAME, BIK, BANK_ACCOUNT, WORK, POSITION, PASSPORT_SCAN)
        with allure.step('Проверка функциональности настроек - «Юридическое лицо»'):
            with allure.step('Установить тип поля "Заполнение поля «Страна регистрации»" в "Обязательно"'):
                REGISTRATION_COUNTRY = ContractAction.REQUIRED
                page.select_radiobutton_ContractLegalPersonType(ContractLegalPersonType.REGISTRATION_COUNTRY,
                                                                REGISTRATION_COUNTRY)
            with allure.step('Установить тип поля "Заполнение поля «Дата регистрации»" в "Обязательно"'):
                REGISTRATION_DATE = ContractAction.REQUIRED
                page.select_radiobutton_ContractLegalPersonType(ContractLegalPersonType.REGISTRATION_DATE,
                                                                REGISTRATION_DATE)
            with allure.step('Установить тип поля "Заполнение поля «ФИО ответственного»" в "Обязательно"'):
                AUTHORIZED_PERSON = ContractAction.REQUIRED
                page.select_radiobutton_ContractLegalPersonType(ContractLegalPersonType.AUTHORIZED_PERSON,
                                                                AUTHORIZED_PERSON)
            with allure.step('Установить тип поля "Заполнение поля «ИНН»" в "Обязательно"'):
                INN = ContractAction.REQUIRED
                page.select_radiobutton_ContractLegalPersonType(ContractLegalPersonType.INN, INN)
            with allure.step('Установить тип поля "Заполнение поля «КПП»" в "Обязательно"'):
                KPP = ContractAction.REQUIRED
                page.select_radiobutton_ContractLegalPersonType(ContractLegalPersonType.KPP, KPP)
            with allure.step('Установить тип поля "Заполнение поля «ОГРН»" в "Обязательно"'):
                OGRN = ContractAction.REQUIRED
                page.select_radiobutton_ContractLegalPersonType(ContractLegalPersonType.OGRN, OGRN)
            with allure.step('Установить тип поля "Заполнение поля «ОКПО»" в "Обязательно"'):
                OKPO = ContractAction.REQUIRED
                page.select_radiobutton_ContractLegalPersonType(ContractLegalPersonType.OKPO, OKPO)
            with allure.step('Установить тип поля "Заполнение поля «Ссылка на карточку организации»" в "Обязательно"'):
                CARD_LINK = ContractAction.REQUIRED
                page.select_radiobutton_ContractLegalPersonType(ContractLegalPersonType.CARD_LINK, CARD_LINK)
            with allure.step('Установить тип поля "Заполнение поля «Юридический адрес»" в "Обязательно"'):
                LEGAL_ADDRESS = ContractAction.REQUIRED
                page.select_radiobutton_ContractLegalPersonType(ContractLegalPersonType.LEGAL_ADDRESS, LEGAL_ADDRESS)
            with allure.step('Установить тип поля "Заполнение поля «Фактический адрес»" в "Обязательно"'):
                REAL_ADDRESS = ContractAction.REQUIRED
                page.select_radiobutton_ContractLegalPersonType(ContractLegalPersonType.REAL_ADDRESS, REAL_ADDRESS)
            with allure.step('Установить тип поля "Заполнение поля «Наименование банка заказчика»" в "Обязательно"'):
                BANK_NAME = ContractAction.REQUIRED
                page.select_radiobutton_ContractLegalPersonType(ContractLegalPersonType.BANK_NAME, BANK_NAME)
            with allure.step('Установить тип поля "Заполнение поля «БИК»" в "Обязательно"'):
                BIK = ContractAction.REQUIRED
                page.select_radiobutton_ContractLegalPersonType(ContractLegalPersonType.BIK, BIK)
            with allure.step('Установить тип поля "Заполнение поля «Расчетный счет»" в "Обязательно"'):
                CHECKING_ACCOUNT = ContractAction.REQUIRED
                page.select_radiobutton_ContractLegalPersonType(ContractLegalPersonType.CHECKING_ACCOUNT,
                                                                CHECKING_ACCOUNT)
            with allure.step('Установить тип поля "Заполнение поля «Корреспондентский счет»" в "Обязательно"'):
                CORRESPONDENT_ACCOUNT = ContractAction.REQUIRED
                page.select_radiobutton_ContractLegalPersonType(ContractLegalPersonType.CORRESPONDENT_ACCOUNT,
                                                                CORRESPONDENT_ACCOUNT)
            with allure.step('Установить тип поля "Заполнение поля «Заполнение поля «Телефон»" в "Обязательно"'):
                PHONE = ContractAction.REQUIRED
                page.select_radiobutton_ContractLegalPersonType(ContractLegalPersonType.PHONE, PHONE)
            with allure.step('Установить тип поля "Файл скан-копии гарантийного письма" в "Обязательно"'):
                GUARANTEE_LETTER_SCAN = ContractAction.REQUIRED
                page.select_radiobutton_ContractLegalPersonType(ContractLegalPersonType.GUARANTEE_LETTER_SCAN,
                                                                GUARANTEE_LETTER_SCAN)
            with allure.step('Установить тип поля "Файл скан-копии карточки организации" в "Обязательно"'):
                CARD_SCAN = ContractAction.REQUIRED
                page.select_radiobutton_ContractLegalPersonType(ContractLegalPersonType.CARD_SCAN, CARD_SCAN)
            with allure.step('Установить тип поля "Файл скан-копии документа" в "Обязательно"'):
                CREDENTIAL_PROOF_SCAN = ContractAction.REQUIRED
                page.select_radiobutton_ContractLegalPersonType(ContractLegalPersonType.CREDENTIAL_PROOF_SCAN,
                                                                CREDENTIAL_PROOF_SCAN)
            with allure.step('Проверка записи настроек в settings.contract_legal_person_type'):
                page.check_records_ContractLegalPersonType(title_pk, REGISTRATION_COUNTRY, REGISTRATION_DATE,
                                                           AUTHORIZED_PERSON, INN, KPP, OGRN, OKPO, CARD_LINK,
                                                           LEGAL_ADDRESS, REAL_ADDRESS, BANK_NAME, BIK,
                                                           CHECKING_ACCOUNT, CORRESPONDENT_ACCOUNT, PHONE,
                                                           GUARANTEE_LETTER_SCAN, CARD_SCAN, CREDENTIAL_PROOF_SCAN)

    def testing_of_the_RC_settings_abitur_main(self, browser):
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        title_org = random_en(8)
        title_pk = random_en(8)
        system = random_en(8)
        campaign_id = random_number(10)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
                with allure.step('Добавить ПК в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(campaign_id, title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Приемная кампания"'):
            page.open_window_setting('Приемная кампания')
        with allure.step('Выбрать ранее созданную ОО'):
            page.select_field('Настройки ОО', title_org)
        with allure.step('Выбрать ранее созданную ПК'):
            page.select_field('Настройки ПК', title_pk)
        with allure.step('Открыть настройки "Основные настройки"'):
            page.open_setting_pk('AbiturMain')
        with allure.step('Включить чекбокс ".zip"'):
            ZIP = AbiturMain.TRUE
            page.select_checkbox_AbiturMain(AbiturMain.ZIP)
        with allure.step('Выключить чекбокс ".pdf"'):
            PDF = AbiturMain.FALSE
            page.select_checkbox_AbiturMain(AbiturMain.PDF)
        with allure.step('Выключить чекбокс ".jpg"'):
            JPG = AbiturMain.FALSE
            page.select_checkbox_AbiturMain(AbiturMain.JPG)
        with allure.step('Включить чекбокс ".png"'):
            PNG = AbiturMain.TRUE
            page.select_checkbox_AbiturMain(AbiturMain.PNG)
        with allure.step('Включить чекбокс ".gif"'):
            GIF = AbiturMain.TRUE
            page.select_checkbox_AbiturMain(AbiturMain.GIF)
        with allure.step('Включить чекбокс ".doc"'):
            DOC = AbiturMain.TRUE
            page.select_checkbox_AbiturMain(AbiturMain.DOC)
        with allure.step('Включить чекбокс ".docx"'):
            DOCX = AbiturMain.TRUE
            page.select_checkbox_AbiturMain(AbiturMain.DOCX)
        with allure.step('Включить чекбокс ".rtf"'):
            RTF = AbiturMain.TRUE
            page.select_checkbox_AbiturMain(AbiturMain.RTF)
        with allure.step('Включить чекбокс ".rar"'):
            RAR = AbiturMain.TRUE
            page.select_checkbox_AbiturMain(AbiturMain.RAR)
        with allure.step('Ввести в поле "Максимальное количество онлайн-заявлений" валидное число'):
            online_requests_count = int(random_number(2))
            page.input_field(0, online_requests_count, 'clear')
        with allure.step('Проверка записи настроек в settings.abitur_main'):
            page.check_records_AbiturMain(title_pk, ZIP, PDF, JPG, PNG, GIF, DOC, DOCX, RTF, RAR, online_requests_count)

    def testing_of_the_turning_off_all_PC_settings_abitur_main(self, browser):
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        title_org = random_en(8)
        title_pk = random_en(8)
        system = random_en(8)
        campaign_id = random_number(10)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
                with allure.step('Добавить ПК в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(campaign_id, title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Приемная кампания"'):
            page.open_window_setting('Приемная кампания')
        with allure.step('Выбрать ранее созданную ОО'):
            page.select_field('Настройки ОО', title_org)
        with allure.step('Выбрать ранее созданную ПК'):
            page.select_field('Настройки ПК', title_pk)
        with allure.step('Открыть настройки "Основные настройки"'):
            page.open_setting_pk('AbiturMain')
        with allure.step('Выключить чекбокс ".pdf"'):
            page.select_checkbox_AbiturMain(AbiturMain.PDF)
        with allure.step('Выключить чекбокс ".jpg"'):
            page.select_checkbox_AbiturMain(AbiturMain.JPG)
        with allure.step('Проверка срабатывания валидатора при некорректной записи'):
            page.check_validation_settings()

    def testing_of_the_RC_settings_abitur_third_step(self, browser):
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        title_org = random_en(8)
        title_pk = random_en(8)
        system = random_en(8)
        campaign_id = random_number(10)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
                with allure.step('Добавить ПК в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(campaign_id, title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Приемная кампания"'):
            page.open_window_setting('Приемная кампания')
        with allure.step('Выбрать ранее созданную ОО'):
            page.select_field('Настройки ОО', title_org)
        with allure.step('Выбрать ранее созданную ПК'):
            page.select_field('Настройки ПК', title_pk)
        with allure.step('Открыть настройки "Основные настройки"'):
            page.open_setting_pk('ThirdStep')
        with allure.step('Установить тип поля "СНИЛС" в "Заполняется, обязательно только для граждан РФ"'):
            SNILS = ThirdStep.REQUIRED_FOR_RF
            page.select_checkbox_ThirdStep(ThirdStep.SNILS, SNILS)
        with allure.step('Установить тип поля "Заполнение результатов освоения" в "Обязательно"'):
            EDUCATIONAL_PROGRAM_RESULTS = ThirdStep.REQUIRED
            page.select_checkbox_ThirdStep(ThirdStep.EDUCATIONAL_PROGRAM_RESULTS, EDUCATIONAL_PROGRAM_RESULTS)
        with allure.step('Установить тип поля "Заполнение данных родственника" в "Обязательно"'):
            RELATIVE_DATA = ThirdStep.REQUIRED
            page.select_checkbox_ThirdStep(ThirdStep.RELATIVE_DATA, RELATIVE_DATA)
        with allure.step('Установить тип поля "Информация о втором родственнике" в "Не заполняется"'):
            SECOND_RELATIVE_DATA = ThirdStep.NOT_REQUIRED
            page.select_checkbox_ThirdStep(ThirdStep.SECOND_RELATIVE_DATA, SECOND_RELATIVE_DATA)
        with allure.step('Установить тип поля "Заполнение контактного телефона родственника" в "Обязательно"'):
            RELATIVE_CONTACT_PHONE = ThirdStep.REQUIRED
            page.select_checkbox_ThirdStep(ThirdStep.RELATIVE_CONTACT_PHONE, RELATIVE_CONTACT_PHONE)
        with allure.step('Установить тип поля "Основной иностранный язык" в "Не заполняется"'):
            MAIN_FOREIGN_LANGUAGE = ThirdStep.NOT_REQUIRED
            page.select_checkbox_ThirdStep(ThirdStep.MAIN_FOREIGN_LANGUAGE, MAIN_FOREIGN_LANGUAGE)
        with allure.step('Установить тип поля "Место работы и должность" в "Не заполняется"'):
            WORK_POSITION = ThirdStep.NOT_REQUIRED
            page.select_checkbox_ThirdStep(ThirdStep.WORK_POSITION, WORK_POSITION)
        with allure.step('Установить тип поля "ИНН" в "Заполняется, обязательно только для граждан РФ"'):
            INN = ThirdStep.REQUIRED_FOR_RF
            page.select_checkbox_ThirdStep(ThirdStep.INN, INN)
        with allure.step('Установить тип поля "Информация о подготовительных курсах" в "Не заполняется"'):
            COURSES_INFO = ThirdStep.NOT_REQUIRED
            page.select_checkbox_ThirdStep(ThirdStep.COURSES_INFO, COURSES_INFO)
        with allure.step('Установить тип поля "Источники информации об образовательной организаци" в "Не заполняется"'):
            EDUCATIONAL_ORGANIZATION_INFO_SOURCES = ThirdStep.NOT_REQUIRED
            page.select_checkbox_ThirdStep(ThirdStep.EDUCATIONAL_ORGANIZATION_INFO_SOURCES,
                                           EDUCATIONAL_ORGANIZATION_INFO_SOURCES)
        with allure.step('Установить тип поля "Информация о спортивных достижениях" в "Не заполняется"'):
            SPORTS_ACHIEVEMENTS_INFO = ThirdStep.NOT_REQUIRED
            page.select_checkbox_ThirdStep(ThirdStep.SPORTS_ACHIEVEMENTS_INFO, SPORTS_ACHIEVEMENTS_INFO)
        with allure.step('Установить тип поля "Информация о службе в армии" в "Не заполняется"'):
            MILITARY_INFO = ThirdStep.NOT_REQUIRED
            page.select_checkbox_ThirdStep(ThirdStep.MILITARY_INFO, MILITARY_INFO)
        with allure.step('Проверка записи настроек в settings.abitur_main'):
            page.check_records_ThirdStep(title_pk, SNILS, EDUCATIONAL_PROGRAM_RESULTS, RELATIVE_DATA,
                                         SECOND_RELATIVE_DATA, RELATIVE_CONTACT_PHONE, MAIN_FOREIGN_LANGUAGE,
                                         WORK_POSITION, INN, COURSES_INFO, EDUCATIONAL_ORGANIZATION_INFO_SOURCES,
                                         SPORTS_ACHIEVEMENTS_INFO, MILITARY_INFO)

    def testing_of_the_RC_settings_abitur_entrants_search(self, browser):
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        title_org = random_en(8)
        title_pk = random_en(8)
        system = random_en(8)
        campaign_id = random_number(10)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
                with allure.step('Добавить ПК в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(campaign_id, title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Приемная кампания"'):
            page.open_window_setting('Приемная кампания')
        with allure.step('Выбрать ранее созданную ОО'):
            page.select_field('Настройки ОО', title_org)
        with allure.step('Выбрать ранее созданную ПК'):
            page.select_field('Настройки ПК', title_pk)
        with allure.step('Открыть настройки "Поиск абитуриентов"'):
            page.open_setting_pk('EntrantsSearch')
        with allure.step('Установить тип поля "Данные для поиска" в "Не получать"'):
            page.select_checkbox_EntrantsSearch(EntrantsSearch.DATA, EntrantsSearch.NOT_RECEIVE, '')
        with allure.step('Установить тип поля "Данные для поиска" в "Получать"'):
            DATA = EntrantsSearch.RECEIVE
            page.select_checkbox_EntrantsSearch(EntrantsSearch.DATA, DATA, '')
        with allure.step('Установить тип вкладки "Раздел «Поиск»" в "Отображать"'):
            SEARCH_SECTION = EntrantsSearch.RECEIVE
            page.select_checkbox_EntrantsSearch(EntrantsSearch.SEARCH_SECTION, SEARCH_SECTION, '')
        with allure.step('Ввести в поле "Текст подсказки" 8 символов'):
            PROMPT_TEXT = random_en(8)
            page.select_checkbox_EntrantsSearch(EntrantsSearch.PROMPT_TEXT, 'Correct', PROMPT_TEXT)
        with allure.step('Установить тип вкладки "Отображать вместо ФИО" в "Отображать"'):
            SNILS_OR_UNIQUE_NUMBER = EntrantsSearch.RECEIVE
            page.select_checkbox_EntrantsSearch(EntrantsSearch.SNILS_OR_UNIQUE_NUMBER, SNILS_OR_UNIQUE_NUMBER, '')
        with allure.step('Установить тип вкладки "Расписание вступительных испытаний" в "Отображать"'):
            ENTRANCE_EXAMINATIONS_SCHEDULE = EntrantsSearch.RECEIVE
            page.select_checkbox_EntrantsSearch(EntrantsSearch.ENTRANCE_EXAMINATIONS_SCHEDULE,
                                                ENTRANCE_EXAMINATIONS_SCHEDULE, '')
        with allure.step('Проверка записи настроек в settings.abitur_entrants_search'):
            page.check_records_EntrantsSearch(title_pk, DATA, SEARCH_SECTION, PROMPT_TEXT, SNILS_OR_UNIQUE_NUMBER,
                                              ENTRANCE_EXAMINATIONS_SCHEDULE)

    def testing_of_the_RC_abitur_settings_creating_regulatory_documents_with_valid_data(self, browser):
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        title_org = random_en(8)
        title_pk = random_en(8)
        system = random_en(8)
        title_category = random_en(8)
        title_type = random_en(8)
        title_regulation = random_en(8)
        short_title_regulation = random_en(8)
        description_regulation = random_en(8)
        link_regulation = random_en(8)
        active = 0
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
                with allure.step('Добавить ПК в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(random_number(10), title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
            with allure.step('Создать тип нормативного документа'):
                with allure.step('Добавить запись в regulation_categories'):
                    SETTING().add_data_regulation_categories(title_category, '', True)
                with allure.step('Добавить запись в regulation_types, и привязать ранее созданную первую категорию'):
                    SETTING().add_data_regulation_types(title_type, random_en(8), title_category, True)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Приемная кампания"'):
            page.open_window_setting('Приемная кампания')
        with allure.step('Выбрать ранее созданную ОО'):
            page.select_field('Настройки ОО', title_org)
        with allure.step('Выбрать ранее созданную ПК'):
            page.select_field('Настройки ПК', title_pk)
        with allure.step('Открыть настройки "Нормативные документы"'):
            page.open_setting_pk('Regulations')
        with allure.step('Нажать кнопку "Новый нормативный документ"'):
            page.create_new_company('Correct')
        with allure.step('Выбрать в селекте "Тип" ранее созданный тип документа'):
            regulation_type_id = SETTING().get_id_regulation_types_by_title(title_type)
            page.select_categories_regulatory_document(regulation_type_id)
        with allure.step('Ввести в поле "Название" 8 символов'):
            page.input_field(0, title_regulation, '')
        with allure.step('Ввести в поле "Сокращенное название" 8 символов'):
            page.input_field(1, short_title_regulation, '')
        with allure.step('Ввести в поле "Описание нормативного документа" 8 символов'):
            page.input_field(2, description_regulation, '')
        with allure.step('Ввести в поле "Ссылка на интернет-ресурс" 8 символов'):
            page.input_field(3, link_regulation, '')
        with allure.step('Добавить в поле "Файл для скачивания" валидный документ'):
            page.add_uploadFiles('files/doc/doc.pdf')
        with allure.step('Выключить чекбокс "Действительный документ"'):
            page.select_checkbox(0)
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_validators_regulations('Correct', '')
        with allure.step('Проверка записи в abitur.regulations'):
            page.check_record_regulation(title_regulation, short_title_regulation, regulation_type_id,
                                         description_regulation, link_regulation, active, title_org, title_pk)
        with allure.step('Удалить ранее созданный документ'):
            page.perform_an_action_on_an_document(title_category, 'remove', 'Correct')

    def testing_of_the_RC_abitur_settings_creating_regulatory_documents_with_limit_values(self, browser):
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        title_org = random_en(8)
        title_pk = random_en(8)
        system = random_en(8)
        title_category = random_en(8)
        title_type = random_en(8)
        title_regulation = random_en(255)
        short_title_regulation = random_en(255)
        description_regulation = random_en(255)
        link_regulation = random_en(255)
        active = 0
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
                with allure.step('Добавить ПК в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(random_number(10), title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
            with allure.step('Создать тип нормативного документа'):
                with allure.step('Добавить запись в regulation_categories'):
                    SETTING().add_data_regulation_categories(title_category, '', True)
                with allure.step('Добавить запись в regulation_types, и привязать ранее созданную первую категорию'):
                    SETTING().add_data_regulation_types(title_type, random_en(8), title_category, True)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Приемная кампания"'):
            page.open_window_setting('Приемная кампания')
        with allure.step('Выбрать ранее созданную ОО'):
            page.select_field('Настройки ОО', title_org)
        with allure.step('Выбрать ранее созданную ПК'):
            page.select_field('Настройки ПК', title_pk)
        with allure.step('Открыть настройки "Нормативные документы"'):
            page.open_setting_pk('Regulations')
        with allure.step('Нажать кнопку "Новый нормативный документ"'):
            page.create_new_company('Correct')
        with allure.step('Выбрать в селекте "Тип" ранее созданный тип документа'):
            regulation_type_id = SETTING().get_id_regulation_types_by_title(title_type)
            page.select_categories_regulatory_document(regulation_type_id)
        with allure.step('Ввести в поле "Название" 8 символов'):
            page.input_field(0, title_regulation, '')
        with allure.step('Ввести в поле "Сокращенное название" 8 символов'):
            page.input_field(1, short_title_regulation, '')
        with allure.step('Ввести в поле "Описание нормативного документа" 8 символов'):
            page.input_field(2, description_regulation, '')
        with allure.step('Ввести в поле "Ссылка на интернет-ресурс" 8 символов'):
            page.input_field(3, link_regulation, '')
        with allure.step('Добавить в поле "Файл для скачивания" валидный документ'):
            page.add_uploadFiles('files/doc/doc.pdf')
        with allure.step('Выключить чекбокс "Действительный документ"'):
            page.select_checkbox(0)
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_validators_regulations('Correct', '')
        with allure.step('Проверка записи в abitur.regulations'):
            page.check_record_regulation(title_regulation, short_title_regulation, regulation_type_id,
                                         description_regulation, link_regulation, active, title_org, title_pk)
        with allure.step('Удалить ранее созданный документ'):
            page.perform_an_action_on_an_document(title_category, 'remove', 'Correct')

    def testing_of_the_RC_abitur_settings_creating_regulatory_documents_with_exorbitant_values(self, browser):
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        title_org = random_en(8)
        title_pk = random_en(8)
        system = random_en(8)
        title_category = random_en(8)
        title_type = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
                with allure.step('Добавить ПК в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(random_number(10), title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
            with allure.step('Создать тип нормативного документа'):
                with allure.step('Добавить запись в regulation_categories'):
                    SETTING().add_data_regulation_categories(title_category, '', True)
                with allure.step('Добавить запись в regulation_types, и привязать ранее созданную первую категорию'):
                    SETTING().add_data_regulation_types(title_type, random_en(8), title_category, True)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Приемная кампания"'):
            page.open_window_setting('Приемная кампания')
        with allure.step('Выбрать ранее созданную ОО'):
            page.select_field('Настройки ОО', title_org)
        with allure.step('Выбрать ранее созданную ПК'):
            page.select_field('Настройки ПК', title_pk)
        with allure.step('Открыть настройки "Нормативные документы"'):
            page.open_setting_pk('Regulations')
        with allure.step('Нажать кнопку "Новый нормативный документ"'):
            page.create_new_company('Correct')
        with allure.step('Выбрать в селекте "Тип" ранее созданный тип документа'):
            regulation_type_id = SETTING().get_id_regulation_types_by_title(title_type)
            page.select_categories_regulatory_document(regulation_type_id)
        with allure.step('Проверка валидации поля "Название" на ввод запредельных значений'):
            with allure.step('Ввести в поле 256 символов'):
                page.input_field(0, random_en(256), '')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«название» не может превышать')
            with allure.step('Вернуть поле в исходное состояние'):
                page.input_field(0, random_en(8), 'clear')
        with allure.step('Проверка валидации поля "Сокращенное название" на ввод запредельных значений'):
            with allure.step('Ввести в поле 256 символов'):
                page.input_field(1, random_en(256), '')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«сокращенное название» не может превышать')
            with allure.step('Вернуть поле в исходное состояние'):
                page.input_field(1, random_en(8), 'clear')
        with allure.step('Проверка валидации поля "Ссылка на интернет-ресурс" на ввод запредельных значений'):
            with allure.step('Ввести в поле 256 символов'):
                page.input_field(3, random_en(256), '')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«ссылка» не может превышать')
            with allure.step('Вернуть поле в исходное состояние'):
                page.input_field(3, random_en(8), 'clear')

    def testing_of_the_RC_abitur_settings_creating_regulatory_documents_with_empty_values(self, browser):
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        title_org = random_en(8)
        title_pk = random_en(8)
        system = random_en(8)
        title_category = random_en(8)
        title_type = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
                with allure.step('Добавить ПК в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(random_number(10), title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
            with allure.step('Создать тип нормативного документа'):
                with allure.step('Добавить запись в regulation_categories'):
                    SETTING().add_data_regulation_categories(title_category, '', True)
                with allure.step('Добавить запись в regulation_types, и привязать ранее созданную первую категорию'):
                    SETTING().add_data_regulation_types(title_type, random_en(8), title_category, True)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Приемная кампания"'):
            page.open_window_setting('Приемная кампания')
        with allure.step('Выбрать ранее созданную ОО'):
            page.select_field('Настройки ОО', title_org)
        with allure.step('Выбрать ранее созданную ПК'):
            page.select_field('Настройки ПК', title_pk)
        with allure.step('Открыть настройки "Нормативные документы"'):
            page.open_setting_pk('Regulations')
        with allure.step('Нажать кнопку "Новый нормативный документ"'):
            page.create_new_company('Correct')
        with allure.step('Проверка валидации обязательного поля "Тип"'):
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«тип» обязательно')
            with allure.step('Вернуть поле в исходное состояние'):
                regulation_type_id = SETTING().get_id_regulation_types_by_title(title_type)
                page.select_categories_regulatory_document(regulation_type_id)
        with allure.step('Проверка валидации обязательного поля "Название"'):
            with allure.step('Оставить поле пустым'):
                page.input_field(0, ' ', '')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_validators('Incorrect', '«название» обязательно')
            with allure.step('Вернуть в исходное состояние поле "Название"'):
                page.input_field(0, random_en(8), 'clear')

    def testing_of_the_RC_settings_tips_for_applicant(self, browser):
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        title_org = random_en(8)
        title_pk = random_en(8)
        system = random_en(8)
        campaign_id = random_number(10)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
                with allure.step('Добавить ПК в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(campaign_id, title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Приемная кампания"'):
            page.open_window_setting('Приемная кампания')
        with allure.step('Выбрать ранее созданную ОО'):
            page.select_field('Настройки ОО', title_org)
        with allure.step('Выбрать ранее созданную ПК'):
            page.select_field('Настройки ПК', title_pk)
        with allure.step('Открыть настройки "Подсказки для абитуриента"'):
            page.open_setting_pk('Tips_for_applicant')
        with allure.step('Ввести в поле "Я уже получил(а) образование" 8 символов'):
            haveEduLevel = random_en(8)
            page.input_field(0, haveEduLevel, '')
        with allure.step('Ввести в поле "Я хочу поступать на образовательные программы" 8 символов'):
            wantRequestType = random_en(8)
            page.input_field(1, wantRequestType, '')
        with allure.step('Ввести в поле "Способ возврата оригиналов" 8 символов'):
            originalReturnWay = random_en(8)
            page.input_field(2, originalReturnWay, '')
        with allure.step('Ввести в поле "Я уже сдавал(а) или буду сдавать ЕГЭ по предметам" 8 символов'):
            stateExamCodes = random_en(8)
            page.input_field(3, stateExamCodes, '')
        with allure.step('Ввести в поле "Прошу допустить к участию во ВУ" 8 символов'):
            agreeEntranceExams = random_en(8)
            page.input_textarea(0, agreeEntranceExams, '')
        with allure.step('Ввести в поле "Мне необходимы специальные условия" 8 символов'):
            haveDisability = random_en(8)
            page.input_field(4, haveDisability, '')
        with allure.step('Ввести в поле "Я участвовал(а) в олимпиадах школьников" 8 символов'):
            haveOlympiad = random_en(8)
            page.input_field(5, haveOlympiad, '')
        with allure.step('Ввести в поле "У меня есть особые права" 8 символов'):
            exclusiveRights = random_en(8)
            page.input_field(6, exclusiveRights, '')
        with allure.step('Ввести в поле "Я заключал(а) договор о целевом обучении" 8 символов'):
            haveAgreementTargetEdu = random_en(8)
            page.input_field(7, haveAgreementTargetEdu, '')
        with allure.step('Ввести в поле "Параметры для выбора направлений подготовки" 8 символов'):
            competitionParams = random_en(8)
            page.input_textarea(1, competitionParams, '')
        with allure.step('Ввести в поле "Выбор направлений подготовки и конкурсов" 8 символов'):
            competitions = random_en(8)
            page.input_textarea(2, competitions, '')
        with allure.step('Ввести в поле "Определение приоритетности выбранных направлений" 8 символов'):
            competitionPriorities = random_en(8)
            page.input_textarea(3, competitionPriorities, '')
        with allure.step('Ввести в поле "Тип удостоверения" 8 символов'):
            cardType = random_en(8)
            page.input_field(8, cardType, '')
        with allure.step('Ввести в поле "Гражданство" 8 символов'):
            citizenship = random_en(8)
            page.input_field(9, citizenship, '')
        with allure.step('Ввести в поле "Кем выдано удостоверение" 8 символов'):
            issuancePlace = random_en(8)
            page.input_field(10, issuancePlace, '')
        with allure.step('Ввести в поле "Место рождения" 8 символов'):
            birthPlace = random_en(8)
            page.input_field(11, birthPlace, '')
        with allure.step('Ввести в поле "Адрес регистрации" 8 символов'):
            regAddress = random_en(8)
            page.input_field(12, regAddress, '')
        with allure.step('Ввести в поле "Адрес проживания" 8 символов'):
            factAddress = random_en(8)
            page.input_field(13, factAddress, '')
        with allure.step('Ввести в поле "СНИЛС" 8 символов'):
            snilsNumber = random_en(8)
            page.input_textarea(4, snilsNumber, '')
        with allure.step('Ввести в поле "Страна" 8 символов'):
            eduOrganizationCountry = random_en(8)
            page.input_field(14, eduOrganizationCountry, '')
        with allure.step('Ввести в поле "Населенный пункт" 8 символов'):
            eduOrganizationSettlement = random_en(8)
            page.input_field(15, eduOrganizationSettlement, '')
        with allure.step('Ввести в поле "Образовательная организация" 8 символов'):
            eduOrganization = random_en(8)
            page.input_field(16, eduOrganization, '')
        with allure.step('Ввести в поле "Вид документа" 8 символов'):
            eduDocumentKind = random_en(8)
            page.input_field(17, eduDocumentKind, '')
        with allure.step('Ввести в поле "Серия" 8 символов'):
            seriaEduDocument = random_en(8)
            page.input_field(18, seriaEduDocument, '')
        with allure.step('Ввести в поле "Число оценок «Удовлетворительно»" 8 символов'):
            mark3 = random_en(8)
            page.input_field(19, mark3, '')
        with allure.step('Ввести в поле "Число оценок «Хорошо»" 8 символов'):
            mark4 = random_en(8)
            page.input_field(20, mark4, '')
        with allure.step('Ввести в поле "Число оценок «Отлично»" 8 символов'):
            mark5 = random_en(8)
            page.input_field(21, mark5, '')
        with allure.step('Ввести в поле "Средний балл" 8 символов'):
            avgMarkAsLong = random_en(8)
            page.input_field(22, avgMarkAsLong, '')
        with allure.step('Ввести в поле "Средний балл (в 100 б. шкале)" 8 символов'):
            avgScoreAsLong = random_en(8)
            page.input_field(23, avgScoreAsLong, '')
        with allure.step('Ввести в поле "Результаты освоения ОП" 8 символов'):
            marks = random_en(8)
            page.input_field(24, marks, '')
        with allure.step('Ввести в поле "Индивидуальные достижения" 8 символов'):
            individualAchievements = random_en(8)
            page.input_textarea(5, individualAchievements, '')
        with allure.step('Ввести в поле "Общежитие" 8 символов'):
            needDorm = random_en(8)
            page.input_field(25, needDorm, '')
        with allure.step('Ввести в поле "Ближайшие родственники" 8 символов'):
            relatives = random_en(8)
            page.input_field(26, relatives, '')
        with allure.step('Ввести в поле "Основной иностранный язык" 8 символов'):
            foreignLanguage = random_en(8)
            page.input_field(27, foreignLanguage, '')
        with allure.step('Ввести в поле "Файл скан-копии удостоверения личности" 8 символов'):
            identityCardPagesScans = random_en(8)
            page.input_textarea(6, identityCardPagesScans, '')
        with allure.step('Ввести в поле "Файл скан-копии другого удостоверения личности" 8 символов'):
            otherIdentityCardForPassExamsPagesScans = random_en(8)
            page.input_textarea(7, otherIdentityCardForPassExamsPagesScans, '')
        with allure.step('Ввести в поле "Файл скан-копии документа об образовании" 8 символов'):
            educationDocumentTitlePageScans = random_en(8)
            page.input_textarea(8, educationDocumentTitlePageScans, '')
        with allure.step('Ввести в поле "Файл скан-копии СНИЛС" 8 символов'):
            snilsScans = random_en(8)
            page.input_textarea(9, snilsScans, '')
        with allure.step('Ввести в поле "Файл скан-копии подтверждающих индивидуальные достижения" 8 символов'):
            individualAchievementsScans = random_en(8)
            page.input_textarea(10, individualAchievementsScans, '')
        with allure.step('Ввести в поле "Файл скан-копии льготы" 8 символов'):
            benefitForDormScans = random_en(8)
            page.input_textarea(11, benefitForDormScans, '')
        with allure.step('Ввести в поле "Файл скан-копии согласия на обработку персональных данных" 8 символов'):
            consentToPersonalDataScans = random_en(8)
            page.input_textarea(12, consentToPersonalDataScans, '')
        with allure.step('Ввести в поле "Файл скан-копии заявления" 8 символов'):
            applicationScans = random_en(8)
            page.input_textarea(13, applicationScans, '')
        with allure.step('Ввести в поле "Файл скан-копии заявления о согласии на зачисление" 8 символов'):
            consentToEnrollmentScans = random_en(8)
            page.input_textarea(14, consentToEnrollmentScans, '')
        with allure.step('Ввести в поле "Подача согласия на зачисление" 8 символов'):
            submittingConsentForEnrollment = random_en(8)
            page.input_textarea(15, submittingConsentForEnrollment, '')
        with allure.step('Ввести в поле "Скан-копия заявления о согласии на зачисление" 8 символов'):
            enrollmentConsentStatementScan = random_en(8)
            page.input_field(28, enrollmentConsentStatementScan, '')
        with allure.step('Ввести в поле "Подача отказа от зачисления" 8 символов'):
            submittingRefusalToEnroll = random_en(8)
            page.input_textarea(16, submittingRefusalToEnroll, '')
        with allure.step('Ввести в поле "Скан-копия заявления об отказе от зачисления" 8 символов'):
            enrollmentRefusalApplicationScan = random_en(8)
            page.input_field(29, enrollmentRefusalApplicationScan, '')
        with allure.step('Ввести в поле "Отзыв заявления" 8 символов'):
            withdrawalOfApplication = random_en(8)
            page.input_textarea(17, withdrawalOfApplication, '')
        with allure.step('Ввести в поле "Скан-копия заявления об отзыве заявления" 8 символов'):
            applicationToWithdrawApplicationScan = random_en(8)
            page.input_field(30, applicationToWithdrawApplicationScan, '')
        with allure.step('Открыть настройки "Подсказки для абитуриента"'):
            page.open_setting_pk('Tips_for_applicant')
        with allure.step('Проверка записи в setting.tips_for_applicant'):
            page.check_records_TipsForApplicant(title_pk, haveEduLevel, wantRequestType, originalReturnWay,
                                                stateExamCodes, agreeEntranceExams, haveDisability, haveOlympiad,
                                                exclusiveRights, haveAgreementTargetEdu, competitionParams,
                                                competitions, competitionPriorities, cardType, citizenship,
                                                issuancePlace, birthPlace, regAddress, factAddress, snilsNumber,
                                                eduOrganizationCountry, eduOrganizationSettlement, eduOrganization,
                                                eduDocumentKind, seriaEduDocument, mark3, mark4, mark5, avgMarkAsLong,
                                                avgScoreAsLong, marks, individualAchievements, needDorm, relatives,
                                                foreignLanguage, identityCardPagesScans,
                                                otherIdentityCardForPassExamsPagesScans,
                                                educationDocumentTitlePageScans, snilsScans,
                                                individualAchievementsScans, benefitForDormScans,
                                                consentToPersonalDataScans, applicationScans,
                                                consentToEnrollmentScans, submittingConsentForEnrollment,
                                                enrollmentConsentStatementScan, submittingRefusalToEnroll,
                                                enrollmentRefusalApplicationScan, withdrawalOfApplication,
                                                applicationToWithdrawApplicationScan)

    def testing_of_the_RC_settings_abitur_entrant_list(self, browser):
        type_pk = 'Прием на обучение по программам среднего профессионального образования'
        title_org = random_en(8)
        title_pk = random_en(8)
        system = random_en(8)
        campaign_id = random_number(10)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
                with allure.step('Добавить ПК в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(campaign_id, title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Приемная кампания"'):
            page.open_window_setting('Приемная кампания')
        with allure.step('Выбрать ранее созданную ОО'):
            page.select_field('Настройки ОО', title_org)
        with allure.step('Выбрать ранее созданную ПК'):
            page.select_field('Настройки ПК', title_pk)
        with allure.step('Открыть настройки "Списки поступающих"'):
            page.open_setting_pk('EntrantLists')
        with allure.step('Установить тип поля "Раздел «Списки поступающих»" в "Не отображать"'):
            page.select_radiobutton_EntrantLists(EntrantLists.ENTRANT_LISTS_SECTION, EntrantLists.DISABLE)
        with allure.step('Установить тип поля "Раздел «Списки поступающих»" в "Отображать"'):
            ENTRANT_LISTS_SECTION = EntrantLists.ENABLE
            page.select_radiobutton_EntrantLists(EntrantLists.ENTRANT_LISTS_SECTION, ENTRANT_LISTS_SECTION)
        with allure.step('Установить тип поля "Образовательные программы в списке" в "Не отображать"'):
            EDUCATIONAL_PROGRAMS_ENTRANT_LISTS = EntrantLists.DISABLE
            page.select_radiobutton_EntrantLists(EntrantLists.EDUCATIONAL_PROGRAMS_ENTRANT_LISTS,
                                                 EDUCATIONAL_PROGRAMS_ENTRANT_LISTS)
        with allure.step('Установить тип поля "Образовательные программы на карточке" в "Не отображать"'):
            EDUCATIONAL_PROGRAMS_ENTRANT_CARD = EntrantLists.DISABLE
            page.select_radiobutton_EntrantLists(EntrantLists.EDUCATIONAL_PROGRAMS_ENTRANT_CARD,
                                                 EDUCATIONAL_PROGRAMS_ENTRANT_CARD)
        with allure.step('Установить тип поля "Отображать вместо ФИО абитуриента СНИЛС" в "Не отображать"'):
            SNILS_OR_UNIQUE_NUMBER = EntrantLists.DISABLE
            page.select_radiobutton_EntrantLists(EntrantLists.SNILS_OR_UNIQUE_NUMBER, SNILS_OR_UNIQUE_NUMBER)
        with allure.step('Установить тип поля "Приоритет выбранного конкурса" в "Не отображать"'):
            COMPETITION_PRIORITY = EntrantLists.DISABLE
            page.select_radiobutton_EntrantLists(EntrantLists.COMPETITION_PRIORITY, COMPETITION_PRIORITY)
        with allure.step('Установить тип поля "Списки по виду ЦП" в "Не отображать"'):
            TARGETED_ADMISSION = EntrantLists.DISABLE
            page.select_radiobutton_EntrantLists(EntrantLists.TARGETED_ADMISSION, TARGETED_ADMISSION)
        with allure.step('Установить тип поля "Основание для приема без ВИ" в "Не отображать"'):
            WITHOUT_ENTRANCE_EXAMINATIONS = EntrantLists.DISABLE
            page.select_radiobutton_EntrantLists(EntrantLists.WITHOUT_ENTRANCE_EXAMINATIONS,
                                                 WITHOUT_ENTRANCE_EXAMINATIONS)
        with allure.step('Установить тип поля "Оригинал документа об образовании" в "Не отображать"'):
            ORIGINAL_EDUCATIONAL_DOCUMENT = EntrantLists.DISABLE
            page.select_radiobutton_EntrantLists(EntrantLists.ORIGINAL_EDUCATIONAL_DOCUMENT,
                                                 ORIGINAL_EDUCATIONAL_DOCUMENT)
        with allure.step('Установить тип поля "Согласие на зачисление" в "Не отображать"'):
            CONSENT_TO_ENROLLMENT = EntrantLists.DISABLE
            page.select_radiobutton_EntrantLists(EntrantLists.CONSENT_TO_ENROLLMENT, CONSENT_TO_ENROLLMENT)
        with allure.step('Установить тип поля "Номер заявления" в "Не отображать"'):
            REQUEST_NUMBER = EntrantLists.DISABLE
            page.select_radiobutton_EntrantLists(EntrantLists.REQUEST_NUMBER, REQUEST_NUMBER)
        with allure.step('Проверка записи в settings.abitur_entrant_list'):
            page.check_records_EntrantLists(title_pk, ENTRANT_LISTS_SECTION, EDUCATIONAL_PROGRAMS_ENTRANT_LISTS,
                                            EDUCATIONAL_PROGRAMS_ENTRANT_CARD, SNILS_OR_UNIQUE_NUMBER,
                                            COMPETITION_PRIORITY, TARGETED_ADMISSION, WITHOUT_ENTRANCE_EXAMINATIONS,
                                            ORIGINAL_EDUCATIONAL_DOCUMENT, CONSENT_TO_ENROLLMENT, REQUEST_NUMBER)

    def testing_of_the_RC_settings_accepting_document_deadlines(self, browser):
        type_pk = 'Прием на обучение по программам бакалавриата, специалитета'
        title_org = random_en(8)
        title_pk = random_en(8)
        system = random_en(8)
        campaign_id = random_number(10)
        REQUEST_PK = RequestType.BACH_SPEC
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую "ОО"'):
                with allure.step('Добавить ОО в external_org_units'):
                    SETTING().add_educational_organization(title_org, None)
                with allure.step('Добавить систему в uni_installations'):
                    SETTING().add_uni_installations(title_org, system, random_en(8), random_en(8), 1)
                with allure.step('Добавить ПК в enrollment_campaigns'):
                    SETTING().add_enrollment_campaigns(campaign_id, title_org, title_pk, random_en(8),
                                                       type_pk, system, 'active')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Приемная кампания"'):
            page.open_window_setting('Приемная кампания')
        with allure.step('Выбрать ранее созданную ОО'):
            page.select_field('Настройки ОО', title_org)
        with allure.step('Выбрать ранее созданную ПК'):
            page.select_field('Настройки ПК', title_pk)
        with allure.step('Открыть настройки "Сроки приема заявлений"'):
            page.open_setting_pk('DocumentDeadlines')
        with allure.step('Выбрать валидную дату начала в поле "Бакалавриат/Очноя/По договору"'):
            FORM_PK1 = DocumentDeadlines.OCHNAYA
            TYPE_PK1 = DocumentDeadlines.PO_DOGOVORU
            STARTS_AT1 = datetime.datetime(now.year, now.month, now.day, 0, 0)
            page.input_date_DocumentDeadlines('Очная', 'По договору', 'start')
        with allure.step('Выбрать валидную дату окончания в поле "Бакалавриат/Очноя/В рамках КЦП"'):
            ENDS_AT1 = datetime.datetime(now.year, now.month, now.day, 0, 0)
            page.input_date_DocumentDeadlines('Очная', 'По договору', 'end')
        with allure.step('Проверка записи в abitur.accepting_document_deadlines'):
            page.check_records_DocumentDeadlines(title_pk, REQUEST_PK, FORM_PK1, TYPE_PK1, STARTS_AT1, ENDS_AT1)
        with allure.step('Выбрать валидную дату начала в поле "Бакалавриат/Очно-заочная/В рамках КЦП"'):
            FORM_PK2 = DocumentDeadlines.OCHNO_ZAOCHNAYA
            TYPE_PK2 = DocumentDeadlines.V_RAMKAH_KCP
            STARTS_AT2 = datetime.datetime(now.year, now.month, now.day, 0, 0)
            page.input_date_DocumentDeadlines('Очно-заочная', 'В рамках КЦП', 'start')
        with allure.step('Выбрать валидную дату окончания в поле "Бакалавриат/Очно-заочная/В рамках КЦП"'):
            ENDS_AT2 = datetime.datetime(now.year, now.month, now.day, 0, 0)
            page.input_date_DocumentDeadlines('Очно-заочная', 'В рамках КЦП', 'end')
        with allure.step('Проверка записи в abitur.accepting_document_deadlines'):
            page.check_records_DocumentDeadlines(title_pk, REQUEST_PK, FORM_PK2, TYPE_PK2, STARTS_AT2, ENDS_AT2)


@pytest.mark.abitur
@allure.epic('Функциональное тестирование панели абитуриента')
@allure.feature('Постусловие для завершения тест-кейсов')
class TestAbitur_POSTCONDITION:
    @staticmethod
    def test_delete_a_RC():
        with allure.step('Удалить записи созданные в ПК (PRECONDITION) таблица online_entrant_requests'):
            ABITUR_DB().delete_record_online_entrant_requests(RECEPTION_COMPANY)
        with allure.step('Удалить все записи в таблице attached_files'):
            ABITUR_DB().delete_all_attached_files()
        with allure.step('Удалить все записи в таблице external_org_contacts'):
            ABITUR_DB().delete_all_external_org_contacts()
        with allure.step('Удалить запись ранее созданной ПК в таблице enrollment_campaigns'):
            SETTING().delete_record_enrollment_campaigns(RECEPTION_COMPANY)
        with allure.step('Удалить запись ранее созданной системы в таблице uni_installations'):
            SETTING().delete_record_uni_installations(SYSTEM)
        with allure.step('Удалить запись ранее созданной ОО в таблице external_org_units'):
            SETTING().delete_record_external_org_units(EDUCATIONAL_ORGANIZATION)
