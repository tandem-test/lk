import pytest
import allure

from .pages.database.db_logic import USERS, ADMINS, ROLES, SETTING, PERMISSION
from .pages.cabinet_page import Profile, Dashboard
from .pages.admin_page import Users, Setting, Authorization
from .pages.precondition import link, password, admin_login, random_en, random_number, Roles
from .pages.authentication_page import RegLogin


@pytest.mark.base
@allure.link(link + '/register', name='ТАНДЕМ ЛК - Форма регистрации')
@allure.epic('Функциональное тестирование модуля "BASE"')
@allure.feature('Тестирование функциональности формы аутентификации')
@allure.feature('Тестирование функциональности формы регистрации')
class TestAuthentication_REGISTRATION:
    @pytest.mark.smoke
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.story('Позитивная проверка формы регистрации')
    def testing_the_registration_form_with_correct_data(self, browser):
        email = random_en(8) + '@test.com'
        last_name = random_en(8)
        first_name = random_en(8)
        middle_name = random_en(8)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Нажать "Регистрация"'):
            page.open_registration_page()
        with allure.step('Ввести в поле "Email" временную почту'):
            page.input_email(email)
        with allure.step('Ввести в поле "Фамилия" 8 символов'):
            page.input_last_name(last_name)
        with allure.step('Ввести в поле "Имя" 8 символов'):
            page.input_name(first_name)
        with allure.step('Ввести в поле "Отчество" 8 символов'):
            page.input_middle_name(middle_name)
        with allure.step('Ввести в поле "Пароль" 8 символов'):
            page.input_password(password)
        with allure.step('Повторно ввести пароль в поле "Пароль еще раз"'):
            page.input_password_confirmation(password)
        with allure.step('Поставить галочку "Даю согласие на обработку"'):
            page.put_a_tick_in_the_checkbox()
        with allure.step('Нажать "Зарегистрироваться"'):
            page.data_validation_check('Correct', 'Registration', '')
        with allure.step('Проверить запись в abitur.users'):
            page.check_records_database(email, last_name, first_name, middle_name)

    @allure.story('Позитивная проверка формы регистрации')
    def testing_the_registration_form_with_only_required_correct_data(self, browser):
        email = random_en(8) + '@test.com'
        last_name = random_en(8)
        first_name = random_en(8)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Нажать "Регистрация"'):
            page.open_registration_page()
        with allure.step('Ввести в поле "Email" временную почту'):
            page.input_email(email)
        with allure.step('Ввести в поле "Фамилия" 8 символов'):
            page.input_last_name(last_name)
        with allure.step('Ввести в поле "Имя" 8 символов'):
            page.input_name(first_name)
        with allure.step('Ввести в поле "Пароль" 8 символов'):
            page.input_password(password)
        with allure.step('Повторить ранее введённые данные в поле "Пароль еще раз"'):
            page.input_password_confirmation(password)
        with allure.step('Поставить галочку "Даю согласие на обработку"'):
            page.put_a_tick_in_the_checkbox()
        with allure.step('Нажать "Зарегистрироваться"'):
            page.data_validation_check('Correct', 'Registration', '')
        with allure.step('Проверить запись в abitur.users'):
            page.check_records_database(email, last_name, first_name, None)

    @allure.story('Позитивная проверка формы регистрации')
    def testing_the_registration_form_with_input_of_limit_values(self, browser):
        email = random_en(246) + '@test.com'
        last_name = random_en(255)
        first_name = random_en(255)
        middle_name = random_en(255)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Нажать "Регистрация"'):
            page.open_registration_page()
        with allure.step('Ввести в поле "Email" 255 символов'):
            page.input_email(email)
        with allure.step('Ввести в поле "Фамилия" 255 символов'):
            page.input_last_name(last_name)
        with allure.step('Ввести в поле "Имя" 255 символов'):
            page.input_name(first_name)
        with allure.step('Ввести в поле "Отчество" 255 символов'):
            page.input_middle_name(middle_name)
        with allure.step('Ввести в поле "Пароль" 255 символов'):
            page.input_password(last_name)
        with allure.step('Повторить ранее введённые данные в поле "Пароль еще раз"'):
            page.input_password_confirmation(last_name)
        with allure.step('Поставить галочку "Даю согласие на обработку"'):
            page.put_a_tick_in_the_checkbox()
        with allure.step('Нажать "Зарегистрироваться"'):
            page.data_validation_check('Correct', 'Registration', '')
        with allure.step('Проверить запись в abitur.users'):
            page.check_records_database(email, last_name, first_name, middle_name)

    @allure.story('Негативная проверка формы регистрации')
    def testing_the_registration_form_with_input_beyond_limit_values(self, browser):
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Нажать "Регистрация"'):
            page.open_registration_page()
        with allure.step('Проверка валидации поля "Email" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Email" 256 символов'):
                page.input_email(random_en(247) + '@test.com')
            with allure.step('Ввести в поле "Фамилия" 8 символов'):
                page.input_last_name(random_en(8))
            with allure.step('Ввести в поле "Имя" 8 символов'):
                page.input_name(random_en(8))
            with allure.step('Ввести в поле "Пароль" 8 символов'):
                page.input_password(password)
            with allure.step('Повторно ввести пароль в поле "Пароль еще раз"'):
                page.input_password_confirmation(password)
            with allure.step('Поставить галочку "Даю согласие на обработку"'):
                page.put_a_tick_in_the_checkbox()
            with allure.step('Нажать "Зарегистрироваться"'):
                page.data_validation_check('Incorrect', 'Registration', '«email» не может превышать')
        with allure.step('Проверка валидации поля "Фамилия" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Email" временную почту'):
                page.input_email(random_en(8) + '@test.com')
            with allure.step('Ввести в поле "Фамилия" 256 символов'):
                page.input_last_name(random_en(256))
            with allure.step('Ввести в поле "Имя" 8 символов'):
                page.input_name(random_en(8))
            with allure.step('Ввести в поле "Пароль" 8 символов'):
                page.input_password(password)
            with allure.step('Повторно ввести пароль в поле "Пароль еще раз"'):
                page.input_password_confirmation(password)
            with allure.step('Нажать "Зарегистрироваться"'):
                page.data_validation_check('Incorrect', 'Registration', '«фамилия» не может превышать')
        with allure.step('Проверка валидации поля "Имя" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Email" временную почту'):
                page.input_email(random_en(8) + '@test.com')
            with allure.step('Ввести в поле "Фамилия" 8 символов'):
                page.input_last_name(random_en(8))
            with allure.step('Ввести в поле "Имя" 256 символов'):
                page.input_name(random_en(256))
            with allure.step('Ввести в поле "Пароль" 8 символов'):
                page.input_password(password)
            with allure.step('Повторно ввести пароль в поле "Пароль еще раз"'):
                page.input_password_confirmation(password)
            with allure.step('Нажать "Зарегистрироваться"'):
                page.data_validation_check('Incorrect', 'Registration', '«имя» не может превышать')
        with allure.step('Проверка валидации поля "Отчество" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Email" временную почту'):
                page.input_email(random_en(8) + '@test.com')
            with allure.step('Ввести в поле "Фамилия" 8 символов'):
                page.input_last_name(random_en(8))
            with allure.step('Ввести в поле "Имя" 8 символов'):
                page.input_name(random_en(8))
            with allure.step('Ввести в поле "Отчество" 256 символов'):
                page.input_middle_name(random_en(256))
            with allure.step('Ввести в поле "Пароль" 8 символов'):
                page.input_password(password)
            with allure.step('Повторно ввести пароль в поле "Пароль еще раз"'):
                page.input_password_confirmation(password)
            with allure.step('Нажать "Зарегистрироваться"'):
                page.data_validation_check('Incorrect', 'Registration', '«отчество» не может превышать')

    @allure.story('Негативная проверка формы регистрации')
    def testing_the_registration_form_with_used_email(self, browser):
        email = random_en(8) + '@test.com'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать новую верифицированную учётную запись'):
                USERS().create_a_new_verified_user(random_en(8), random_en(8), None, email)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        page = RegLogin(browser, link)
        with allure.step('Нажать "Регистрация"'):
            page.open_registration_page()
        with allure.step('Ввести в поле "Email" адрес ранее созданного пользователя(Precondition)'):
            page.input_email(email)
        with allure.step('Ввести в поле "Фамилия" 8 символов'):
            page.input_last_name(random_en(8))
        with allure.step('Ввести в поле "Имя" 8 символов'):
            page.input_name(random_en(8))
        with allure.step('Ввести в поле "Пароль" 8 символов'):
            page.input_password(password)
        with allure.step('Повторить ранее введённые данные в поле "Пароль еще раз"'):
            page.input_password_confirmation(password)
        with allure.step('Поставить галочку "Даю согласие на обработку"'):
            page.put_a_tick_in_the_checkbox()
        with allure.step('Нажать "Зарегистрироваться"'):
            page.data_validation_check('Incorrect', 'Registration', '«email» уже существует')

    @allure.story('Негативная проверка формы регистрации')
    def testing_the_registration_form_with_different_passwords_used(self, browser):
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Нажать "Регистрация"'):
            page.open_registration_page()
        with allure.step('Подготовка формы для выполнения тестирования'):
            with allure.step('Ввести в поле "Email" временную почту'):
                page.input_email(random_en(8) + '@test.com')
            with allure.step('Ввести в поле "Фамилия" 8 символов'):
                page.input_last_name(random_en(8))
            with allure.step('Ввести в поле "Имя" 8 символов'):
                page.input_name(random_en(8))
            with allure.step('Поставить галочку "Даю согласие на обработку"'):
                page.put_a_tick_in_the_checkbox()
        with allure.step('Ввести в поле "Пароль" 8 символов'):
            page.input_password(random_en(8))
        with allure.step('Ввести в поле "Пароль еще раз" 8 символов'):
            page.input_password_confirmation(random_en(8))
        with allure.step('Нажать "Зарегистрироваться"'):
            page.data_validation_check('Incorrect', 'Registration', '«пароль» не совпадает')
        with allure.step('Ввести в поле "Пароль" 7 символов'):
            page.input_password(random_en(7))
        with allure.step('Ввести в поле "Пароль еще раз" 7 символов'):
            page.input_password_confirmation(random_en(7))
        with allure.step('Нажать "Зарегистрироваться"'):
            page.data_validation_check('Incorrect', 'Registration', ' «пароль» должно быть не меньше')

    @allure.story('Позитивная проверка формы регистрации')
    def testing_the_registration_form_with_special_characters(self, browser):
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Нажать "Регистрация"'):
            page.open_registration_page()
        with allure.step('Проверка записи формы при вводе спецсимвола "/"'):
            email_1 = random_en(8) + '@test.com'
            symbol_1 = '////////'
            with allure.step('Ввести в поле "Email" временную почту'):
                page.input_email(email_1)
            with allure.step('Ввести в поле "Фамилия" спецсимвол'):
                page.input_last_name(symbol_1)
            with allure.step('Ввести в поле "Имя" спецсимвол'):
                page.input_name(symbol_1)
            with allure.step('Ввести в поле "Отчество" спецсимвол'):
                page.input_middle_name(symbol_1)
            with allure.step('Ввести в поле "Пароль" спецсимвол"'):
                page.input_password(symbol_1)
            with allure.step('Повторно ввести пароль в поле "Пароль еще раз"'):
                page.input_password_confirmation(symbol_1)
            with allure.step('Поставить галочку "Даю согласие на обработку"'):
                page.put_a_tick_in_the_checkbox()
            with allure.step('Нажать "Зарегистрироваться"'):
                page.data_validation_check('Correct', 'Registration', '')
            with allure.step('Проверить запись в abitur.users'):
                page.check_records_database(email_1, symbol_1, symbol_1, symbol_1)
        with allure.step('Нажать кнопку "Выйти" (окно верификации)'):
            page.exit_the_verification_window()
        page = RegLogin(browser, link)
        with allure.step('Нажать "Регистрация"'):
            page.open_registration_page()
        with allure.step('Проверка записи формы при вводе XSS инъекции "<script>alert()</script>"'):
            email_2 = random_en(8) + '@test.com'
            symbol_2 = '<script>alert()</script>'
            with allure.step('Ввести в поле "Email" временную почту'):
                page.input_email(email_2)
            with allure.step('Ввести в поле "Фамилия" спецсимвол'):
                page.input_last_name(symbol_2)
            with allure.step('Ввести в поле "Имя" спецсимвол'):
                page.input_name(symbol_2)
            with allure.step('Ввести в поле "Отчество" спецсимвол'):
                page.input_middle_name(symbol_2)
            with allure.step('Ввести в поле "Пароль" спецсимвол'):
                page.input_password(symbol_2)
            with allure.step('Повторно ввести пароль в поле "Пароль еще раз"'):
                page.input_password_confirmation(symbol_2)
            with allure.step('Поставить галочку "Даю согласие на обработку"'):
                page.put_a_tick_in_the_checkbox()
            with allure.step('Нажать "Зарегистрироваться"'):
                page.data_validation_check('Correct', 'Registration', '')
            with allure.step('Проверить запись в abitur.users'):
                page.check_records_database(email_2, symbol_2, symbol_2, symbol_2)
        with allure.step('Нажать кнопку "Выйти" (окно верификации)'):
            page.exit_the_verification_window()
        page = RegLogin(browser, link)
        with allure.step('Нажать "Регистрация"'):
            page.open_registration_page()
        with allure.step('Проверка записи формы при вводе спецсимвола "@"'):
            email_3 = random_en(8) + '@test.com'
            symbol_3 = '@@@@@@@@'
            with allure.step('Ввести в поле "Email" временную почту'):
                page.input_email(email_3)
            with allure.step('Ввести в поле "Фамилия" спецсимвол'):
                page.input_last_name(symbol_3)
            with allure.step('Ввести в поле "Имя" спецсимвол'):
                page.input_name(symbol_3)
            with allure.step('Ввести в поле "Отчество" спецсимвол'):
                page.input_middle_name(symbol_3)
            with allure.step('Ввести в поле "Пароль" спецсимвол'):
                page.input_password(symbol_3)
            with allure.step('Повторно ввести пароль в поле "Пароль еще раз"'):
                page.input_password_confirmation(symbol_3)
            with allure.step('Поставить галочку "Даю согласие на обработку"'):
                page.put_a_tick_in_the_checkbox()
            with allure.step('Нажать "Зарегистрироваться"'):
                page.data_validation_check('Correct', 'Registration', '')
            with allure.step('Проверить запись в abitur.users'):
                page.check_records_database(email_3, symbol_3, symbol_3, symbol_3)
        with allure.step('Нажать кнопку "Выйти" (окно верификации)'):
            page.exit_the_verification_window()
        page = RegLogin(browser, link)
        with allure.step('Нажать "Регистрация"'):
            page.open_registration_page()
        with allure.step('Проверка записи формы при вводе спецсимволов "!@#$%^&*()_+-"'):
            email_4 = random_en(8) + '@test.com'
            symbol_4 = "!@#$%^&*()_+-"
            with allure.step('Ввести в поле "Email" временную почту'):
                page.input_email(email_4)
            with allure.step('Ввести в поле "Фамилия" спецсимвол'):
                page.input_last_name(symbol_4)
            with allure.step('Ввести в поле "Имя" спецсимвол'):
                page.input_name(symbol_4)
            with allure.step('Ввести в поле "Отчество" спецсимвол'):
                page.input_middle_name(symbol_4)
            with allure.step('Ввести в поле "Пароль" спецсимвол'):
                page.input_password(symbol_4)
            with allure.step('Повторно ввести пароль в поле "Пароль еще раз"'):
                page.input_password_confirmation(symbol_4)
            with allure.step('Поставить галочку "Даю согласие на обработку"'):
                page.put_a_tick_in_the_checkbox()
            with allure.step('Нажать "Зарегистрироваться"'):
                page.data_validation_check('Correct', 'Registration', '')
            with allure.step('Проверить запись в abitur.users'):
                page.check_records_database(email_4, symbol_4, symbol_4, symbol_4)
        with allure.step('Нажать кнопку "Выйти" (окно верификации)'):
            page.exit_the_verification_window()
        page = RegLogin(browser, link)
        with allure.step('Нажать "Регистрация"'):
            page.open_registration_page()
        with allure.step('Проверка записи формы при вводе спецсимвола "اللغة العربية"'):
            email_5 = random_en(8) + '@test.com'
            symbol_5 = "اللغة العربية"
            with allure.step('Ввести в поле "Email" временную почту'):
                page.input_email(email_5)
            with allure.step('Ввести в поле "Фамилия" спецсимвол'):
                page.input_last_name(symbol_5)
            with allure.step('Ввести в поле "Имя" спецсимвол'):
                page.input_name(symbol_5)
            with allure.step('Ввести в поле "Отчество" спецсимвол'):
                page.input_middle_name(symbol_5)
            with allure.step('Ввести в поле "Пароль" спецсимвол'):
                page.input_password(symbol_5)
            with allure.step('Повторно ввести пароль в поле "Пароль еще раз"'):
                page.input_password_confirmation(symbol_5)
            with allure.step('Поставить галочку "Даю согласие на обработку"'):
                page.put_a_tick_in_the_checkbox()
            with allure.step('Нажать "Зарегистрироваться"'):
                page.data_validation_check('Correct', 'Registration', '')
            with allure.step('Проверить запись в abitur.users'):
                page.check_records_database(email_5, symbol_5, symbol_5, symbol_5)

    @allure.story('Позитивная проверка формы регистрации')
    def testing_the_functionality_of_sending_a_new_email_with_verification(self, browser):
        box = random_en(8) + '@test.com'
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Нажать "Регистрация"'):
            page.open_registration_page()
        with allure.step('Ввести в поле "Email" временную почту'):
            page.input_email(box)
        with allure.step('Ввести в поле "Фамилия" 8 символов'):
            page.input_last_name(random_en(8))
        with allure.step('Ввести в поле "Имя" 8 символов'):
            page.input_name(random_en(8))
        with allure.step('Ввести в поле "Пароль" 8 символов'):
            page.input_password(password)
        with allure.step('Повторить ранее введённые данные в поле "Пароль еще раз"'):
            page.input_password_confirmation(password)
        with allure.step('Поставить галочку "Даю согласие на обработку"'):
            page.put_a_tick_in_the_checkbox()
        with allure.step('Нажать "Зарегистрироваться"'):
            page.data_validation_check('Correct', 'Registration', '')
        with allure.step('Нажать на кнопку "Отправить новую ссылку"'):
            page.send_new_link_the_verification_window()


@pytest.mark.base
@allure.link(link + '/login', name='ТАНДЕМ ЛК - Форма авторизации')
@allure.epic('Функциональное тестирование модуля "BASE"')
@allure.feature('Тестирование функциональности формы аутентификации')
@allure.feature('Тестирование функциональности формы авторизации')
class TestAuthentication_LOGIN:
    @pytest.mark.smoke
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.story('Позитивная проверка формы')
    def testing_the_authorization_form_with_correct_data(self, browser):
        email = random_en(8) + '@test.com'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать новую верифицированную учётную запись'):
                USERS().create_a_new_verified_user(random_en(8), random_en(8), None, email)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition)'):
            page.input_email(email)
        with allure.step('Ввести в поле "Пароль" (Precondition)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')

    @allure.story('Позитивная проверка формы')
    def testing_the_authorization_form_with_input_of_limit_values(self, browser):
        email = random_en(246) + '@test.com'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать новую верифицированную учётную запись с предельными значениями'):
                USERS().create_a_new_verified_user(random_en(255), random_en(255), None, email)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" ранее зарегистрированный адрес'):
            page.input_email(email)
        with allure.step('Ввести в поле "Пароль" ранее зарегистрированный пароль'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')

    @allure.story('Негативная проверка формы')
    def testing_the_authorization_form_with_incorrect_data(self, browser):
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" несуществующий адрес'):
            page.input_email(random_en(8) + '@test.com')
        with allure.step('Ввести в поле "Пароль" 8 символов'):
            page.input_password(random_en(8))
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Incorrect', 'Login', '')
        with allure.step('Ввести в поле "E-mail" существующий адрес'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" некорректный пароль'):
            page.input_password(random_en(8))
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Incorrect', 'Login', 'Неправильное имя пользователя или пароль')

    @allure.story('Негативная проверка формы')
    def testing_the_authorization_form_with_input_beyond_limit_values(self, browser):
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" 256 символов'):
            page.input_email(random_en(247) + '@test.com')
        with allure.step('Ввести в поле "Пароль" 256 символов'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Incorrect', 'Login', 'Неправильное имя пользователя или пароль.')


@pytest.mark.base
@allure.link(link + '/support', name='ТАНДЕМ ЛК - Форма поддержки')
@allure.epic('Функциональное тестирование модуля "BASE"')
@allure.feature('Тестирование функциональности формы аутентификации')
@allure.feature('Тестирование функциональности формы поддержки')
class TestAuthentication_SUPPORT:
    @allure.story('Позитивная проверка формы')
    def testing_the_support_form_with_valid_data(self, browser):
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Включить виджет "Поддержка пользователей"'):
                SETTING().update_settings_app('support_active', '1')
                SETTING().update_settings_app('support_link_name', 'Поддержка')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Открыть форму поддержки'):
            page.open_support_page()
        with allure.step('Ввести в поле "Тема обращения" 8 символов'):
            page.input_title(random_en(8), None)
        with allure.step('Ввести в поле "Ваш E-mail" валидный адрес'):
            page.input_reply_email(random_en(8) + '@test.com', None)
        with allure.step('Ввести в поле "Текст обращения" 8 символов'):
            page.input_content(random_en(8), None)
        with allure.step('Нажать конпку "Отправить"'):
            page.click_send_button('Correct', '')

    @allure.story('Позитивная проверка формы')
    def testing_the_support_form_with_limit_values(self, browser):
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Включить виджет "Поддержка пользователей"'):
                SETTING().update_settings_app('support_active', '1')
                SETTING().update_settings_app('support_link_name', 'Поддержка')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Открыть форму поддержки'):
            page.open_support_page()
        with allure.step('Ввести в поле "Тема обращения" 191 символ'):
            page.input_title(random_en(191), None)
        with allure.step('Ввести в поле "Ваш E-mail" 191 символ'):
            page.input_reply_email(random_en(182) + '@test.com', None)
        with allure.step('Ввести в поле "Текст обращения" 2048 символов'):
            page.input_content(random_en(2048), None)
        with allure.step('Нажать конпку "Отправить"'):
            page.click_send_button('Correct', '')

    @allure.story('Негативная проверка формы')
    def testing_the_support_form_with_beyond_limit_values(self, browser):
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Включить виджет "Поддержка пользователей"'):
                SETTING().update_settings_app('support_active', '1')
                SETTING().update_settings_app('support_link_name', 'Поддержка')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Открыть форму поддержки'):
            page.open_support_page()
        with allure.step('Проверка валидации поля "Тема обращения" на ввод заграничных значений'):
            with allure.step('Ввести в поле "Тема обращения" 192 символов'):
                page.input_title(random_en(192), None)
            with allure.step('Нажать конпку "Отправить"'):
                page.click_send_button('Incorrect', '«тема обращения» не может превышать')
            with allure.step('Вернуть поле в исхдное состояние'):
                page.input_title(random_en(8), 'yes')
        with allure.step('Проверка валидации поля "Ваш E-mail" на ввод заграничных значений'):
            with allure.step('Ввести в поле "Ваш E-mail" 192 символов'):
                page.input_reply_email(random_en(183) + '@test.com', None)
            with allure.step('Нажать конпку "Отправить"'):
                page.click_send_button('Incorrect', '«ваш e-mail» не может превышать')
            with allure.step('Вернуть поле в исхдное состояние'):
                page.input_reply_email(random_en(8) + '@test.com', 'yes')
        with allure.step('Проверка валидации поля "Текст обращения" на ввод заграничных значений'):
            with allure.step('Ввести в поле "Текст обращения" 2049 символов'):
                page.input_content(random_en(2049), None)
            with allure.step('Нажать конпку "Отправить"'):
                page.click_send_button('Incorrect', '«текст обращения» не может превышать')
            with allure.step('Вернуть поле в исхдное состояние'):
                page.input_content(random_en(8), 'yes')

    @allure.story('Негативная проверка формы')
    def testing_the_support_form_with_an_empty_value(self, browser):
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Включить виджет "Поддержка пользователей"'):
                SETTING().update_settings_app('support_active', '1')
                SETTING().update_settings_app('support_link_name', 'Поддержка')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Открыть форму поддержки'):
            page.open_support_page()
        with allure.step('Проверка валидации обязательного поля "Тема обращения"'):
            with allure.step('Оставить поле пустым'):
                page.input_title(' ', None)
            with allure.step('Нажать конпку "Отправить"'):
                page.click_send_button('Incorrect', '«тема обращения» обязательно')
            with allure.step('Вернуть поле в исхдное состояние'):
                page.input_title(random_en(8), 'yes')
        with allure.step('Проверка валидации обязательного поля "Ваш E-mail"'):
            with allure.step('Оставить поле пустым'):
                page.input_reply_email(' ', None)
            with allure.step('Нажать конпку "Отправить"'):
                page.click_send_button('Incorrect', '«ваш e-mail» обязательно')
            with allure.step('Вернуть поле в исхдное состояние'):
                page.input_reply_email(random_en(8) + '@test.com', 'yes')
        with allure.step('Проверка валидации обязательного поля "Текст обращения"'):
            with allure.step('Оставить поле пустым'):
                page.input_content(' ', None)
            with allure.step('Нажать конпку "Отправить"'):
                page.click_send_button('Incorrect', '«текст обращения» обязательно')
            with allure.step('Вернуть поле в исхдное состояние'):
                page.input_content(random_en(8), 'yes')

    @allure.story('Негативная проверка формы')
    def testing_the_support_form_with_an_invalid_address(self, browser):
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Включить виджет "Поддержка пользователей"'):
                SETTING().update_settings_app('support_active', '1')
                SETTING().update_settings_app('support_link_name', 'Поддержка')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Открыть форму поддержки'):
            page.open_support_page()
        with allure.step('Ввести в поле "Тема обращения" 8 символов'):
            page.input_title(random_en(8), None)
        with allure.step('Ввести в поле "Текст обращения" 8 символов'):
            page.input_content(random_en(8), None)
        with allure.step('Проверка валидации поля "Ваш E-mail" на ввод некорректного значения'):
            with allure.step('Ввести в поле 8 символов'):
                page.input_reply_email(random_en(8), None)
            with allure.step('Нажать конпку "Отправить"'):
                page.click_send_button('Incorrect', '«ваш e-mail» должно быть действительным')
            with allure.step('Ввести в поле 8 символов + "@"'):
                page.input_reply_email(random_en(8) + '@', None)
            with allure.step('Нажать конпку "Отправить"'):
                page.click_send_button('Incorrect', '«ваш e-mail» должно быть действительным')


@pytest.mark.base
@allure.epic('Функциональное тестирование модуля "BASE"')
@allure.feature('Функциональное тестирование панели администрирования')
@allure.feature('Проверка функциональности раздела "Авторизация"')
class TestAdministration_AUTHORIZATION:
    @allure.link(link + '/admin/authorization/permissions', name='ТАНДЕМ ЛК - Разрешения')
    @allure.story('Проверка функциональности вкладки "Разрешения"')
    def testing_the_functionality_of_displaying_the_previously_created_permission(self, browser):
        permission = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Добавить новое разрешение в abitur.permissions'):
                PERMISSION().add_new_permission(permission, permission)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Authorization(browser, link)
        with allure.step('Открыть вкладку "Разрешения"'):
            page.open_authorization_window("Разрешения")
        with allure.step('Найти ранее созданную запись'):
            page.search_permission(permission)
        with allure.step('Удалить ранее созданную запись в abitur.permissions'):
            PERMISSION().deleted_new_permission(permission)

    @allure.link(link + '/admin/authorization/permissions', name='ТАНДЕМ ЛК - Разрешения')
    @allure.story('Проверка функциональности вкладки "Разрешения"')
    @pytest.mark.xfail(reason="Ошибка функциональности вкладки Роли при добавление новой записи")
    def testing_the_functionality_of_assigning_a_new_permission_to_a_role(self, browser):
        permission = random_en(8)
        role = 'student'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Добавить новое разрешение в abitur.permissions'):
                PERMISSION().add_new_permission(permission, permission)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Authorization(browser, link)
        with allure.step('Открыть вкладку "Роли"'):
            page.open_authorization_window("Роли")
        with allure.step('Нажать конпку "Редактировать" напротив роли "Студент"'):
            page.edit_role_permissions(permission, role)
        with allure.step('Включить ранее созданное разрешение'):
            page.select_checkbox_permissions(permission)
        with allure.step('Нажать кнопку "Вернуться к списку"'):
            page.back_to_table_roles()
        with allure.step('Проверка записи разрешения в abitur.model_has_roles'):
            page.check_record_permission_in_role(permission, role)


@pytest.mark.base
@allure.link(link + '/admin/users', name='ТАНДЕМ Личный кабинет')
@allure.epic('Функциональное тестирование модуля "BASE"')
@allure.feature('Функциональное тестирование панели администрирования')
@allure.feature('Проверка функциональности раздела "Пользователи"')
class TestAdministration_USERS:
    @allure.story('Проверка функциональности вкладки "Пользователь"')
    def testing_the_validity_of_the_name_and_data_entered_during_registration(self, browser):
        last_name = random_en(8)
        first_name = random_en(8)
        middle_name = random_en(8)
        search = last_name + ' ' + first_name + ' ' + middle_name
        email = random_en(8) + '@test.com'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую верифицированную учётную запись'):
                USERS().create_a_new_verified_user(last_name, first_name, middle_name, email)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Users(browser, link)
        with allure.step('Открыть вкладку "Пользователи"'):
            page.open_window_users()
        with allure.step('Открыть ранее созданного пользователя'):
            page.search_user(search, 'OPEN', email)

    @allure.story('Проверка функциональности вкладки "Пользователь"')
    def testing_login_functionality_under_a_previously_created_user(self, browser):
        last_name = random_en(8)
        first_name = random_en(8)
        middle_name = random_en(8)
        search = last_name + ' ' + first_name + ' ' + middle_name
        email = random_en(8) + '@test.com'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую верифицированную учётную запись'):
                USERS().create_a_new_verified_user(last_name, first_name, middle_name, email)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Users(browser, link)
        with allure.step('Открыть вкладку "Пользователи"'):
            page.open_window_users()
        with allure.step('Открыть ранее созданного пользователя'):
            page.search_user(search, 'OPEN', email)
        with allure.step('Нажать кнопку "Войти под пользователем"'):
            page.login_as_user()

    @allure.story('Проверка функциональности вкладки "Пользователь"')
    def testing_the_functionality_of_assigning_administrator_rights_to_a_new_user(self, browser):
        last_name = random_en(8)
        first_name = random_en(8)
        middle_name = random_en(8)
        search = last_name + ' ' + first_name + ' ' + middle_name
        email = random_en(8) + '@test.com'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую верифицированную учётную запись'):
                USERS().create_a_new_verified_user(last_name, first_name, middle_name, email)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Users(browser, link)
        with allure.step('Открыть вкладку "Пользователи"'):
            page.open_window_users()
        with allure.step('Открыть ранее созданного пользователя'):
            page.search_user(search, 'OPEN', email)
        with allure.step('Включить чекбокс "Администратор"'):
            page.assign_administration_rule()
        with allure.step('Проверить запись в abitur.admins'):
            page.check_record_admins_rule(email)

    @allure.story('Проверка функциональности вкладки "Роли и разрешения"')
    def testing_the_functionality_of_issuing_and_deleting_a_role(self, browser):
        last_name = random_en(8)
        first_name = random_en(8)
        middle_name = random_en(8)
        search = last_name + ' ' + first_name + ' ' + middle_name
        role = 'student'
        email_student = random_en(8) + '@test.com'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Создать новую верифицированную учётную запись'):
                USERS().create_a_new_verified_user(last_name, first_name, middle_name, email_student)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Users(browser, link)
        with allure.step('Открыть вкладку "Пользователи"'):
            page.open_window_users()
        with allure.step('Открыть ранее созданного пользователя (Precondition)'):
            page.search_user(search, 'OPEN', email_student)
        with allure.step('Открыть вкладку "Роли и разрешения"'):
            page.open_tubs('Роли и разрешения')
        with allure.step('Нажать кнопку "Добавить"'):
            page.click_add_button('OPEN', 'Роли')
        with allure.step('Выбрать роль "Студент"'):
            page.select_role(ROLES().get_id_by_name(role))
        with allure.step('Нажать кнопку "Добавить"'):
            page.click_add_button('CLOSE', 'Роли')
        with allure.step('Проверить запись в abitur.model_has_roles'):
            ROLES().get_data_on_role_assignment_by_model_id(email_student)
        with allure.step('Нажать кнопку "Удалить" у ранее прикреплённой роли'):
            page.click_delete_button(0, 'Роли')

    @pytest.mark.xfail(reason='Визуальная ошибка удаления роли у пользователя в панеле администрирования')
    @allure.story('Проверка функциональности вкладки "Роли и разрешения"')
    def testing_the_functionality_of_issuing_multiple_roles_and_removing_them(self, browser):
        last_name = random_en(8)
        first_name = random_en(8)
        middle_name = random_en(8)
        search = last_name + ' ' + first_name + ' ' + middle_name
        email_multi = random_en(8) + '@test.com'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Подготовка учетной записи для выполнения тестирования'):
                with allure.step('Создать новую верифицированную учётную запись'):
                    USERS().create_a_new_verified_user(last_name, first_name, middle_name, email_multi)
                with allure.step('Назначить роль "Сотрудник"'):
                    ROLES().assign_role_to_user(email_multi, Roles.employee)
                with allure.step('Назначить роль "Родитель"'):
                    ROLES().assign_role_to_user(email_multi, Roles.parent)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Users(browser, link)
        with allure.step('Открыть вкладку "Пользователи"'):
            page.open_window_users()
        with allure.step('Открыть ранее созданного пользователя (Precondition)'):
            page.search_user(search, 'OPEN', email_multi)
        with allure.step('Открыть вкладку "Роли и разрешения"'):
            page.open_tubs('Роли и разрешения')
        with allure.step('Нажать кнопку "Удалить" у ранее прикреплённой роли'):
            page.click_delete_button(1, 'Роли')
        with allure.step('Нажать кнопку "Удалить" у ранее прикреплённой роли'):
            page.click_delete_button(0, 'Роли')

    @allure.story('Проверка функциональности вкладки "Роли и разрешения"')
    def testing_the_functionality_of_adding_and_deleting_rights_to_a_user(self, browser):
        last_name = random_en(8)
        first_name = random_en(8)
        middle_name = random_en(8)
        search = last_name + ' ' + first_name + ' ' + middle_name
        email_user = random_en(8) + '@test.com'
        permission = 'view-orders'  # Просмотр приказов
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Подготовка учетной записи для выполнения тестирования'):
                with allure.step('Создать новую верифицированную учётную запись'):
                    USERS().create_a_new_verified_user(last_name, first_name, middle_name, email_user)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Users(browser, link)
        with allure.step('Открыть вкладку "Пользователи"'):
            page.open_window_users()
        with allure.step('Открыть ранее созданного пользователя (Precondition)'):
            page.search_user(search, 'OPEN', email_user)
        with allure.step('Открыть вкладку "Роли и разрешения"'):
            page.open_tubs('Роли и разрешения')
        with allure.step('Открыть вкладку "Разрешения"'):
            page.open_tab_window('Разрешения')
        with allure.step('Нажать кнопку "Добавить"'):
            page.click_add_button('OPEN', 'Разрешения')
        with allure.step('Выбрать разрешение "Просмотр приказов"'):
            page.select_permission(PERMISSION().get_id_permission_by_name(permission))
        with allure.step('Нажать кнопку "Добавить"'):
            page.click_add_button('CLOSE', 'Разрешения')
        with allure.step('Проверить запись в abitur.model_has_permissions'):
            PERMISSION().get_data_on_role_permission_by_model_id(email_user)
        with allure.step('Нажать кнопку "Удалить" у ранее прикрепленного разрешения'):
            page.click_delete_button(0, 'Разрешения')

    @allure.story('Проверка функциональности вкладки "Роли и разрешения"')
    @pytest.mark.xfail(reason='Визуальная ошибка удаления роли у пользователя в панеле администрирования')
    def testing_the_functionality_of_removing_multiple_associated_rights(self, browser):
        last_name = random_en(8)
        first_name = random_en(8)
        middle_name = random_en(8)
        search = last_name + ' ' + first_name + ' ' + middle_name
        email_user = random_en(8) + '@test.com'
        permission_1 = 'view-student-menu'  # Просмотр меню студента
        permission_2 = 'view-trajectory'  # Просмотр учебной траектории
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Подготовка учетной записи для выполнения тестирования'):
                with allure.step('Создать новую верифицированную учётную запись'):
                    USERS().create_a_new_verified_user(last_name, first_name, middle_name, email_user)
                with allure.step('Добавить разрешение "Просмотр меню студента"'):
                    PERMISSION().assign_permission_to_user(email_user, permission_1)
                with allure.step('Добавить разрешение "Просмотр учебной траектории"'):
                    PERMISSION().assign_permission_to_user(email_user, permission_2)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Users(browser, link)
        with allure.step('Открыть вкладку "Пользователи"'):
            page.open_window_users()
        with allure.step('Открыть ранее созданного пользователя (Precondition)'):
            page.search_user(search, 'OPEN', email_user)
        with allure.step('Открыть вкладку "Роли и разрешения"'):
            page.open_tubs('Роли и разрешения')
        with allure.step('Открыть вкладку "Разрешения"'):
            page.open_tab_window('Разрешения')
        with allure.step('Нажать кнопку "Удалить" у ранее прикрепленного разрешения'):
            page.click_delete_button(1, 'Разрешения')
        with allure.step('Нажать кнопку "Удалить" у ранее прикрепленного разрешения'):
            page.click_delete_button(0, 'Разрешения')

    @allure.story('Проверка функциональности вкладки "Безопасность"')
    def testing_functionality_authorization_after_changing_the_password(self, browser):
        last_name = random_en(8)
        first_name = random_en(8)
        middle_name = random_en(8)
        search = last_name + ' ' + first_name + ' ' + middle_name
        email_user = random_en(8) + '@test.com'
        new_password = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Подготовка учетной записи для выполнения тестирования'):
                with allure.step('Создать новую верифицированную учётную запись'):
                    USERS().create_a_new_verified_user(last_name, first_name, middle_name, email_user)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Users(browser, link)
        with allure.step('Открыть вкладку "Пользователи"'):
            page.open_window_users()
        with allure.step('Открыть ранее созданного пользователя'):
            page.search_user(search, 'OPEN', email_user)
        with allure.step('Открыть вкладку "Безопасность"'):
            page.open_tubs("Безопасность")
        page = Profile(browser, link)
        with allure.step('Ввести в поле "Новый пароль" 8 символов'):
            page.input_password(new_password)
        with allure.step('Повторить ранее введённый пароль в поле "Новый пароль еще раз"'):
            page.input_password_confirmation(new_password)
        page = Users(browser, link)
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_password_validators('Correct', email_user, '')
        with allure.step('Нажать кнопку "Покинуть учётную запись"'):
            page = Profile(browser, link)
            page.exit_profile()
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Пользователь)'):
            page.input_email(email_user)
        with allure.step('Ввести новый пароль в поле "Пароль"'):
            page.input_password(new_password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')

    @allure.story('Проверка функциональности вкладки "Безопасность"')
    def testing_the_functionality_of_changing_a_password_with_invalid_values(self, browser):
        last_name = random_en(8)
        first_name = random_en(8)
        middle_name = random_en(8)
        search = last_name + ' ' + first_name + ' ' + middle_name
        email_user = random_en(8) + '@test.com'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Подготовка учетной записи для выполнения тестирования'):
                with allure.step('Создать новую верифицированную учётную запись'):
                    USERS().create_a_new_verified_user(last_name, first_name, middle_name, email_user)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (Precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Users(browser, link)
        with allure.step('Открыть вкладку "Пользователи"'):
            page.open_window_users()
        with allure.step('Открыть ранее созданного пользователя'):
            page.search_user(search, 'OPEN', email_user)
        with allure.step('Открыть вкладку "Безопасность"'):
            page.open_tubs('Безопасность')
        page = Profile(browser, link)
        with allure.step('Проверка валидации поля при вводе минимального предельного значения'):
            min_password = random_en(7)
            with allure.step('Ввести в поле "Новый пароль" 7 символов'):
                page.input_password(min_password)
            with allure.step('Повторить ранее введённый пароль в поле "Новый пароль еще раз"'):
                page.input_password_confirmation(min_password)
            page = Users(browser, link)
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_password_validators('Incorrect', email_user, '«пароль» должно быть не меньше')
        page = Profile(browser, link)
        with allure.step('Проверка валидации поля при вводе разных данных'):
            with allure.step('Ввести в поле "Новый пароль" 8 символов'):
                page.input_password(random_en(8))
            with allure.step('Ввести в поле "Новый пароль еще раз" 8 символов'):
                page.input_password_confirmation(random_en(8))
            page = Users(browser, link)
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_password_validators('Incorrect', email_user, '«пароль» не совпадает с подтверждением')


@pytest.mark.base
@allure.epic('Функциональное тестирование модуля "BASE"')
@allure.feature('Функциональное тестирование панели администратора')
@allure.feature('Проверка функциональности раздела "Настройки"')
class TestAdministration_SETTING:
    @pytest.mark.smoke
    @allure.link(link + '/admin/settings/assign-roles', name='ТАНДЕМ ЛК - Настройки получения ролей')
    @allure.feature('Проверка функциональности вкладки "Получение ролей"')
    def testing_the_functionality_of_the_student_role_settings_widget(self, browser):
        page = RegLogin(browser, link)
        role = 'Студент'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                SETTING().delete_entries_in_assign_role_by_columns()
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Получение ролей"'):
            page.open_window_setting('getting_roles')
        with allure.step('Выбрать роль "Студент"'):
            page.open_page_setting_role(role)
        with allure.step('Проверка записи виджета "Имя" в таблицу abitur.assign_role_by_columns'):
            form = 'first_name'
            with allure.step('Включить подтверждение получения роли по необязательному полю "Имя"'):
                page.input_checkbox_widget(form)
            with allure.step('Включить подтверждение получения роли по обязательному полю "Дата рождения"'):
                page.input_checkbox_widget('birth_date')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Correct', '')
            with allure.step('Проверка записи в abitur.assign_role_by_columns'):
                page.check_records_form(role, form)
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()
        with allure.step('Проверка записи виджета "Фамилия" в таблицу abitur.assign_role_by_columns'):
            form = 'last_name'
            with allure.step('Включить подтверждение получения роли по необязательному полю "Фамилия"'):
                page.input_checkbox_widget(form)
            with allure.step('Включить подтверждение получения роли по обязательному полю "Дата рождения"'):
                page.input_checkbox_widget('birth_date')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Correct', '')
            with allure.step('Проверка записи в abitur.assign_role_by_columns'):
                page.check_records_form(role, form)
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()
        with allure.step('Проверка записи виджета "Отчество" в таблицу abitur.assign_role_by_columns'):
            form = 'middle_name'
            with allure.step('Включить подтверждение получения роли по необязательному полю "Отчество"'):
                page.input_checkbox_widget(form)
            with allure.step('Включить подтверждение получения роли по обязательному полю "Дата рождения"'):
                page.input_checkbox_widget('birth_date')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Correct', '')
            with allure.step('Проверка записи в abitur.assign_role_by_columns'):
                page.check_records_form(role, form)
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()
        with allure.step('Проверка записи виджета "Дата рождения" в таблицу abitur.assign_role_by_columns'):
            form = 'birth_date'
            with allure.step('Включить подтверждение получения роли по обязательному полю "Дата рождения"'):
                page.input_checkbox_widget(form)
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Correct', '')
            with allure.step('Проверка записи в abitur.assign_role_by_columns'):
                page.check_records_form(role, form)
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()
        with allure.step('Проверка записи виджета "Последние 4 цифры" в таблицу abitur.assign_role_by_columns'):
            form = 'last_4_numbers_passport'
            with allure.step('Включить подтверждение получения роли по обязательному полю "Последние 4 цифры"'):
                page.input_checkbox_widget(form)
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Correct', '')
            with allure.step('Проверка записи в abitur.assign_role_by_columns'):
                page.check_records_form(role, form)
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()
        with allure.step('Проверка записи виджета "СНИЛС" в таблицу abitur.assign_role_by_columns'):
            form = 'snils'
            with allure.step('Включить подтверждение получения роли по обязательному полю "СНИЛС"'):
                page.input_checkbox_widget(form)
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Correct', '')
            with allure.step('Проверка записи в abitur.assign_role_by_columns'):
                page.check_records_form(role, form)
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()
        with allure.step('Проверка записи виджета "ИНН" в таблицу abitur.assign_role_by_columns'):
            form = 'inn'
            with allure.step('Включить подтверждение получения роли по обязательному полю "ИНН"'):
                page.input_checkbox_widget(form)
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Correct', '')
            with allure.step('Проверка записи в abitur.assign_role_by_columns'):
                page.check_records_form(role, form)
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()
        with allure.step('Проверка записи виджета "Номер зачетной книжки" в таблицу abitur.assign_role_by_columns'):
            form = 'book_number'
            with allure.step('Включить подтверждение получения роли по обязательному полю "Номер зачетной книжки"'):
                page.input_checkbox_widget(form)
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Correct', '')
            with allure.step('Проверка записи в abitur.assign_role_by_columns'):
                page.check_records_form(role, form)
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()
        with allure.step('Проверка записи виджета "Табельный номер" в таблицу abitur.assign_role_by_columns'):
            form = 'code'
            with allure.step('Включить подтверждение получения роли по обязательному полю "Табельный номер"'):
                page.input_checkbox_widget(form)
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Correct', '')
            with allure.step('Проверка записи в abitur.assign_role_by_columns'):
                page.check_records_form(role, form)
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()
        with allure.step('Проверка записи поля "Описание" в таблицу abitur.roles'):
            description = random_en(8)
            form = 'code'
            with allure.step('Включить подтверждение получения роли по обязательному полю "Табельный номер"'):
                page.input_checkbox_widget(form)
            with allure.step('Ввести в поле "Описание" 8 символов'):
                page.input_assignment_description(description)
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Correct', '')
            with allure.step('Проверка записи в abitur.roles'):
                page.check_records_assignment_description(description, role)
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()

    @allure.feature('Проверка функциональности вкладки "Получение ролей"')
    @allure.link(link + '/admin/settings/assign-roles', name='ТАНДЕМ ЛК - Настройки получения ролей')
    def testing_the_functionality_of_displaying_input_fields_for_the_student(self, browser):
        role = 'Студент'
        email_abitur = random_en(8) + '@test.com'
        description = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать верефицированную учётную запись'):
                USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email_abitur)
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                SETTING().delete_entries_in_assign_role_by_columns()
            with allure.step('Подготовка форм ввода для роли Студент'):
                with allure.step('Добавить описание для роли'):
                    ROLES().update_assignment_description_in_role(role, description)
                with allure.step('Включить форму ввода "Имя"'):
                    SETTING().add_form_assignment_record_for_role('first_name', role)
                with allure.step('Включить форму ввода "Фамилия"'):
                    SETTING().add_form_assignment_record_for_role('last_name', role)
                with allure.step('Включить форму ввода "Отчество"'):
                    SETTING().add_form_assignment_record_for_role('middle_name', role)
                with allure.step('Включить форму ввода "Дата рождения"'):
                    SETTING().add_form_assignment_record_for_role('birth_date', role)
                with allure.step('Включить форму ввода "Последние 4 цифры паспорта"'):
                    SETTING().add_form_assignment_record_for_role('last_4_numbers_passport', role)
                with allure.step('Включить форму ввода "СНИЛС"'):
                    SETTING().add_form_assignment_record_for_role('snils', role)
                with allure.step('Включить форму ввода "ИНН"'):
                    SETTING().add_form_assignment_record_for_role('inn', role)
                with allure.step('Включить форму ввода "Номер зачетной книжки"'):
                    SETTING().add_form_assignment_record_for_role('book_number', role)
                with allure.step('Включить форму ввода "Табельный номер"'):
                    SETTING().add_form_assignment_record_for_role('code', role)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition)'):
            page.input_email(email_abitur)
        with allure.step('Ввести в поле "Пароль" (Precondition)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Dashboard(browser, link)
        with allure.step('Выбрать роль "Обучающийся"'):
            page.select_role(role, description)
        page = RegLogin(browser, link)
        with allure.step('Ввести в поле "Имя" 8 символов'):
            page.input_name(random_en(8))
        with allure.step('Ввести в поле "Фамилия" 8 символов'):
            page.input_last_name(random_en(8))
        with allure.step('Ввести в поле "Отчество" 8 символов'):
            page.input_middle_name(random_en(8))
        page = Dashboard(browser, link)
        with allure.step('Ввести в поле "Дата рождения" - 22.02.2002'):
            page.input_birth_date('22.02.2002')
        with allure.step('Ввести в поле "Последние 4 цифры паспорта" 4 цифры'):
            page.input_last_4_numbers_passport(random_number(4))
        with allure.step('Ввести в поле "СНИЛС" 11 цифр'):
            page.input_snils(random_number(11))
        with allure.step('Ввести в поле "ИНН" 12 цифр'):
            page.input_inn(random_number(12))
        with allure.step('Ввести в поле "Номер зачетной книжки" 6 цифр'):
            page.input_book_number(random_number(6))
        with allure.step('Ввести в поле "Табельный номер" 6 цифр'):
            page.input_code(random_number(6))
        with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
            SETTING().delete_entries_in_assign_role_by_columns()

    @allure.feature('Проверка функциональности вкладки "Получение ролей"')
    @allure.link(link + '/admin/settings/assign-roles', name='ТАНДЕМ ЛК - Настройки получения ролей')
    def testing_the_functionality_of_the_student_role_settings_widget_for_mandatory_checkboxes(self, browser):
        page = RegLogin(browser, link)
        role = 'Студент'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                SETTING().delete_entries_in_assign_role_by_columns()
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Получение ролей"'):
            page.open_window_setting('getting_roles')
        with allure.step('Проверка валидации обязательных виджетов'):
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()
            with allure.step('Выбрать роль "Студент"'):
                page.open_page_setting_role(role)
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Incorrect', 'Выберите хотя бы один параметр')
            with allure.step('Нажать кнопку "К списку ролей"'):
                page.click_to_the_list_of_roles()
        with allure.step('Проверка валидации при включении необязательных виджетов'):
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()
            with allure.step('Подготовка форм ввода для роли Студент'):
                with allure.step('Включить форму ввода "Имя"'):
                    SETTING().add_form_assignment_record_for_role('first_name', role)
                with allure.step('Включить форму ввода "Фамилия"'):
                    SETTING().add_form_assignment_record_for_role('last_name', role)
                with allure.step('Включить форму ввода "Отчество"'):
                    SETTING().add_form_assignment_record_for_role('middle_name', role)
            with allure.step('Выбрать роль "Студент"'):
                page.open_page_setting_role(role)
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Incorrect', 'Выберите хотя бы один параметр')

    @allure.feature('Проверка функциональности вкладки "Получение ролей"')
    @allure.link(link + '/admin/settings/assign-roles', name='ТАНДЕМ ЛК - Настройки получения ролей')
    def testing_the_functionality_of_the_employee_role_settings_widget(self, browser):
        page = RegLogin(browser, link)
        role = 'Сотрудник'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                SETTING().delete_entries_in_assign_role_by_columns()
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Получение ролей"'):
            page.open_window_setting('getting_roles')
        with allure.step('Выбрать роль "Сотрудник"'):
            page.open_page_setting_role(role)
        with allure.step('Проверка записи виджета "Имя" в таблицу abitur.assign_role_by_columns'):
            form = 'first_name'
            with allure.step('Включить подтверждение получения роли по необязательному полю "Имя"'):
                page.input_checkbox_widget(form)
            with allure.step('Включить подтверждение получения роли по обязательному полю "Дата рождения"'):
                page.input_checkbox_widget('birth_date')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Correct', '')
            with allure.step('Проверка записи в abitur.assign_role_by_columns'):
                page.check_records_form(role, form)
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()
        with allure.step('Проверка записи виджета "Фамилия" в таблицу abitur.assign_role_by_columns'):
            form = 'last_name'
            with allure.step('Включить подтверждение получения роли по необязательному полю "Фамилия"'):
                page.input_checkbox_widget(form)
            with allure.step('Включить подтверждение получения роли по обязательному полю "Дата рождения"'):
                page.input_checkbox_widget('birth_date')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Correct', '')
            with allure.step('Проверка записи в abitur.assign_role_by_columns'):
                page.check_records_form(role, form)
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()
        with allure.step('Проверка записи виджета "Отчество" в таблицу abitur.assign_role_by_columns'):
            form = 'middle_name'
            with allure.step('Включить подтверждение получения роли по необязательному полю "Отчество"'):
                page.input_checkbox_widget(form)
            with allure.step('Включить подтверждение получения роли по обязательному полю "Дата рождения"'):
                page.input_checkbox_widget('birth_date')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Correct', '')
            with allure.step('Проверка записи в abitur.assign_role_by_columns'):
                page.check_records_form(role, form)
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()
        with allure.step('Проверка записи виджета "Дата рождения" в таблицу abitur.assign_role_by_columns'):
            form = 'birth_date'
            with allure.step('Включить подтверждение получения роли по обязательному полю "Дата рождения"'):
                page.input_checkbox_widget(form)
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Correct', '')
            with allure.step('Проверка записи в abitur.assign_role_by_columns'):
                page.check_records_form(role, form)
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()
        with allure.step('Проверка записи виджета "Последние 4 цифры" в таблицу abitur.assign_role_by_columns'):
            form = 'last_4_numbers_passport'
            with allure.step('Включить подтверждение получения роли по обязательному полю "Последние 4 цифры"'):
                page.input_checkbox_widget(form)
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Correct', '')
            with allure.step('Проверка записи в abitur.assign_role_by_columns'):
                page.check_records_form(role, form)
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()
        with allure.step('Проверка записи виджета "СНИЛС" в таблицу abitur.assign_role_by_columns'):
            form = 'snils'
            with allure.step('Включить подтверждение получения роли по обязательному полю "СНИЛС"'):
                page.input_checkbox_widget(form)
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Correct', '')
            with allure.step('Проверка записи в abitur.assign_role_by_columns'):
                page.check_records_form(role, form)
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()
        with allure.step('Проверка записи виджета "ИНН" в таблицу abitur.assign_role_by_columns'):
            form = 'inn'
            with allure.step('Включить подтверждение получения роли по обязательному полю "ИНН"'):
                page.input_checkbox_widget(form)
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Correct', '')
            with allure.step('Проверка записи в abitur.assign_role_by_columns'):
                page.check_records_form(role, form)
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()
        with allure.step('Проверка записи виджета "Номер зачетной книжки" в таблицу abitur.assign_role_by_columns'):
            form = 'book_number'
            with allure.step('Включить подтверждение получения роли по обязательному полю "Номер зачетной книжки"'):
                page.input_checkbox_widget(form)
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Correct', '')
            with allure.step('Проверка записи в abitur.assign_role_by_columns'):
                page.check_records_form(role, form)
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()
        with allure.step('Проверка записи виджета "Табельный номер" в таблицу abitur.assign_role_by_columns'):
            form = 'code'
            with allure.step('Включить подтверждение получения роли по обязательному полю "Табельный номер"'):
                page.input_checkbox_widget(form)
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Correct', '')
            with allure.step('Проверка записи в abitur.assign_role_by_columns'):
                page.check_records_form(role, form)
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()
        with allure.step('Проверка записи поля "Описание" в таблицу abitur.roles'):
            description = random_en(8)
            form = 'code'
            with allure.step('Включить подтверждение получения роли по обязательному полю "Табельный номер"'):
                page.input_checkbox_widget(form)
            with allure.step('Ввести в поле "Описание" 8 символов'):
                page.input_assignment_description(description)
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Correct', '')
            with allure.step('Проверка записи в abitur.roles'):
                page.check_records_assignment_description(description, role)
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()

    @allure.feature('Проверка функциональности вкладки "Получение ролей"')
    @allure.link(link + '/admin/settings/assign-roles', name='ТАНДЕМ ЛК - Настройки получения ролей')
    def testing_the_functionality_of_displaying_input_fields_for_the_employee(self, browser):
        role = 'Сотрудник'
        description = random_en(8)
        email_employer = random_en(8) + '@test.com'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать верефицированную учётную запись'):
                USERS().create_a_new_verified_user(random_en(8), random_en(8), random_en(8), email_employer)
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                SETTING().delete_entries_in_assign_role_by_columns()
            with allure.step('Подготовка форм ввода для роли Сотрудник'):
                with allure.step('Добавить описание для роли'):
                    ROLES().update_assignment_description_in_role(role, description)
                with allure.step('Включить форму ввода "Имя"'):
                    SETTING().add_form_assignment_record_for_role('first_name', role)
                with allure.step('Включить форму ввода "Фамилия"'):
                    SETTING().add_form_assignment_record_for_role('last_name', role)
                with allure.step('Включить форму ввода "Отчество"'):
                    SETTING().add_form_assignment_record_for_role('middle_name', role)
                with allure.step('Включить форму ввода "Дата рождения"'):
                    SETTING().add_form_assignment_record_for_role('birth_date', role)
                with allure.step('Включить форму ввода "Последние 4 цифры паспорта"'):
                    SETTING().add_form_assignment_record_for_role('last_4_numbers_passport', role)
                with allure.step('Включить форму ввода "СНИЛС"'):
                    SETTING().add_form_assignment_record_for_role('snils', role)
                with allure.step('Включить форму ввода "ИНН"'):
                    SETTING().add_form_assignment_record_for_role('inn', role)
                with allure.step('Включить форму ввода "Номер зачетной книжки"'):
                    SETTING().add_form_assignment_record_for_role('book_number', role)
                with allure.step('Включить форму ввода "Табельный номер"'):
                    SETTING().add_form_assignment_record_for_role('code', role)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition)'):
            page.input_email(email_employer)
        with allure.step('Ввести в поле "Пароль" (Precondition)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Dashboard(browser, link)
        with allure.step('Выбрать роль "Сотрудник"'):
            page.select_role(role, description)
        page = RegLogin(browser, link)
        with allure.step('Ввести в поле "Имя" 8 символов'):
            page.input_name(random_en(8))
        with allure.step('Ввести в поле "Фамилия" 8 символов'):
            page.input_last_name(random_en(8))
        with allure.step('Ввести в поле "Отчество" 8 символов'):
            page.input_middle_name(random_en(8))
        page = Dashboard(browser, link)
        with allure.step('Ввести в поле "Дата рождения" - 22.02.2002'):
            page.input_birth_date('22.02.2002')
        with allure.step('Ввести в поле "Последние 4 цифры паспорта" 4 цифры'):
            page.input_last_4_numbers_passport(random_number(4))
        with allure.step('Ввести в поле "СНИЛС" 11 цифр'):
            page.input_snils(random_number(11))
        with allure.step('Ввести в поле "ИНН" 12 цифр'):
            page.input_inn(random_number(12))
        with allure.step('Ввести в поле "Номер зачетной книжки" 6 цифр'):
            page.input_book_number(random_number(6))
        with allure.step('Ввести в поле "Табельный номер" 6 цифр'):
            page.input_code(random_number(6))
        with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
            SETTING().delete_entries_in_assign_role_by_columns()

    @allure.feature('Проверка функциональности вкладки "Получение ролей"')
    @allure.link(link + '/admin/settings/assign-roles', name='ТАНДЕМ ЛК - Настройки получения ролей')
    def testing_the_functionality_of_the_employee_role_settings_widget_for_mandatory_checkboxes(self, browser):
        page = RegLogin(browser, link)
        role = 'Сотрудник'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                SETTING().delete_entries_in_assign_role_by_columns()
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Получение ролей"'):
            page.open_window_setting('getting_roles')
        with allure.step('Проверка валидации обязательных виджетов'):
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()
            with allure.step('Выбрать роль "Сотрудник"'):
                page.open_page_setting_role(role)
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Incorrect', 'Выберите хотя бы один параметр')
            with allure.step('Нажать кнопку "К списку ролей"'):
                page.click_to_the_list_of_roles()
        with allure.step('Проверка валидации при включении необязательных виджетов'):
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()
            with allure.step('Подготовка форм ввода для роли Сотрудник'):
                with allure.step('Включить форму ввода "Имя"'):
                    SETTING().add_form_assignment_record_for_role('first_name', role)
                with allure.step('Включить форму ввода "Фамилия"'):
                    SETTING().add_form_assignment_record_for_role('last_name', role)
                with allure.step('Включить форму ввода "Отчество"'):
                    SETTING().add_form_assignment_record_for_role('middle_name', role)
            with allure.step('Выбрать роль "Сотрудник"'):
                page.open_page_setting_role(role)
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Incorrect', 'Выберите хотя бы один параметр')

    @allure.feature('Проверка функциональности вкладки "Получение ролей"')
    @allure.link(link + '/admin/settings/assign-roles', name='ТАНДЕМ ЛК - Настройки получения ролей')
    def testing_the_functionality_of_the_parent_role_settings_widget(self, browser):
        role = 'Родитель'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                SETTING().delete_entries_in_assign_role_by_columns()
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Получение ролей"'):
            page.open_window_setting('getting_roles')
        with allure.step('Выбрать роль "Родитель"'):
            page.open_page_setting_role(role)
        with allure.step('Проверка записи виджета "Имя" в таблицу abitur.assign_role_by_columns'):
            form = 'first_name'
            with allure.step('Включить подтверждение получения роли по необязательному полю "Имя"'):
                page.input_checkbox_widget(form)
            with allure.step('Включить подтверждение получения роли по обязательному полю "Дата рождения"'):
                page.input_checkbox_widget('birth_date')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Correct', '')
            with allure.step('Проверка записи в abitur.assign_role_by_columns'):
                page.check_records_form(role, form)
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()
        with allure.step('Проверка записи виджета "Фамилия" в таблицу abitur.assign_role_by_columns'):
            form = 'last_name'
            with allure.step('Включить подтверждение получения роли по необязательному полю "Фамилия"'):
                page.input_checkbox_widget(form)
            with allure.step('Включить подтверждение получения роли по обязательному полю "Дата рождения"'):
                page.input_checkbox_widget('birth_date')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Correct', '')
            with allure.step('Проверка записи в abitur.assign_role_by_columns'):
                page.check_records_form(role, form)
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()
        with allure.step('Проверка записи виджета "Отчество" в таблицу abitur.assign_role_by_columns'):
            form = 'middle_name'
            with allure.step('Включить подтверждение получения роли по необязательному полю "Отчество"'):
                page.input_checkbox_widget(form)
            with allure.step('Включить подтверждение получения роли по обязательному полю "Дата рождения"'):
                page.input_checkbox_widget('birth_date')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Correct', '')
            with allure.step('Проверка записи в abitur.assign_role_by_columns'):
                page.check_records_form(role, form)
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()
        with allure.step('Проверка записи виджета "Дата рождения" в таблицу abitur.assign_role_by_columns'):
            form = 'birth_date'
            with allure.step('Включить подтверждение получения роли по обязательному полю "Дата рождения"'):
                page.input_checkbox_widget(form)
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Correct', '')
            with allure.step('Проверка записи в abitur.assign_role_by_columns'):
                page.check_records_form(role, form)
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()
        with allure.step('Проверка записи виджета "Последние 4 цифры" в таблицу abitur.assign_role_by_columns'):
            form = 'last_4_numbers_passport'
            with allure.step('Включить подтверждение получения роли по обязательному полю "Последние 4 цифры"'):
                page.input_checkbox_widget(form)
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Correct', '')
            with allure.step('Проверка записи в abitur.assign_role_by_columns'):
                page.check_records_form(role, form)
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()
        with allure.step('Проверка записи виджета "СНИЛС" в таблицу abitur.assign_role_by_columns'):
            form = 'snils'
            with allure.step('Включить подтверждение получения роли по обязательному полю "СНИЛС"'):
                page.input_checkbox_widget(form)
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Correct', '')
            with allure.step('Проверка записи в abitur.assign_role_by_columns'):
                page.check_records_form(role, form)
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()
        with allure.step('Проверка записи виджета "ИНН" в таблицу abitur.assign_role_by_columns'):
            form = 'inn'
            with allure.step('Включить подтверждение получения роли по обязательному полю "ИНН"'):
                page.input_checkbox_widget(form)
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Correct', '')
            with allure.step('Проверка записи в abitur.assign_role_by_columns'):
                page.check_records_form(role, form)
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()
        with allure.step('Проверка записи виджета "Номер зачетной книжки" в таблицу abitur.assign_role_by_columns'):
            form = 'book_number'
            with allure.step('Включить подтверждение получения роли по обязательному полю "Номер зачетной книжки"'):
                page.input_checkbox_widget(form)
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Correct', '')
            with allure.step('Проверка записи в abitur.assign_role_by_columns'):
                page.check_records_form(role, form)
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()
        with allure.step('Проверка записи виджета "Табельный номер" в таблицу abitur.assign_role_by_columns'):
            form = 'code'
            with allure.step('Включить подтверждение получения роли по обязательному полю "Табельный номер"'):
                page.input_checkbox_widget(form)
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Correct', '')
            with allure.step('Проверка записи в abitur.assign_role_by_columns'):
                page.check_records_form(role, form)
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()
        with allure.step('Проверка записи поля "Описание" в таблицу abitur.roles'):
            description = random_en(8)
            form = 'code'
            with allure.step('Включить подтверждение получения роли по обязательному полю "Табельный номер"'):
                page.input_checkbox_widget(form)
            with allure.step('Ввести в поле "Описание" 8 символов'):
                page.input_assignment_description(description)
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Correct', '')
            with allure.step('Проверка записи в abitur.roles'):
                page.check_records_assignment_description(description, role)
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()

    @allure.feature('Проверка функциональности вкладки "Получение ролей"')
    @allure.link(link + '/admin/settings/assign-roles', name='ТАНДЕМ ЛК - Настройки получения ролей')
    def testing_the_functionality_of_the_parents_role_settings_widget_for_mandatory_checkboxes(self, browser):
        role = 'Родитель'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                SETTING().delete_entries_in_assign_role_by_columns()
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Получение ролей"'):
            page.open_window_setting('getting_roles')
        with allure.step('Проверка валидации обязательных виджетов'):
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()
            with allure.step('Выбрать роль "Родитель"'):
                page.open_page_setting_role(role)
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Incorrect', 'Выберите хотя бы один параметр')
            with allure.step('Нажать кнопку "К списку ролей"'):
                page.click_to_the_list_of_roles()
        with allure.step('Проверка валидации при включении необязательных виджетов'):
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                page.clear_table_and_refresh()
            with allure.step('Подготовка форм ввода для роли Родитель'):
                with allure.step('Включить форму ввода "Имя"'):
                    SETTING().add_form_assignment_record_for_role('first_name', role)
                with allure.step('Включить форму ввода "Фамилия"'):
                    SETTING().add_form_assignment_record_for_role('last_name', role)
                with allure.step('Включить форму ввода "Отчество"'):
                    SETTING().add_form_assignment_record_for_role('middle_name', role)
            with allure.step('Выбрать роль "Родитель"'):
                page.open_page_setting_role(role)
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_checkbox_validators('Incorrect', 'Выберите хотя бы один параметр')

    @pytest.mark.skip(reason='Функциональность для роли "Родитель" отсутствует на стенде, и находиться в разработке')
    @allure.feature('Проверка функциональности вкладки "Получение ролей"')
    @allure.link(link + '/admin/settings/assign-roles', name='ТАНДЕМ ЛК - Настройки получения ролей')
    def testing_the_functionality_of_displaying_input_fields_for_the_parents(self, browser):
        role = 'Родитель'
        description = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
                SETTING().delete_entries_in_assign_role_by_columns()
            with allure.step('Подготовка форм ввода для роли Родитель'):
                with allure.step('Добавить описание для роли'):
                    ROLES().update_assignment_description_in_role(role, description)
                with allure.step('Включить форму ввода "Имя"'):
                    SETTING().add_form_assignment_record_for_role('first_name', role)
                with allure.step('Включить форму ввода "Фамилия"'):
                    SETTING().add_form_assignment_record_for_role('last_name', role)
                with allure.step('Включить форму ввода "Отчество"'):
                    SETTING().add_form_assignment_record_for_role('middle_name', role)
                with allure.step('Включить форму ввода "Дата рождения"'):
                    SETTING().add_form_assignment_record_for_role('birth_date', role)
                with allure.step('Включить форму ввода "Последние 4 цифры паспорта"'):
                    SETTING().add_form_assignment_record_for_role('last_4_numbers_passport', role)
                with allure.step('Включить форму ввода "СНИЛС"'):
                    SETTING().add_form_assignment_record_for_role('snils', role)
                with allure.step('Включить форму ввода "ИНН"'):
                    SETTING().add_form_assignment_record_for_role('inn', role)
                with allure.step('Включить форму ввода "Номер зачетной книжки"'):
                    SETTING().add_form_assignment_record_for_role('book_number', role)
                with allure.step('Включить форму ввода "Табельный номер"'):
                    SETTING().add_form_assignment_record_for_role('code', role)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Dashboard(browser, link)
        with allure.step('Выбрать роль "Родитель"'):
            page.select_role(role, description)
        page = RegLogin(browser, link)
        with allure.step('Ввести в поле "Имя" 8 символов'):
            page.input_name(random_en(8))
        with allure.step('Ввести в поле "Фамилия" 8 символов'):
            page.input_last_name(random_en(8))
        with allure.step('Ввести в поле "Отчество" 8 символов'):
            page.input_middle_name(random_en(8))
        page = Dashboard(browser, link)
        with allure.step('Ввести в поле "Дата рождения" - 22.02.2002'):
            page.input_birth_date('22.02.2002')
        with allure.step('Ввести в поле "Последние 4 цифры паспорта" 4 цифры'):
            page.input_last_4_numbers_passport(random_number(4))
        with allure.step('Ввести в поле "СНИЛС" 11 цифр'):
            page.input_snils(random_number(11))
        with allure.step('Ввести в поле "ИНН" 12 цифр'):
            page.input_inn(random_number(12))
        with allure.step('Ввести в поле "Номер зачетной книжки" 6 цифр'):
            page.input_book_number(random_number(6))
        with allure.step('Ввести в поле "Табельный номер" 6 цифр'):
            page.input_code(random_number(6))
        with allure.step('Очистить таблицу abitur.assign_role_by_columns'):
            SETTING().delete_entries_in_assign_role_by_columns()

    ####################################################################################################################
    @allure.feature('Проверка функциональности вкладки "Общие"')
    @allure.link(link + '/admin/settings/general', name='ТАНДЕМ ЛК - Общие настройки')
    def testing_the_functionality_of_the_LKA_operation_mode_widget(self, browser):
        setting = 'is_multiple_org_units'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Выключить виджет "Прием документов в несколько образовательных организаций"'):
                SETTING().update_settings_app(setting, '')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Общие"'):
            page.open_window_setting('general')
        with allure.step('Включить виджет "Прием документов в несколько образовательных организаций"'):
            page.select_checkbox_is_multiple_org_units()
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_general_validators(0, 'Correct', '')
        with allure.step('Проверка записей данных в abitur.settings'):
            page.check_records_mail_setting(setting, '1')

    @allure.feature('Проверка функциональности вкладки "Общие"')
    @allure.link(link + '/admin/settings/general', name='ТАНДЕМ ЛК - Общие настройки')
    def testing_the_functionality_of_disabling_the_LCA_mode_widget_by_having_multiple_organizations(self, browser):
        setting = 'is_multiple_org_units'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Включить виджет "Прием документов в несколько образовательных организаций"'):
                SETTING().update_settings_app(setting, '1')
            with allure.step('Добавить ОО в external_org_units'):
                title_org1 = random_en(8)
                SETTING().add_educational_organization(title_org1, None)
            with allure.step('Добавить ОО в external_org_units'):
                title_org2 = random_en(8)
                SETTING().add_educational_organization(title_org2, None)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Общие"'):
            page.open_window_setting('general')
        with allure.step('Выключить виджет "Прием документов в несколько образовательных организаций"'):
            page.select_checkbox_is_multiple_org_units()
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_general_validators(0, 'Incorrect', '')
        with allure.step('Проверка записей данных в abitur.settings'):
            page.check_records_mail_setting(setting, '1')
        with allure.step('Удалить записи ранее созданных ОО в таблице external_org_units'):
            SETTING().delete_record_external_org_units(title_org1)
            SETTING().delete_record_external_org_units(title_org2)

    @allure.feature('Проверка функциональности вкладки "Общие"')
    @allure.link(link + '/admin/settings/general', name='ТАНДЕМ ЛК - Общие настройки')
    def testing_the_functionality_of_the_interface_customization_widget(self, browser):
        setting = 'show_assign_role_widget_from_home'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Общие"'):
            page.open_window_setting('general')
        with allure.step('Выключить виджет "Получения роли на главной странице"'):
            page.input_show_assign_role_widget_from_home()
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_general_validators(3, 'Correct', '')
        with allure.step('Проверка записей данных в abitur.settings'):
            page.check_records_mail_setting(setting, 'false')
        page = Dashboard(browser, link)
        with allure.step('Открыть вкладку "Рабочий стол"'):
            page.open_window_desktop()
        with allure.step('Проверить отображение виджета "Получения роли на главной странице"'):
            page.role_selection_widget_disabled()
        with allure.step('Включить виджет "Получения роли на главной странице"'):
            SETTING().update_settings_app(setting, 'true')

    @allure.feature('Проверка функциональности вкладки "Общие"')
    @allure.link(link + '/admin/settings/general', name='ТАНДЕМ ЛК - Общие настройки')
    def testing_the_functionality_of_mail_settings_with_valid_data(self, browser):
        mail_server = 'smtp.mailtrap.io'
        mail_server_port = '2525'
        mail_server_login = '8d4e826d167512'
        mail_server_password = 'a3880decfc4286'
        mail_server_encryption = 'tls'
        mail_server_from_address = random_en(8) + '@test.com'
        mail_server_from_name = random_en(8)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Общие"'):
            page.open_window_setting('general')
        with allure.step('Ввести в поле "Почтовый сервер" - smtp.mailtrap.io'):
            page.input_mail_server(mail_server)
        with allure.step('Ввести в поле "Порт" - 2525'):
            page.input_mail_server_port(mail_server_port)
        with allure.step('Ввести в поле "Имя пользователя" - 8d4e826d167512'):
            page.input_mail_server_login(mail_server_login)
        with allure.step('Ввести в поле "Пароль" - a3880decfc4286'):
            page.input_mail_server_password(mail_server_password)
        with allure.step('Ввести в поле "Шифрование" - tls'):
            page.input_mail_server_encryption(mail_server_encryption)
        with allure.step('Ввести в поле "Адрес отправителя" валидный адрес'):
            page.input_mail_server_from_address(mail_server_from_address)
        with allure.step('Ввести в поле "От кого" 8 символов'):
            page.input_mail_server_from_name(mail_server_from_name)
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_general_validators(1, 'Correct', '')
        with allure.step('Проверка записи данных в abitur.settings'):
            page.check_records_mail_setting('mail_server', mail_server)
            page.check_records_mail_setting('mail_server_port', mail_server_port)
            page.check_records_mail_setting('mail_server_login', mail_server_login)
            page.check_records_mail_setting('mail_server_password', mail_server_password)
            page.check_records_mail_setting('mail_server_encryption', mail_server_encryption)
            page.check_records_mail_setting('mail_server_from_address', mail_server_from_address)
            page.check_records_mail_setting('mail_server_from_name', mail_server_from_name)

    @allure.feature('Проверка функциональности вкладки "Общие"')
    @allure.link(link + '/admin/settings/general', name='ТАНДЕМ ЛК - Общие настройки')
    def testing_the_functionality_of_mail_settings_with_limit_values(self, browser):
        mail_server = random_en(255)
        mail_server_port = '65535'
        mail_server_login = random_en(255)
        mail_server_password = random_en(255)
        mail_server_encryption = random_en(255)
        mail_server_from_address = random_en(246) + '@test.com'
        mail_server_from_name = random_en(255)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Общие"'):
            page.open_window_setting('general')
        with allure.step('Ввести в поле "Почтовый сервер" 8 символов'):
            page.input_mail_server(mail_server)
        with allure.step('Ввести в поле "Порт" 6 цифр'):
            page.input_mail_server_port(mail_server_port)
        with allure.step('Ввести в поле "Имя пользователя" 8 символов'):
            page.input_mail_server_login(mail_server_login)
        with allure.step('Ввести в поле "Пароль" 255 символов'):
            page.input_mail_server_password(mail_server_password)
        with allure.step('Ввести в поле "Шифрование" 255 символов'):
            page.input_mail_server_encryption(mail_server_encryption)
        with allure.step('Ввести в поле "Адрес отправителя" 255 символов'):
            page.input_mail_server_from_address(mail_server_from_address)
        with allure.step('Ввести в поле "От кого" 255 символов'):
            page.input_mail_server_from_name(mail_server_from_name)
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_general_validators(1, 'Correct', '')
        with allure.step('Проверка записи данных в abitur.settings'):
            page.check_records_mail_setting('mail_server', mail_server)
            page.check_records_mail_setting('mail_server_port', mail_server_port)
            page.check_records_mail_setting('mail_server_login', mail_server_login)
            page.check_records_mail_setting('mail_server_password', mail_server_password)
            page.check_records_mail_setting('mail_server_encryption', mail_server_encryption)
            page.check_records_mail_setting('mail_server_from_address', mail_server_from_address)
            page.check_records_mail_setting('mail_server_from_name', mail_server_from_name)

    @allure.feature('Проверка функциональности вкладки "Общие"')
    @allure.link(link + '/admin/settings/general', name='ТАНДЕМ ЛК - Общие настройки')
    def testing_mail_configuration_validation_with_out_of_scope_values(self, browser):
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Общие"'):
            page.open_window_setting('general')
        with allure.step('Проверка валидации поля "Почтовый сервер" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Почтовый сервер" 256 символов'):
                page.input_mail_server(random_en(256))
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_general_validators(1, 'Incorrect', 'почтовый сервер не может превышать')
            with allure.step('Очистить поле "Почтовый сервер"'):
                page.input_mail_server(' ')
        with allure.step('Проверка валидации поля "Порт" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Порт" 256 цифр'):
                page.input_mail_server_port(random_number(256))
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_general_validators(1, 'Incorrect', 'порт не может быть')
            with allure.step('Очистить поле "Порт"'):
                page.input_mail_server_port(' ')
        with allure.step('Проверка валидации поля "Имя пользователя" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Имя пользователя" 256 символов'):
                page.input_mail_server_login(random_en(256))
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_general_validators(1, 'Incorrect', 'имя пользователя не может')
            with allure.step('Очистить поле "Имя пользователя"'):
                page.input_mail_server_login(' ')
        with allure.step('Проверка валидации поля "Пароль" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Пароль" 256 символов'):
                page.input_mail_server_password(random_en(256))
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_general_validators(1, 'Incorrect', 'пароль не может превышать')
            with allure.step('Очистить поле "Пароль"'):
                page.input_mail_server_password(' ')
        with allure.step('Проверка валидации поля "Шифрование" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Шифрование" 256 символов'):
                page.input_mail_server_encryption(random_en(256))
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_general_validators(1, 'Incorrect', 'шифрование не может превышать')
            with allure.step('Очистить поле "Шифрование"'):
                page.input_mail_server_encryption(' ')
        with allure.step('Проверка валидации поля "Адрес отправителя" на ввод запредельных значений'):
            with allure.step('Ввести в поле "Адрес отправителя" 256 символов'):
                page.input_mail_server_from_address(random_en(247) + '@test.com')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_general_validators(1, 'Incorrect', 'адрес отправителя не может')
            with allure.step('Очистить поле "Адрес отправителя"'):
                page.input_mail_server_from_address(' ')
        with allure.step('Проверка валидации поля "От кого" на ввод запредельных значений'):
            with allure.step('Ввести в поле "От кого" 256 символов'):
                page.input_mail_server_from_name(random_en(256))
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_general_validators(1, 'Incorrect', 'от кого не может превышать')
            with allure.step('Очистить поле "От кого"'):
                page.input_mail_server_from_name(' ')

    @allure.feature('Проверка функциональности вкладки "Общие"')
    @pytest.mark.xfail(reason='Ошибка функциональности настроек "Названия/URL" приложения')
    @allure.link(link + '/admin/settings/general', name='ТАНДЕМ ЛК - Общие настройки')
    def testing_the_functionality_of_the_application_name_settings(self, browser):
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
        app_name = random_en(8)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Общие"'):
            page.open_window_setting('general')
        with allure.step('Ввести в поле "Название приложения" 8 символов'):
            page.input_app_name(app_name)
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_general_validators(0, 'Correct', '')
        with allure.step('Проверка записи данных в abitur.settings'):
            page.check_records_mail_setting('app_name', app_name)
        with allure.step('Проверка изменения TITLE'):
            page.check_title_page(app_name)

    @allure.feature('Проверка функциональности вкладки "Общие"')
    @pytest.mark.xfail(reason='Ошибка функциональности настроек "Названия/URL" приложения')
    @allure.link(link + '/admin/settings/general', name='ТАНДЕМ ЛК - Общие настройки')
    def testing_the_functionality_of_the_application_name_settings(self, browser):
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
        app_name = random_en(8)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Общие"'):
            page.open_window_setting('general')
        with allure.step('Ввести в поле "Название приложения" 8 символов'):
            page.input_app_name(app_name)
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_general_validators(0, 'Correct', '')
        with allure.step('Проверка записи данных в abitur.settings'):
            page.check_records_mail_setting('app_name', app_name)
        with allure.step('Проверка изменения TITLE'):
            page.check_title_page(app_name)

    @allure.feature('Проверка функциональности вкладки "Общие"')
    @pytest.mark.xfail(reason='Ошибка функциональности настроек "Названия/URL" приложения')
    @allure.link(link + '/admin/settings/general', name='ТАНДЕМ ЛК - Общие настройки')
    def testing_the_functionality_of_the_app_name_settings_with_a_limit_value(self, browser):
        setting = 'app_name'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
        app_name = random_en(255)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Общие"'):
            page.open_window_setting('general')
        with allure.step('Ввести в поле "Название приложения" 255 символов'):
            page.input_app_name(app_name)
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_general_validators(0, 'Correct', '')
        with allure.step('Проверка записи данных в abitur.settings'):
            page.check_records_mail_setting('app_name', app_name)
        with allure.step('Проверка изменения TITLE'):
            page.check_title_page(app_name)
        with allure.step('Вернуть в исходное состояние "TITLE"'):
            SETTING().update_settings_app(setting, link)

    @allure.feature('Проверка функциональности вкладки "Общие"')
    @allure.link(link + '/admin/settings/general', name='ТАНДЕМ ЛК - Общие настройки')
    def testing_the_functionality_of_the_app_name_settings_with_an_outrageous_value(self, browser):
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
        app_name = random_en(256)
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Общие"'):
            page.open_window_setting('general')
        with allure.step('Ввести в поле "Название приложения" 256 символов'):
            page.input_app_name(app_name)
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_general_validators(0, 'Incorrect', 'название приложения не может превышать')

    @allure.feature('Проверка функциональности вкладки "Общие"')
    @pytest.mark.skip(reason='Ошибка функциональности настроек "Названия/URL" приложения')
    @allure.link(link + '/admin/settings/general', name='ТАНДЕМ ЛК - Общие настройки')
    def testing_the_functionality_of_the_application_address_settings(self, browser):
        app_url = '192.168.1.4:8080'
        setting = 'app_url'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Общие"'):
            page.open_window_setting('general')
        with allure.step('Ввести в поле "URL" - 192.168.1.4:8080'):
            page.input_app_url(app_url)
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_general_validators(0, 'Correct', '')
        with allure.step('Проверка записи данных в abitur.settings'):
            page.check_records_mail_setting('app_url', app_url)
        with allure.step('Проверка изменения настроект адреса приложения'):
            page = Setting(browser, 'http://' + app_url)
            page.open()
        with allure.step('Вернуть в исходное состояние "URL"'):
            SETTING().update_settings_app(setting, link)

    @allure.feature('Проверка функциональности вкладки "Общие"')
    @allure.link(link + '/admin/settings/general', name='ТАНДЕМ ЛК - Общие настройки')
    def testing_the_functionality_of_the_application_address_settings_with_a_limit_value(self, browser):
        app_url = random_number(255)
        setting = 'app_url'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Общие"'):
            page.open_window_setting('general')
        with allure.step('Ввести в поле "URL" - 255 цифр'):
            page.input_app_url(app_url)
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_general_validators(0, 'Correct', '')
        with allure.step('Проверка записи данных в abitur.settings'):
            page.check_records_mail_setting('app_url', app_url)
        with allure.step('Вернуть в исходное состояние "URL"'):
            SETTING().update_settings_app(setting, link)

    @allure.feature('Проверка функциональности вкладки "Общие"')
    @allure.link(link + '/admin/settings/general', name='ТАНДЕМ ЛК - Общие настройки')
    def testing_the_functionality_of_the_application_address_settings_with_an_outrageous_value(self, browser):
        app_url = random_number(256)
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Общие"'):
            page.open_window_setting('general')
        with allure.step('Ввести в поле "URL" - 256 цифр'):
            page.input_app_url(app_url)
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_general_validators(0, 'Incorrect', 'URL не может превышать')

    @allure.feature('Проверка функциональности вкладки "Общие"')
    @allure.link(link + '/admin/settings/general', name='ТАНДЕМ ЛК - Общие настройки')
    def testing_the_functionality_of_the_settings_for_changing_the_app_logo_with_a_valid_file(self, browser):
        setting = 'logo'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Общие"'):
            page.open_window_setting('general')
        with allure.step('Проверка валидации на загрузку файла в формате .jpg'):
            with allure.step('Загрузить логотип в формате .jpg'):
                page.change_logo_app('files/images/photo.jpg')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_general_validators(0, 'CorrectFile', '')
            logo = 'settings/logo.jpg'
            with allure.step('Проверка записи данных в abitur.settings'):
                page.check_records_mail_setting(setting, logo)
        with allure.step('Проверка валидации на загрузку файла в формате .png'):
            with allure.step('Загрузить логотип в формате .png'):
                page.change_logo_app('files/images/photo.png')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_general_validators(0, 'CorrectFile', '')
            logo = 'settings/logo.png'
            with allure.step('Проверка записи данных в abitur.settings'):
                page.check_records_mail_setting(setting, logo)

    @allure.feature('Проверка функциональности вкладки "Общие"')
    @allure.link(link + '/admin/settings/general', name='ТАНДЕМ ЛК - Общие настройки')
    def testing_the_functionality_of_the_logo_removal_setting(self, browser):
        setting = 'logo'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Общие"'):
            page.open_window_setting('general')
        with allure.step('Загрузить логотип в формате .jpg'):
            page.change_logo_app('files/images/photo.jpg')
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_general_validators(0, 'CorrectFile', '')
            logo = 'settings/logo.jpg'
        with allure.step('Проверка записи данных в abitur.settings'):
            page.check_records_mail_setting(setting, logo)
        with allure.step('Нажать кнопку "Удалить"'):
            logo = ''
            page.deleting_logo_app()
        with allure.step('Проверка записи данных в abitur.settings'):
            page.check_records_mail_setting(setting, logo)

    @allure.feature('Проверка функциональности вкладки "Общие"')
    @allure.link(link + '/admin/settings/general', name='ТАНДЕМ ЛК - Общие настройки')
    def testing_the_functionality_of_the_logo_removal_setting(self, browser):
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Общие"'):
            page.open_window_setting('general')
        with allure.step('Загрузить логотип в формате .jpg'):
            page.change_logo_app('files/images/photo.jpg')
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_general_validators(0, 'CorrectFile', '')
        with allure.step('Нажать кнопку "Обрезать"'):
            page.prone_logo_app()

    @allure.feature('Проверка функциональности вкладки "Общие"')
    @pytest.mark.xfail(reason='Ошибка функциональности валидатора при загрузки файл с невалидным форматом')
    @allure.link(link + '/admin/settings/general', name='ТАНДЕМ ЛК - Общие настройки')
    def testing_the_functionality_of_the_settings_for_changing_the_app_logo_with_a_invalid_file(self, browser):
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Общие"'):
            page.open_window_setting('general')
        with allure.step('Проверка валидации на загрузку файла в формате .webp'):
            with allure.step('Загрузить логотип в формате .webp'):
                page.change_logo_app('files/images/photo.webp')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_general_validators(0, 'IncorrectFile', 'Доступные форматы: jpg, png')
        with allure.step('Проверка валидации на загрузку файла в формате .gif'):
            with allure.step('Загрузить логотип в формате .gif'):
                page.change_logo_app('files\\images\\photo.gif')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_general_validators(0, 'IncorrectFile', 'Доступные форматы: jpg, png')
        with allure.step('Проверка валидации на загрузку файла в формате .tiff'):
            with allure.step('Загрузить логотип в формате .tiff'):
                page.change_logo_app('files\\images\\photo.tiff')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_general_validators(0, 'IncorrectFile', 'Доступные форматы: jpg, png')
        with allure.step('Проверка валидации на загрузку файла в формате .docx'):
            with allure.step('Загрузить логотип в формате .docx'):
                page.change_logo_app('files\\doc\\doc.docx')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_general_validators(0, 'IncorrectFile', 'Доступные форматы: jpg, png')

    @allure.feature('Проверка функциональности вкладки "Общие"')
    @allure.link(link + '/admin/settings/general', name='ТАНДЕМ ЛК - Общие настройки')
    def testing_the_functionality_of_the_settings_for_changing_the_app_icon_with_a_valid_file(self, browser):
        setting = 'icon'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Общие"'):
            page.open_window_setting('general')
        with allure.step('Проверка валидации на загрузку файла в формате .ico'):
            with allure.step('Загрузить иконку в формате .ico'):
                page.change_icon_app('files/images/photo.ico')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_general_validators(0, 'CorrectFile', '')
            icon = 'settings/icon.ico'
            with allure.step('Проверка записи данных в abitur.settings'):
                page.check_records_mail_setting(setting, icon)
        with allure.step('Проверка валидации на загрузку файла в формате .png'):
            with allure.step('Загрузить иконку в формате .png'):
                page.change_icon_app('files/images/photo.png')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_general_validators(0, 'CorrectFile', '')
            icon = 'settings/icon.png'
            with allure.step('Проверка записи данных в abitur.settings'):
                page.check_records_mail_setting(setting, icon)

    @allure.feature('Проверка функциональности вкладки "Общие"')
    @pytest.mark.xfail(reason='Ошибка функциональности валидатора при загрузки файл с невалидным форматом')
    @allure.link(link + '/admin/settings/general', name='ТАНДЕМ ЛК - Общие настройки')
    def testing_the_functionality_of_the_settings_for_changing_the_app_icon_with_a_invalid_file(self, browser):
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Общие"'):
            page.open_window_setting('general')
        with allure.step('Проверка валидации на загрузку файла в формате .docx'):
            with allure.step('Загрузить иконку в формате .docx'):
                page.change_icon_app('files/doc/doc.docx')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_general_validators(0, 'IncorrectFile', 'Доступные форматы: ico, png')
        with allure.step('Проверка валидации на загрузку файла в формате .pdf'):
            with allure.step('Загрузить иконку в формате .pdf'):
                page.change_icon_app('files/doc/doc.pdf')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_general_validators(0, 'IncorrectFile', 'Доступные форматы: ico, png')
        with allure.step('Проверка валидации на загрузку файла в формате .jpg'):
            with allure.step('Загрузить иконку в формате .jpg'):
                page.change_icon_app('files/photo/photo.jpg')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_general_validators(0, 'IncorrectFile', 'Доступные форматы: ico, png')

    @allure.feature('Проверка функциональности вкладки "Общие"')
    @allure.link(link + '/admin/settings/general', name='ТАНДЕМ ЛК - Общие настройки')
    def testing_widget_functionality_allow_self_deletion(self, browser):
        setting = 'allow_delete_profile'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Выключить виджет "Разрешить самостоятельное удаление"'):
                SETTING().update_settings_app(setting, '0')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Общие"'):
            page.open_window_setting('general')
        with allure.step('Включить чекбокс "Разрешить самостоятельное удаление"'):
            page.select_settings_allow_delete_profile()
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_general_validators(4, 'Correct', '')
        with allure.step('Проверка записей данных в abitur.settings'):
            page.check_records_mail_setting(setting, '1')
        page = Profile(browser, link)
        with allure.step('Нажать линк "Профиль" в левом боковом меню'):
            page.open_profile()
        with allure.step('Нажать на кнопку "Удалить учетную запись"'):
            page.delete_profile_account('Skip', 0)
        with allure.step('Выключить виджет "Разрешить самостоятельное удаление"'):
            SETTING().update_settings_app(setting, '0')
        with allure.step('Обновить страницу'):
            page.not_delete_profile_button(0)

    @allure.feature('Проверка функциональности вкладки "Общие"')
    @allure.link(link + '/admin/settings/general', name='ТАНДЕМ ЛК - Общие настройки')
    def testing_widget_functionality_enable_two_factor_authentication(self, browser):
        setting = 'enable_2fa'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Включить виджет "Включить двухфакторную аутентификацию"'):
                SETTING().update_settings_app(setting, '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Общие"'):
            page.open_window_setting('general')
        with allure.step('Выключить чекбокс "Включить двухфакторную аутентификацию"'):
            page.select_setting_enable_2fa()
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_general_validators(4, 'Correct', '')
        with allure.step('Проверка записей данных в abitur.settings'):
            page.check_records_mail_setting(setting, '0')
        page = Profile(browser, link)
        with allure.step('Нажать линк "Профиль" в левом боковом меню'):
            page.open_profile()
        with allure.step('Проверка отсутствия виджета'):
            page.search_2fa_widget('Incorrect')
        with allure.step('Включить виджет "Включить двухфакторную аутентификацию"'):
            SETTING().update_settings_app(setting, '1')
        with allure.step('Проверка наличия виджета'):
            page.search_2fa_widget('Correct')

    @allure.feature('Проверка функциональности вкладки "Общие"')
    @allure.link(link + '/admin/settings/general', name='ТАНДЕМ ЛК - Общие настройки')
    def testing_the_functionality_of_the_widget_allow_changing_the_full_name(self, browser):
        setting = 'allow_change_fullname'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Включить виджет "Разрешить изменять ФИО"'):
                SETTING().update_settings_app(setting, '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Общие"'):
            page.open_window_setting('general')
        with allure.step('Выключить чекбокс "Разрешить изменять ФИО"'):
            page.select_settings_allow_change_fullname()
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_general_validators(4, 'Correct', '')
        with allure.step('Проверка записей данных в abitur.settings'):
            page.check_records_mail_setting(setting, '0')
        page = Profile(browser, link)
        with allure.step('Нажать линк "Профиль" в левом боковом меню'):
            page.open_profile()
        with allure.step('Проверка отсутвия возможности редактировать ФИО'):
            page.search_change_full_name_widget('Incorrect')
        with allure.step('Включить виджет "Разрешить изменять ФИО"'):
            SETTING().update_settings_app(setting, '1')
        with allure.step('Проверка возможности редактировать ФИО'):
            page.search_change_full_name_widget('Correct')

    @allure.feature('Проверка функциональности вкладки "Общие"')
    @allure.link(link + '/admin/settings/general', name='ТАНДЕМ ЛК - Общие настройки')
    def testing_the_widget_supporting_users_with_valid_data(self, browser):
        setting = 'support_active'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Включить виджет "Поддержка пользователей"'):
                SETTING().update_settings_app(setting, '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (Precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Общие"'):
            page.open_window_setting('general')
        with allure.step('Ввести в поле "E-mail поддержки" валидный адрес'):
            support_email = random_en(8) + '@test.com'
            page.input_support_email(support_email, 'yes')
        with allure.step('Ввести в поле "Название ссылки" 8 символов'):
            support_link_name = random_en(8)
            page.input_support_link_name(support_link_name, 'yes')
        with allure.step('Ввести в поле "Текст подсказки" 8 символов'):
            support_prompt = random_en(8)
            page.input_support_prompt(support_prompt, 'yes')
        with allure.step('Выключить чекбокс "Поддержка пользователей"'):
            page.select_checkbox_support_active()
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_general_validators(2, 'Correct', '')
        with allure.step('Проверка записей данных в settings.support_email'):
            page.check_records_mail_setting('support_email', support_email)
        with allure.step('Проверка записей данных в settings.support_link_name'):
            page.check_records_mail_setting('support_link_name', support_link_name)
        with allure.step('Проверка записей данных в settings.support_prompt'):
            page.check_records_mail_setting('support_prompt', support_prompt)
        with allure.step('Проверка записей данных в settings.support_active'):
            page.check_records_mail_setting(setting, '')

    @allure.feature('Проверка функциональности вкладки "Общие"')
    @allure.link(link + '/admin/settings/general', name='ТАНДЕМ ЛК - Общие настройки')
    def testing_the_widget_supporting_users_with_limit_values(self, browser):
        setting = 'support_active'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Включить виджет "Поддержка пользователей"'):
                SETTING().update_settings_app(setting, '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (Precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Общие"'):
            page.open_window_setting('general')
        with allure.step('Ввести в поле "E-mail поддержки" валидный адрес'):
            support_email = random_en(182) + '@test.com'
            page.input_support_email(support_email, 'yes')
        with allure.step('Ввести в поле "Название ссылки" 8 символов'):
            support_link_name = random_en(64)
            page.input_support_link_name(support_link_name, 'yes')
        with allure.step('Ввести в поле "Текст подсказки" 8 символов'):
            support_prompt = random_en(2048)
            page.input_support_prompt(support_prompt, 'yes')
        with allure.step('Нажать кнопку "Сохранить"'):
            page.checking_general_validators(2, 'Correct', '')
        with allure.step('Проверка записей данных в settings.support_email'):
            page.check_records_mail_setting('support_email', support_email)
        with allure.step('Проверка записей данных в settings.support_link_name'):
            page.check_records_mail_setting('support_link_name', support_link_name)
        with allure.step('Проверка записей данных в settings.support_active'):
            page.check_records_mail_setting(setting, '1')

    @allure.feature('Проверка функциональности вкладки "Общие"')
    @allure.link(link + '/admin/settings/general', name='ТАНДЕМ ЛК - Общие настройки')
    def testing_the_widget_supporting_users_with_beyond_limit_values(self, browser):
        setting = 'support_active'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Включить виджет "Поддержка пользователей"'):
                SETTING().update_settings_app(setting, '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (Precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Общие"'):
            page.open_window_setting('general')
        with allure.step('Проверка валидации поля "E-mail поддержки" на ввод заграничных значений'):
            with allure.step('Ввести в поле "E-mail поддержки" 192 символа'):
                page.input_support_email(random_en(183) + '@test.com', 'yes')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_general_validators(2, 'Incorrect', '')
            with allure.step('Вернуть поле в исхдное состояние'):
                page.input_support_email(random_en(8) + '@test.com', 'yes')
        with allure.step('Проверка валидации поля "Название ссылки" на ввод заграничных значений'):
            with allure.step('Ввести в поле "Название ссылки" 66 символов'):
                page.input_support_link_name(random_en(66), 'yes')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_general_validators(2, 'Incorrect', '')
            with allure.step('Вернуть поле в исхдное состояние'):
                page.input_support_link_name(random_en(8), 'yes')
        with allure.step('Проверка валидации поля "Название ссылки" на ввод заграничных значений'):
            with allure.step('Ввести в поле "Текст подсказки" 2049 символов'):
                page.input_support_prompt(random_en(2049), 'yes')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_general_validators(2, 'Incorrect', '')
            with allure.step('Вернуть поле в исхдное состояние'):
                page.input_support_prompt(random_en(8), 'yes')

    @allure.feature('Проверка функциональности вкладки "Общие"')
    @allure.link(link + '/admin/settings/general', name='ТАНДЕМ ЛК - Общие настройки')
    def testing_the_widget_supporting_users_with_an_empty_value(self, browser):
        setting = 'support_active'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Включить виджет "Поддержка пользователей"'):
                SETTING().update_settings_app(setting, '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (Precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Общие"'):
            page.open_window_setting('general')
        with allure.step('Проверка валидации обязательного поля "E-mail поддержки"'):
            with allure.step('Оставить поле "E-mail поддержки" пустым'):
                page.input_support_email(' ', 'yes')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_general_validators(2, 'Incorrect', '')
            with allure.step('Вернуть поле в исхдное состояние'):
                page.input_support_email(random_en(8) + '@test.com', 'yes')
        with allure.step('Проверка валидации обязательного поля "Название ссылки"'):
            with allure.step('Оставить поле "Название ссылки" пустым'):
                page.input_support_link_name(' ', 'yes')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_general_validators(2, 'Incorrect', '')
            with allure.step('Вернуть поле в исхдное состояние'):
                page.input_support_link_name(random_en(8), 'yes')

    @allure.feature('Проверка функциональности вкладки "Общие"')
    @allure.link(link + '/admin/settings/general', name='ТАНДЕМ ЛК - Общие настройки')
    def testing_the_widget_supporting_users_with_an_invalid_address(self, browser):
        setting = 'support_active'
        with allure.step('Подготовка данных для выполнения тестирования'):
            with allure.step('Создать учетную запись администратора'):
                ADMINS().create_a_user_with_administrator_rights()
            with allure.step('Включить виджет "Поддержка пользователей"'):
                SETTING().update_settings_app(setting, '1')
        page = RegLogin(browser, link)
        with allure.step('Открыть стенд ЛКА'):
            page.open()
        with allure.step('Ввести в поле "E-mail" (Precondition-Администратор)'):
            page.input_email(admin_login)
        with allure.step('Ввести в поле "Пароль" (Precondition-Администратор)'):
            page.input_password(password)
        with allure.step('Нажать "Войти"'):
            page.data_validation_check('Correct', 'Login', '')
        page = Setting(browser, link)
        with allure.step('Открыть вкладку "Общие"'):
            page.open_window_setting('general')
        with allure.step('Проверка валидации обязательного поля "E-mail поддержки"'):
            with allure.step('Оставить поле "E-mail поддержки" пустым'):
                page.input_support_email(' ', 'yes')
        with allure.step('Проверка валидации поля "Ваш E-mail" на ввод некорректного значения'):
            with allure.step('Ввести в поле 8 символов'):
                page.input_support_email(random_en(8), 'yes')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_general_validators(2, 'Incorrect', '')
            with allure.step('Ввести в поле 8 символов + "@"'):
                page.input_support_email(random_en(8) + '@', 'yes')
            with allure.step('Нажать кнопку "Сохранить"'):
                page.checking_general_validators(2, 'Incorrect', '')
